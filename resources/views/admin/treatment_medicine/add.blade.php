@extends('layouts.app')
@section('content')
<?php $x = 1;?>

	<div class="intro-y box mt-5">
		<div class="flex flex-col sm:flex-row items-center p-5 border-b border-gray-200 dark:border-dark-5">
			<h2 class="font-medium text-base mr-auto">@if($treatment_medicine) {{ 'Edit' }} @else Add New @endif Treatment Medicine</h2>
		</div>
		<div class="intro-y datatable-wrapper box p-5 col-span-12 overflow-auto lg:overflow-hidden">
			<form id="treatment_medicineForm">
				<div class="grid grid-cols-12 gap-6 mt-5">
					<div class="intro-y col-span-12 lg:col-span-6">
						<div>
							<label>Name</label>
							<input type="text" name="name" value="<?php if($treatment_medicine){ echo $treatment_medicine->name;}?>" class="input w-full border mt-2" placeholder="Name" id="name" required>
						</div>
					</div>
					<div class="intro-y col-span-12 lg:col-span-6">
						<div>
							<label>Other Name</label>
							<input type="text" name="other_name" value="<?php if($treatment_medicine){ echo $treatment_medicine->other_name; }?>" class="input w-full border mt-2" placeholder="Other Name" id="other_name">
						</div>
					</div>
					<div class="intro-y col-span-12 lg:col-span-6">
						<div>
							<label>Chemical Name</label>
							<input type="text" name="chemical_name" value="<?php if($treatment_medicine){ echo $treatment_medicine->chemical_name; }?>" class="input w-full border mt-2" placeholder="Chemical Name" id="chemical_name" >
						</div>
					</div>					
					<div class="intro-y col-span-12 lg:col-span-4">
						<label>Treatment/Service</label>
						<div class="mt-2">
							<select id="service_id" name="service_id" class="select2 w-full" required>
								<option value="">--Select--</option>
								@if($services && $services->count() > 0)
									@foreach($services as $service)
										<option value="{{ $service->id }}" <?php if($treatment_medicine && $treatment_medicine->service_id == $service->id){ echo 'selected';}?> >{{ $service->name }}</option>
									@endforeach
								@endif
							</select>
						</div>
					</div>

					<div class="intro-y col-span-12 lg:col-span-4">
						<div>
							<label>Starting Price</label>
							<input type="number" name="start_price" value="<?php if($treatment_medicine){ echo $treatment_medicine->start_price;	}?>" class="input w-full border mt-2" placeholder="Starting Price" id="start_price" required>
						</div>
					</div>

					<div class="intro-y col-span-12 lg:col-span-12">
						<label>Description</label>
						<textarea name="description" id="description" class="input w-full border mt-2" cols="50" rows="4"><?php if($treatment_medicine){ echo $treatment_medicine->description; }?></textarea>
					</div>

					<div class="intro-y col-span-12 lg:col-span-6">
						<label>Display Images</label>
						<input type="file" name="images[]" class="input w-full border mt-2" id="treatment_medicine_images" multiple="multiple">
						<div class="images_preview mt-3">
							@if($treatment_medicine && json_decode($treatment_medicine->images))
								@foreach(json_decode($treatment_medicine->images) as $key => $value)
									<input type="hidden" name="old_image[]" value="{{ $value }}">
									<span class="pip"><img class="imageThumb" src="/{{ $value }}"></span>
								@endforeach
							@endif
						</div>
					</div>
					
					<div class="intro-y col-span-12 lg:col-span-12">
						<p class="mt-5">FAQ : Question/Answers</p>
				
						@if($faq_rows && $faq_rows->count() > 0)
						<section class="more_items_wrapper">
							
							@foreach($faq_rows as $row)
								<div class="grid grid-cols-12 gap-6 mt-2 filds_wrapper">
									<div class="intro-y col-span-12 lg:col-span-11 mt-5">
										<input type="text" name="faq_data[question][]" class="input w-full border" placeholder="Question" value="{{ $row->question_data }}">
									</div>
									<div class="intro-y col-span-12 lg:col-span-1 mt-5">
										<button type="button" class="button button--md w-24 btn_red remove_item" style="float:right;">Remove</button>
									</div>									
									<div class="intro-y col-span-12 lg:col-span-12">
										<textarea  name="faq_data[answer][]" class="myanswer input w-full border" placeholder="Answer" cols="50" rows="4" id="faq_answer{{$x}}">{{ $row->answer_data }}</textarea>
									</div>
								</div>
								@php
									$x++;
								@endphp
							@endforeach						
						</section>
					@else
						<div class="grid grid-cols-12 gap-6 mt-2">
							<div class="intro-y col-span-12 lg:col-span-11 mt-5">
								<input type="text" name="faq_data[question][]" class="input w-full border" placeholder="Question">
							</div>
							<div class="intro-y col-span-12 lg:col-span-1 mt-5">
								<button type="button" class="button button--md w-24 btn_red remove_item" style="float:right;">Remove</button>
							</div>							
							<div class="intro-y col-span-12 lg:col-span-12">
								<textarea name="faq_data[answer][]" class="myanswer input w-full border" placeholder="Answer" cols="50" rows="4" id="faq_answer{{$x}}"></textarea>
							</div>
						</div>
						<section class="more_items_wrapper"></section>					
					@endif	
					</div>
					<div class="intro-y col-span-12 lg:col-span-12">							
						<button type="button" class="button button--md w-24 mr-1 mb-2 btn_blue add_item">Add More</button>
					</div>						
					
					<div class="intro-y col-span-12 lg:col-span-4">
						<label>Active Status</label>
						<div class="mt-2">
							<input type="checkbox" name="status" class="input input--switch border" <?php if($treatment_medicine && $treatment_medicine->status == "1"){ echo 'checked';}?>>
						</div>
					</div>
				</div>
				<div class="grid grid-cols-12 gap-6 mt-12">
					<div class="intro-y col-span-12 lg:col-span-12">
						<input type="hidden" name="id" value="<?php if($treatment_medicine){ echo $treatment_medicine->id;}?>"/>
						<input type="hidden" name="page" value="<?php if($treatment_medicine){ echo 'edit';} else { echo 'add'; }?>"/>
						<button type="submit" id="saveBtn" class="button button--md w-24 mr-1 mb-2 btn_blue">Save</button>
						<button type="reset" class="button button--md w-24 mr-1 mb-2 btn_red" onclick="location.href = '{{ route('admin.treatment_medicines') }}';">Cancel</button>
					</div>
				</div>
			</form>
		</div>
	</div>

	<script type="text/javascript">
		CKEDITOR.replace( 'description' );
				
		var count = <?php echo $x;?>;
		var faq_rows = <?php echo $faq_rows ? $faq_rows->count() : 0;?>

		if(faq_rows > 0){
			for(var i=1; i<=faq_rows; i++){
			
				CKEDITOR.replace('faq_answer'+i, {
					filebrowserUploadUrl: "{{route('image-upload', ['_token' => csrf_token() ])}}",
					filebrowserImageUploadUrl: "{{route('image-upload', ['_token' => csrf_token() ])}}",
					filebrowserUploadMethod: 'form'
				});
				
			}
		}
		CKEDITOR.add;


	$("#treatment_medicineForm").submit(function(e) {
		e.preventDefault();
		
	
		$('textarea.myanswer').each(function () {
		   var $textarea = $(this);
		   //console.log($textarea.attr('id'));
		   var ans_val = CKEDITOR.instances[$textarea.attr('id')].getData();
		   //console.log(ans_val);
		   $textarea.val(ans_val);
		   //console.log('textAreaVal=',$textarea.val());
		});
			
		var description = CKEDITOR.instances.description.getData();		
		var formData = new FormData(this);
		formData.append('description', description);
		
		$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			url: "{{ route('admin.treatment_medicines.store') }}",
			data: formData,
			type: "POST",
			cache:false,
			contentType: false,
			processData: false,
			// dataType: 'json',
			success: function (response) {
				if(response['status'] == 'success'){
					swal({
						title: response['message'],
						icon: 'success'
					});
					$('#treatment_medicineForm').trigger("reset");
					setTimeout(function(){
					swal.close();
					window.location = "{{ route('admin.treatment_medicines') }}"; }, 1000);
				}
			},
			error: function (data) {
				let errorArr = [];
				let errors = data.responseJSON.errors;
				let emailErr = errors.email[0];
				// $.each(errors, function(key, value) {

				// });
				//console.log('Error:', data);
				// swal({
				// 	title: emailErr,
				// 	icon: 'error'
				// })
				// $('#saveBtn').html('Save');
			}
		});
	});


	// Image reader
	if (window.File && window.FileList && window.FileReader) {
		$("#treatment_medicine_images").on("change", function(e) {
			$('.pip').remove();
			var files = e.target.files,
			filesLength = files.length;
			for (var i = 0; i < filesLength; i++) {
				var f = files[i]
				var fileReader = new FileReader();
				fileReader.onload = (function(e) {
					var file = e.target;
					$images = "<span class='pip'>" +
						"<img class='imageThumb' src=\"" + e.target.result + "\" title=\"" + file.name + "\" width='80' height='80'/>" +
						"</span>";
					$(".images_preview").append($images);
					$(".remove").click(function(){
						$(this).parent(".pip").remove();
					});
				});
				fileReader.readAsDataURL(f);
			}
		});
	} else {
		alert("Your browser doesn't support to File API")
	}
	
	$(document).ready(function() {	
		$('.add_item').click(function() {
			$('.more_items_wrapper').append(
				'<div class="grid grid-cols-12 gap-6 mt-2 filds_wrapper">' +
					'<div class="intro-y col-span-12 lg:col-span-11 mt-5">' +
						'<input type="text" name="faq_data[question][]" class="input w-full border" placeholder="Question">' +
					'</div>' +
					'<div class="intro-y col-span-12 lg:col-span-1 mt-5">' +
						'<button type="button" class="button button--md w-24 btn_red remove_item" style="float:right;">Remove</button>' +
					'</div>' +					
					'<div class="intro-y col-span-12 lg:col-span-12">' +
						'<textarea name="faq_data[answer][]" class="myanswer input w-full border" placeholder="Answer" cols="50" rows="4" id="faq_answer'+count+'"></textarea>' +
					'</div>' +
				'</div>'
			);
			CKEDITOR.replace('faq_answer'+count);
			CKEDITOR.add;			
			count++;
		});

		$(document).on('click', '.more_items_wrapper .remove_item', function() {
			$(this).closest('.filds_wrapper').remove();
		});
	});			
</script>
</html>
@endsection