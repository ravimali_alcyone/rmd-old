@extends('layouts.app')

@section('content')
		<div class="intro-y box mt-5">
			<div class="flex flex-col sm:flex-row items-center p-5 border-b border-gray-200 dark:border-dark-5">
				<h2 class="font-medium text-base mr-auto">@if($user) {{ 'Edit' }} @else Add New @endif Sub Admin</h2>
			</div>
			<div class="intro-y datatable-wrapper box p-5 col-span-12 overflow-auto lg:overflow-hidden">				
				<form id="SubAdminForm">
					<!-- BEGIN: Form Layout -->				
					<div class="grid grid-cols-12 gap-6 mt-5">
						<div class="intro-y col-span-12 lg:col-span-12 myForm">
							<div class="intro-y box p-5">
								<input type="hidden" name="id" value="<?php if($user){ echo $user->id;}?>"/>
								<div>
									<label>Name</label>
									<input type="text" class="input w-full border mt-2 validate" placeholder="Enter Name" id="name" name="name" value="<?php if($user){ echo $user->name;}?>" required>
								</div>
								<div class="mt-4">
									<label>Email</label>
									<input type="email" class="input w-full border mt-2" placeholder="Enter Email" id="email" name="email" value="<?php if($user){ echo $user->email;}?>" required>
								</div>
								<div class="mt-4">
									<label>Privileges</label>
									<div class="mt-2">
										<select name="privileges[]" data-placeholder="Select privileges" class="select2 w-full" multiple required>
										<?php if($user && $user->menu_permissions != ''){
											$privileges = json_decode($user->menu_permissions,true);}else{$privileges = array();}
										?>
											<option value="manage_online_visits"  <?php if($privileges && in_array('manage_online_visits',$privileges)){echo 'selected';}?> >Online Visits</option>
											<option value="visit_pool"  <?php if($privileges && in_array('visit_pool',$privileges)){echo 'selected';}?> >Visit Pool</option>
											<option value="manage_orders"  <?php if($privileges && in_array('manage_orders',$privileges)){echo 'selected';}?> >Orders</option>
											<option value="manage_products"  <?php if($privileges && in_array('manage_products',$privileges)){echo 'selected';}?> >Products</option>
											<option value="manage_providers"  <?php if($privileges && in_array('manage_providers',$privileges)){echo 'selected';}?> >Providers</option>
											<option value="manage_patients"  <?php if($privileges && in_array('manage_patients',$privileges)){echo 'selected';}?> >Patients</option>
											<option value="manage_contact_us"  <?php if($privileges && in_array('manage_contact_us',$privileges)){echo 'selected';}?> >Contact Us</option>
											<option value="finances"  <?php if($privileges && in_array('finances',$privileges)){echo 'selected';}?> >Finances</option>
											<option value="manage_website_content"  <?php if($privileges && in_array('manage_website_content',$privileges)){echo 'selected';}?> >Website Content</option>
											<option value="manage_sub_admin"  <?php if($privileges && in_array('manage_sub_admin',$privileges)){echo 'selected';}?> >Sub Admin</option>
											<option value="manage_questions"  <?php if($privileges && in_array('manage_questions',$privileges)){echo 'selected';}?> >Questions</option>
											<option value="manage_faq"  <?php if($privileges && in_array('manage_faq',$privileges)){echo 'selected';}?> >FAQ</option>
										</select>
									</div>
								</div>
								<div class="mt-4">
									<label>Active Status</label>
									<div class="mt-2">
										<input type="checkbox" name="status" class="input input--switch border" <?php if($user && $user->status == "1"){ echo 'checked';}?>>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="grid grid-cols-12 gap-6 mt-12">
						<div class="intro-y col-span-12 lg:col-span-12">
							<button type="submit" id="saveBtn" class="button button--md w-24 mr-1 mb-2 btn_blue">Save</button>
							<button type="reset" class="button button--md w-24 mr-1 mb-2 btn_red" onclick="location.href = '{{ route('admin.subadmins') }}';">Cancel</button>
						</div>
					</div>					
					<!-- END: Form Layout -->
				</form>					
			</div>
		</div>

<script src="{{ asset('js/jquery.min.js')}}"></script>
<script src="{{ asset('plugins/jquery-validation/jquery.validate.min.js')}}"></script>

<script type="text/javascript">

  if ($(".myForm").length > 0) {
    $("#SubAdminForm").validate({
      rules: {
        name: {
          required: true
        },
        email: {
          required: true
        },
        privileges: {
          required: true
        }
      },
      errorElement: 'div',
	  submitHandler: function(form) {
		// do other things for a valid form
		$.ajax({
		  headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		  },
		  data: $('#SubAdminForm').serialize(),
		  url: "{{ route('admin.subadmins.store') }}",
		  type: "POST",
		 // dataType: 'json',
		  success: function (response) {
				//response = JSON.parse(res);
				if(response['status'] == 'success'){

				  swal({
					title: response['message'],
					icon: 'success'
				  });
				  setTimeout(function(){
				  swal.close();
				  window.location = "{{ route('admin.subadmins') }}"; }, 1000);
				}


		  },
		  error: function (data) {
			  let errorArr = [];
			  let errors = data.responseJSON.errors;
			  let emailErr = errors.email[0];
/* 			  $.each(errors, function(key, value) {

			  }) */;
			  //console.log('Error:', data);
			  swal({
				title: emailErr,
				icon: 'error'
			  })
			  $('#saveBtn').html('Save');
		  }
		});
	  }
    });

  }


</script>
</html>
@endsection