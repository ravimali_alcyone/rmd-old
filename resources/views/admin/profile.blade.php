
@extends('layouts.app')
@section('content')
    <!-- BEGIN: Content -->
    <div class="content">
        <div class="intro-y flex items-center mt-8">
            <h2 class="text-lg font-medium mr-auto">Profile</h2>
        </div>
        <!-- BEGIN: Profile Info -->
        <div class="intro-y box px-5 pt-5 mt-5">
            <div class="flex flex-col lg:flex-row border-b border-gray-200 dark:border-dark-5 pb-5 -mx-5">
                <div class="flex flex-1 px-5 items-center justify-center lg:justify-start">
				
				
                    <div class="w-20 h-40 sm:w-40 sm:h-40 flex-none lg:w-40 lg:h-40 image-fit relative profile2-text mb-3">
                        @if($user->image != null)
                            <img alt="" class="rounded-full" src="{{env('APP_URL')}}/{{$user->image}}">
                        @else
                            <img alt="" class="rounded-full" src="{{env('APP_URL')}}/dist/images/user_icon.png">
                        @endif
                    </div>
                    <div class="w-20 h-40 sm:w-40 sm:h-40 flex-none lg:w-40 lg:h-40 image-fit relative profile2-input mb-3" style="display:none;">	
						<form id="ProfileImageForm">
							<input type="file" name="image" class="input w-30 border mt-2" id="user_image">
							<div class="images_preview w-20 mt-3 mb-3">
							</div>
							<button type="submit" id="saveBtn3" class="button button--sm w-10 mr-1 mb-2 btn_blue"><i data-feather="check"></i></button>
							<button type="button" id="profile2_close" class="button button--sm w-10 mr-1 mb-2 btn_red"><i data-feather="x"></i></button>
						</form>
                    </div>					
					<button type="button" id="profile2" class="button button--sm px-2 mr-1 mb-2 border text-gray-700 dark:bg-dark-5 dark:text-gray-300" title="Chane Image" ><i data-feather="edit"></i></button>
                    <div class="ml-5">
                        <div class="w-24 sm:w-40 truncate sm:whitespace-normal font-medium text-lg">@if($user->name != null) {{ucfirst($user->name)}} @endif</div>
                    </div>
                </div>
                <div class="flex mt-6 lg:mt-0 items-center lg:items-start flex-1 flex-col justify-center text-gray-600 dark:text-gray-300 px-5 border-l border-r border-gray-200 dark:border-dark-5 border-t lg:border-t-0 pt-5 lg:pt-0">
				
					<button type="button" id="profile1" class="button button--sm px-2 mr-1 mb-2 border text-gray-700 dark:bg-dark-5 dark:text-gray-300" title="Edit" ><i data-feather="edit"></i></button>
					
					<div class="profile1-text">
						<div class="truncate sm:whitespace-normal flex items-center email-text"> <i data-feather="mail" class="w-4 h-4 mr-2"></i> @if($user->email != null) <span>{{$user->email}} </span> @endif </div>
						<div class="truncate sm:whitespace-normal flex items-center mt-3 phone-text"> <i data-feather="phone-call" class="w-4 h-4 mr-2"></i> @if($user->phone != null) <span> {{$user->phone}} </span> @endif </div>
						<div class="sm:whitespace-normal flex items-center mt-3 address-text"> <i data-feather="home" class="w-4 h-4 mr-2"></i> @if($user->address != null) <span> {{$user->address}} </span> @endif </div>
					</div>
					
					<div class="profile1-input" style="display:none;">
						<form id="ContactDetails">
						<label>Email</label><input type="text" class="input w-full border mt-1 mb-2 validate" id="email" name="email" value="@if($user->email != null) {{$user->email}} @endif">
						<label>Phone</label><input type="text" class="input w-full border mt-1 mb-2 validate" id="phone" name="phone" value="@if($user->phone != null) {{$user->phone}} @endif">
						<label>Address</label><input type="text" class="input w-full border mt-1 mb-2 validate" id="address" name="address" value="@if($user->address != null) {{$user->address}} @endif">
							<button type="submit" id="saveBtn2" class="button button--sm w-10 mr-1 mb-2 btn_blue"><i data-feather="check"></i></button>
							<button type="button" id="profile1_close" class="button button--sm w-10 mr-1 mb-2 btn_red"><i data-feather="x"></i></button>
						</form>
					</div>
                </div>
            </div>
			<div class="nav-tabs flex flex-col sm:flex-row justify-center lg:justify-start">
				<a data-toggle="tab" data-target="#profile" href="javascript:;" class="py-4 sm:mr-8 flex items-center active"> <i class="w-4 h-4 mr-2" data-feather="user"></i> Profile </a>
				<a data-toggle="tab" data-target="#change-password" href="javascript:;" class="py-4 sm:mr-8 flex items-center"> <i class="w-4 h-4 mr-2" data-feather="lock"></i> Change Password </a>
				<a data-toggle="tab" data-target="#settings" href="javascript:;" class="py-4 sm:mr-8 flex items-center"> <i class="w-4 h-4 mr-2" data-feather="settings"></i> Settings </a>
			</div>				
        </div>
        <!-- END: Profile Info -->
	
        <div class="tab-content mt-5">
            <div class="tab-content__pane active" id="profile">
                <div class="grid grid-cols-12 gap-6">
                    <!-- BEGIN: Daily Sales -->
                    <div class="intro-y box col-span-12 lg:col-span-6">
                        <div class="p-5">
                            <div class="relative flex items-center">
                                <div class="mr-auto">
                                    <a href="#" class="font-medium">Gender</a>
                                </div>
                                <div class="font-medium text-gray-600">{{ucfirst($user->gender) ?? ''}}</div>
                            </div>
                            <div class="relative flex items-center mt-5">
                                <div class="mr-auto">
                                    <a href="#" class="font-medium">Date of Birth</a>
                                </div>
                                <div class="font-medium text-gray-600">@if($user->dob != null) {{ucfirst($user->dob)}} @endif</div>
                            </div>
                            <div class="relative flex items-center mt-5">
                                <div class="mr-auto">
                                    <a href="#" class="font-medium">Age</a>
                                </div>
                                <div class="font-medium text-gray-600">@if($user->age != null) {{ucfirst($user->age)}} @endif</div>
                            </div>
                            <div class="relative flex items-center mt-5">
                                <div class="mr-auto">
                                    <a href="#" class="font-medium">Status</a>
                                </div>
                                <div class="font-medium text-gray-600">@if($user->status == 1) Active @else Blocked @endif</div>
                            </div>
                        </div>
                    </div>

                    <div class="intro-y box col-span-12 lg:col-span-6">
                        <div class="p-5">
                            <div class="relative flex items-center">
                                <div class="mr-auto">
                                    <a href="#" class="font-medium">RMD ID</a>
                                </div>
                                <div class="font-medium text-gray-600">@if($user->rmd_ad_id != null) {{$user->rmd_ad_id}} @endif</div>
                            </div>
                            <div class="relative flex items-center mt-5">
                                <div class="mr-auto">
                                    <a href="#" class="font-medium">Google Adsense ID</a>
                                </div>
                                <div class="font-medium text-gray-600">@if($user->google_adsense_id != null) {{$user->google_adsense_id}} @endif</div>
                            </div>
                            <div class="relative flex items-center mt-5">
                                <div class="mr-auto">
                                    <a href="#" class="font-medium">Bing Ads ID</a>
                                </div>
                                <div class="font-medium text-gray-600">@if($user->bing_ads_id != null) {{$user->bing_ads_id}} @endif</div>
                            </div>
                            <div class="relative flex items-center mt-5">
                                <div class="mr-auto">
                                    <a href="#" class="font-medium">Amazon ID</a>
                                </div>
                                <div class="font-medium text-gray-600">@if($user->amazon_id != null) {{$user->amazon_id}} @endif</div>
                            </div>

                        </div>
                    </div>
                    <!-- END: Daily Sales -->
                </div>
            </div>
            <div class="tab-content__pane" id="change-password">
                <div class="grid grid-cols-12 gap-6">
                    <!-- BEGIN: Daily Sales -->
                    <div class="intro-y box col-span-12 lg:col-span-6 myForm">
                        <div class="p-5">
							<form id="ChangePasswordForm">
								<div class="relative flex items-center">
									<div class="mr-auto">
										<a href="#" class="font-medium">Old Password</a>
									</div>
									<div class="font-medium text-gray-600">
										<input type="password" class="input w-full border mt-2 validate" id="old_password" name="old_password" required>
									</div>
								</div>
								<div class="relative flex items-center mt-5">
									<div class="mr-auto">
										<a href="#" class="font-medium">Password</a>
									</div>
									<div class="font-medium text-gray-600">
										<input type="password" class="input w-full border mt-2 validate" id="password" name="password" required>
									</div>
								</div>
								<div class="relative flex items-center mt-5">
									<div class="mr-auto">
										<a href="#" class="font-medium">Confirm Password</a>
									</div>
									<div class="font-medium text-gray-600">
										<input type="password" class="input w-full border mt-2 validate" id="password_confirmation" name="password_confirmation" required>
									</div>
								</div>
								<div class="relative flex items-center mt-5">
									<div class="mr-auto">
										<button type="submit" id="saveBtn" class="button button--md w-24 mr-1 mb-2 btn_blue">Submit</button>
									</div>

								</div>
							</form>	
                        </div>
                    </div>
                    <!-- END: Daily Sales -->
                </div>
            </div>			
        </div>
    </div>
    <!-- END: Content -->
	
<script src="{{ asset('js/jquery.min.js')}}"></script>
<script src="{{ asset('plugins/jquery-validation/jquery.validate.min.js')}}"></script>

<script type="text/javascript">

  if ($(".myForm").length > 0) {
    $("#ChangePasswordForm").validate({
      rules: {
        old_password: {
          required: true
        },
        password: {
          required: true
        },
        password_confirmation: {
          required: true
        }
      },
      errorElement: 'div',
	  submitHandler: function(form) {
		// do other things for a valid form
		$.ajax({
		  headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		  },
		  data: $('#ChangePasswordForm').serialize(),
		  url: "{{ route('admin.change_password') }}",
		  type: "POST",
		 // dataType: 'json',
		  success: function (response) {
				if(response['status'] == 'success'){
				  swal({
					title: response['message'],
					icon: 'success'
				  });
				  $('#ChangePasswordForm').trigger("reset");
				  setTimeout(function(){
					swal.close();
				  }, 1000);
				}else if(response['status'] == 'error'){
				  swal({
					title: response['message'],
					icon: 'error'
				  })
				  $('#saveBtn').html('Submit');					
				}


		  },
		  error: function (data) {
			  let errorArr = [];
			  let errors = data.responseJSON.errors;
			  let passwordErr = errors.password[0];
			  swal({
				title: passwordErr,
				icon: 'error'
			  })
			  $('#saveBtn').html('Submit');
		  }
		});
	  }
    });

  }


	$('#profile1').click(function(){
		$('.profile1-input').show();
		$('.profile1-text').hide();
	});

	$('#profile1_close').click(function(){
		$('.profile1-input').hide();
		$('.profile1-text').show();
	});


	$('#ContactDetails').submit(function(e){
		e.preventDefault();
		$.ajax({
		  headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		  },
		  data: $('#ContactDetails').serialize(),
		  url: "{{ route('admin.contact_details') }}",
		  type: "POST",
		 // dataType: 'json',
		  success: function (response) {
				if(response['status'] == 'success'){
					
					$('.email-text span').text($('#email').val());
					$('.phone-text span').text($('#phone').val());
					$('.address-text span').text($('#address').val());
				  swal({
					title: response['message'],
					icon: 'success'
				  });
				  $('#profile1_close').trigger("click");
				  setTimeout(function(){
					swal.close();
				  }, 1000);
				}else if(response['status'] == 'error'){
				  swal({
					title: response['message'],
					icon: 'error'
				  })
						
				}


		  },
		  error: function (data) {
			  let Err = 'Error Occured';
			  let errors = data.responseJSON.errors;
			  
			  $.each(errors, function(key, value) {
				  Err = value[0];
			  });
			  
			  swal({
				title: Err,
				icon: 'error'
			  })
		  }
		});	
	});


	$('#profile2').click(function(){
		$('.profile2-input').show();
		$('.profile2-text').hide();
	});

	$('#profile2_close').click(function(){
		$('.profile2-input').hide();
		$('.profile2-text').show();
	});


	// Image reader
	if (window.File && window.FileList && window.FileReader) {
		$("#user_image").on("change", function(e) {
			$('.pip').remove();
			var files = e.target.files,
			filesLength = files.length;
			for (var i = 0; i < filesLength; i++) {
				var f = files[i]
				var fileReader = new FileReader();
				fileReader.onload = (function(e) {
					var file = e.target;

					$images = "<span class='pip'>" +
						"<img class='imageThumb' src=\"" + e.target.result + "\" title=\"" + file.name + "\" width='80' height='80'/>" +
						"</span>";
					$(".images_preview").append($images);
					// "<br/><span class=\"remove\">Remove image</span>" +
					$(".remove").click(function(){
						$(this).parent(".pip").remove();
					});
				});
				fileReader.readAsDataURL(f);
			}
		});
	} else {
		alert("Your browser doesn't support to File API")
	}
	
	$("#ProfileImageForm").submit(function(e) {
		e.preventDefault();

		var formData = new FormData(this);

		$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			url: "{{ route('admin.profile_image') }}",
			data: formData,
			type: "POST",
			cache:false,
			contentType: false,
			processData: false,
			// dataType: 'json',
			success: function (response) {
				//response = JSON.parse(res);
				if(response['status'] == 'success'){
					swal({
						title: response['message'],
						icon: 'success'
					});
					$('#ProfileImageForm').trigger("reset");
					setTimeout(function(){
					swal.close();
					location.reload(); }, 1000);
				}
			},
			error: function (data) {
			  let Err = 'Error Occured';
			  let errors = data.responseJSON.errors;
			  
			  $.each(errors, function(key, value) {
				  Err = value[0];
			  });
			  
			  swal({
				title: Err,
				icon: 'error'
			  })
			}
		});
	});	
</script>	
@endsection