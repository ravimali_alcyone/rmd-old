@extends('layouts.app')
@section('content')
	<div class="intro-y box mt-5">
		<div class="flex flex-col sm:flex-row items-center p-5 border-b border-gray-200 dark:border-dark-5">
			<h2 class="font-medium text-base mr-auto">@if($testimonial) {{ 'Edit' }} @else Add New @endif Testimonial</h2>
		</div>
		<div class="intro-y datatable-wrapper box p-5 col-span-12 overflow-auto lg:overflow-hidden">
			<form id="TestimonialForm">						
				<div class="grid grid-cols-12 gap-6 mt-5">
					<div class="intro-y col-span-12 lg:col-span-6">
						<div>
							<label>Name</label>
							<input type="text" name="name" value="<?php if($testimonial){ echo $testimonial->name;}?>" class="input w-full border mt-2" placeholder="Name" id="name" required>
						</div>
					</div>	
					<div class="intro-y col-span-12 lg:col-span-6">
						<label>Image</label>
						<input type="file" name="image" class="input w-full border mt-2" id="testimonial_image">
						<div class="images_preview mt-3">
						
							@if($testimonial && $testimonial->image)
								<input type="hidden" name="old_image" value="{{ $testimonial->image }}">
								<span class="pip"><img class="imageThumb" src="/{{ $testimonial->image }}"></span>
							@endif
						</div>
					</div>	
					<div class="intro-y col-span-12 lg:col-span-6">
						<label>Star Rating</label>
						<div class="mt-2">
							<select id="star_rating" name="star_rating" class="select2 w-full"  required>
								<option value="1" <?php if($testimonial && $testimonial->star_rating == 1){ echo 'selected'; }?> >1</option>
								<option value="2" <?php if($testimonial && $testimonial->star_rating == 2){ echo 'selected'; }?> >2</option>
								<option value="3" <?php if($testimonial && $testimonial->star_rating == 3){ echo 'selected'; }?> >3</option>
								<option value="4" <?php if($testimonial && $testimonial->star_rating == 4){ echo 'selected'; }?>>4</option>
								<option value="5" <?php if($testimonial && $testimonial->star_rating == 5){ echo 'selected'; }?> >5</option>
							</select>
						</div>
					</div>					
					<div class="intro-y col-span-12 lg:col-span-12">
						<label>Comments</label>
						<textarea name="comments" class="input w-full border mt-2" cols="20" rows="3"><?php if($testimonial){ echo $testimonial->comments; }?></textarea>
					</div>
					
					
				</div>				

				<div class="mt-4">
					<label>Active Status</label>
					<div class="mt-2">
						<input type="checkbox" name="status" class="input input--switch border" @if($testimonial && $testimonial->status == 1) checked @endif>
					</div>
				</div>
				<div class="grid grid-cols-12 gap-6 mt-12">
					<div class="intro-y col-span-12 lg:col-span-12">
						<input type="hidden" name="id" value="<?php if($testimonial){ echo $testimonial->id;}?>"/>
						<input type="hidden" name="page" value="<?php if($testimonial){ echo 'edit';} else { echo 'add'; }?>"/>
						<button type="submit" id="saveBtn" class="button button--md w-24 mr-1 mb-2 btn_blue">Save</button>
						<button type="reset" class="button button--md w-24 mr-1 mb-2 btn_red" onclick="location.href = '{{ route('admin.testimonials') }}';">Cancel</button>
					</div>
				</div>
			</form>
		</div>
	</div>

	<script type="text/javascript">
		$(document).ready(function() {

			$("#TestimonialForm").submit(function(e) {
				e.preventDefault();
				var formData = new FormData(this);

				$.ajax({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					},
					url: "{{ route('admin.testimonials.store') }}",
					data: formData,
					type: "POST",
					cache:false,
					contentType: false,
					processData: false,
					// dataType: 'json',
					success: function (response) {
						//response = JSON.parse(res);
						if(response['status'] == 'success'){
							swal({
								title: response['message'],
								icon: 'success'
							});
							$('#TestimonialForm').trigger("reset");
							setTimeout(function(){
								swal.close();
								window.location = "{{ route('admin.testimonials') }}";
							}, 1000);
						}
					},
					error: function (data) {
						//console.log('Error:', data);
						swal({
							title: 'Error Occured.',
							icon: 'error'
						})
						$('#saveBtn').html('Save');
					}
				});
			});

		});

	// Image reader
	if (window.File && window.FileList && window.FileReader) {
		$("#testimonial_image").on("change", function(e) {
			$('.pip').remove();
			var files = e.target.files,
			filesLength = files.length;
			for (var i = 0; i < filesLength; i++) {
				var f = files[i]
				var fileReader = new FileReader();
				fileReader.onload = (function(e) {
					var file = e.target;

					$images = "<span class='pip'>" +
						"<img class='imageThumb' src=\"" + e.target.result + "\" title=\"" + file.name + "\" width='80' height='80'/>" +
						"</span>";
					$(".images_preview").append($images);
					// "<br/><span class=\"remove\">Remove image</span>" +
					$(".remove").click(function(){
						$(this).parent(".pip").remove();
					});
				});
				fileReader.readAsDataURL(f);
			}
		});
	} else {
		alert("Your browser doesn't support to File API")
	}
	</script>
</html>
@endsection