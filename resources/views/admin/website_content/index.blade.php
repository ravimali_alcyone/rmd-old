 @extends('layouts.app')
@section('content')
	<div class="intro-y box mt-5">

        <div class="col-span-12 sm:col-span-6 xl:col-span-3 intro-y p-5">
            <div class="home_page_sections_wrapper">
                <a href="{{ URL('/admin/website_content/about') }}" class="section">
                    <h3 class="font-medium text-base mr-auto">About</h3>
                </a>
                <a href="{{ URL('/admin/website_content/contact') }}" class="section">
                    <h3 class="font-medium text-base mr-auto">Contact us</h3>
                </a>				
                <a href="{{ URL('/admin/website_content/privacy_policy') }}" class="section">
                    <h3 class="font-medium text-base mr-auto">Privacy Policy</h3>
                </a>
                <a href="{{ URL('/admin/website_content/term_conditions') }}" class="section">
                    <h3 class="font-medium text-base mr-auto">Terms & Conditions</h3>
                </a>

            </div>
        </div>
	</div>
@endsection