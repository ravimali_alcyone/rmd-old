@extends('layouts.app')
@section('content')
	<div class="intro-y box mt-5">
		<div class="flex flex-col sm:flex-row items-center p-5 border-b border-gray-200 dark:border-dark-5">
			<h2 class="font-medium text-base mr-auto">@if($service_pricing) {{ 'Edit' }} @else Add New @endif Service Pricing</h2>
		</div>
		<div class="intro-y datatable-wrapper box p-5 col-span-12 overflow-auto lg:overflow-hidden">
			<form id="ServicePricingForm">
				<input type="hidden" name="id" value="<?php if($service_pricing){ echo $service_pricing->id;}?>"/>						
				<div class="grid grid-cols-12 gap-6 mt-5">
					<div class="intro-y col-span-12 lg:col-span-9">
						<div>
							<label>Name</label>
							<input type="text" name="name" value="<?php if($service_pricing){ echo $service_pricing->name;}?>" class="input w-full border mt-2" placeholder="Name" id="name" required>
						</div>
					</div>
					<div class="intro-y col-span-12 lg:col-span-3">
						<div>
							<label>Total Months</label>
							<input type="number" min="1" name="total_months" value="<?php if($service_pricing){ echo $service_pricing->total_months; }?>" class="input w-full border mt-2" placeholder="Total Months" id="total_months" required>
						</div>
					</div>					
					<div class="intro-y col-span-12 lg:col-span-3">
						<label>Monthly Plan</label>
						<div class="mt-2">
							<select id="is_monthly" name="is_monthly" class="select2 w-full">
								<option value="1" <?php if($service_pricing && $service_pricing->is_monthly == '1'){ echo 'selected'; }?>>Yes</option>
								<option value="0" <?php if($service_pricing && $service_pricing->is_monthly == '0'){ echo 'selected'; }?>>No</option>
							</select>
						</div>
					</div>
					<div class="intro-y col-span-12 lg:col-span-3">
						<div>
							<label>Monthly Price</label>
							<input type="number" name="monthly_price" value="<?php if($service_pricing){ echo $service_pricing->monthly_price;	}?>" class="input w-full border mt-2" placeholder="Monthly Price" id="monthly_price" required>
						</div>
					</div>
					<div class="intro-y col-span-12 lg:col-span-6">
						<div>
							<label>Monthly Plan Text</label>
							<input type="text" name="monthly_plan_text" value="<?php if($service_pricing){ echo $service_pricing->monthly_plan_text;}?>" class="input w-full border mt-2" placeholder="Monthly Plan Text" id="monthly_plan_text" required>
						</div>
					</div>					
					<div class="intro-y col-span-12 lg:col-span-3">
						<label>Total Plan</label>
						<div class="mt-2">
							<select id="is_total" name="is_total" class="select2 w-full">
								<option value="1" <?php if($service_pricing && $service_pricing->is_total == '1'){ echo 'selected'; }?>>Yes</option>
								<option value="0" <?php if($service_pricing && $service_pricing->is_total == '0'){ echo 'selected'; }?>>No</option>
							</select>
						</div>
					</div>					
					<div class="intro-y col-span-12 lg:col-span-3">
						<div>
							<label>Total Price</label>
							<input type="number" name="total_price" value="<?php if($service_pricing){ echo $service_pricing->total_price;	}?>" class="input w-full border mt-2" placeholder="Total Price" id="total_price">
						</div>
					</div>
					<div class="intro-y col-span-12 lg:col-span-6">
						<div>
							<label>Total Plan Text</label>
							<input type="text" name="total_plan_text" value="<?php if($service_pricing){ echo $service_pricing->total_plan_text;}?>" class="input w-full border mt-2" placeholder="Total Plan Text" id="total_plan_text">
						</div>
					</div>
					<div class="intro-y col-span-12 lg:col-span-6">
						<div>
							<label>Special Text</label>
							<input type="text" name="special" value="<?php if($service_pricing){ echo $service_pricing->special;}?>" class="input w-full border mt-2" placeholder="Special Text" id="special">
						</div>
					</div>
					<div class="intro-y col-span-12 lg:col-span-3">
						<label>Most Popular?</label>
						<div class="mt-2">
							<select id="is_most_popular" name="is_most_popular" class="select2 w-full">
								<option value="0" <?php if($service_pricing && $service_pricing->is_most_popular == '0'){ echo 'selected'; }?>>No</option>							
								<option value="1" <?php if($service_pricing && $service_pricing->is_most_popular == '1'){ echo 'selected'; }?>>Yes</option>
							</select>
						</div>
					</div>					
					<div class="intro-y col-span-12 lg:col-span-12">
						<label>Detail</label>
						<textarea name="detail" id="detail" class="input w-full border mt-2" cols="30" rows="3" required><?php if($service_pricing){ echo $service_pricing->detail; }?></textarea>
					</div>
					
					<div class="intro-y col-span-12 lg:col-span-12">
						<label>Plan Features</label>
						<div class="mt-2">
							<select id="all_features" name="features[]" class="select2 w-full" multiple>
						@if($all_features)
							@foreach($all_features as $af)
								<option value="{{$af->id}}" <?php if($selected_features && in_array($af->id,$selected_features)){ echo 'selected'; } ?> >{{$af->name}}</option>
							@endforeach
						@endif
							</select>
						</div>
					</div>	
					
				</div>
				
				<div class="mt-4">
					<label>Active Status</label>
					<div class="mt-2">
						<input type="checkbox" name="status" class="input input--switch border" @if($service_pricing && $service_pricing->status == 1) checked @endif>
					</div>
				</div>
				<div class="grid grid-cols-12 gap-6 mt-12">
					<div class="intro-y col-span-12 lg:col-span-12">
						<button type="submit" id="saveBtn" class="button button--md w-24 mr-1 mb-2 btn_blue">Save</button>
						<button type="reset" class="button button--md w-24 mr-1 mb-2 btn_red" onclick="location.href = '{{ route('admin.service_pricing') }}';">Cancel</button>
					</div>
				</div>
			</form>
		</div>
	</div>

	<script type="text/javascript">
		$(document).ready(function() {

			$("#ServicePricingForm").submit(function(e) {
				e.preventDefault();
				$.ajax({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					},
					data: $('#ServicePricingForm').serialize(),
					url: "{{ route('admin.service_pricing.store') }}",
					type: "POST",
					// dataType: 'json',
					success: function (response) {
						//response = JSON.parse(res);
						if(response['status'] == 'success'){
							swal({
								title: response['message'],
								icon: 'success'
							});
							$('#ServicePricingForm').trigger("reset");
							setTimeout(function(){
								swal.close();
								window.location = "{{ route('admin.service_pricing') }}";
							}, 1000);
						}
					},
					error: function (data) {
						//console.log('Error:', data);
						swal({
							title: 'Error Occured.',
							icon: 'error'
						})
						$('#saveBtn').html('Save');
					}
				});
			});

			$('.add_item').click(function() {
				$('.more_items_wrapper').append(
					'<div class="grid grid-cols-12 gap-6 mt-2 filds_wrapper">' +
						'<div class="intro-y col-span-12 lg:col-span-3">' +
							'<input type="text" name="features[feature][]" class="input w-full border" placeholder="Feature">' +
						'</div>' +
						'<div class="intro-y col-span-12 lg:col-span-6">' +
							'<input type="text" name="features[desc][]" class="input w-full border" placeholder="Description">' +
						'</div>' +
						'<div class="intro-y col-span-12 lg:col-span-3">' +
							'<button type="button" class="button button--md w-24 mr-1 mb-2 btn_red remove_item">Remove</button>' +
						'</div>' +
					'</div>'
				);
			});

			$(document).on('click', '.more_items_wrapper .remove_item', function() {
				$(this).closest('.filds_wrapper').remove();
			});
		});

	</script>
</html>
@endsection