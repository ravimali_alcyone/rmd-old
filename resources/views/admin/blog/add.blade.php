@extends('layouts.app')
@section('content')
	<div class="intro-y box mt-5">
		<div class="flex flex-col sm:flex-row items-center p-5 border-b border-gray-200 dark:border-dark-5">
			<h2 class="font-medium text-base mr-auto">@if($blog) {{ 'Edit' }} @else Add New @endif Blog</h2>
		</div>
		<div class="intro-y datatable-wrapper box p-5 col-span-12 overflow-auto lg:overflow-hidden">
			<form id="blogForm">
				<div class="grid grid-cols-12 gap-6 mt-5">
					<div class="intro-y col-span-12 lg:col-span-12">
						<div>
							<label>Title</label>
							<input type="text" name="name" value="<?php if($blog){ echo $blog->name;}?>" class="input w-full border mt-2" placeholder="Title" id="name" required>
						</div>
					</div>
					<div class="intro-y col-span-6 lg:col-span-3">
						<div>
							<label>Permalink</label>
							<input type="text" class="input w-full border mt-2" placeholder="{{env('APP_URL')}}/blogs/" disabled>
						</div>
					</div>
					<div class="intro-y col-span-6 lg:col-span-9">
						<div class="mt-5">
							<input type="text" name="slug" value="<?php if($blog){ echo $blog->slug;}?>" class="input w-full border mt-2" placeholder="Permalink" id="slug" required>
						</div>
					</div>					
					<div class="intro-y col-span-12 lg:col-span-4">
						<label>Category</label>
						<div class="mt-2">
							<select id="category_id" name="category" class="select2 w-full" onchange="getSubCategories(this)" required>
								<option value="">--Select--</option>
								@if($categories && $categories->count() > 0)
									@foreach($categories as $category)
										<option value="{{ $category->id }}" <?php if($blog && $blog->category == $category->id){ echo 'selected';}?> >{{ $category->name }}</option>
									@endforeach
								@endif
							</select>
						</div>
					</div>

					<div class="intro-y col-span-12 lg:col-span-12">
						<label>Content</label>
						<textarea name="description" id="description" class="input w-full border mt-2"><?php if($blog){ echo $blog->description; }?></textarea>
					</div>

					<div class="intro-y col-span-12 lg:col-span-12">
						<label>Featured Image</label>
						<input type="file" name="image" class="input w-full border mt-2" id="blog_image" >
						<div class="image_preview mt-3">
							@if($blog && $blog->image != '')
								
								<input type="hidden" name="old_image" value="{{ $blog->image }}">
								<span class="pip"><img class="imageThumb" src="/{{ $blog->image }}"></span>
							@endif
						</div>
					</div>
					
					<div class="intro-y col-span-12 lg:col-span-4">
						<label>Active Status</label>
						<div class="mt-2">
							<input type="checkbox" name="status" class="input input--switch border" <?php if($blog && $blog->status == "1"){ echo 'checked';}?>>
						</div>
					</div>
				</div>
				<div class="grid grid-cols-12 gap-6 mt-12">
					<div class="intro-y col-span-12 lg:col-span-12">
						<input type="hidden" name="id" value="<?php if($blog){ echo $blog->id;}?>"/>
						<input type="hidden" name="page" value="<?php if($blog){ echo 'edit';} else { echo 'add'; }?>"/>
						<button type="submit" id="saveBtn" class="button button--md w-24 mr-1 mb-2 btn_blue">Save</button>
						<button type="reset" class="button button--md w-24 mr-1 mb-2 btn_red" onclick="location.href = '{{ route('admin.blogs') }}';">Cancel</button>
					</div>
				</div>
			</form>
		</div>
	</div>

	<script type="text/javascript">
		CKEDITOR.replace('description',{
			height: '400px',
		});
		CKEDITOR.add;


	$("#blogForm").submit(function(e) {
		e.preventDefault();
		var description = CKEDITOR.instances.description.getData();

		var formData = new FormData(this);
		formData.append('description', description);

		$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			// data: $('#blogForm').serialize(),
			url: "{{ route('admin.blogs.store') }}",
			data: formData,
			type: "POST",
			cache:false,
			contentType: false,
			processData: false,
			// dataType: 'json',
			success: function (response) {
				//response = JSON.parse(res);
				if(response['status'] == 'success'){
					swal({
						title: response['message'],
						icon: 'success'
					});
					$('#blogForm').trigger("reset");
					setTimeout(function(){
					swal.close();
					window.location = "{{ route('admin.blogs') }}"; }, 1000);
				}
			},
			error: function (data) {
				let errorArr = [];
				let errors = data.responseJSON.errors;
				let emailErr = errors.email[0];
				// $.each(errors, function(key, value) {

				// });
				//console.log('Error:', data);
				// swal({
				// 	title: emailErr,
				// 	icon: 'error'
				// })
				// $('#saveBtn').html('Save');
			}
		});
	});

	// Image reader
	if (window.File && window.FileList && window.FileReader) {
		$("#blog_image").on("change", function(e) {
			$('.pip').remove();
			var files = e.target.files,
			filesLength = files.length;
			for (var i = 0; i < filesLength; i++) {
				var f = files[i]
				var fileReader = new FileReader();
				fileReader.onload = (function(e) {
					var file = e.target;
					$image = "<span class='pip'>" +
						"<img class='imageThumb' src=\"" + e.target.result + "\" title=\"" + file.name + "\" />" +
						"</span>";
					$(".image_preview").append($image);
					$(".remove").click(function(){
						$(this).parent(".pip").remove();
					});
				});
				fileReader.readAsDataURL(f);
			}
		});
	} else {
		alert("Your browser doesn't support to File API")
	}
	
$("#name").bind("keyup change", function(e) {
    let text = $("#name").val();
	let slug = convertToSlug(text);
	$("#slug").val(slug);
})	

function convertToSlug(Text)
{
    return Text
        .toLowerCase()
        .replace(/[^\w ]+/g,'')
        .replace(/ +/g,'-')
        ;
}
</script>
</html>
@endsection