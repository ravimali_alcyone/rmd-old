
@extends('layouts.app')
@section('content')
    <!-- BEGIN: Content -->
    <div class="content">
        <div class="intro-y flex items-center mt-8">
            <h2 class="text-lg font-medium mr-auto">Blog Detail</h2>
        </div>

        <!-- END: Profile Info -->
        <div class="tab-content mt-5">
            <div class="tab-content__pane active" id="blog_detail_page">
                <div class="grid grid-cols-12 gap-6">
                    <!-- BEGIN: Daily Sales -->
                    <div class="intro-y box col-span-12 lg:col-span-6">
                        <div class="p-5">
                            <div class="blog_main_img">
                                @if(isset($images))
                                    <div class="image-zoom relative">
                                        <img src="/{{ $images[0] }}" data-zoom="/{{ $images[0] }}" alt="" class="w-full">
                                    </div>
                                @endif
                            </div>

                            <div class="blog_thumbnail_section mt-3">
                                @if(isset($images))
                                    @foreach($images as $key => $value)
                                        <img src="/{{ $value }}" alt="">
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="intro-y box col-span-12 lg:col-span-6">
                        <div class="p-5">
                            <h2 class="text-3xl text-gray-700 dark:text-gray-600">{{ $blog->name ?? ''}}</h2>
                            <p class="mt-2">{!! $blog->description ?? '' !!}</p>

                            <h2 class="text-xl text-gray-700 dark:text-gray-600 leading-none mt-10">Detail</h2>
                            <p class="mt-2">{!! $blog->details ?? '' !!}</p>

                            <p class="text-gray-700 dark:text-gray-600 leading-none mt-10"><b>Category</b> - {!! ucfirst($category) ?? '' !!}</p>
                            <p class="text-gray-700 dark:text-gray-600 leading-none mt-4"><b>Price</b> - {!! $blog->price_value ?? '' !!}</p>
                            <p class="text-gray-700 dark:text-gray-600 leading-none mt-4"><b>Price Type</b> - {!! ucfirst($blog->price_type) ?? '' !!}</p>
                            <p class="text-gray-700 dark:text-gray-600 leading-none mt-4"><b>Discount Price</b> - {!! $blog->discount_price ?? '' !!}</p>
                            <p class="text-gray-700 dark:text-gray-600 leading-none mt-4"><b>Direct Purchase</b> - {!! $blog->is_direct_purchase == 1 ? 'Yes' : 'No' ?? '' !!}</p>
                            <p class="text-gray-700 dark:text-gray-600 leading-none mt-4"><b>Stock Status</b> - {!! ucfirst($blog->stock_status) ?? '' !!}</p>
                        </div>
                    </div>

                    @if(isset($blog->blog_info) && $blog->blog_info != '' && $blog->blog_info != null)
                        <div class="intro-y box col-span-12 lg:col-span-12" id="blog_info">
                            <div class="p-5">
                                <!-- <h2 class="text-3xl text-gray-700 dark:text-gray-600">Blog Info</h2> -->
                                <h2 class="text-xl text-gray-700 dark:text-gray-600 leading-none">Blog Info</h2>
                                <p class="mt-2">{!! $blog->blog_info ?? '' !!}</p>
                            </div>
                        </div>
                    @endif

                    @if(isset($blog->safety_info) && $blog->safety_info != '' && $blog->safety_info != null)
                        <div class="intro-y box col-span-12 lg:col-span-12">
                            <div class="p-5">
                                <!-- <h2 class="text-3xl text-gray-700 dark:text-gray-600">Safety Info</h2> -->
                                <h2 class="text-xl text-gray-700 dark:text-gray-600 leading-none">Safety Info</h2>
                                <p class="mt-2">{!! $blog->safety_info ?? '' !!}</p>
                            </div>
                        </div>
                    @endif

                    @if(isset($blog->associate_problems) && $blog->associate_problems != '' && $blog->associate_problems != null)
                        <div class="intro-y box col-span-12 lg:col-span-12">
                            <div class="p-5">
                                <!-- <h2 class="text-3xl text-gray-700 dark:text-gray-600">Associate Problems</h2> -->
                                <h2 class="text-xl text-gray-700 dark:text-gray-600 leading-none">Associate Problems</h2>
                                <p class="mt-2">{!! $blog->associate_problems ?? '' !!}</p>
                            </div>
                        </div>
                    @endif
                </div>

                <div class="intro-y col-span-12 lg:col-span-4 mt-5">
                    <div class="">
                        <div class="relative flex items-center">
                            <button type="button" class="button w-24 mr-1 mb-2 btn_dark ml-2" onclick="javascript:location.href = '/admin/blogs'">Back</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END: Content -->
    </div>

    <script>
        $(document).ready(function() {
            $("#blog_info").find('table').addClass('table table-report table-report--bordered sub_admin_table');

            $(".blog_thumbnail_section img").click(function() {
                $(".blog_main_img img").attr('src', $(this).attr('src')).attr('data-zoom', $(this).attr('src'));
            });
            $(".blog_main_img img").mouseover(function() {
                $(this).css('opacity', '0');
            });
            $(".blog_main_img img").mouseout(function() {
                $(this).css('opacity', '1');
            });
        });
    </script>
@endsection