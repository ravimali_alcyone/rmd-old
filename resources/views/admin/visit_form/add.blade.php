@extends('layouts.app')
@section('content')
	<div class="intro-y box mt-5">
		<div class="flex flex-col sm:flex-row items-center p-5 border-b border-gray-200 dark:border-dark-5">
			<h2 class="font-medium text-base mr-auto">@if($visit_form) {{ 'Edit' }} @else Add New @endif Visit Form</h2>
		</div>
		<div class="intro-y datatable-wrapper box p-5 col-span-12 overflow-auto lg:overflow-hidden">
			<form action="" id="VisitFieldForm">
				<div class="grid grid-cols-12 gap-6 mt-5">
					<div class="intro-y col-span-12 lg:col-span-6">
						<label>Service/Treatment</label>
						<select id="service_id" name="service_id" class="select2 w-full">
							<option value="">--Select--</option>
							@if($services && $services->count() > 0)
								@foreach($services as $service)
									<option value="{{ $service->id }}" @if($visit_form && $visit_form->service_id == $service->id) {{ 'selected' }} @endif >{{ $service->name }}</option>
								@endforeach
							@endif
						</select>
					</div>
					<div class="intro-y col-span-12 lg:col-span-6">
						<label>Form Title</label>
						<input type="text" name="name" class="input w-full border" placeholder="Form Title" value="<?php if($visit_form){ echo $visit_form->name; }?>" required>
					</div>
					<div class="intro-y col-span-12 lg:col-span-6">
						<label>Short Info(Optional)</label>
						<textarea name="short_info" class="input w-full border mt-2" cols="20" rows="3"><?php if($visit_form){ echo $visit_form->short_info; }?></textarea>
					</div>
					<div class="intro-y col-span-12 lg:col-span-6">
						<label>Instructions(Optional)</label>
						<textarea name="instructions" class="input w-full border mt-2" cols="20" rows="3"><?php if($visit_form){ echo $visit_form->instructions; }?></textarea>
					</div>
					<div class="intro-y col-span-12 lg:col-span-6">
						<label>Active Status</label>
						<div class="mt-2">
							<input type="checkbox" name="status" class="input input--switch border" @if($visit_form && $visit_form->status == 1) checked @endif>
						</div>
					</div>
				</div>


				<div class="grid grid-cols-12 gap-6 mt-6">
					<div class="intro-y col-span-12 lg:col-span-12 pt-5 border-t border-gray-200 dark:border-dark-5">
						<input type="hidden" name="id" value="<?php if($visit_form){ echo $visit_form->id;}?>"/>
						<button type="submit" id="saveBtn" class="button button--md w-24 mr-1 mb-2 btn_blue">Next</button>
						<button type="reset" class="button button--md w-24 mr-1 mb-2 btn_red" onclick="location.href = '{{ route('admin.visit_forms') }}';">Cancel</button>
					</div>
				</div>
			</form>
		</div>
	</div>


<script type="text/javascript">

	$("#VisitFieldForm").submit(function(e) {
		e.preventDefault();
		$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			data: $('#VisitFieldForm').serialize(),
			url: "{{ route('admin.visit_forms.store') }}",
			type: "POST",
			// dataType: 'json',
			success: function (response) {
				//response = JSON.parse(res);
				var formid;
				if(response['status'] == 'success'){
					formid = response['id'];
					swal({
						title: response['message'],
						icon: 'success'
					});
					
					setTimeout(function(){
						swal.close();
						$('#VisitFieldForm').trigger("reset");
						window.location = "{{ env('APP_URL') }}/admin/visit_form_questions/add/"+formid;
					}, 500);
				}
			},
			error: function (data) {
				//console.log('Error:', data);
				swal({
					title: 'Error Occured.',
					icon: 'error'
				})
				$('#saveBtn').html('Next');
			}
		});
	});

</script>
</html>
@endsection

