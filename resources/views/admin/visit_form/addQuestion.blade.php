@extends('layouts.app')
@section('content')
	<div class="intro-y box mt-5 position-relative">
		<div class="flex flex-col sm:flex-row items-center p-5 border-b border-gray-200 dark:border-dark-5">
			<h2 class="font-medium text-base mr-auto">Visit Form Questions Pool<?php echo ' - <b>'.$visit_form->name.'</b>';?></h2>
		</div>
		<div class="intro-y datatable-wrapper box p-5 col-span-12 overflow-auto lg:overflow-hidden">
			<form action="" id="VisitFieldForm">

				<div class="visit_form_fields">

					<section class="more_items_wrapper mt-6 pb-0">
			<?php
				$step_no = 0;
				if($visit_form){
					if($visit_questions && $visit_questions->count() > 0){
						foreach($visit_questions as $key => $row){
			?>

			<?php if($row->question_type == 'short_text'){ ?>

				<div class="formElements mt-6" parent-id="{{$row->parent_id}}" data-id="{{$step_no}}">
					<div class="label_wrapper">
						<label class="font-bold">Short Text Question</label>
						<div class="action_btns_srapper">
							<button type="button" class="button eye-shrink px-2 mr-1 bg-gray-200 text-gray-600" title="Shrink"><span class="w-5 h-5 flex items-center justify-center"> <i data-feather="eye-off" class="w-4 h-4"></i> </span> </button>
							<button type="button" class="button eye-expand px-2 mr-1 bg-gray-200 text-gray-600" title="Expand"><span class="w-5 h-5 flex items-center justify-center"> <i data-feather="eye" class="w-4 h-4"></i> </span> </button>
							<button type="button" class="cls_btn button px-2 mr-1 bg-theme-6 text-white btn_red" onclick="remove_element(this)" title="Remove"><span class="w-5 h-5 flex items-center justify-center"> <i data-feather="trash" class="w-4 h-4"></i> </span> </button>
							<input type="hidden" class="question_parent_id" name="visit_form_data[<?php echo $step_no;?>][short_text][parent_id]"  value="{{$row->parent_id}}">
							<input type="hidden" class="question_is_parent" name="visit_form_data[<?php echo $step_no;?>][short_text][is_parent]" value="{{$row->is_parent}}">
							<input type="hidden" class="q_id" name="visit_form_data[<?php echo $step_no;?>][short_text][q_id]" value="{{$row->id}}">
						</div>
					</div>
					<div class="grid grid-cols-12 gap-6 mt-5 mb-5 options_wrapper">
						<div class="intro-y col-span-12 lg:col-span-12">
							<input type="text" class="input w-full border" name="visit_form_data[<?php echo $step_no;?>][short_text][label]" placeholder="Type a question" value="{{$row->question_text}}">
						</div>
					</div>
				</div>

			<?php }elseif($row->question_type == 'long_text'){ ?>

				<div class="formElements mt-6" parent-id="{{$row->parent_id}}" data-id="{{$step_no}}">
					<div class="label_wrapper">
						<label class="font-bold">Long Text Question</label>
						<div class="action_btns_srapper">
							<button type="button" class="button eye-shrink px-2 mr-1 bg-gray-200 text-gray-600" title="Shrink"><span class="w-5 h-5 flex items-center justify-center"> <i data-feather="eye-off" class="w-4 h-4"></i> </span> </button>
							<button type="button" class="button eye-expand px-2 mr-1 bg-gray-200 text-gray-600" title="Expand"><span class="w-5 h-5 flex items-center justify-center"> <i data-feather="eye" class="w-4 h-4"></i> </span> </button>
							<button type="button" class="cls_btn button px-2 mr-1 bg-theme-6 text-white btn_red" onclick="remove_element(this)" title="Remove"><span class="w-5 h-5 flex items-center justify-center"> <i data-feather="trash" class="w-4 h-4"></i> </span> </button>
							<input type="hidden" class="question_parent_id" name="visit_form_data[<?php echo $step_no;?>][long_text][parent_id]" value="{{$row->parent_id}}">
							<input type="hidden" class="question_is_parent" name="visit_form_data[<?php echo $step_no;?>][long_text][is_parent]" value="{{$row->is_parent}}">
							<input type="hidden" class="q_id" name="visit_form_data[<?php echo $step_no;?>][long_text][q_id]" value="{{$row->id}}">
						</div>
					</div>
					<div class="grid grid-cols-12 gap-6 mt-5 mb-5 options_wrapper">
						<div class="intro-y col-span-12 lg:col-span-12">
							<textarea name="visit_form_data[<?php echo $step_no;?>][long_text][label]" class="input w-full border mt-2" cols="20" rows="3" placeholder="Type a question">{{$row->question_text}}</textarea>
						</div>
					</div>
				</div>

			<?php }elseif($row->question_type == 'yes_no'){ ?>

				<div class="formElements mt-6" parent-id="{{$row->parent_id}}" data-id="{{$step_no}}">
					<div class="label_wrapper">
						<label class="font-bold">Yes/No Question</label>
						<div class="action_btns_srapper">
							<button type="button" class="button eye-shrink px-2 mr-1 bg-gray-200 text-gray-600" title="Shrink"><span class="w-5 h-5 flex items-center justify-center"> <i data-feather="eye-off" class="w-4 h-4"></i> </span> </button>
							<button type="button" class="button eye-expand px-2 mr-1 bg-gray-200 text-gray-600" title="Expand"><span class="w-5 h-5 flex items-center justify-center"> <i data-feather="eye" class="w-4 h-4"></i> </span> </button>
							<button type="button" class="cls_btn button px-2 mr-1 bg-theme-6 text-white btn_red" onclick="remove_element(this)" title="Remove"><span class="w-5 h-5 flex items-center justify-center"> <i data-feather="trash" class="w-4 h-4"></i> </span> </button>
							<input type="hidden" class="question_parent_id" name="visit_form_data[<?php echo $step_no;?>][yes_no][parent_id]" value="{{$row->parent_id}}">
							<input type="hidden" class="question_is_parent" name="visit_form_data[<?php echo $step_no;?>][yes_no][is_parent]" value="{{$row->is_parent}}">
							<input type="hidden" class="q_id" name="visit_form_data[<?php echo $step_no;?>][yes_no][q_id]" value="{{$row->id}}">
						</div>
					</div>
					<div class="grid grid-cols-12 gap-6 mt-5 mb-5 options_wrapper">
						<div class="intro-y col-span-12 lg:col-span-12">
							<input name="visit_form_data[<?php echo $step_no;?>][yes_no][label]" class="input w-full border" placeholder="Type a question" value="{{$row->question_text}}">
						</div>
						<div class="intro-y col-span-12 lg:col-span-12 ml-12">
					<?php $optionsArray = App\Http\Controllers\admin\VisitFormController::getOptions($visit_form->id,$row->id);
						if($optionsArray && $optionsArray->count() > 0){
							$x=0;
							foreach($optionsArray as $op){
								if($op->positive == 1){
									$yn_type = 'positive';
								}else{
									$yn_type = 'negative';
								}
					?>
							<div class="intro-y col-span-12 lg:col-span-12 <?php if($x != 0){ echo 'mt-5'; }?>">
								<div class="input_wrapper">
									<input type="text" class="input w-full border" name="visit_form_data[<?php echo $step_no;?>][yes_no][<?php echo $yn_type;?>][value]" placeholder="<?php echo ucfirst($yn_type);?> Label" value="{{$op->option_text}}">
									<div class="flex items-center text-gray-700 dark:text-gray-500 ml-8 mr-8 conditional_checkbox_wrapper">
										<input type="checkbox" class="input border mr-2" <?php if($op->is_conditional == 1){ echo 'checked'; }?>>
										<input type="hidden" name="visit_form_data[<?php echo $step_no;?>][yes_no][<?php echo $yn_type;?>][cond]" value="{{$op->is_conditional}}" class="is_conditional">
										<input type="hidden" name="visit_form_data[<?php echo $step_no;?>][yes_no][<?php echo $yn_type;?>][op_id]" value="{{$op->id}}" class="option_id">
										<input type="hidden" name="visit_form_data[<?php echo $step_no;?>][yes_no][<?php echo $yn_type;?>][next_question_id]" value="{{$op->next_question_id}}" class="next_question_id">
										<label class="cursor-pointer select-none" for="vertical-remember-me">Is conditional?</label>
									</div>
									
								</div>
							</div>
					<?php $x++; } } ?>
						</div>
					</div>
				</div>

			<?php }elseif($row->question_type == 'single'){ ?>

				<div class="formElements mt-6" parent-id="{{$row->parent_id}}" data-id="{{$step_no}}">
					<div class="label_wrapper">
						<label class="font-bold">Single Choice Question</label>
						<div class="action_btns_srapper">
							<button type="button" class="button eye-shrink px-2 mr-1 bg-gray-200 text-gray-600" title="Shrink"><span class="w-5 h-5 flex items-center justify-center"> <i data-feather="eye-off" class="w-4 h-4"></i> </span> </button>
							<button type="button" class="button eye-expand px-2 mr-1 bg-gray-200 text-gray-600" title="Expand"><span class="w-5 h-5 flex items-center justify-center"> <i data-feather="eye" class="w-4 h-4"></i> </span> </button>
							<button type="button" class="cls_btn button px-2 mr-1 bg-theme-6 text-white btn_red" onclick="remove_element(this)" title="Remove"><span class="w-5 h-5 flex items-center justify-center"> <i data-feather="trash" class="w-4 h-4"></i> </span> </button>
							<input type="hidden" class="question_parent_id" name="visit_form_data[<?php echo $step_no;?>][single][parent_id]" value="{{$row->parent_id}}">
							<input type="hidden" class="question_is_parent" name="visit_form_data[<?php echo $step_no;?>][single][is_parent]" value="{{$row->is_parent}}">
							<input type="hidden" class="q_id" name="visit_form_data[<?php echo $step_no;?>][single][q_id]" value="{{$row->id}}">
						</div>
					</div>
					<div class="mb-5 options_wrapper">
						<div class="grid grid-cols-12 gap-6 mt-5">
							<div class="intro-y col-span-12 lg:col-span-12">
								<input type="text" class="input w-full border" name="visit_form_data[<?php echo $step_no;?>][single][label]" placeholder="Type a question" value="{{$row->question_text}}">
							</div>
						</div>
						<div class="grid grid-cols-12 gap-6 ml-12 mt-5 options_box">

					<?php $optionsArray = App\Http\Controllers\admin\VisitFormController::getOptions($visit_form->id,$row->id);
						if($optionsArray && $optionsArray->count() > 0){
							foreach($optionsArray as $op){?>
							<div class="intro-y col-span-12 lg:col-span-12 single-choice">
								<div class="input_wrapper">
									<input type="text" class="input w-full border" name="visit_form_data[<?php echo $step_no;?>][single][option][value][]" placeholder="Type option" value="{{$op->option_text}}">
									<div class="flex items-center text-gray-700 dark:text-gray-500 ml-8 mr-8 conditional_checkbox_wrapper">
										<input type="checkbox" class="input border mr-2" <?php if($op->is_conditional == 1){ echo 'checked'; }?> >
										<input type="hidden" name="visit_form_data[<?php echo $step_no;?>][single][option][cond][]" value="{{$op->is_conditional}}" class="is_conditional">
										<input type="hidden" name="visit_form_data[<?php echo $step_no;?>][single][option][op_id][]" value="{{$op->id}}" class="option_id">
										<input type="hidden" name="visit_form_data[<?php echo $step_no;?>][single][option][next_question_id][]" value="{{$op->next_question_id}}" class="next_question_id">
										<label class="cursor-pointer select-none" for="vertical-remember-me">Is conditional?</label>
									</div>
								</div>
							</div>
					<?php } } ?>

						</div>
						<div class="grid grid-cols-12 gap-6 mt-5 ml-12 add_remove_btns_wrapper">
							<div class="intro-y col-span-12 lg:col-span-12">
								<button type="button" class="button px-2 mr-1 bg-theme-9 text-white" onclick="add_option(this)" title="Add option"> <span class="w-5 h-5 flex items-center justify-center"> <i data-feather="plus" class="w-4 h-4"></i> </span> </button>
								<button type="button" class="cls_btn button px-2 mr-1 bg-theme-6 text-white btn_red" onclick="remove_option(this)" title="Remove option"><span class="w-5 h-5 flex items-center justify-center"> <i data-feather="minus"></i> </span> </button>
							</div>
						</div>
					</div>
				</div>

			<?php }elseif($row->question_type == 'multiple'){ ?>

				<div class="formElements mt-6" parent-id="{{$row->parent_id}}" data-id="{{$step_no}}">
					<div class="label_wrapper">
						<label class="font-bold">Multiple Choice Question</label>
						<div class="action_btns_srapper">
							<button type="button" class="button eye-shrink px-2 mr-1 bg-gray-200 text-gray-600" title="Shrink"><span class="w-5 h-5 flex items-center justify-center"> <i data-feather="eye-off" class="w-4 h-4"></i> </span> </button>
							<button type="button" class="button eye-expand px-2 mr-1 bg-gray-200 text-gray-600" title="Expand"><span class="w-5 h-5 flex items-center justify-center"> <i data-feather="eye" class="w-4 h-4"></i> </span> </button>
							<button type="button" class="cls_btn button px-2 mr-1 bg-theme-6 text-white btn_red" onclick="remove_element(this)" title="Remove"><span class="w-5 h-5 flex items-center justify-center"> <i data-feather="trash" class="w-4 h-4"></i> </span> </button>
							<input type="hidden" class="question_parent_id" name="visit_form_data[<?php echo $step_no;?>][multiple][parent_id]" value="{{$row->parent_id}}">
							<input type="hidden" class="question_is_parent" name="visit_form_data[<?php echo $step_no;?>][multiple][is_parent]" value="{{$row->is_parent}}">
							<input type="hidden" class="q_id" name="visit_form_data[<?php echo $step_no;?>][multiple][q_id]" value="{{$row->id}}">
						</div>
					</div>
					<div class="mb-5 options_wrapper">
						<div class="grid grid-cols-12 gap-6 mt-5">
							<div class="intro-y col-span-12 lg:col-span-12">
								<input type="text" class="input w-full border" name="visit_form_data[<?php echo $step_no;?>][multiple][label]" placeholder="Type a question" value="{{$row->question_text}}">
							</div>
						</div>
						<div class="grid grid-cols-12 gap-6 mt-5 ml-12 options_box">
					<?php $optionsArray = App\Http\Controllers\admin\VisitFormController::getOptions($visit_form->id,$row->id);
						if($optionsArray && $optionsArray->count() > 0){
							foreach($optionsArray as $op){?>
							<div class="intro-y col-span-12 lg:col-span-12 multiple-choice">
								<div class="input_wrapper">
									<input type="text" class="input w-full border" name="visit_form_data[<?php echo $step_no;?>][multiple][option][value][]" placeholder="Type option" value="{{$op->option_text}}">
									<div class="flex items-center text-gray-700 dark:text-gray-500 ml-8 mr-8 conditional_checkbox_wrapper">
										<input type="checkbox" class="input border mr-2" <?php if($op->is_conditional == 1){ echo 'checked'; }?>>
										<input type="hidden" name="visit_form_data[<?php echo $step_no;?>][multiple][option][cond][]" value="{{$op->is_conditional}}"  class="is_conditional">
										<input type="hidden" name="visit_form_data[<?php echo $step_no;?>][multiple][option][op_id][]" value="{{$op->id}}" class="option_id">
										<input type="hidden" name="visit_form_data[<?php echo $step_no;?>][multiple][option][next_question_id][]" value="{{$op->next_question_id}}" class="next_question_id">
										<label class="cursor-pointer select-none" for="vertical-remember-me">Is conditional?</label>
									</div>
								</div>
							</div>
					<?php } } ?>
						</div>
						<div class="grid grid-cols-12 gap-6 mt-5 ml-12 add_remove_btns_wrapper">
							<div class="intro-y col-span-12 lg:col-span-12">
								<button type="button" class="button px-2 mr-1 bg-theme-9 text-white" onclick="add_option2(this)" title="Add option"> <span class="w-5 h-5 flex items-center justify-center"> <i data-feather="plus" class="w-4 h-4"></i> </span> </button>
								<button type="button" class="cls_btn button px-2 mr-1 bg-theme-6 text-white btn_red" onclick="remove_option2(this)" title="Remove option"><span class="w-5 h-5 flex items-center justify-center"> <i data-feather="minus"></i> </span> </button>
							</div>
						</div>
					</div>
				</div>
			<?php } ?>

				<?php	$step_no++; } } } ?>
						</section>
				</div>
				<div class="grid grid-cols-12 gap-6 mt-6" id="FormActionBtn">
					<div class="intro-y col-span-12 lg:col-span-12 pt-5 border-t border-gray-200 dark:border-dark-5">
						<input type="hidden" name="formid" id="formid" value="<?php if($visit_form){ echo $visit_form->id;}?>"/>
						<button type="submit" id="saveBtn" class="button button--md w-24 mr-1 mb-2 btn_blue" <?php if($visit_questions && $visit_questions->count() > 0){}else{ echo 'disabled';}?>>Save</button>
						<button type="reset" class="button button--md w-24 mr-1 mb-2 btn_red" onclick="location.href = '{{env('APP_URL')}}<?php echo '/admin/visit_forms/add/'.$visit_form->id; ?>'">Cancel</button>
					</div>
				</div>
			</form>
		</div>
	</div>

	<div class="hidden_form_elements" style="display:none;">
		<div id="form_field_type_1">
			<div class="formElements mt-6" parent-id="" data-id="">
				<div class="label_wrapper">
					<label class="font-bold">Short Text Question</label>
					<div class="action_btns_srapper">
						<button type="button" class="button eye-shrink px-2 mr-1 bg-gray-200 text-gray-600" title="Shrink"><span class="w-5 h-5 flex items-center justify-center"> <i data-feather="eye-off" class="w-4 h-4"></i> </span> </button>
						<button type="button" class="button eye-expand px-2 mr-1 bg-gray-200 text-gray-600" title="Expand"><span class="w-5 h-5 flex items-center justify-center"> <i data-feather="eye" class="w-4 h-4"></i> </span> </button>
						<button type="button" class="cls_btn button px-2 mr-1 bg-theme-6 text-white btn_red" onclick="remove_element(this)" title="Remove"><span class="w-5 h-5 flex items-center justify-center"> <i data-feather="trash" class="w-4 h-4"></i> </span> </button>
						<input type="hidden" class="question_parent_id" name="visit_form_data[0][short_text][parent_id]" >
						<input type="hidden" class="question_is_parent" name="visit_form_data[0][short_text][is_parent]" value="1">
						<input type="hidden" class="q_id" name="visit_form_data[0][short_text][q_id]" value="">
					</div>
				</div>
				<div class="grid grid-cols-12 gap-6 mt-5 mb-5 options_wrapper">
					<div class="intro-y col-span-12 lg:col-span-12">
						<input type="text" class="input w-full border" name="visit_form_data[0][short_text][label]" placeholder="Type a question">
					</div>
				</div>
			</div>
		</div>

		<div id="form_field_type_2">
			<div class="formElements mt-6" parent-id="" data-id="">
				<div class="label_wrapper">
					<label class="font-bold">Long Text Question</label>
					<div class="action_btns_srapper">
						<button type="button" class="button eye-shrink px-2 mr-1 bg-gray-200 text-gray-600" title="Shrink"><span class="w-5 h-5 flex items-center justify-center"> <i data-feather="eye-off" class="w-4 h-4"></i> </span> </button>
						<button type="button" class="button eye-expand px-2 mr-1 bg-gray-200 text-gray-600" title="Expand"><span class="w-5 h-5 flex items-center justify-center"> <i data-feather="eye" class="w-4 h-4"></i> </span> </button>
						<button type="button" class="cls_btn button px-2 mr-1 bg-theme-6 text-white btn_red" onclick="remove_element(this)" title="Remove"><span class="w-5 h-5 flex items-center justify-center"> <i data-feather="trash" class="w-4 h-4"></i> </span> </button>
						<input type="hidden" class="question_parent_id" name="visit_form_data[0][long_text][parent_id]" >
						<input type="hidden" class="question_is_parent" name="visit_form_data[0][long_text][is_parent]" value="1">
						<input type="hidden" class="q_id" name="visit_form_data[0][long_text][q_id]" value="">
					</div>
				</div>
				<div class="grid grid-cols-12 gap-6 mt-5 mb-5 options_wrapper">
					<div class="intro-y col-span-12 lg:col-span-12">
						<textarea name="visit_form_data[0][long_text][label]" class="input w-full border mt-2" cols="20" rows="3" placeholder="Type a question"></textarea>
					</div>
				</div>
			</div>
		</div>

		<div id="form_field_type_3">
			<div class="formElements mt-6" parent-id="" data-id="">
				<div class="label_wrapper">
					<label class="font-bold">Yes/No Question</label>
					<div class="action_btns_srapper">
						<button type="button" class="button eye-shrink px-2 mr-1 bg-gray-200 text-gray-600" title="Shrink"><span class="w-5 h-5 flex items-center justify-center"> <i data-feather="eye-off" class="w-4 h-4"></i> </span> </button>
						<button type="button" class="button eye-expand px-2 mr-1 bg-gray-200 text-gray-600" title="Expand"><span class="w-5 h-5 flex items-center justify-center"> <i data-feather="eye" class="w-4 h-4"></i> </span> </button>
						<button type="button" class="cls_btn button px-2 mr-1 bg-theme-6 text-white btn_red" onclick="remove_element(this)" title="Remove"><span class="w-5 h-5 flex items-center justify-center"> <i data-feather="trash" class="w-4 h-4"></i> </span> </button>
						<input type="hidden" class="question_parent_id" name="visit_form_data[0][yes_no][parent_id]" >
						<input type="hidden" class="question_is_parent" name="visit_form_data[0][yes_no][is_parent]" value="1">
						<input type="hidden" class="q_id" name="visit_form_data[0][yes_no][q_id]" value="">
					</div>
				</div>
				<div class="grid grid-cols-12 gap-6 mt-5 mb-5 options_wrapper">
					<div class="intro-y col-span-12 lg:col-span-12">
						<input name="visit_form_data[0][yes_no][label]" class="input w-full border" placeholder="Type a question">
					</div>
					<div class="intro-y col-span-12 lg:col-span-12 ml-12">
						<div class="intro-y col-span-12 lg:col-span-12">
							<div class="input_wrapper">
								<input type="text" class="input w-full border" name="visit_form_data[0][yes_no][positive][value]" placeholder="Positive Option" value="Yes">
								<div class="flex items-center text-gray-700 dark:text-gray-500 ml-8 mr-8 conditional_checkbox_wrapper">
									<input type="checkbox" class="input border mr-2">
									<input type="hidden" name="visit_form_data[0][yes_no][positive][cond]" value="0" class="is_conditional">
									<input type="hidden" name="visit_form_data[0][yes_no][positive][op_id]" value="" class="option_id">
									<input type="hidden" name="visit_form_data[0][yes_no][positive][next_question_id]" value="0" class="next_question_id">
									<label class="cursor-pointer select-none" for="vertical-remember-me">Is conditional?</label>
								</div>
							</div>
						</div>
						<div class="intro-y col-span-12 lg:col-span-12 mt-5">
							<div class="input_wrapper">
								<input type="text" class="input w-full border" name="visit_form_data[0][yes_no][negative][value]" placeholder="Negative Label" value="No">
								<div class="flex items-center text-gray-700 dark:text-gray-500 ml-8 mr-8 conditional_checkbox_wrapper">
									<input type="checkbox" class="input border mr-2">
									<input type="hidden" name="visit_form_data[0][yes_no][negative][cond]" value="0" class="is_conditional">
									<input type="hidden" name="visit_form_data[0][yes_no][negative][op_id]" value="" class="option_id">
									<input type="hidden" name="visit_form_data[0][yes_no][negative][next_question_id]" value="0" class="next_question_id">
									<label class="cursor-pointer select-none" for="vertical-remember-me">Is conditional?</label>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div id="form_field_type_4">
			<div class="formElements mt-6" parent-id="" data-id="">
				<div class="label_wrapper">
					<label class="font-bold">Single Choice Question</label>
					<div class="action_btns_srapper">
						<button type="button" class="button eye-shrink px-2 mr-1 bg-gray-200 text-gray-600" title="Shrink"><span class="w-5 h-5 flex items-center justify-center"> <i data-feather="eye-off" class="w-4 h-4"></i> </span> </button>
						<button type="button" class="button eye-expand px-2 mr-1 bg-gray-200 text-gray-600" title="Expand"><span class="w-5 h-5 flex items-center justify-center"> <i data-feather="eye" class="w-4 h-4"></i> </span> </button>
						<button type="button" class="cls_btn button px-2 mr-1 bg-theme-6 text-white btn_red" onclick="remove_element(this)" title="Remove"><span class="w-5 h-5 flex items-center justify-center"> <i data-feather="trash" class="w-4 h-4"></i> </span> </button>
						<input type="hidden" class="question_parent_id" name="visit_form_data[0][single][parent_id]" >
						<input type="hidden" class="question_is_parent" name="visit_form_data[0][single][is_parent]" value="1">
						<input type="hidden" class="q_id" name="visit_form_data[0][single][q_id]" value="">
					</div>
				</div>
				<div class="mb-5 options_wrapper">
					<div class="grid grid-cols-12 gap-6 mt-5">
						<div class="intro-y col-span-12 lg:col-span-12">
							<input type="text" class="input w-full border" name="visit_form_data[0][single][label]" placeholder="Type a question">
						</div>
					</div>
					<div class="grid grid-cols-12 gap-6 ml-12 mt-5 options_box">
						<div class="intro-y col-span-12 lg:col-span-12 single-choice">
							<div class="input_wrapper">
								<input type="text" class="input w-full border" name="visit_form_data[0][single][option][value][]" placeholder="Type option" value="">
								<div class="flex items-center text-gray-700 dark:text-gray-500 ml-8 mr-8 conditional_checkbox_wrapper">
									<input type="checkbox" class="input border mr-2">
									<input type="hidden" name="visit_form_data[0][single][option][cond][]" value="0" class="is_conditional">
									<input type="hidden" name="visit_form_data[0][single][option][op_id][]" value="" class="option_id">
									<input type="hidden" name="visit_form_data[0][single][option][next_question_id][]" value="0" class="next_question_id">
									<label class="cursor-pointer select-none" for="vertical-remember-me">Is conditional?</label>
								</div>
							</div>
						</div>
						<div class="intro-y col-span-12 lg:col-span-12 single-choice">
							<div class="input_wrapper">
								<input type="text" class="input w-full border" name="visit_form_data[0][single][option][value][]" placeholder="Type option" value="">
								<div class="flex items-center text-gray-700 dark:text-gray-500 ml-8 mr-8 conditional_checkbox_wrapper">
									<input type="checkbox" class="input border mr-2">
									<input type="hidden" name="visit_form_data[0][single][option][cond][]" value="0" class="is_conditional">
									<input type="hidden" name="visit_form_data[0][single][option][op_id][]" value="" class="option_id">
									<input type="hidden" name="visit_form_data[0][single][option][next_question_id][]" value="0" class="next_question_id">
									<label class="cursor-pointer select-none" for="vertical-remember-me">Is conditional?</label>
								</div>
							</div>
						</div>
						<div class="intro-y col-span-12 lg:col-span-12 single-choice">
							<div class="input_wrapper">
								<input type="text" class="input w-full border" name="visit_form_data[0][single][option][value][]" placeholder="Type option" value="">
								<div class="flex items-center text-gray-700 dark:text-gray-500 ml-8 mr-8 conditional_checkbox_wrapper">
									<input type="checkbox" class="input border mr-2">
									<input type="hidden" name="visit_form_data[0][single][option][cond][]" value="0" class="is_conditional">			<input type="hidden" name="visit_form_data[0][single][option][op_id][]" value="" class="option_id">
									<input type="hidden" name="visit_form_data[0][single][option][next_question_id][]" value="0" class="next_question_id">
									<label class="cursor-pointer select-none" for="vertical-remember-me">Is conditional?</label>
								</div>
							</div>
						</div>
						<div class="intro-y col-span-12 lg:col-span-12 single-choice">
							<div class="input_wrapper">
								<input type="text" class="input w-full border" name="visit_form_data[0][single][option][value][]" placeholder="Type option" value="">
								<div class="flex items-center text-gray-700 dark:text-gray-500 ml-8 mr-8 conditional_checkbox_wrapper">
									<input type="checkbox" class="input border mr-2">
									<input type="hidden" name="visit_form_data[0][single][option][cond][]" value="0" class="is_conditional">
									<input type="hidden" name="visit_form_data[0][single][option][op_id][]" value="" class="option_id">
									<input type="hidden" name="visit_form_data[0][single][option][next_question_id][]" value="0" class="next_question_id">
									<label class="cursor-pointer select-none" for="vertical-remember-me">Is conditional?</label>
								</div>
							</div>
						</div>
					</div>
					<div class="grid grid-cols-12 gap-6 mt-5 ml-12 add_remove_btns_wrapper">
						<div class="intro-y col-span-12 lg:col-span-12">
							<button type="button" class="button px-2 mr-1 bg-theme-9 text-white" onclick="add_option(this)" title="Add option"> <span class="w-5 h-5 flex items-center justify-center"> <i data-feather="plus" class="w-4 h-4"></i> </span> </button>
							<button type="button" class="cls_btn button px-2 mr-1 bg-theme-6 text-white btn_red" onclick="remove_option(this)" title="Remove option"><span class="w-5 h-5 flex items-center justify-center"> <i data-feather="minus"></i> </span> </button>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div id="form_field_type_5">
			<div class="formElements mt-6" parent-id="" data-id="">
				<div class="label_wrapper">
					<label class="font-bold">Multiple Choice Question</label>
					<div class="action_btns_srapper">
						<button type="button" class="button eye-shrink px-2 mr-1 bg-gray-200 text-gray-600" title="Shrink"><span class="w-5 h-5 flex items-center justify-center"> <i data-feather="eye-off" class="w-4 h-4"></i> </span> </button>
						<button type="button" class="button eye-expand px-2 mr-1 bg-gray-200 text-gray-600" title="Expand"><span class="w-5 h-5 flex items-center justify-center"> <i data-feather="eye" class="w-4 h-4"></i> </span> </button>
						<button type="button" class="cls_btn button px-2 mr-1 bg-theme-6 text-white btn_red" onclick="remove_element(this)" title="Remove"><span class="w-5 h-5 flex items-center justify-center"> <i data-feather="trash" class="w-4 h-4"></i> </span> </button>
						<input type="hidden" class="question_parent_id" name="visit_form_data[0][multiple][parent_id]" >
						<input type="hidden" class="question_is_parent" name="visit_form_data[0][multiple][is_parent]" value="1">
						<input type="hidden" class="q_id" name="visit_form_data[0][multiple][q_id]" value="">
					</div>
				</div>
				<div class="mb-5 options_wrapper">
					<div class="grid grid-cols-12 gap-6 mt-5">
						<div class="intro-y col-span-12 lg:col-span-12">
							<input type="text" class="input w-full border" name="visit_form_data[0][multiple][label]" placeholder="Type a question">
						</div>
					</div>
					<div class="grid grid-cols-12 gap-6 mt-5 ml-12 options_box">
						<div class="intro-y col-span-12 lg:col-span-12 multiple-choice">
							<div class="input_wrapper">
								<input type="text" class="input w-full border" name="visit_form_data[0][multiple][option][value][]" placeholder="Type option" value="">
								<div class="flex items-center text-gray-700 dark:text-gray-500 ml-8 mr-8 conditional_checkbox_wrapper">
									<input type="checkbox" class="input border mr-2">
									<input type="hidden" name="visit_form_data[0][multiple][option][cond][]" value="0" class="is_conditional">
									<input type="hidden" name="visit_form_data[0][multiple][option][op_id][]" value="" class="option_id">
									<input type="hidden" name="visit_form_data[0][multiple][option][next_question_id][]" value="0" class="next_question_id">
									<label class="cursor-pointer select-none" for="vertical-remember-me">Is conditional?</label>
								</div>
							</div>
						</div>
						<div class="intro-y col-span-12 lg:col-span-12 multiple-choice">
							<div class="input_wrapper">
								<input type="text" class="input w-full border" name="visit_form_data[0][multiple][option][value][]" placeholder="Type option" value="">
								<div class="flex items-center text-gray-700 dark:text-gray-500 ml-8 mr-8 conditional_checkbox_wrapper">
									<input type="checkbox" class="input border mr-2">
									<input type="hidden" name="visit_form_data[0][multiple][option][cond][]" value="0" class="is_conditional">
									<input type="hidden" name="visit_form_data[0][multiple][option][op_id][]" value="" class="option_id">
									<input type="hidden" name="visit_form_data[0][multiple][option][next_question_id][]" value="0" class="next_question_id">
									<label class="cursor-pointer select-none" for="vertical-remember-me">Is conditional?</label>
								</div>
							</div>
						</div>
						<div class="intro-y col-span-12 lg:col-span-12 multiple-choice">
							<div class="input_wrapper">
								<input type="text" class="input w-full border" name="visit_form_data[0][multiple][option][value][]" placeholder="Type option" value="">
								<div class="flex items-center text-gray-700 dark:text-gray-500 ml-8 mr-8 conditional_checkbox_wrapper">
									<input type="checkbox" class="input border mr-2">
									<input type="hidden" name="visit_form_data[0][multiple][option][cond][]" value="0" class="is_conditional">
									<input type="hidden" name="visit_form_data[0][multiple][option][op_id][]" value="" class="option_id">
									<input type="hidden" name="visit_form_data[0][multiple][option][next_question_id][]" value="0" class="next_question_id">
									<label class="cursor-pointer select-none" for="vertical-remember-me">Is conditional?</label>
								</div>
							</div>
						</div>
						<div class="intro-y col-span-12 lg:col-span-12 multiple-choice">
							<div class="input_wrapper">
								<input type="text" class="input w-full border" name="visit_form_data[0][multiple][option][value][]" placeholder="Type option" value="">
								<div class="flex items-center text-gray-700 dark:text-gray-500 ml-8 mr-8 conditional_checkbox_wrapper">
									<input type="checkbox" class="input border mr-2">
									<input type="hidden" name="visit_form_data[0][multiple][option][cond][]" value="0" class="is_conditional">
									<input type="hidden" name="visit_form_data[0][multiple][option][op_id][]" value="" class="option_id">
									<input type="hidden" name="visit_form_data[0][multiple][option][next_question_id][]" value="0" class="next_question_id">
									<label class="cursor-pointer select-none" for="vertical-remember-me">Is conditional?</label>
								</div>
							</div>
						</div>
					</div>
					<div class="grid grid-cols-12 gap-6 mt-5 ml-12 add_remove_btns_wrapper">
						<div class="intro-y col-span-12 lg:col-span-12">
							<button type="button" class="button px-2 mr-1 bg-theme-9 text-white" onclick="add_option2(this)" title="Add option"> <span class="w-5 h-5 flex items-center justify-center"> <i data-feather="plus" class="w-4 h-4"></i> </span> </button>
							<button type="button" class="cls_btn button px-2 mr-1 bg-theme-6 text-white btn_red" onclick="remove_option2(this)" title="Remove option"><span class="w-5 h-5 flex items-center justify-center"> <i data-feather="minus"></i> </span> </button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Side Button -->
	<div class="side_btn_wrapper">
		<a href="#">Add New Question <i data-feather="plus"></i></a>
	</div>
	<div class="right_sidebar_fq">
		<div class="header">
			<h1>Choose Question Type</h1>
			<a href="#" class="close_sidebar"><i data-feather="x"></i></a>
		</div>
		<div class="body">
			<ul>
			@if($form_field_types)
				@foreach($form_field_types as $fft)
					<li><a href="javascript:void(0);" onclick="add_element({{$fft->id}})"><i data-feather="{{$fft->icon}}"></i> {{$fft->field_type_name}}</a></li>
				@endforeach
			@endif				
			</ul>
		</div>
	</div>

	<script type="text/javascript">
		var step = <?php echo $step_no - 1;?>;
		var formid;
		$(document).ready(function() {
			// Right sidebar
			$(".side_btn_wrapper").click(function() {
				$(this).css("right", "-176px");
				$(".right_sidebar_fq").css("right", "33px");
			});

			$(".close_sidebar, .right_sidebar_fq .body ul>li>a").click(function() {
				$(".right_sidebar_fq").css("right", "-300px");
				$(".side_btn_wrapper").css("right", "33px");
			});


			$("#VisitFieldForm").submit(function(e) {
				e.preventDefault();
				$.ajax({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					},
					data: $('#VisitFieldForm').serialize(),
					url: "{{ route('admin.visit_form_questions.store') }}",
					type: "POST",
					// dataType: 'json',
					success: function (response) {
						formid = $("#formid").val();
						//response = JSON.parse(res);
						if(response['status'] == 'success'){
							swal({
								title: response['message'],
								icon: 'success'
							});

							setTimeout(function(){
								swal.close();
								$('#VisitFieldForm').trigger("reset");
								window.location = "{{ route('admin.visit_forms') }}";
							}, 500);
						}
					},
					error: function (data) {
						//console.log('Error:', data);
						swal({
							title: 'Error Occured.',
							icon: 'error'
						})
						$('#saveBtn').html('Save');
					}
				});
			});

			// Accordion
			$(document).on("click", ".formElements .label_wrapper .eye-shrink", function() {
				$(this).hide();
				$(this).closest(".formElements").find(".eye-expand").show();
				$(this).closest(".formElements").find(".options_wrapper").slideUp();
			});
			$(document).on("click", ".formElements .label_wrapper .eye-expand", function() {
				$(this).hide();
				$(this).closest(".formElements").find(".eye-shrink").show();
				$(this).closest(".formElements").find(".options_wrapper").slideDown();
			});

			/***** Conditional Question add/remove ****/

			$(document).on("click", ".conditional_checkbox_wrapper input[type='checkbox']", function() {
				$(this).toggleClass('active');

				if($(this).prop("checked") == true){
					$(this).next('input.is_conditional').val(1);
					let options;

					@if($form_field_types)
						@foreach($form_field_types as $fft)
							options += '<option value="{{$fft->id}}">{{$fft->field_type_name}}</option>';
						@endforeach
					@endif

					let html = '<select id="" class="form_element select2 mt-2" onchange="child_question_add(this);">' +
									'<option value="">Select Child Question Type</option>' + options +
								'</select>';

					$(this).closest(".input_wrapper").append(html);
					$(".select2").select2();

				} else {
					$(this).next('input.is_conditional').val(0);
					let parentClass = $(this).parent().next("select.form_element").attr('class').split(' ');
					parentClass = parentClass[parentClass.length-1];
					let fields = parentClass.split('_');
					let id = fields[1];
					let className = '.mychild_'+id;
					$(this).closest(".input_wrapper").find(".select2, .add_new").remove();
					$('button.cls_btn_child'+className).trigger('click');

				}

				//console.log('checkbox value=',$(this).val());
			});

			// Append sub questions
			//$(document).on("click", ".sub_q_add_new", function() {

			//});
		});

		function child_question_add(e){

			let val = e.value;

			if(val == ''){
				alert('Please select any element.');
				return false;
			}

			let element_id = '#form_field_type_'+val;
			let parent_id = $(e).parent().parent().parent().parent().parent().attr('data-id');
			//console.log('parent_id',parent_id);
			step += 1;

			$(element_id+' *').filter('input,textarea').each(function(){

				if($(this).is('[name]')){
					let attrName = $(this).attr('name');
					let newName = attrName.replace(/\[\d+](?!.*\[\d)/, '['+step+']');
					$(this).attr('name',newName);
					//console.log('step= '+step+' and newName= ',newName);
				}

			});

			let html = $(element_id).html();
			//console.log('child html',html);
			let rbtnClass = 'cls_btn'+'_child mychild_'+step;
			let nClass = 'myparent_'+step;

			var new_html = html.replace('cls_btn',rbtnClass);

			let old_text = '[parent_id]"';
			let new_text = '[parent_id]" value="'+parent_id+'"';

			new_html = new_html.replace(old_text,new_text);

			let old_text2 = '[is_parent]" value="1"';
			let new_text2 = '[is_parent]" value="0"';

			new_html = new_html.replace(old_text2,new_text2);

			let old_id = '"formElements mt-6" parent-id="" data-id=""';
			let new_id = '"formElements mt-6" parent-id="'+parent_id+'" data-id="'+step+'"';

			new_html = new_html.replace(old_id,new_id);

			$(e).addClass(nClass);
			$(e).prop('disabled', true);
			$(e).prev('.conditional_checkbox_wrapper').find('input[type="checkbox"]').prop('readonly', true);
			$(e).prev('.conditional_checkbox_wrapper').find('input.next_question_id').val(step);

			$('.more_items_wrapper').append(new_html);


			$('html, body').animate({
				scrollTop: $("#FormActionBtn").offset().top
			}, 1000);
			
			//reset select dropdown
			$('#form_element').val('').trigger('change');

			//Add Disabled to remove button of this question
			$(e).parent().parent().parent().parent().parent().find('.label_wrapper .action_btns_srapper button.btn_red').prop('disabled',true);
			//$(e).parent().parent().parent().parent().parent().find('.label_wrapper .action_btns_srapper button.eye-shrink').trigger('click');
			
		}



		function add_element(id){
			//let id = $("#form_element").val();
			if(id == ''){
				alert('Please select any element.');
				return false;
			}
			let element_id = '#form_field_type_'+id;

			step += 1;

			$(element_id+' *').filter('input,textarea').each(function(){

				if($(this).is('[name]')){
					let attrName = $(this).attr('name');
					let newName = attrName.replace(/\[\d+](?!.*\[\d)/, '['+step+']');
					$(this).attr('name',newName);
					//console.log('step= '+step+' and newName= ',newName);
				}
			});

			let html = $(element_id).html();

			let old_id = '"formElements mt-6" parent-id="" data-id=""';
			let new_id = '"formElements mt-6" parent-id="" data-id="'+step+'"';

			html = html.replace(old_id,new_id);

			$("#saveBtn").prop('disabled',false);
			
			$('.more_items_wrapper').append(html);
			$('html, body').animate({
				scrollTop: $("#FormActionBtn").offset().top
			}, 1000);			

			//reset select dropdown
			$('#form_element').val('').trigger('change');
		}

		function remove_element(e) {
			//if(confirm("Are you sure?")) {
				if($(e).hasClass('cls_btn_child')){
					let childClass = $(e).attr('class').split(' ')[1];
					let fields = childClass.split('_');
					let id = fields[1];
					let className = '.myparent_'+id;
					//Remove Disabled from remove button of parent question
					$('.form_element'+className).parent().parent().parent().parent().parent().find('.label_wrapper .action_btns_srapper button.btn_red').prop('disabled',false);
					$('.form_element'+className).prev('.conditional_checkbox_wrapper').find('input').prop('checked',false);
					$('.form_element'+className).prev('.conditional_checkbox_wrapper').find('input').prop('readonly', false);
					$('.form_element'+className).select2('destroy');
					$('.form_element'+className).remove();
					//$('.form_element'+className).prop('disabled', false);
					//$('.form_element'+ className).select2("val", "");
				}
				$(e).closest('.formElements').remove();
			//}
			
			//if ($('#VisitFieldForm .more_items_wrapper').is(':empty')){
			if($.trim($(".more_items_wrapper").html())==''){
				//console.log('empty data');
				$("#saveBtn").prop('disabled',true);
			}
		}

		function add_option(e){
			let last = $(e).parent().parent().parent().find('div.grid .single-choice:last-child');
			let optionHtml = last[0].outerHTML;
			$(e).parent().parent().parent().find('.options_box').append(optionHtml);
			$(e).parent().parent().parent().find('div.grid .single-choice:last-child').find('input').val('');
		}

		function remove_option(e){
			let length = $(e).parent().parent().parent().find('div.grid .single-choice').length;
			let last = $(e).parent().parent().parent().find('div.grid .single-choice:last-child');
			if(length > 1){
				last.remove();
			}
		}

		function add_option2(e){
			let last = $(e).parent().parent().parent().find('div.grid .multiple-choice:last-child');
			let optionHtml = last[0].outerHTML;
			$(e).parent().parent().parent().find('.options_box').append(optionHtml);
			$(e).parent().parent().parent().find('div.grid .multiple-choice:last-child').find('input').val('');

		}

		function remove_option2(e){
			let length = $(e).parent().parent().parent().find('div.grid .multiple-choice').length;
			let last = $(e).parent().parent().parent().find('div.grid .multiple-choice:last-child');
			if(length > 1){
				last.remove();
			}
		}
	</script>
</html>
@endsection