@extends('layouts.app')
@section('content')
	<div class="intro-y box mt-5">
		<div class="flex flex-col sm:flex-row items-center p-5 border-b border-gray-200 dark:border-dark-5">
			<h2 class="font-medium text-base mr-auto">@if($plan_features) {{ 'Edit' }} @else Add New @endif Plan Feature</h2>
		</div>
		<div class="intro-y datatable-wrapper box p-5 col-span-12 overflow-auto lg:overflow-hidden">
			<form id="AllPlanFeaturesForm">
				<input type="hidden" name="id" value="<?php if($plan_features){ echo $plan_features->id;}?>"/>						
				<div class="grid grid-cols-12 gap-6 mt-5">
					<div class="intro-y col-span-12 lg:col-span-9">
						<div>
							<label>Name</label>
							<input type="text" name="name" value="<?php if($plan_features){ echo $plan_features->name;}?>" class="input w-full border mt-2" placeholder="Name" id="name" required>
						</div>
					</div>
					<div class="intro-y col-span-12 lg:col-span-3">
						<div>
							<label>Price</label>
							<input type="number" name="price" value="<?php if($plan_features){ echo $plan_features->price;	}?>" class="input w-full border mt-2" placeholder="Price" id="price" required>
						</div>
					</div>					
					<div class="intro-y col-span-12 lg:col-span-12">
						<label>Detail</label>
						<textarea name="detail" id="detail" class="input w-full border mt-2" cols="30" rows="3" required><?php if($plan_features){ echo $plan_features->detail; }?></textarea>
					</div>
										
				</div>
				
				<div class="mt-4">
					<label>Active Status</label>
					<div class="mt-2">
						<input type="checkbox" name="status" class="input input--switch border" @if($plan_features && $plan_features->status == 1) checked @endif>
					</div>
				</div>
				<div class="grid grid-cols-12 gap-6 mt-12">
					<div class="intro-y col-span-12 lg:col-span-12">
						<button type="submit" id="saveBtn" class="button button--md w-24 mr-1 mb-2 btn_blue">Save</button>
						<button type="reset" class="button button--md w-24 mr-1 mb-2 btn_red" onclick="location.href = '{{ route('admin.plan_features') }}';">Cancel</button>
					</div>
				</div>
			</form>
		</div>
	</div>

	<script type="text/javascript">
		$(document).ready(function() {

			$("#AllPlanFeaturesForm").submit(function(e) {
				e.preventDefault();
				$.ajax({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					},
					data: $('#AllPlanFeaturesForm').serialize(),
					url: "{{ route('admin.plan_features.store') }}",
					type: "POST",
					// dataType: 'json',
					success: function (response) {
						//response = JSON.parse(res);
						if(response['status'] == 'success'){
							swal({
								title: response['message'],
								icon: 'success'
							});
							$('#AllPlanFeaturesForm').trigger("reset");
							setTimeout(function(){
								swal.close();
								window.location = "{{ route('admin.plan_features') }}";
							}, 1000);
						}
					},
					error: function (data) {
						//console.log('Error:', data);
						swal({
							title: 'Error Occured.',
							icon: 'error'
						})
						$('#saveBtn').html('Save');
					}
				});
			});
		});

	</script>
</html>
@endsection