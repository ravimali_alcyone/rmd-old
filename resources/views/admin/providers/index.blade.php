@extends('layouts.app')
@section('content')
	<div class="intro-y flex flex-col sm:flex-row items-center mt-8">
		<h2 class="text-lg font-medium mr-auto">Providers List</h2>
	</div>

	<div class="grid grid-cols-12 gap-6 mt-5">
		<div class="intro-y col-span-12 items-center">
			<form id="tableFilters">
				<div class="col-span-4">
					<label>Category</label>
					<select id="filter_category" class="select2 w-full">
						<option value="">Any</option>
						@if($categories && $categories->count() > 0)
							@foreach($categories as $category)
								<option value="{{ $category->id }}" <?php echo $category->id == $cat_id ?  'selected' : ''; ?>>{{ $category->name }}</option>
							@endforeach
						@endif
					</select>
				</div>

				<div class="col-span-4 ml-2">
					<label>Status</label>
					<select id="filter_status" class="select2 w-full">
						<option value="" class="any">Any</option>
						<option value="1">Active</option>
						<option value="0">Blocked</option>
					</select>
				</div>
				<div class="w-auto mt-3 mt-md-0">
					<button type="button" class="button w-24 mr-1 mb-2 btn_dark ml-2" id="filter">Show</button>
					<button type="reset" class="button w-24 mr-1 mb-2 btn_red float-right ml-2" id="reset">Reset</button>
				</div>
			</form>
		</div>

		<div class="intro-y datatable-wrapper box p-5 col-span-12 overflow-auto lg:overflow-hidden">
			<table id="myTable" class="table table-report--bordered display w-full sub_admin_table dataTable">
				<thead>
					<tr>
						<th class="border-b-2">S.No.</th>
						<th class="border-b-2">Name</th>
						<th class="border-b-2">Image</th>
						<th class="border-b-2">Category</th>
						<th class="border-b-2">Status</th>
						<th class="border-b-2">Actions</th>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
		</div>
	</div>

	<script type="text/javascript">
		var table;
		$(document).ready(function(){
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});
			fill_datatable();

			function fill_datatable(filter_category = '', filter_status = ''){
				table = $('#myTable').DataTable({
					processing: true,
					serverSide: true,
					responsive: true,
					paging: true,
					ordering: false,
					info: false,
					displayLength: 10,
					language: {
						processing: '<img src="/dist/images/loading.gif"/>'
					},
					lengthMenu: [
					[10, 25, 50, -1],
					[10, 25, 50, "All"]
					],
					ajax:{
						url: "{{ route('admin.providers') }}",
						data:{filter_category: filter_category, filter_status: filter_status}
					},
					columns: [
						{data: 'DT_RowIndex', name: 'DT_RowIndex'},
						{data: 'name', name: 'name'},
						{data: 'image', name: 'image'},
						{data: 'category', name: 'category'},
						{data: 'status', name: 'status'},
						{data: 'action', name: 'action', orderable: false, searchable: false},
					]
				});
			}

			$('#filter').click(function(){
				var filter_category = $('#filter_category').val();
				var filter_status = $('#filter_status').val();
				$('#myTable').DataTable().destroy();
            	fill_datatable(filter_category, filter_status);
			});

			$('#reset').click(function(){
				$(".select2-selection__rendered").attr('title', 'Any').html('Any');
				$('#myTable').DataTable().destroy();
				fill_datatable();
			});

			let cat_id = "{{ $cat_id }}";
			if(cat_id != null) {
				$("#filter").trigger("click");
			}

		});

		function deleteRow(id) {
			swal({
				title: "Are you sure to delete?",
				text: "",
				icon: 'warning',
				buttons: {
				cancel: true,
				delete: 'Yes, Delete It'
				}
			}).then((isConfirm) => {
				if (!isConfirm) {
					return false;
				} else {
					$.ajax({
						type: "DELETE",
						url: "{{ env('APP_URL').'/admin/providers/destroy' }}"+'/'+id,
						success: function (response) {

							if(response['status'] == 'success'){
							swal({
								title: response['message'],
								icon: 'success'
							});
							setTimeout(function(){
							swal.close();
							}, 1000);
							}
							table.draw();
						},
						error: function (data) {
							console.log('Error:', data);
							swal({
								title: 'Error Occured',
								icon: 'error'
							});
						}
					});
				}
			});
		}

		function changeStatus(id) {
			$.ajax({
				url: "{{ url('admin/providers/change_status') }}",
				type: "POST",
				data: {'id': id},
				success: function(response) {
					console.log(response);
				}
			});
		}
	</script>
</html>
@endsection