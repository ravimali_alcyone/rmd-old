@extends('front.online_visit_layout.app')

@section('content')
    <div class="tabs_wrapper welcome">
        <div>
            <a href="javascript:void(0);" class="how_it_works active">How it works</a>
            <a href="javascript:void(0);" class="prescription" style="display:none;">Prescription</a>
        </div>
    </div>

    <div class="main_welcome_wrapper">
        <section class="welcome_content_wrapper">
            <div class="how_it_works">
                <h3 class="heading">Hi {{$user->first_name}}, <br>Thanks for starting your online visit.</h3>
                <div class="content_box">
                    <div class="box">
                        <span class="number">1</span>
                        <h3>Answer questions about your health.</h3>
                        <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos.</p>
                    </div>
                    <div class="box">
                        <span class="number">2</span>
                        <h3>Your answers will be sent to your doctor for review.</h3>
                        <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos.</p>
                    </div>
                    <div class="box">
                        <span class="number">3</span>
                        <h3>Your healthcare professional will follow up.</h3>
                        <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos.</p>
                    </div>
                </div>
                <button class="continue_btn btn btn-primary" onclick="location.href = '{{ route('online_visit_basics') }}';">Continue My Visit</button>
            </div>
            <div class="prescription">
                <!-- <h3 class="heading">Hi {{$user->first_name}}, <br>Thanks for starting your online visit.</h3> -->
                <!-- <div class="content_box">
                    <div class="box">
                        <span class="number">4</span>
                        <h3>Answer questions about your health.</h3>
                        <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos.</p>
                    </div>
                    <div class="box">
                        <span class="number">5</span>
                        <h3>Your answers will be sent to your doctor for review.</h3>
                        <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos.</p>
                    </div>
                    <div class="box">
                        <span class="number">6</span>
                        <h3>Your healthcare professional will follow up.</h3>
                        <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos.</p>
                    </div>
                </div>
                <button class="continue_btn btn btn-primary">Continue My Visit</button> -->
            </div>
        </section>
    </div>
    <script>
        $(document).ready(function() {
            $(".tabs_wrapper.welcome>div>a").click(function() {
                $(".tabs_wrapper.welcome>div>a").removeClass("active");
                $(this).addClass("active");
            });

            $(".tabs_wrapper.welcome .how_it_works").click(function() {
                $(".welcome_content_wrapper .prescription").hide();
                $(".welcome_content_wrapper .how_it_works").show();
            });
            $(".tabs_wrapper.welcome .prescription").click(function() {
                $(".welcome_content_wrapper .how_it_works").hide();
                $(".welcome_content_wrapper .prescription").show();
            });
        });
    </script>	
@endsection