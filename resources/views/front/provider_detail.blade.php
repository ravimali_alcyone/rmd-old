@extends('front.layouts.app')
<style>
    .head_bg{
        border-bottom: 1px solid #ff6c66;
    }
</style>

@section('content')
    <section class="provider_detail_sec">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="custom_breadcrumb">
                        <a href="{{env('APP_URL')}}/providers">All Providers</a> > <a href="{{env('APP_URL')}}/providers/{{$provider['id']}}">Dr. Gaby</a>
                    </div>

                    <div class="provider_box">
                        <div class="img_box">
                            <img src="{{$provider['image']}}" alt="{{$provider['name']}}" class="img-fluid">
                        </div>
                        <div class="des_box">
                            <h3 class="name">{{$provider['name']}}</h3>
                            <h5 class="sub_title">{{$provider['provider_categories']}}</h5>
                            <p class="description">{{$provider['short_info']}}</p>
                            <button type="button" class="btn btn-primary b-0">See Plans</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="about_box">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h3>About {{$provider['name']}}</h3>
                        <div class="text_box">
                            <h6 class="heading">Clinical Expertise:</h6>
                            <p class="des">{{$provider['clinical_expertise']}}</p>
                        </div>
                        <div class="text_box">
                            <h6 class="heading">Credentials:</h6>
                            <p class="des">{{$provider['credentials']}}</p>
                        </div>
                        <div class="text_box">
                            <h6 class="heading">Awards/Honors</h6>
                            <p class="des">{{$provider['awards']}}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="meet_providers_sec">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h3 class="heading">Meet other providers</h3>
                    <div class="member_wrapper">
					
				@if($other_providers)
					@foreach($other_providers as $row)
                        <div class="item_mem">
                            <div class="exprt_box">
                                <div class="exprt_img"><img src="{{$row['image']}}" class="img-fluid" alt="{{$row['name']}}"></div>
                                <div class="eprt_info">
                                    <strong>{{$row['name']}}</strong>
                                    <p>{{$row['provider_categories']}}</p>
                                    <a class="vb_link" href="{{env('APP_URL')}}/providers/{{$row['id']}}">View full bio <i class="fa fa-long-arrow-alt-right"></i></a>
                                </div>
                            </div>
                        </div>
					@endforeach
				@endif
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="work_join_sec about experts_p providers_d">
        <div class="data_wrap">
            <div class="container">
                <div class="content_wrapper">
                    <div class="title">Best in class medical experts
                        <p>Lorem ipsum dolor sit amet, consetetur</p>
                    </div>
                    <div class="boxes_wrapper d_flex_j_center">
                        <div class="box">
                            <span class="fas fa-file-medical icon"></span>
                            <h6 class="title">Licensed &amp; <br>accredited doctors</h6>
                        </div>
                        <div class="box">
                            <span class="medal_icon icon"></span>
                            <h6 class="title">Trained in Functional <br>Medicine</h6>
                        </div>
                        <div class="box">
                            <span class="fas fa-flask icon"></span>
                            <h6 class="title">Experienced in personalized <br>labs &amp; analysis</h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection