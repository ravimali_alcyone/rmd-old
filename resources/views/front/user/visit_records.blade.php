@extends('front.dashboard_layout.app')

@section('content')

	<div class="col-xl-9 col-lg-9 col-md-12">
		<div class="main-center-data">
			<h3 class="display-username">My Visits</h3>
			<div class="status-box mt-15 bg-white round-crn pd-20-30 mt-15 hover-effect-box">
				<ul class="m-0 p-0">
					<li>Online Consult</li>
					<li>DOCTOR REVIEW</li>
					<li>APPROVE PROGRAM</li>
				</ul>
				<h4 class="font-weight-bold mt-15">Skin care</h4>
				<p class="note m-0">By turning on this toggle, I agree to receive texts from RMD and/or Tripathi to (888) 444-9999 that might be considered marketing and that may be sent using an auto dialer. I understand that agreeing is not required to purchase.</p>
			</div>
			<div class="status-box mt-15 bg-white round-crn pd-20-30 mt-15 hover-effect-box">
				<ul class="completed-status m-0 p-0">
					<li>Online Consult</li>
					<li>DOCTOR REVIEW</li>
					<li>APPROVE PROGRAM</li>
				</ul>
				<h4 class="font-weight-bold mt-15">Erectile dysfunction treatment</h4>
				<p class="note m-0">You completed an annual visit on Sep 30. You’re all set with your doctor!</p>
				<div class="provider-and-treatment mt-30">
					<div class="provider">
						<h6 class="label-mute">PROVIDER</h6>
						<div class="provider-details mt-15">
							<img src="{{asset('dashboard/img/doctor-1.svg')}}" alt="doctor" />
							Dr. Alice Brown
						</div>
					</div>
					<div class="provider">
						<h6 class="label-mute">TREATMENT</h6>
						<div class="provider-details mt-15">
							<img src="{{asset('dashboard/img/treatment.svg')}}" alt="doctor" />
							Sildenafil
						</div>
					</div>
				</div>
			</div>
			<div class="custom-pagination mt-15">
				<nav aria-label="Page navigation example">
				  <ul class="pagination">
					<li class="page-item"><a class="page-link" href="#">Previous</a></li>
					<li class="page-item"><a class="page-link" href="#">1</a></li>
					<li class="page-item"><a class="page-link" href="#">2</a></li>
					<li class="page-item"><a class="page-link" href="#">3</a></li>
					<li class="page-item"><a class="page-link" href="#">Next</a></li>
				  </ul>
				</nav>
			</div>
		</div>
	</div>
	
@endsection