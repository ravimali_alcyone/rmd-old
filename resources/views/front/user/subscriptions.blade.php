@extends('front.dashboard_layout.app')

@section('content')

                <div class="col-xl-9 col-lg-9 col-md-12">
                    <div class="main-center-data">
                        <div class="heading-and-upgrade-button">
                            <h3 class="display-username">My Subscriptions</h3>
                            <a href="#" class="theme-btn">UPGRADE</a>
                        </div>
                        <div class="to-scroll-outer">
                        <div class="informative-block bg-white round-crn pd-20-30 mt-15 hover-effect-box my-subscriptions">
                            <div class="inactive">INACTIVE</div>
                            <div class="clearfix"></div>
                            <h4 class="font-weight-bold">12 Month Plan</h4>
                            <p class="note m-0">One full year of care and access to membership benefits.</p>
                            <ul class="p-0">
                                <li>Plans Start:  <b>14 JAN 2020</b></li>
                                <li>Plans Ends:  <b>12 DEC 2021</b></li>
                            </ul>
                            <div class="subs-price">
                                <span class="subs-price-value theme-color"><span class="d-block">TOTAL:</span>$150.00</span>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="informative-block bg-white round-crn pd-20-30 mt-15 hover-effect-box my-subscriptions">
                            <div class="active-subs">ACTIVE</div>
                            <div class="clearfix"></div>
                            <h4 class="font-weight-bold">4 Month Plan</h4>
                            <p class="note m-0">Work with your care team and access your membership benefits.</p>
                            <ul class="p-0">
                                <li>Plans Start:  <b>14 JAN 2020</b></li>
                                <li>Plans Ends:  <b>12 DEC 2021</b></li>
                            </ul>
                            <div class="subs-price">
                                <span class="subs-price-value theme-color"><span class="d-block">TOTAL:</span>$50.00</span>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="informative-block bg-white round-crn pd-20-30 mt-15 hover-effect-box my-subscriptions">
                            <div class="inactive">EXPIRED</div>
                            <div class="clearfix"></div>
                            <h4 class="font-weight-bold">FREE TRIAL</h4>
                            <p class="note m-0">Work with our Care Team online to understand your health concerns and start a personalized health plan.</p>
                            <ul class="p-0">
                                <li>Plans Start:  <b>14 JAN 2020</b></li>
                                <li>Plans Ends:  <b>12 DEC 2021</b></li>
                            </ul>
                            <div class="subs-price">
                                <span class="subs-price-value theme-color"><span class="d-block">TOTAL:</span>$150.00</span>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="informative-block bg-white round-crn pd-20-30 mt-15 hover-effect-box my-subscriptions">
                            <div class="inactive">EXPIRED</div>
                            <div class="clearfix"></div>
                            <h4 class="font-weight-bold">12 Month Plan</h4>
                            <p class="note m-0">One full year of care and access to membership benefits.</p>
                            <ul class="p-0">
                                <li>Plans Start:  <b>14 JAN 2020</b></li>
                                <li>Plans Ends:  <b>12 DEC 2021</b></li>
                            </ul>
                            <div class="subs-price">
                                <span class="subs-price-value theme-color"><span class="d-block">TOTAL:</span>$150.00</span>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="informative-block bg-white round-crn pd-20-30 mt-15 hover-effect-box my-subscriptions">
                            <div class="inactive">EXPIRED</div>
                            <div class="clearfix"></div>
                            <h4 class="font-weight-bold">4 Month Plan</h4>
                            <p class="note m-0">Work with your care team and access your membership benefits.</p>
                            <ul class="p-0">
                                <li>Plans Start:  <b>14 JAN 2020</b></li>
                                <li>Plans Ends:  <b>12 DEC 2021</b></li>
                            </ul>
                            <div class="subs-price">
                                <span class="subs-price-value theme-color"><span class="d-block">TOTAL:</span>$50.00</span>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="informative-block bg-white round-crn pd-20-30 mt-15 hover-effect-box my-subscriptions">
                            <div class="inactive">EXPIRED</div>
                            <div class="clearfix"></div>
                            <h4 class="font-weight-bold">FREE TRIAL</h4>
                            <p class="note m-0">Work with our Care Team online to understand your health concerns and start a personalized health plan.</p>
                            <ul class="p-0">
                                <li>Plans Start:  <b>14 JAN 2020</b></li>
                                <li>Plans Ends:  <b>12 DEC 2021</b></li>
                            </ul>
                            <div class="subs-price">
                                <span class="subs-price-value theme-color"><span class="d-block">TOTAL:</span>$0.00</span>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        </div>
                        <div class="custom-pagination mt-15">
                            <nav aria-label="Page navigation example">
                              <ul class="pagination">
                                <li class="page-item"><a class="page-link" href="#">Previous</a></li>
                                <li class="page-item"><a class="page-link" href="#">1</a></li>
                                <li class="page-item"><a class="page-link" href="#">2</a></li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item"><a class="page-link" href="#">Next</a></li>
                              </ul>
                            </nav>
                        </div>
                    </div>
                </div>
	
@endsection