@extends('front.dashboard_layout.app')

@section('content')

	<div class="col-xl-6 col-lg-6 col-md-12">
		<div class="main-center-data">
			<h3 class="display-username">Profile Setup</h3>
			<div class="informative-block bg-white round-crn pd-20-30 mt-15 hover-effect-box">
				<h4 class="font-weight-bold">User Info</h4>
				<span class="edit-details"><img src="{{asset('dashboard/img/edit.svg')}}" alt="edit" /></span>
				<table class="table-responsive mt-15">
					<tbody>
						<tr>
							<td class="label-mute">Name</td>
							<td>{{auth()->user()->name}}</td>
						</tr>
						<tr>
							<td class="label-mute">Email</td>
							<td>{{auth()->user()->email}}</td>
						</tr>
						<tr>
							<td class="label-mute">Phone</td>
							<td>{{auth()->user()->phone}}</td>
						</tr>
						<tr>
							<td class="label-mute">DOB</td>
							<td>{{date('m-d-Y',strtotime(auth()->user()->dob))}}</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="informative-block bg-white round-crn pd-20-30 mt-15 hover-effect-box">
				<h4 class="font-weight-bold">Shipping Info</h4>
				<span class="edit-details"><img src="{{asset('dashboard/img/edit.svg')}}" alt="edit" /></span>
				<table class="table-responsive mt-15">
					<tbody>
						<tr>
							<td class="label-mute">Address</td>
							<td>{{auth()->user()->address}}</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="informative-block bg-white round-crn pd-20-30 mt-15 hover-effect-box">
				<h4 class="font-weight-bold">Password</h4>
				<span class="edit-details"><img src="{{asset('dashboard/img/edit.svg')}}" alt="edit" /></span>
				<table class="table-responsive mt-15">
					<tbody>
						<tr>
							<td class="label-mute">Password</td>
							<td>*********</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	
	<div class="col-xl-3 col-lg-3 col-md-12">
		<div class="informative-block bg-white round-crn pd-20-30 settings hover-effect-box">
			<h4 class="font-weight-bold">Settings</h4>
			<div class="stwich-toggle">
				<div class="custom-control custom-switch">
				  <input type="checkbox" class="custom-control-input" id="customSwitch1">
				  <label class="custom-control-label" for="customSwitch1"></label>
				</div>
			</div>
			<h6 class="label-mute mt-15">SMS Notifications</h6>
			<p class="note m-0">By turning on this toggle, I agree to receive texts from RMD and/or {{auth()->user()->name}} to {{auth()->user()->phone}} that might be considered marketing and that may be sent using an auto dialer. I understand that agreeing is not required to purchase.</p>
		</div>
	</div>	
	
	
@endsection