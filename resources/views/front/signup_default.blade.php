@extends('front.online_visit_layout.app')

@section('content')
	
	<section class="sign_in_sec_wrapper">
		<div class="sign_in_sec shadow">
			<img src="{{asset('assets/images/rmd_logo_red.svg')}}" alt="rmd_logo_red">
			<h1 class="title">Sign up to your RMD platform account to continue.</h1>


			<form id="signupForm">
				<div class="form-group">
					<input type="email" name="email" class="form-control" placeholder="Email" id="email" required="required" autofocus>
				</div>
				<div class="input_wrapper">
					<div class="form-group">
						<input type="text" name="first_name" class="form-control" placeholder="First Name" id="first_name" required="required">
					</div>
					<div class="form-group">
						<input type="text" name="last_name" class="form-control" placeholder="Last Name" id="last_name" required="required">
					</div>
				</div>
				<div class="input_wrapper">
					<div class="form-group">
						<input type="password" name="password" class="form-control" placeholder="Password" id="password" required="required">
					</div>
					<div class="form-group">
						<input type="password" name="password_confirmation" class="form-control" placeholder="Confirm Password" id="confirm_password" required="required">
					</div>
				</div>
				<div class="agree_with_terms_box">
					<div class="checkbox d-inline-block">
						<input type="checkbox" id="checkbox2" name="agree" value="1">
						<label for="checkbox2"><span></span></label>
					</div>
					<span>I agree to <a href="{{env('APP_URL')}}/terms-conditions" class="terms">terms</a> and <a href="{{env('APP_URL')}}/privacy-policy">privacy policy</a> and consent to <a href="#">telehealth</a></span>
				</div>
				<button type="submit" id="saveBtn" class="btn btn-primary">Submit</button>
				<p class="mb-0">Already a member? <a href="{{env('APP_URL')}}/sign_in" class="login_link link">Log in and continue</a></p>
			</form>
		</div>
	</section>		
	
	<script>
		
			
		$("#signupForm").submit(function(e) {
			e.preventDefault();
			$(".loader").css('display', 'flex');
			
			$.ajax({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				data: $('#signupForm').serialize(),
				url: "{{ route('signup_submission') }}",
				type: "POST",
				// dataType: 'json',
				success: function (response) {
	
					if(response['status'] == 'success'){
						swal({
							title: response['message'],
							icon: 'success'
						});

						setTimeout(function(){
							$(".loader").css('display', 'none');
							swal.close();
							$('#signupForm').trigger("reset");
							window.location = response['redirect'];
						}, 2000);
					}
				},
				error: function (data) {
					$(".loader").css('display', 'none');
					let errors = data.responseJSON.errors;
					
					$.each(errors, function(key, value) {
						errorAlert(value[0],5000,'bottom-left');
					});					
					$('#saveBtn').html('Submit');
				}
			});
		});		
	</script>	
@endsection