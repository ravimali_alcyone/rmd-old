<!DOCTYPE HTML>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	@if(isset($meta_description))<meta name="description" content="{{$meta_description}}">@endif
    <title>{{ config('app.name') }} @if(isset($meta_title)) - {{$meta_title}} @endif</title>
	<meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="{{asset('assets/images/favicon.png')}}" rel="shortcut icon" type="image/x-icon">
    <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css" type="text/css" rel="stylesheet">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;600;700&display=swap">
    <link href="{{asset('assets/css/main.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/owl.carousel.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/responsive.css')}}" rel="stylesheet" />
	<link rel="stylesheet" href="{{asset('plugins/sweetalert/sweetalert.css')}}" />
	<link href="{{asset('assets/css/select2.min.css')}}" rel="stylesheet" />
	<link href="{{asset('assets/css/jquery-toaster.css')}}" rel="stylesheet" />
	
	<script src="{{asset('assets/js/jquery.min.js')}}"></script>
	<script src="{{asset('assets/js/custom.js')}}"></script>
	<script src="{{asset('plugins/sweetalert/sweetalert.min.js')}}"></script>
	<script src="{{asset('assets/js/select2.min.js')}}"></script>	
	<script src="{{asset('assets/js/jquery-toaster.min.js')}}"></script>	
</head>

<body id="page-top">
    <header>
        <!-- Navigation -->
        <div id="mainNav" class="<?php if($page == 'home'){echo 'fixed-top';}else{echo 'head_bg';}?>">
            <nav class="navbar navbar-expand-lg ">
                <div class="container">
                    <a class="navbar-brand js-scroll-trigger" href="{{env('APP_URL')}}#page-top"><img src="{{asset('assets/images/rmd_logo_red.svg')}}" class="logo logo_red" alt="ReplenishMD"/><img src="{{asset('assets/images/rmd_logo_white.svg')}}" class="logo logo_white" alt="ReplenishMD"/></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                      	<span class="fa fa-bars"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarResponsive">
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item">
                                <a class="nav-link js-scroll-trigger" href="{{env('APP_URL')}}/plans">Plans</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link js-scroll-trigger" href="{{env('APP_URL')}}/how_it_works">How it works</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link js-scroll-trigger" href="{{env('APP_URL')}}/about">About us</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link js-scroll-trigger" href="#">Good Stuff</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link js-scroll-trigger" href="#">Shop</a>
                            </li>
                            <li class="nav-item">
                                <a class="js-scroll-trigger btn-primary" href="{{route('sign_in')}}">Sign In</a>
                            </li>
                            <li class="nav-item">
                                <a class="js-scroll-trigger blue_btn" href="{{route('signup')}}">Join Now</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </div>
    </header>