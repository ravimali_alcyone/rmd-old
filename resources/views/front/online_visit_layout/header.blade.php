<!DOCTYPE HTML>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	@if(isset($meta_description))<meta name="description" content="{{$meta_description}}">@endif
    <title>{{ config('app.name') }} @if(isset($meta_title)) - {{$meta_title}} @endif</title>
	<meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="{{asset('assets/images/favicon.png')}}" rel="shortcut icon" type="image/x-icon">
    <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css" type="text/css" rel="stylesheet">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;600;700&display=swap">
    <link href="{{asset('assets/css/main.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/owl.carousel.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/responsive.css')}}" rel="stylesheet" />
	<link rel="stylesheet" href="{{asset('plugins/sweetalert/sweetalert.css')}}" />
	<link href="{{asset('assets/css/select2.min.css')}}" rel="stylesheet" />
	<link href="{{asset('assets/css/jquery-toaster.css')}}" rel="stylesheet" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css">
	
	<script src="{{asset('assets/js/jquery.min.js')}}"></script>
	<script src="{{asset('plugins/sweetalert/sweetalert.min.js')}}"></script>
	<script src="{{asset('assets/js/select2.min.js')}}"></script>	
	<script src="{{asset('assets/js/jquery-toaster.min.js')}}"></script>
    <script src="{{asset('assets/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('assets/js/jquery.easing.min.js')}}"></script>
    <script src="{{asset('assets/js/owl.carousel.js')}}" type="text/javascript"></script>	
	<script src="{{asset('assets/js/custom.js')}}"></script>	
</head>

<body id="page-top">
    <!-- Loader -->
    <div class="loader" style="display:none;">
        <img src="{{asset('assets/images/loading.gif')}}" alt="loading">
    </div>
	
    <header class="sign_up_header shadow">
        <div class="wrapper">
            <div>
                <a href="{{env('APP_URL')}}"><img src="{{asset('assets/images/rmd_logo_red.svg')}}" alt="Red Logo RMD"></a>
            </div>
            <div>
                <a href="#" class="blue_btn">Help?</a>
            </div>
        </div>
    </header>		

