@extends('front.online_visit_layout.app')

@section('content')
    <section class="sign_in_sec_wrapper lost_pass">
        <div class="sign_in_sec shadow">
            <img src="{{asset('assets/images/rmd_logo_red.svg')}}" alt="rmd_logo_red">
            <h1 class="title">Enter your email to reset your password.</h1>
            <form id="forgotPassForm">
                <div class="form-group">
                    <input type="email" class="form-control" placeholder="Email" id="email">
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
            <a href="{{ route('sign_in') }}" class="link">Sign In</a>
        </div>
    </section>
	
	<script>
		$("#forgotPassForm").submit(function(e) {
			e.preventDefault();
			$(".loader").css('display', 'flex');
			
			$.ajax({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				data: $('#forgotPassForm').serialize(),
				url: "{{ route('signin_submission') }}",
				type: "POST",
				// dataType: 'json',
				success: function (response) {
	
					if(response['status'] == 'success'){
						swal({
							title: response['message'],
							icon: 'success'
						});

						setTimeout(function(){
							$(".loader").css('display', 'none');
							swal.close();
							$('#forgotPassForm').trigger("reset");
							//window.location = "{{ route('online_visit_welcome') }}";
						}, 2000);
					}
				},
				error: function (data) {
					$(".loader").css('display', 'none');
					let errors = data.responseJSON.errors;
					
					$.each(errors, function(key, value) {
						errorAlert(value[0],5000,'bottom-left');
					});					
/* 					swal({
						title: 'Error Occured.',
						icon: 'error'
					}) */
				}
			});
		});		
	</script>	
	
@endsection