<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	@if(isset($meta_description))<meta name="description" content="{{$meta_description}}">@endif
    <title>{{ config('app.name') }} @if(isset($meta_title)) - {{$meta_title}} @endif</title>
    <link href="{{asset('assets/images/favicon.png')}}" rel="shortcut icon" type="image/x-icon">
	<meta name="csrf-token" content="{{ csrf_token() }}">	
    <!-- Google Font -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,200;0,300;0,500;0,600;0,700;0,800;1,200;1,300;1,400;1,500;1,600;1,700;1,800&display=swap" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <!-- Bootstrap CSS -->
	<link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <!-- Style.css -->
    <link rel="stylesheet" type="text/css" href="{{asset('dashboard/css/style.css')}}">

  </head>
  <body class="pb-70">
    
    <!-- Navbar -->
    <nav class="navbar navbar-expand-lg custom-navbar">
        <div class="container-fluid">
          <a class="navbar-brand" href="{{env('APP_URL')}}"><img src="{{asset('dashboard/img/logo.svg')}}" alt="logo"></a>
          <div class="show-user-mobile">
              <a class="profile-icon-dropdown" data-toggle="collapse" href="#showprofiledw" role="button" aria-expanded="false" aria-controls="showprofiledw">
			@if(auth()->user()->image)			  
			  <img src="{{env('APP_URL')}}/{{auth()->user()->image}}" alt="{{auth()->user()->name}}"/> 
			@else
				<img src="{{asset('dashboard/img/user-profile.jpg')}}" alt="{{auth()->user()->name}}"/> 
			@endif
			  {{auth()->user()->name}}</a>
                <div class="collapse" id="showprofiledw">
                    <ul class="m-0 p-0">
                        <li>
                            <a href="#">
                                <img src="{{asset('dashboard/img/draw.svg')}}" alt="left-nav-icons">
                                Edit profile
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src="{{asset('dashboard/img/settings.svg')}}" alt="left-nav-icons">
                                Settings
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src="{{asset('dashboard/img/logout.svg')}}" alt="left-nav-icons">
                                Logout
                            </a>
                        </li>
                    </ul>
                </div>
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"><i class="fa fa-bars" aria-hidden="true"></i></span>
          </button>

          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <form class="form-inline my-2 my-lg-0">
              <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
              <button class="btn btn-outline-success my-2 my-sm-0" type="submit"><img src="{{asset('dashboard/img/search.svg')}}" alt="search-icon"/></button>
            </form>
            <ul class="navbar-nav ml-auto">
              <li class="nav-item active">
                <a class="nav-link" href="{{route('user.dashboard')}}"><img src="{{asset('dashboard/img/home.svg')}}" alt="header-nav-icon"> Home</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#"><img src="{{asset('dashboard/img/users.svg')}}" alt="header-nav-icon"> Network</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#"><img src="{{asset('dashboard/img/chat.svg')}}" alt="header-nav-icon"> Messaging</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#"><img src="{{asset('dashboard/img/notification.svg')}}" alt="header-nav-icon"> Notifications</a>
              </li>
              <li class="nav-item">				
              <a class="profile-icon-dropdown" data-toggle="collapse" href="#showprofiledw" role="button" aria-expanded="false" aria-controls="showprofiledw">
			@if(auth()->user()->image)			  
			  <img src="{{env('APP_URL')}}/{{auth()->user()->image}}" alt="{{auth()->user()->name}}"/> 
			@else
				<img src="{{env('APP_URL')}}/dist/images/user_icon.png" alt="{{auth()->user()->name}}"/> 
			@endif
			  {{auth()->user()->name}}</a>

			  
                <div class="collapse" id="showprofiledw">
                    <ul class="m-0 p-0">
                    	<li>
	                		<a href="{{route('user.profile')}}">
                                <img src="{{asset('dashboard/img/draw.svg')}}" alt="left-nav-icons">
                                Edit profile
                            </a>
                    	</li>
                    	<li>
                    		<a href="#">
                                <img src="{{asset('dashboard/img/settings.svg')}}" alt="left-nav-icons">
                                Settings
                            </a>
                    	</li>
                    	<li>
                    		<a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                <img src="{{asset('dashboard/img/logout.svg')}}" alt="left-nav-icons">
                                Logout
                            </a>
							<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">@csrf</form>
                    	</li>
                    </ul>
                </div>
              </li>
            </ul>
          </div>
      </div>
    </nav>

    <!-- Navbar Ends -->
	
	<!-- Main Section Side Bar -->
	
    <section class="main-dashboard mt-30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-3 col-lg-3 col-md-12">
                    <div class="left-nav">
                        <ul class="p-0 m-0">
                            <li>
                                <a href="{{route('user.dashboard')}}" <?php if(isset($page) && $page == 'user_dashboard'){ echo 'class="active"';}?> >
                                    <img src="{{asset('dashboard/img/home-2.svg')}}" alt="left-nav-icons"/>
                                    Dashboard
                                </a>
                            </li>
                            <li>
                                <a href="{{route('user.profile')}}" <?php if(isset($page) && $page == 'user_profile'){ echo 'class="active"';}?> >
                                    <img src="{{asset('dashboard/img/user2.svg')}}" alt="left-nav-icons"/>
                                    My Profile
                                </a>
                            </li>
                            <li>
                                <a href="{{route('user.visit_records')}}" <?php if(isset($page) && $page == 'user_visit_records'){ echo 'class="active"';}?> >
                                    <img src="{{asset('dashboard/img/bag.svg')}}" alt="left-nav-icons"/>
                                    My Visits Records
                                </a>
                            </li>
                            <li>
                                <a href="{{route('user.subscriptions')}}" <?php if(isset($page) && $page == 'user_subscriptions'){ echo 'class="active"';}?> >
                                    <img src="{{asset('dashboard/img/send.svg')}}" alt="left-nav-icons"/>
                                    My Subscriptions
                                </a>
                            </li>
                            <li>
                                <a href="{{route('user.orders')}}" <?php if(isset($page) && $page == 'user_orders'){ echo 'class="active"';}?> >
                                    <img src="{{asset('dashboard/img/order.svg')}}" alt="left-nav-icons"/>
                                    Orders
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <img src="{{asset('dashboard/img/calender.svg')}}" alt="left-nav-icons"/>
                                    Appointments
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <img src="{{asset('dashboard/img/chat2.svg')}}" alt="left-nav-icons"/>
                                    Messages <span class="notification-no">2</span>
                                </a>
                            </li>
                        </ul>
                        <h4>Community</h4>
                        <ul class="p-0 m-0">
                            <li>
                                <a href="#">
                                    <img src="{{asset('dashboard/img/doctor.svg')}}" alt="left-nav-icons"/>
                                    Doctor Consult
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <img src="{{asset('dashboard/img/ask.svg')}}" alt="left-nav-icons"/>
                                    Quick Q & A
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>	