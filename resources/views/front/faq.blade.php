@extends('front.layouts.app')

@section('content')
    <section class="inner_banner faqs_banner">
        <div class="blue_bg_overlay">
            <div class="container">
                <div class="content_wrapper">
                    <div class="b_text text-center">
                        <h1>Hello, how we can help?</h1>
                        <div class="search_box">
                            <div class="treat_selection">
                                <span class="fa"></span>
                                <input type="search" name="search" placeholder="Ask a question..." class="form-control">
                            </div>
                            <button type="submit" class="btn btn-primary">Search</button>
                        </div>
                        <p>or choose a category to quickly find the help you need</p>
					@if($categories && $categories->count() > 0)
                        <div class="btns_wrapper faqs_btns_wrapper">
					<?php $i=0;?>
						@foreach($categories as $category)
                            <button class="btn <?php if($i == 0){echo 'active';}?>" data="faq_category_{{$category->id}}">{{$category->name}}</button>
					<?php $i++;?>
						@endforeach
                        </div>
					@endif
                    </div>
                </div>
            </div>
            <img src="/assets/images/bottom_curve.svg" alt="bottom_curve">
        </div>
    </section>

    <section class="faqs_wrapper">
        <div class="container">
	@if($faqs && $faqs->count() > 0)
		<?php $k=1;?>
		@foreach($faqs as $faq)
            <section id="faq_category_{{$faq->category_id}}" class="<?php if($k == 1){echo 'mt-0';}?>">
                <h1 class="title">{{$faq->category}}</h1>
                <div class="faqs">
                    <div id="accordion_{{$faq->category_id}}">
				@if($faq && $faq->faq_data != '')
					@php
						$faq_rows = json_decode($faq->faq_data, true);
					@endphp

					@if($faq_rows)
						<?php $x = 1;?>
						@foreach($faq_rows as $row)						
                        <div class="card">
                            <div class="card-header">
                                <a class="<?php if($k == 1 && $x == 1){ }else{ echo 'collapsed';}?> card-link" data-toggle="collapse" href="#collapse_category_{{$faq->category_id}}_{{$x}}"><span class="blue_dot"></span> {{$row['question']}}</a>
                            </div>
                            <div id="collapse_category_{{$faq->category_id}}_{{$x}}" class="collapse <?php if($k == 1 && $x == 1){echo 'show';}?>" data-parent="#accordion_{{$faq->category_id}}">
                                <div class="card-body">
                                    {{$row['answer']}}
                                </div>
                            </div>
                        </div>
						<?php $x++;?>
						
						@endforeach
					@endif
				@endif
                    </div>
                </div>
            </section>
			<?php $k++;?>
		@endforeach
	@endif
        </div>
    </section>

    <section class="oac plans_oac faqs_q">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="title">You still have a question?
                        <p>If you cannot find answer to your question in our FAQ, you can always contact us. we will answer to you shortly!</p>
                    </div>
                    <div class="boxes_wrapper d_flex_j_center">
                        <div class="box">
                            <span class="icon"><img src="/assets/images/phone_call_icon.png" alt="phone_call_icon"></span>
                            <h6 class="title text-center"><a href="tel:+ 800-000-0-0021" class="text-dark">+ 800-000-0-0021</a></h6>
                            <p class="text-center">We are always happy to help.</p>
                        </div>
                        <div class="box">
                            <span class="icon"><img src="/assets/images/icon_envelope_outline.svg" alt="icon_envelope_outline"></span>
                            <h6 class="title text-center"><a href="mailto:support@rmd.com" class="text-dark">support@rmd.com</a></h6>
                            <p class="text-center">The best way to get answer faster.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

<script>
        $(document).ready(function() {
            $(".faqs_btns_wrapper button").on("click", function() {
                let id = $(this).attr("data");
                $(".faqs_btns_wrapper button").removeClass("active");
                $(this).addClass("active");
                window.location.href = "#" + id;

                $('body,html').animate({
                    scrollTop: $('#' + id).offset().top - 230
                }, 300);
            });
        });	
</script>
@endsection