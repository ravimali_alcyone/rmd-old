@extends('front.layouts.app')

@section('content')
    <section class="hero">
        <div class="hero_img_bg">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="hero_wrap">
                            <div class="hero_text">
                                <h1>Look and feel your best</h1>
                                <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et.</p>
                                <a class="blue_btn" href="#" id="get_started_btn">Get Started Today</a>
                            </div>
                            <div class="hero_slider">
                                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                                    <ol class="carousel-indicators">
                                        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                                        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                                        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                                    </ol>
                                    <div class="carousel-inner">
                                        <div class="carousel-item active">
                                            <img src="/assets/images/slide1.png" class="d-block w-100" alt="...">
                                        </div>
                                        <div class="carousel-item">
                                            <img src="/assets/images/why_rmd_1.png" class="d-block w-100" alt="...">
                                        </div>
                                        <div class="carousel-item">
                                            <img src="/assets/images/why_rmd_2.png" class="d-block w-100" alt="...">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="search_home" id="consult">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="title">Start Your Treatment Consultation</div>

                    <form action="/treatment" method="GET">
                        <div class="sh_form">
                            <div class="gender_box">
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="customRadioInline1" name="g" class="custom-control-input" value="male" onchange="get_services(this)" checked>
                                    <label class="custom-control-label" for="customRadioInline1">Male</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="customRadioInline2" name="g" class="custom-control-input" value="female" onchange="get_services(this)">
                                    <label class="custom-control-label" for="customRadioInline2">Female</label>
                                </div>
                            </div>
                            <div class="treat_selection">
                                <select id="services" class="custom-select select2" name="t" required>
									<option value="" selected>Select consult type...</option>
							@if($services && $services->count() > 0)
								@foreach($services as $service)	
									<option value="{{$service->id}}">{{$service->name}}</option>
								@endforeach
							@endif
								</select>
                            </div>
							{{ csrf_field() }}
                            <button type="submit" class="btn blue_btn">Search</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </section>
    
    <div class="why_rmd why_rmd_2">
        <div class="title">Why ReplenishMD?
            <p>This is the reason why you should have join us.</p>
        </div>
        <div class="step_wrap">
            <div class="step_block">
                <div class="step_img">
                    <div class="box ml-0 mt-0">
                        <div class="img_box">
                            <img src="./assets/images/why_rmd_1.png" alt="why_rmd_1" class="w_rmd_img">
                            <img src="./assets/images/orange.png" alt="Orange" class="ob_img frt_img">
                        </div>
                        <h1 class="title">Concierge Services</h1>
                        <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et sadipscing elitr, sed diam nonumy eirmod.</p>
                    </div>
                    <div class="box">
                        <div class="img_box">
                            <img src="./assets/images/why_rmd_2.png" alt="why_rmd_2" class="w_rmd_img">
                            <img src="./assets/images/apples.png" alt="Apples" class="ob_img frt_img">
                        </div>
                        <h1 class="title">Virtual Visits</h1>
                        <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et sadipscing elitr, sed diam nonumy eirmod.</p>
                    </div>
                </div>
            </div>
            <div class="step_block step_block_2">
                <div class="step_img step_img_2">
                    <div class="box ml-0 mt-0">
                        <div class="img_box">
                            <img src="./assets/images/why_rmd_3.png" alt="why_rmd_3" class="w_rmd_img">
                            <img src="./assets/images/straberry.png" alt="Straberry" class="ob_img strawberry frt_img">
                        </div>
                        <h1 class="title">Join Our Community</h1>
                        <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et sadipscing elitr, sed diam nonumy eirmod.</p>
                    </div>
                    <div class="box">
                        <div class="img_box">
                            <img src="./assets/images/why_rmd_4.png" alt="why_rmd_4" class="w_rmd_img">
                            <img src="./assets/images/blueberry.png" alt="Blueberry" class="ob_img blueberry frt_img">
                        </div>
                        <h1 class="title">Much More</h1>
                        <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et sadipscing elitr, sed diam nonumy eirmod.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    

    <div class="our_member_exp">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="title">Latest Experiences Shared by Our Members
                        <p>Lorem ipsum dolor sit amet, consetetur</p>
                    </div>
                    <div class="owl-carousel exp_carousel">
                        <div class="item">
                            <div class="hv_box">
                                <div class="video_frame"><img class="img_play" src="/assets/images/play_icon.svg" alt=""><img class="img-fluid" src="/assets/images/vdo1.png" alt=""></div>
                                <div class="vdo_info">Dainel Stoke, <span>Jan 2020</span></div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="hv_box">
                                <div class="video_frame"><img class="img_play" src="/assets/images/play_icon.svg" alt=""><img class="img-fluid" src="/assets/images/vdo2.png" alt=""></div>
                                <div class="vdo_info">Mariya Antiya, <span>Feb 2020</span></div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="hv_box">
                                <div class="video_frame"><img class="img_play" src="/assets/images/play_icon.svg" alt=""><img class="img-fluid" src="/assets/images/vdo3.png" alt=""></div>
                                <div class="vdo_info">Anaya Smith, <span>Jun 2020</span></div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="hv_box">
                                <div class="video_frame"><iframe width="100%" height="310" src="https://www.youtube.com/embed/EngW7tLk6R8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
                                <div class="vdo_info">Dainel Stoke, <span>Jan 2020</span></div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="hv_box">
                                <div class="video_frame"><img class="img_play" src="/assets/images/play_icon.svg" alt=""><img class="img-fluid" src="/assets/images/vdo1.png" alt=""></div>
                                <div class="vdo_info">Dainel Stoke, <span>Jan 2020</span></div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="hv_box">
                                <div class="video_frame"><img class="img_play" src="/assets/images/play_icon.svg" alt=""><img class="img-fluid" src="/assets/images/vdo2.png" alt=""></div>
                                <div class="vdo_info">Mariya Antiya, <span>Feb 2020</span></div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="hv_box">
                                <div class="video_frame"><img class="img_play" src="/assets/images/play_icon.svg" alt=""><img class="img-fluid" src="/assets/images/vdo3.png" alt=""></div>
                                <div class="vdo_info">Anaya Smith, <span>Jun 2020</span></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <section class="oac">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="title">Our Amazing Community
                        <p>ReplenishMD is more than telemedicine site. We are a community of people who want to look and feel as best as we can.</p>
                    </div>
                    <div class="oac_list">
                        <div class="oac_box">
                            <div class="oacb_icon"><img src="./assets/images/icon1.svg" class="img-fluid" alt=""></div>
                            <div class="oac_detail">
                                <h4>Concierge services</h4>
                                <p>Whether it is a Botox party, a weight loss consultation or testosterone replacement therapy consult, our providers can come to you where ever you are.</p>
                            </div>
                        </div>
                        <div class="oac_box">
                            <div class="oacb_icon"><img src="./assets/images/icon2.svg" class="img-fluid" alt=""></div>
                            <div class="oac_detail">
                                <h4>Get quick and Easy Treatment Options</h4>
                                <p>We've created a host of treatments that members and soon to be members desire. We've made the process to get treated streamlined and more convenient - just for you.</p>
                            </div>
                        </div>
                        <div class="oac_box">
                            <div class="oacb_icon"><img src="./assets/images/icon3.svg" class="img-fluid" alt=""></div>
                            <div class="oac_detail">
                                <h4>Get Support from Our Community</h4>
                                <p>We have a q&a section where you can ask licensed providers questions about treatments and conditions. We also have a forum for added support.</p>
                            </div>
                        </div>
                        <div class="oac_box">
                            <div class="oacb_icon"><img src="./assets/images/icon1.svg" class="img-fluid" alt=""></div>
                            <div class="oac_detail">
                                <h4>Contests/ Challenges</h4>
                                <p>We have a q&a section where you can ask licensed providers questions about treatments and conditions. We also have a forum for added support.</p>
                            </div>
                        </div>
                    </div>
                    <div class="w-100"><a class="btn-primary" href="#">Learn More</a></div>
                </div>
            </div>
        </div>
    </section>
	
	@if($testimonials && $testimonials->count() > 0)
    <section class="client_talk">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="title">Here's What Our Members Have To Say</div>
                    <div class="owl-carousel talks">
					@foreach($testimonials as $testimonial)
                        <div class="item_box">
                            <div class="tk_b">
                                <p>{{$testimonial->comments}}</p>
                                <div class="rate_by">
                                    <div class="reviewr_img"><img src="/{{$testimonial->image}}" class="img-fluid" alt="{{$testimonial->name}}"></div>
                                    <div class="reviewer_info">
                                        <ul class="rating">
											<?php if($testimonial->star_rating){
												for($i=1;$i<=$testimonial->star_rating;$i++){?>
                                            <li><i class="fas fa-star"></i></li>
											<?php } }?>
                                        </ul>
                                        <div class="reviewer_name">{{$testimonial->name}}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
					@endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
	@endif
@if($providers)	
    <section class="experts">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="title">Meet The Experts</div>
                    <div class="owl-carousel expert_caro">
					@foreach($providers as $provider)
                        <div class="item_mem">
                            <div class="exprt_box">
                                <div class="exprt_img"><img src="{{$provider['image']}}" class="img-fluid" alt="{{$provider['name']}}"></div>
                                <div class="eprt_info">
                                    <strong>{{$provider['name']}}</strong>
                                    <p>{{$provider['provider_categories']}}</p>
                                    <a class="vb_link" href="#">View full bio <i class="fa fa-long-arrow-alt-right"></i></a>
                                </div>
                            </div>
                        </div>
					@endforeach	
                    </div>
                    <div class="w-100"><a class="btn-primary" href="{{env('APP_URL')}}/providers">Meet Our Experts</a></div>
                </div>
            </div>
        </div>
    </section>
@endif	
    <section class="h_news">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="title mb-4">Latest from Our Stories</div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="blog_box">
                        <div class="blog_img"><img src="./assets/images/img1.png" class="img-fluid" alt=""></div>
                        <div class="blog_info">
                            <strong>Category Title</strong>
                            <a href="#">Lorem ipsum dolor sit amet, consetetur sadips cing elitr, sed diam nonumy eirmodaccusam.</a>
                            <p>Lorem ipsum dolor sit amet, consetetur sadips cing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="blog_box">
                        <div class="blog_img"><img src="./assets/images/img2.png" class="img-fluid" alt=""></div>
                        <div class="blog_info">
                            <strong>Category Title</strong>
                            <a href="#">Lorem ipsum dolor sit amet, consetetur sadips cing elitr, sed diam nonumy eirmodaccusam.</a>
                            <p>Lorem ipsum dolor sit amet, consetetur sadips cing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="news_list">
                        <div class="news_box">
                            <a href="#">Lorem ipsum dolor sit amet, consetetur sadips cing elitr, sed diam.</a>
                            <div class="n_post">
                                <div class="n_date"><i class="fa fa-calendar-check"></i> 12 August 2020</div>
                                <div class="n_count"><i class="fa fa-comments"></i> 15 comments</div>
                            </div>
                        </div>
                        <div class="news_box">
                            <a href="#">Lorem ipsum dolor sit amet, consetetur sadips cing elitr, sed diam.</a>
                            <div class="n_post">
                                <div class="n_date"><i class="fa fa-calendar-check"></i> 12 August 2020</div>
                                <div class="n_count"><i class="fa fa-comments"></i> 15 comments</div>
                            </div>
                        </div>
                        <div class="news_box">
                            <a href="#">Lorem ipsum dolor sit amet, consetetur sadips cing elitr, sed diam.</a>
                            <div class="n_post">
                                <div class="n_date"><i class="fa fa-calendar-check"></i> 12 August 2020</div>
                                <div class="n_count"><i class="fa fa-comments"></i> 15 comments</div>
                            </div>
                        </div>
                        <div class="news_box">
                            <a href="#">Lorem ipsum dolor sit amet, consetetur sadips cing elitr, sed diam.</a>
                            <div class="n_post">
                                <div class="n_date"><i class="fa fa-calendar-check"></i> 12 August 2020</div>
                                <div class="n_count"><i class="fa fa-comments"></i> 15 comments</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
			
		@if($sponsors && $sponsors->count() > 0)
            <div class="row">
                <div class="col">
                    <div class="title mb-4 mt-5 pt-5">Our Sponsors</div>
                    <div class="owl-carousel sponsers_caro">
					@foreach($sponsors as $sponsor)
                        <div class="sponser_img"><img src="/{{$sponsor->image}}" class="img-fluid" title="{{$sponsor->name}}" alt="{{$sponsor->name}}"></div>
					@endforeach
                    </div>
                </div>
            </div>
		@endif
        </div>
    </section>	
@foreach ($errors->all() as $error)
  <div class="text-danger">{{ $error }}</div>
	<script>
		var msg = "{{$error}}";
		errorAlert(msg,8000,'bottom-left');
	</script>	  
@endforeach	

<script>
	function get_services(e){
		var gender = $(e).val();
		
		$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			data: {'gender':gender},
			url: "{{ route('get_services') }}",
			type: "POST",
			// dataType: 'json',
			success: function (response) {
				$("#services").html(response);
				$("#services").select2();
			}
		});		
	}
</script>
@endsection
