@extends('front.online_visit_layout.app')

@section('content')

    <div class="tabs_wrapper welcome basics m_qs">
        <div>
            <a href="javascript:void(0);" class="how_it_works">Treatment Preference</a>
            <div class="steps">
                <span class="active ml-0"></span>
                <span class="active"></span>
                <span class="active"></span>
                <span></span>
            </div>
        </div>
        <span id="pageBackBtn" class="back_arrow fa fas fa-arrow-left" onclick="location.href = '{{ route('medical_questions') }}';"></span>
        <span id="blockBackBtn" class="back_arrow fa fas fa-arrow-left" onclick="goPrev()" style="display:none;"></span>		
    </div>

    <div class="main_welcome_wrapper">
	
        <section id="welcome_section" class="welcome_content_wrapper basic medical_qs">
            <div class="medical_qs_welcome">
                <h3 class="heading">Treatment Preference</h3>
                <p class="description text-center">If you have a preferred treatment, you can let your doctor know here. While your preference will be taken into consideration, your doctor will prescribe the treatment option best suited for you.</p>
                <div class="img_wrapper text-center">
                    <img src="{{asset('assets/images/treatment_preferemce.svg')}}" alt="treatment_preferemce" class="img-fluid">
                </div>
                <button type="button" class="btn btn-primary" id="welc_btn">Continue</button>
            </div>
        </section>

		<section id="medical_uses" style="display:none;">
			<div class="container number_uses">

				<div class="num_uses">
					<p>Number of Uses</p>
					<h5>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</h5>
					<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>

					<div class="select_box">
						<div class="select_details">
							<div class="check_1">
								<input type="radio" id="usage_4" name="radio-group">
								<label for="usage_4">Use <span>4 times</span> per month </label>
							</div>
							<div class="roman_pic">
								<img src="{{asset('assets/images/iop.svg')}}" alt="">

							</div>
						</div>
					</div>

					<div class="select_box">
						<div class="select_details">
							<div class="check_1">
								<input type="radio" id="usage_6" name="radio-group">
								<label for="usage_6">Use <span>6 times</span> per month </label>
							</div>
							<div class="roman_pic">
								<img src="{{asset('assets/images/iop_2.svg')}}" alt="">

							</div>
						</div>
					</div>


					<div class="select_box">
						<div class="select_details">
							<div class="check_1">
								<input type="radio" id="usage_8" name="radio-group">
								<label for="usage_8">Use <span>8 times</span> per month </label>
							</div>
							<div class="roman_pic">
								<img src="{{asset('assets/images/iop_3.svg')}}" alt="">

							</div>
						</div>
					</div>

					<div class="select_box">
						<div class="select_details">
							<div class="check_1">
								<input type="radio" id="usage_10" name="radio-group">
								<label for="usage_10">Use <span>10 times</span> per month </label>
							</div>
							<div class="roman_pic">
								<img src="{{asset('assets/images/iop_4.svg')}}" alt="">

							</div>
						</div>
					</div>
					
				</div>
				
			</div>
		</section>   

        <section id="medical_drugs" style="display:none;">
            <div class="container number_uses">

                <div class="num_uses">
                    <p>Drugs</p>
                    <h5>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</h5>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>

                    <div class="select_box">
                        <div class="drug_details">
                            <div class="check_1">
                                <input type="radio" id="drugs_1" name="radio-group" >
                                <label for="drugs_1"><span>Sildenafil</span></label>
                                <div class="drug_p">
                                    <Span>$6</Span>
                                    <p>per dose</p>
                                </div>
                            </div>

                            <div class="roman_pic">
                                <img src="{{asset('assets/images/sildenafil.svg')}}" alt="">

                            </div>
                        </div>
                    </div>

                    <div class="select_box">
                        <div class="drug_details">
                            <div class="check_1">
                                <input type="radio" id="drugs_2" name="radio-group" >
                                <label for="drugs_2"><span>Viagra</span> </label>
                                <div class="drug_p">
                                    <Span>$34</Span>
                                    <p>per dose</p>
                                </div>

                            </div>
                            <div class="roman_pic">
                                <img src="{{asset('assets/images/viagra.svg')}}" alt="">

                            </div>
                        </div>
                    </div>


                    <div class="select_box">
                        <div class="drug_details">
                            <div class="check_1">
                                <input type="radio" id="drugs_3" name="radio-group" >
                                <label for="drugs_3"> 
                                <span>Cialis</span><br>
                             </label>
                                <div class="drug_p">
                                    <Span>$45</Span>
                                    <p>per dose</p>
                                </div>
                            </div>
                            <div class="roman_pic">
                                <img src="{{asset('assets/images/cialis.svg')}}" alt="">

                            </div>
                        </div>
                    </div>

                    <div class="select_box">
                        <a href="">
                            <div class="drug_details">
                                <div class="prefer">
                                    <p>No Preference</p>
                                    <p>Please tell my Doctor I do not have a drug preference</p>
                                </div>

                            </div>
                        </a>
                    </div>
                </div>
			</div>
        </section>
		
		<section id="medical_dosage" style="display:none;">
			<div class="container number_uses">

				<div class="num_uses">
					<p>DOSSAGE</p>
					<h5>Do You have any dosage preference for Sildenafil?</h5>

					<div class="dosage_12 dosage_options">
						<p>New to medication? start with:</p>
						<div class="dosage_details">
							<div class="dosage_select">
								<div class="check_1">
									<input type="radio" id="dosages_1" name="radio-group" >
									<label for="dosages_1"><span>60 mg</span> </label>
									<p>3 pills</p>

								</div>
								<div class="drug_p ml-auto">
									<Span>$34</Span>
									<p>per dose</p>
								</div>
							</div>

							<div class="dosage_intro">
								<div>
									<i class="fas fa-lightbulb"></i>
									<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing </p>
								</div>

							</div>


						</div>
					</div>

					<div class="dosage_13 dosage_options">
						<h5>If you know what works for you:</h5>
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
						<div class="dosage_details">
							<div class="dosage_select">
								<div class="check_1">
									<input type="radio" id="dosages_2" name="radio-group" >
									<label for="dosages_2"><span>20 mg</span> </label>
									<p>1 pills</p>

								</div>
								<div class="drug_p ml-auto">
									<Span>$2</Span>
									<p>per dose</p>
								</div>
							</div>
						</div>
						<div class="dosage_details">
							<div class="dosage_select">
								<div class="check_1">
									<input type="radio" id="dosages_3" name="radio-group" >
									<label for="dosages_3"><span>40 mg</span> </label>
									<p>2 pills</p>

								</div>
								<div class="drug_p ml-auto">
									<Span>$8</Span>
									<p>per dose</p>
								</div>
							</div>
						</div>
						<div class="dosage_details">
							<div class="dosage_select">
								<div class="check_1">
									<input type="radio" id="dosages_4" name="radio-group" >
									<label for="dosages_4"><span>80 mg</span> </label>
									<p>4 pills</p>

								</div>
								<div class="drug_p ml-auto">
									<Span>$8</Span>
									<p>per dose</p>
								</div>
							</div>
						</div>
						<div class="dosage_details">
							<div class="dosage_select">
								<div class="check_1">
									<input type="radio" id="dosages_5" name="radio-group" >
									<label for="dosages_5"><span>100 mg</span> </label>
									<p>5 pills</p>

								</div>
								<div class="drug_p ml-auto">
									<Span>$10</Span>
									<p>per dose</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>		
			
		<section id="take_photo" style="display:none;">
			<div class="container pic">

				<div class="cam_pic">
					<P class="para_1">STEP 1 0F 2</P>
					<h5>Share a photo of yourself</h5>
					<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
					<p><a href="">How does this work?</a></p>

					<div class="face_1">
						<img class="face_img" src="{{asset('assets/images/face.svg')}}" alt="">
					</div>

					<button type="button" class="pic_up btn btn-primary" id="photo_submit">Take or Upload Photo</button>

				</div>
			</div>
		</section>
		
		<section id="take_id_card" style="display:none;">
			<div class="container pic">

				<div class="cam_pic">
					<P class="para_1">STEP 2 0F 2</P>
					<h5>Let's add your ID</h5>
					<p>Your doctor or nurse pratitioner will use your photo to verify your identity. This helps us confirm that you're really you.</p>
					<p><a href="#">How does this work?</a></p>

					<div class="face_1">
						<img class="face_img" src="{{asset('assets/images/face.svg')}}" alt="">
					</div>

					<button type="button" class="pic_up btn btn-primary" id="id_card_submit">Take or Upload ID Photo</button>

				</div>
			</div>
		</section>		

		<section id="shipping_info" style="display:none;">
			<div class="container ship">

				<div class="ship_info">
					<h5>Shipping Information</h5>
					<div class="shipping_box">
						<div class="ship_img">
							<img src="{{asset('assets/images/Shipping.png')}}" alt="">
						</div>
						<div class="our_phar">
							<h5>Our Pharmacy</h5>
							<div class="sbox_1">
								<i class="fa fa-check" aria-hidden="true"></i>
								<p>FREE 2 Day Shipping</p>
							</div>
							<div class="sbox_1">
								<i class="fa fa-check" aria-hidden="true"></i>
								<p>Discreet Packaging</p>
							</div>
							<div class="sbox_1">
								<i class="fa fa-check" aria-hidden="true"></i>
								<p>Guarenteed Pricing</p>
							</div>
							<div class="sbox_1">
								<i class="fa fa-check" aria-hidden="true"></i>
								<p>Non-childproof, single dose-packaging</p>
							</div>
						</div>

						<div class="ship_d">
							<p><a href="">Select different pharmacy</a></p>
						</div>
					</div>
					<h5>Shipping Address</h5>
					<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
					<div class="ship_add">
						<input type="text" name="Shipping address" placeholder="Shipping Address">
					</div>


					<button type="button" class="save_b btn btn-primary" id="shipping_submit">Save and Continue</button>
				</div>

			</div>
		</section>
		
		<section id="treatment_review" style="display:none;">
			<div class="container number_uses">

				<div class="num_uses">

					<h5>Review treatment and pay</h5>
					<p>Your Shipping frequency</p>
					<div class="review_box">
						<h6>Quaterly</h6>
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum </p>
					</div>

					<div class="review_box_1">
						<h6>Your plan</h6>
						<div class="sil_review">
							<div class="sil_details">
								<div class="sil_details_img">
									<img src="{{asset('assets/images/viagra.svg')}}" alt="">

								</div>
								<div class="sil_details_intro">
									<h5>Sildenafil</h5>
									<p>20mg</p>
									<p>6 * month ($2 Ea.)</p>
									<p>3 Month Supply</p>
									<p>Edit</p>
								</div>

							</div>
							<div class="sil_details_price ml-auto">
								<p><span>$35</span>/3mon</p>

							</div>


						</div>

						<div class="sil_review">

							<div class="order_pro">
								<h5>First Order promo</h5>

							</div>

							<div class="order_pro ml-auto">
								<span>-$15</span>

							</div>
						</div>


						<div class="sil_review">

							<div class="order_pro">
								<h5>Healthcare professional review</h5>
								<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum </p>

							</div>

							<div class="order_pro ml-auto">
								<span>free</span>

							</div>
						</div>

						<div class="sil_review">

							<div class="order_pro">
								<h5>2 Day Shipping</h5>

							</div>

							<div class="order_pro ml-auto">
								<span>free</span>

							</div>
						</div>

						<div class="sil_review_1">

							<div class="order_pro_v">
								<h5>Due Today</h5>
								<p> <a href="">What am I Changed</a></p>
							</div>

							<div class="order_pro ml-auto">
								<h6>$0</h6>

							</div>
						</div>
					</div>

					<h5>Payments</h5>
					<div class="review_box">
						<div class="check_1">
							<input type="radio" id="payment_card" name="radio-group" checked="">
							<label for="payment_card"><span>Credit or Debit card</span></label>
							<i class="fas fa-lock"></i>
						</div>

					</div>
					<div class="review_box">
						<div class="check_1">
							<input type="radio" id="payment_paypal" name="radio-group" checked="">
							<label for="payment_paypal"><img src="{{asset('assets/images/paypal-logo.svg')}}" alt=""></label>

						</div>

					</div>
					
				</div>
			</div>

		</section>
	
    </div>
    
    <script>
	
	var bSec = '';
	var cSec = '';
	
        $(document).ready(function() {
            $("#welc_btn").click(function() {
                $("#welcome_section").hide();
                $("#medical_uses").show();
				bSec = 'welcome_section';
				cSec = 'medical_uses';
				$('#blockBackBtn').show();
				$('#pageBackBtn').hide();				
            });
        });
		
		
		$("#medical_uses .select_box .select_details input[type=radio]").click(function(){
			
			if ($(this).prop("checked")) {
				$("#medical_uses").hide();
				$("#medical_drugs").show();
				bSec = 'medical_uses';
				cSec = 'medical_drugs';
			}
		});
		
		
		$("#medical_drugs .select_box .drug_details input[type=radio]").click(function(){
			
			if ($(this).prop("checked")) {
				$("#medical_drugs").hide();
				$("#medical_dosage").show();
				bSec = 'medical_drugs';
				cSec = 'medical_dosage';
			}
		});


		$("#medical_dosage .dosage_options .dosage_details input[type=radio]").click(function(){
			
			if ($(this).prop("checked")) {
				$("#medical_dosage").hide();
				$("#take_photo").show();
				bSec = 'medical_dosage';
				cSec = 'take_photo';
			}
		});	
		
		$("#photo_submit").click(function(){
			
			$("#take_photo").hide();
			$("#take_id_card").show();
			bSec = 'take_photo';
			cSec = 'take_id_card';
			
		});

		$("#id_card_submit").click(function(){
			
			$("#take_id_card").hide();
			$("#shipping_info").show();
			bSec = 'take_id_card';
			cSec = 'shipping_info';
			
		});		

		$("#shipping_submit").click(function(){
			
			$("#shipping_info").hide();
			$("#treatment_review").show();
			bSec = 'shipping_info';
			cSec = 'treatment_review';
			
		});
		
		
		function goPrev(){
			if(bSec != '' && cSec != ''){
				
				console.log('bSec',bSec);
				console.log('cSec',cSec);
						
				$('#'+bSec).show();
				$('#'+cSec).hide();
				
				if(bSec == 'welcome_section' && cSec == 'medical_uses'){
					$('#blockBackBtn').hide();
					$('#pageBackBtn').show();
					cSec = 'welcome_section';
					bSec = '';
				}else{
					$('#blockBackBtn').show();
					$('#pageBackBtn').hide();
					let new_btd = $('#'+bSec).prev('section').css({"display": "none"}).attr('id');
					cSec = bSec;
					bSec = new_btd;
				}
					console.log('new current div',cSec);
					console.log('new back to div',bSec);				
			}
		}
    </script>	

@endsection