@extends('layouts.register')

@section('content')
                <!-- BEGIN: Register Info -->
                <div class="hidden xl:flex flex-col min-h-screen">
                    <a href="" class="-intro-x flex items-center pt-5">
                        <img alt="Midone Tailwind HTML Admin Template" class="w-6" src="{{asset('dist/images/logo.svg')}}">
                        <span class="text-white text-lg ml-3"> Replenish<span class="font-medium">MD</span> </span>
                    </a>
                    <div class="my-auto">
                        <img alt="Midone Tailwind HTML Admin Template" class="-intro-x w-1/2 -mt-16" src="{{asset('dist/images/illustration.svg')}}">
                        <div class="-intro-x text-white font-medium text-4xl leading-tight mt-10">
                            A few more clicks to 
                            <br>
                            sign up to your account.
                        </div>
                        <div class="-intro-x mt-5 text-lg text-white dark:text-gray-500">Manage all your accounts in one place</div>
                    </div>
                </div>
                <!-- END: Register Info -->
                <!-- BEGIN: Register Form -->
                <div class="h-screen xl:h-auto flex py-5 xl:py-0 my-10 xl:my-0">
                    <div class="my-auto mx-auto xl:ml-20 bg-white xl:bg-transparent px-5 sm:px-8 py-8 xl:p-0 rounded-md shadow-md xl:shadow-none w-full sm:w-3/4 lg:w-2/4 xl:w-auto">
                        <h2 class="intro-x font-bold text-2xl xl:text-3xl text-center xl:text-left">
                            Provider Sign Up
                        </h2>
                        <div class="intro-x mt-2 text-gray-500 dark:text-gray-500 xl:hidden text-center">A few more clicks to sign in to your account. Manage all your accounts in one place</div>
						
					@foreach ($errors->all() as $error)
						<div class="rounded-md flex items-center px-5 py-4 mb-2 bg-theme-30 text-theme-2"> <i data-feather="alert-octagon" class="w-6 h-6 mr-2"></i> 
							{{ $error }} 
						</div>
			  
					@endforeach	

					@if(Session::has('message'))
						<div class="rounded-md flex items-center px-5 py-4 mb-2 bg-theme-30 text-theme-2"> <i data-feather="alert-octagon" class="w-6 h-6 mr-2"></i> 
							{{ Session::get('message') }}
						</div>						
					@endif
				
						<form class="login-form" method="POST" action="{{ route('register') }}">	
							@csrf				
							<div class="intro-x mt-8">
								<input type="text" name="first_name" value="{{ old('first_name') }}" required class="intro-x login__input input input--lg border border-gray-300 block" placeholder="First Name">
								
								<input type="text" name="last_name" value="{{ old('last_name') }}" required class="intro-x login__input input input--lg border border-gray-300 block mt-4" placeholder="Last Name">
								
								<input id="email" type="email" name="email" value="{{ old('email') }}" required class="intro-x login__input input input--lg border border-gray-300 block mt-4" placeholder="Email">
								
								<input type="password" name="password" value="{{ old('password') }}" required class="intro-x login__input input input--lg border border-gray-300 block mt-4" placeholder="Password">
								
								<input type="password" name="password_confirmation" value="{{ old('password_confirmation') }}" required  class="intro-x login__input input input--lg border border-gray-300 block mt-4" placeholder="Password Confirmation">
								
							</div>
							<div class="intro-x flex items-center text-gray-700 dark:text-gray-600 mt-4 text-xs sm:text-sm">
								<input type="checkbox" class="input border mr-2" id="remember-me">
								<label class="cursor-pointer select-none" for="remember-me">I agree to the ReplenishMD</label>
								<a class="text-theme-1 dark:text-theme-10 ml-1" href="#">Privacy Policy</a>. 
							</div>
							<div class="intro-x mt-5 xl:mt-8 text-center xl:text-left">
								<button type="submit" class="button button--lg w-full xl:w-32 text-white bg-theme-1 xl:mr-3">Register</button>
								<a href="{{ route('login') }}" class="button button--lg w-full xl:w-32 text-gray-700 border border-gray-300 dark:border-dark-5 dark:text-gray-300 mt-3 xl:mt-0">Sign in</a>
							</div>
						</form>
                    </div>
                </div>
                <!-- END: Register Form -->
@endsection
