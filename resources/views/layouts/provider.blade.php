<!DOCTYPE html>
<html lang="en">
    <!-- BEGIN: Head -->
    <head>
        <meta charset="utf-8">
        <link href="{{asset('dist/images/fevicon.svg')}}" rel="shortcut icon">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="ReplenishMD provider ">
        <meta name="keywords" content="ReplenishMD provider">
        <meta name="author" content="Alcyone">
        <title>{{ config('app.name', 'Laravel') }} Provider</title>
		<meta name="csrf-token" content="{{ csrf_token() }}">

        <link rel="stylesheet" href="{{asset('dist/css/app.css')}}" />
        <link rel="stylesheet" href="{{asset('css/custom.css')}}" />
        <link rel="stylesheet" href="{{asset('plugins/sweetalert/sweetalert.css')}}" />

        <script src="{{ asset('js/jquery.min.js')}}"></script>
        <script src="{{asset('plugins/ckeditor/ckeditor.js')}}"></script>
   
    </head>
    <!-- END: Head -->
    <body class="app">
        <!-- BEGIN: Mobile Menu -->
        <div class="mobile-menu md:hidden">
            <div class="mobile-menu-bar">
                <a href="{{ route('provider.home') }}" class="flex mr-auto">
                    <img alt="ReplenishMD Provider" class="w-6" src="{{asset('dist/images/logo.svg')}}">
                </a>
                <a href="javascript:;" id="mobile-menu-toggler"> <i data-feather="bar-chart-2" class="w-8 h-8 text-white transform -rotate-90"></i> </a>
            </div>
            <ul class="border-t border-theme-24 py-5 hidden">
                <li>
                    <a href="{{ route('provider.home') }}" class="menu" <?php if(isset($page) && $page == 'dashboard'){ echo 'menu--active';}?>>
                        <div class="menu__icon"> <i data-feather="home"></i> </div>
                        <div class="menu__title"> Dashboard </div>
                    </a>
                </li>
                <li>
                    <a href="javascript:;" class="menu">
                        <div class="menu__icon"> <svg id="team" xmlns="http://www.w3.org/2000/svg" width="24" height="25.804" viewBox="0 0 24 25.804">
  <g id="Group_36" data-name="Group 36" transform="translate(8.992)">
    <g id="Group_35" data-name="Group 35">
      <path id="Path_23" data-name="Path 23" d="M187.008,0a3.008,3.008,0,1,0,3.008,3.008A3.008,3.008,0,0,0,187.008,0Zm0,5.157a2.149,2.149,0,1,1,2.149-2.149A2.149,2.149,0,0,1,187.008,5.157Z" transform="translate(-184)" fill="#fff"/>
    </g>
  </g>
  <g id="Group_38" data-name="Group 38" transform="translate(6.843 6.446)">
    <g id="Group_37" data-name="Group 37">
      <path id="Path_24" data-name="Path 24" d="M150.446,120h-2.578A3.872,3.872,0,0,0,144,123.868v7.306h1.719v8.165h.86v-9.884h-.86v.859h-.859v-6.446a3.012,3.012,0,0,1,3.008-3.008h2.578a3.012,3.012,0,0,1,3.008,3.008v6.446h-.86v-.859h-.859v9.884h.859v-8.165h1.719v-7.306A3.872,3.872,0,0,0,150.446,120Z" transform="translate(-144 -120)" fill="#fff"/>
    </g>
  </g>
  <g id="Group_40" data-name="Group 40" transform="translate(12 17.804)">
    <g id="Group_39" data-name="Group 39" transform="translate(0 0)">
      <rect id="Rectangle_34" data-name="Rectangle 34" height="8" fill="#fff"/>
    </g>
  </g>
  <g id="Group_42" data-name="Group 42" transform="translate(17.124 4.298)">
    <g id="Group_41" data-name="Group 41">
      <path id="Path_25" data-name="Path 25" d="M354.578,80a2.579,2.579,0,1,0,2.578,2.579A2.578,2.578,0,0,0,354.578,80Zm0,4.3a1.719,1.719,0,1,1,1.719-1.719A1.719,1.719,0,0,1,354.578,84.3Z" transform="translate(-352 -80)" fill="#fff"/>
    </g>
  </g>
  <g id="Group_44" data-name="Group 44" transform="translate(17.124 9.884)">
    <g id="Group_43" data-name="Group 43">
      <path id="Path_26" data-name="Path 26" d="M355.868,184H352v.859h3.868a2.151,2.151,0,0,1,2.149,2.149V192.6h-.86v-.859H356.3V199.9h.859v-6.446h1.719v-6.446A3.012,3.012,0,0,0,355.868,184Z" transform="translate(-352 -184)" fill="#fff"/>
    </g>
  </g>
  <g id="Group_46" data-name="Group 46" transform="translate(15.868 17.62)">
    <g id="Group_45" data-name="Group 45">
      <path id="Path_27" data-name="Path 27" d="M321.719,328v.859H320v.859h1.719v6.446h.859V328Z" transform="translate(-320 -328)" fill="#fff"/>
    </g>
  </g>
  <g id="Group_48" data-name="Group 48" transform="translate(19.107 19.804)">
    <g id="Group_47" data-name="Group 47" transform="translate(0 0)">
      <rect id="Rectangle_35" data-name="Rectangle 35" width="1" height="6" fill="#fff"/>
    </g>
  </g>
  <g id="Group_50" data-name="Group 50" transform="translate(1.719 4.298)">
    <g id="Group_49" data-name="Group 49">
      <path id="Path_28" data-name="Path 28" d="M34.579,80a2.579,2.579,0,1,0,2.579,2.579A2.578,2.578,0,0,0,34.579,80Zm0,4.3A1.719,1.719,0,1,1,36.3,82.579,1.719,1.719,0,0,1,34.579,84.3Z" transform="translate(-32 -80)" fill="#fff"/>
    </g>
  </g>
  <g id="Group_52" data-name="Group 52" transform="translate(0 9.884)">
    <g id="Group_51" data-name="Group 51">
      <path id="Path_29" data-name="Path 29" d="M3.008,184A3.012,3.012,0,0,0,0,187.008v6.446H1.719V199.9h.86v-8.165h-.86v.859H.86v-5.587a2.151,2.151,0,0,1,2.149-2.149H6.876V184Z" transform="translate(0 -184)" fill="#fff"/>
    </g>
  </g>
  <g id="Group_54" data-name="Group 54" transform="translate(5.554 17.62)">
    <g id="Group_53" data-name="Group 53">
      <path id="Path_30" data-name="Path 30" d="M112.859,328.859V328H112v8.165h.859v-6.446h1.719v-.859Z" transform="translate(-112 -328)" fill="#fff"/>
    </g>
  </g>
  <g id="Group_56" data-name="Group 56" transform="translate(3.892 19.804)">
    <g id="Group_55" data-name="Group 55" transform="translate(0 0)">
      <rect id="Rectangle_36" data-name="Rectangle 36" width="1" height="6" fill="#fff"/>
    </g>
  </g>
</svg> </div>
                        <div class="menu__title"> Visit Pool <i data-feather="chevron-down" class="menu__sub-icon"></i> </div>
                    </a>
                    <ul class="">				
                        <li>
                            <a href="#" class="menu online_visits <?php if(isset($page) && $page == 'online_visits'){ echo 'menu--active';}?>">
                                <div class="menu__icon"> <i data-feather="monitor"></i> </div>
                                <div class="menu__title"> Online Visits </div>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="menu concierge_visits <?php if(isset($page) && $page == 'online_visits'){ echo 'menu--active';}?>">
                                <div class="menu__icon"> <i data-feather="users"></i> </div>
                                <div class="menu__title"> Concierge Visits </div>
                            </a>
                        </li>
                    </ul>
                </li>				
                <li>
                    <a href="#" class="menu service_pricing" <?php if(isset($page) && $page == 'visit_forms'){ echo 'menu--active';}?>>
                        <div class="menu__icon"> <i data-feather="file-text"></i> </div>
                        <div class="menu__title"> My Visit Requests </div>
                    </a>
                </li>
                <li>
                    <a href="#" class="menu">
                        <div class="menu__icon"> <i data-feather="command"></i> </div>
                        <div class="menu__title"> Availability </div>
                    </a>
                </li>
                <li>
                    <a href="#" class="menu <?php if(isset($page) && $page == 'products'){ echo 'menu--active';}?>">
                        <div class="menu__icon"> <i data-feather="user"></i> </div>
                        <div class="menu__title"> My Account </div>
                    </a>
                </li>							
            </ul>
        </div>
        <!-- END: Mobile Menu -->
        <div class="flex">
            <!-- BEGIN: Side Menu -->
            <nav class="side-nav">
                <a href="{{ route('provider.home') }}" class="intro-x flex items-center pl-5 pt-4">
                    <img alt="ReplenishMD Provider" class="w-6" src="{{asset('dist/images/logo.svg')}}">
                    <span class="hidden xl:block text-white text-lg ml-3"> Replenish<span class="font-medium">MD</span> </span>
                </a>
                <div class="side-nav__devider my-6"></div>
                <ul>
                    <li>
                        <a href="{{ route('provider.home') }}" class="side-menu <?php if(isset($page) && $page == 'dashboard'){ echo 'side-menu--active';}?>">
                            <div class="side-menu__icon"> <i data-feather="home"></i> </div>
                            <div class="side-menu__title"> Dashboard </div>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:;" class="side-menu <?php if(isset($page) && ( $page == 'online_visits' || $page == 'concierge_visits' )){ echo 'side-menu--active';}?>">
                            <div class="side-menu__icon"> <svg id="team" xmlns="http://www.w3.org/2000/svg" width="24" height="25.804" viewBox="0 0 24 25.804">
  <g id="Group_36" data-name="Group 36" transform="translate(8.992)">
    <g id="Group_35" data-name="Group 35">
      <path id="Path_23" data-name="Path 23" d="M187.008,0a3.008,3.008,0,1,0,3.008,3.008A3.008,3.008,0,0,0,187.008,0Zm0,5.157a2.149,2.149,0,1,1,2.149-2.149A2.149,2.149,0,0,1,187.008,5.157Z" transform="translate(-184)" fill="#fff"/>
    </g>
  </g>
  <g id="Group_38" data-name="Group 38" transform="translate(6.843 6.446)">
    <g id="Group_37" data-name="Group 37">
      <path id="Path_24" data-name="Path 24" d="M150.446,120h-2.578A3.872,3.872,0,0,0,144,123.868v7.306h1.719v8.165h.86v-9.884h-.86v.859h-.859v-6.446a3.012,3.012,0,0,1,3.008-3.008h2.578a3.012,3.012,0,0,1,3.008,3.008v6.446h-.86v-.859h-.859v9.884h.859v-8.165h1.719v-7.306A3.872,3.872,0,0,0,150.446,120Z" transform="translate(-144 -120)" fill="#fff"/>
    </g>
  </g>
  <g id="Group_40" data-name="Group 40" transform="translate(12 17.804)">
    <g id="Group_39" data-name="Group 39" transform="translate(0 0)">
      <rect id="Rectangle_34" data-name="Rectangle 34" height="8" fill="#fff"/>
    </g>
  </g>
  <g id="Group_42" data-name="Group 42" transform="translate(17.124 4.298)">
    <g id="Group_41" data-name="Group 41">
      <path id="Path_25" data-name="Path 25" d="M354.578,80a2.579,2.579,0,1,0,2.578,2.579A2.578,2.578,0,0,0,354.578,80Zm0,4.3a1.719,1.719,0,1,1,1.719-1.719A1.719,1.719,0,0,1,354.578,84.3Z" transform="translate(-352 -80)" fill="#fff"/>
    </g>
  </g>
  <g id="Group_44" data-name="Group 44" transform="translate(17.124 9.884)">
    <g id="Group_43" data-name="Group 43">
      <path id="Path_26" data-name="Path 26" d="M355.868,184H352v.859h3.868a2.151,2.151,0,0,1,2.149,2.149V192.6h-.86v-.859H356.3V199.9h.859v-6.446h1.719v-6.446A3.012,3.012,0,0,0,355.868,184Z" transform="translate(-352 -184)" fill="#fff"/>
    </g>
  </g>
  <g id="Group_46" data-name="Group 46" transform="translate(15.868 17.62)">
    <g id="Group_45" data-name="Group 45">
      <path id="Path_27" data-name="Path 27" d="M321.719,328v.859H320v.859h1.719v6.446h.859V328Z" transform="translate(-320 -328)" fill="#fff"/>
    </g>
  </g>
  <g id="Group_48" data-name="Group 48" transform="translate(19.107 19.804)">
    <g id="Group_47" data-name="Group 47" transform="translate(0 0)">
      <rect id="Rectangle_35" data-name="Rectangle 35" width="1" height="6" fill="#fff"/>
    </g>
  </g>
  <g id="Group_50" data-name="Group 50" transform="translate(1.719 4.298)">
    <g id="Group_49" data-name="Group 49">
      <path id="Path_28" data-name="Path 28" d="M34.579,80a2.579,2.579,0,1,0,2.579,2.579A2.578,2.578,0,0,0,34.579,80Zm0,4.3A1.719,1.719,0,1,1,36.3,82.579,1.719,1.719,0,0,1,34.579,84.3Z" transform="translate(-32 -80)" fill="#fff"/>
    </g>
  </g>
  <g id="Group_52" data-name="Group 52" transform="translate(0 9.884)">
    <g id="Group_51" data-name="Group 51">
      <path id="Path_29" data-name="Path 29" d="M3.008,184A3.012,3.012,0,0,0,0,187.008v6.446H1.719V199.9h.86v-8.165h-.86v.859H.86v-5.587a2.151,2.151,0,0,1,2.149-2.149H6.876V184Z" transform="translate(0 -184)" fill="#fff"/>
    </g>
  </g>
  <g id="Group_54" data-name="Group 54" transform="translate(5.554 17.62)">
    <g id="Group_53" data-name="Group 53">
      <path id="Path_30" data-name="Path 30" d="M112.859,328.859V328H112v8.165h.859v-6.446h1.719v-.859Z" transform="translate(-112 -328)" fill="#fff"/>
    </g>
  </g>
  <g id="Group_56" data-name="Group 56" transform="translate(3.892 19.804)">
    <g id="Group_55" data-name="Group 55" transform="translate(0 0)">
      <rect id="Rectangle_36" data-name="Rectangle 36" width="1" height="6" fill="#fff"/>
    </g>
  </g>
</svg> </div>
                            <div class="side-menu__title"> Visit Pool <i data-feather="chevron-down" class="side-menu__sub-icon"></i> </div>
                        </a>
                        <ul class="<?php if(isset($page) && ( $page == 'online_visits' || $page == 'concierge_visits' )){ echo 'side-menu__sub-open';}?>">
                            <li>
                                <a href="#" class="side-menu <?php if(isset($page) && $page == 'online_visits'){ echo 'side-menu--active';}?>">
                                    <div class="side-menu__icon"> <i data-feather="monitor"></i> </div>
                                    <div class="side-menu__title"> Online Visits </div>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="side-menu  <?php if(isset($page) && $page == 'concierge_visits'){ echo 'side-menu--active';}?>">
                                    <div class="side-menu__icon"> <i data-feather="users"></i> </div>
                                    <div class="side-menu__title"> Concierge Visits </div>
                                </a>
                            </li>
                        </ul>
                    </li>	
                    <li>
                        <a href="#" class="side-menu <?php if(isset($page) && $page == 'visit_forms'){ echo 'side-menu--active';}?>">
                            <div class="side-menu__icon"> <i data-feather="file-text"></i> </div>
                            <div class="side-menu__title"> My Visit Requests </div>
                        </a>
                    </li>						
                    <li>
                        <a href="#" class="side-menu">
                            <div class="side-menu__icon"> <i data-feather="command"></i> </div>
                            <div class="side-menu__title"> Availability </div>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="side-menu <?php if(isset($page) && $page == 'products'){ echo 'side-menu--active';}?>">
                            <div class="side-menu__icon"> <i data-feather="user"></i> </div>
                            <div class="side-menu__title"> My Account </div>
                        </a>
                    </li>						
                </ul>
            </nav>
            <!-- END: Side Menu -->
            <!-- BEGIN: Content -->
            <div class="content">
                <!-- BEGIN: Top Bar -->
                <div class="top-bar">
                    <!-- BEGIN: Breadcrumb -->
                    <div class="-intro-x breadcrumb mr-auto hidden sm:flex"> <a href="#" class="">Application</a> <i data-feather="chevron-right" class="breadcrumb__icon"></i> <a href="#" class="breadcrumb--active">Dashboard</a> </div>
                    <!-- END: Breadcrumb -->
                    <!-- BEGIN: Search -->
                    <div class="intro-x relative mr-3 sm:mr-6">
                        <div class="search hidden sm:block">
                            <input type="text" class="search__input input placeholder-theme-13" placeholder="Search...">
                            <i data-feather="search" class="search__icon dark:text-gray-300"></i>
                        </div>
                        <a class="notification sm:hidden" href="#"> <i data-feather="search" class="notification__icon dark:text-gray-300"></i> </a>
                        <div class="search-result">
                            <div class="search-result__content">
                                <div class="search-result__content__title">Pages</div>
                                <div class="mb-5">
                                    <a href="#" class="flex items-center">
                                        <div class="w-8 h-8 bg-theme-18 text-theme-9 flex items-center justify-center rounded-full"> <i class="w-4 h-4" data-feather="inbox"></i> </div>
                                        <div class="ml-3">Mail Settings</div>
                                    </a>
                                    <a href="#" class="flex items-center mt-2">
                                        <div class="w-8 h-8 bg-theme-17 text-theme-11 flex items-center justify-center rounded-full"> <i class="w-4 h-4" data-feather="users"></i> </div>
                                        <div class="ml-3">Users & Permissions</div>
                                    </a>
                                    <a href="#" class="flex items-center mt-2">
                                        <div class="w-8 h-8 bg-theme-14 text-theme-10 flex items-center justify-center rounded-full"> <i class="w-4 h-4" data-feather="credit-card"></i> </div>
                                        <div class="ml-3">Transactions Report</div>
                                    </a>
                                </div>
                                <div class="search-result__content__title">Users</div>
                                <div class="mb-5">
                                    <a href="#" class="flex items-center mt-2">
                                        <div class="w-8 h-8 image-fit">
                                            <img alt="ReplenishMD" class="rounded-full" src="{{asset('dist/images/profile-5.jpg')}}">
                                        </div>
                                        <div class="ml-3">Nicolas Cage</div>
                                        <div class="ml-auto w-48 truncate text-gray-600 text-xs text-right">nicolascage@left4code.com</div>
                                    </a>
                                    <a href="#" class="flex items-center mt-2">
                                        <div class="w-8 h-8 image-fit">
                                            <img alt="ReplenishMD" class="rounded-full" src="{{asset('dist/images/profile-7.jpg')}}">
                                        </div>
                                        <div class="ml-3">Al Pacino</div>
                                        <div class="ml-auto w-48 truncate text-gray-600 text-xs text-right">alpacino@left4code.com</div>
                                    </a>
                                    <a href="#" class="flex items-center mt-2">
                                        <div class="w-8 h-8 image-fit">
                                            <img alt="ReplenishMD" class="rounded-full" src="{{asset('dist/images/profile-5.jpg')}}">
                                        </div>
                                        <div class="ml-3">Leonardo DiCaprio</div>
                                        <div class="ml-auto w-48 truncate text-gray-600 text-xs text-right">leonardodicaprio@left4code.com</div>
                                    </a>
                                    <a href="#" class="flex items-center mt-2">
                                        <div class="w-8 h-8 image-fit">
                                            <img alt="ReplenishMD" class="rounded-full" src="{{asset('dist/images/profile-10.jpg')}}">
                                        </div>
                                        <div class="ml-3">Robert De Niro</div>
                                        <div class="ml-auto w-48 truncate text-gray-600 text-xs text-right">robertdeniro@left4code.com</div>
                                    </a>
                                </div>
                                <div class="search-result__content__title">My Account</div>
                                <a href="#" class="flex items-center mt-2">
                                    <div class="w-8 h-8 image-fit">
                                        <img alt="ReplenishMD" class="rounded-full" src="{{asset('dist/images/preview-6.jpg')}}">
                                    </div>
                                    <div class="ml-3">Dell XPS 13</div>
                                    <div class="ml-auto w-48 truncate text-gray-600 text-xs text-right">PC &amp; Laptop</div>
                                </a>
                                <a href="#" class="flex items-center mt-2">
                                    <div class="w-8 h-8 image-fit">
                                        <img alt="ReplenishMD" class="rounded-full" src="{{asset('dist/images/preview-14.jpg')}}">
                                    </div>
                                    <div class="ml-3">Samsung Galaxy S20 Ultra</div>
                                    <div class="ml-auto w-48 truncate text-gray-600 text-xs text-right">Smartphone &amp; Tablet</div>
                                </a>
                                <a href="#" class="flex items-center mt-2">
                                    <div class="w-8 h-8 image-fit">
                                        <img alt="ReplenishMD" class="rounded-full" src="{{asset('dist/images/preview-8.jpg')}}">
                                    </div>
                                    <div class="ml-3">Samsung Galaxy S20 Ultra</div>
                                    <div class="ml-auto w-48 truncate text-gray-600 text-xs text-right">Smartphone &amp; Tablet</div>
                                </a>
                                <a href="#" class="flex items-center mt-2">
                                    <div class="w-8 h-8 image-fit">
                                        <img alt="ReplenishMD" class="rounded-full" src="{{asset('dist/images/preview-4.jpg')}}">
                                    </div>
                                    <div class="ml-3">Sony A7 III</div>
                                    <div class="ml-auto w-48 truncate text-gray-600 text-xs text-right">Photography</div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <!-- END: Search -->
                    <!-- BEGIN: Notifications -->
                    <div class="intro-x dropdown relative mr-auto sm:mr-6">
                        <div class="dropdown-toggle notification notification--bullet cursor-pointer"> <i data-feather="bell" class="notification__icon dark:text-gray-300"></i> </div>
                        <div class="notification-content dropdown-box mt-8 absolute top-0 left-0 sm:left-auto sm:right-0 z-20 -ml-10 sm:ml-0">
                            <div class="notification-content__box dropdown-box__content box dark:bg-dark-6">
                                <div class="notification-content__title">Notifications</div>
                                <div class="cursor-pointer relative flex items-center ">
                                    <div class="w-12 h-12 flex-none image-fit mr-1">
                                        <img alt="ReplenishMD" class="rounded-full" src="{{asset('dist/images/profile-5.jpg')}}">
                                        <div class="w-3 h-3 bg-theme-9 absolute right-0 bottom-0 rounded-full border-2 border-white"></div>
                                    </div>
                                    <div class="ml-2 overflow-hidden">
                                        <div class="flex items-center">
                                            <a href="javascript:;" class="font-medium truncate mr-5">Nicolas Cage</a>
                                            <div class="text-xs text-gray-500 ml-auto whitespace-no-wrap">05:09 AM</div>
                                        </div>
                                        <div class="w-full truncate text-gray-600">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem </div>
                                    </div>
                                </div>
                                <div class="cursor-pointer relative flex items-center mt-5">
                                    <div class="w-12 h-12 flex-none image-fit mr-1">
                                        <img alt="ReplenishMD" class="rounded-full" src="{{asset('dist/images/profile-7.jpg')}}">
                                        <div class="w-3 h-3 bg-theme-9 absolute right-0 bottom-0 rounded-full border-2 border-white"></div>
                                    </div>
                                    <div class="ml-2 overflow-hidden">
                                        <div class="flex items-center">
                                            <a href="javascript:;" class="font-medium truncate mr-5">Al Pacino</a>
                                            <div class="text-xs text-gray-500 ml-auto whitespace-no-wrap">01:10 PM</div>
                                        </div>
                                        <div class="w-full truncate text-gray-600">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 20</div>
                                    </div>
                                </div>
                                <div class="cursor-pointer relative flex items-center mt-5">
                                    <div class="w-12 h-12 flex-none image-fit mr-1">
                                        <img alt="ReplenishMD" class="rounded-full" src="{{asset('dist/images/profile-5.jpg')}}">
                                        <div class="w-3 h-3 bg-theme-9 absolute right-0 bottom-0 rounded-full border-2 border-white"></div>
                                    </div>
                                    <div class="ml-2 overflow-hidden">
                                        <div class="flex items-center">
                                            <a href="javascript:;" class="font-medium truncate mr-5">Leonardo DiCaprio</a>
                                            <div class="text-xs text-gray-500 ml-auto whitespace-no-wrap">01:10 PM</div>
                                        </div>
                                        <div class="w-full truncate text-gray-600">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 20</div>
                                    </div>
                                </div>
                                <div class="cursor-pointer relative flex items-center mt-5">
                                    <div class="w-12 h-12 flex-none image-fit mr-1">
                                        <img alt="ReplenishMD" class="rounded-full" src="{{asset('dist/images/profile-10.jpg')}}">
                                        <div class="w-3 h-3 bg-theme-9 absolute right-0 bottom-0 rounded-full border-2 border-white"></div>
                                    </div>
                                    <div class="ml-2 overflow-hidden">
                                        <div class="flex items-center">
                                            <a href="javascript:;" class="font-medium truncate mr-5">Robert De Niro</a>
                                            <div class="text-xs text-gray-500 ml-auto whitespace-no-wrap">01:10 PM</div>
                                        </div>
                                        <div class="w-full truncate text-gray-600">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomi</div>
                                    </div>
                                </div>
                                <div class="cursor-pointer relative flex items-center mt-5">
                                    <div class="w-12 h-12 flex-none image-fit mr-1">
                                        <img alt="ReplenishMD" class="rounded-full" src="{{asset('dist/images/profile-14.jpg')}}">
                                        <div class="w-3 h-3 bg-theme-9 absolute right-0 bottom-0 rounded-full border-2 border-white"></div>
                                    </div>
                                    <div class="ml-2 overflow-hidden">
                                        <div class="flex items-center">
                                            <a href="javascript:;" class="font-medium truncate mr-5">Al Pacino</a>
                                            <div class="text-xs text-gray-500 ml-auto whitespace-no-wrap">01:10 PM</div>
                                        </div>
                                        <div class="w-full truncate text-gray-600">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomi</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END: Notifications -->
                    <!-- BEGIN: Account Menu -->
                    <div class="intro-x dropdown w-8 h-8 relative">
                        <div class="dropdown-toggle w-8 h-8 rounded-full overflow-hidden shadow-lg image-fit zoom-in">
                            <img alt="ReplenishMD" src="{{asset('dist/images/profile-10.jpg')}}">
                        </div>
                        <div class="dropdown-box mt-10 absolute w-56 top-0 right-0 z-20">
                            <div class="dropdown-box__content box bg-theme-38 dark:bg-dark-6 text-white">
                                <div class="p-4 border-b border-theme-40 dark:border-dark-3">
								<?php if(auth()->user()){?>
								<div class="font-medium">{{auth()->user()->name}}</div>
                                    <div class="text-xs text-theme-41 dark:text-gray-600"><?php
									if(auth()->user()->category == '1'){
										echo 'Head Manager';
									}elseif(auth()->user()->category == '2'){
										echo 'Backend Engineer';
									}elseif(auth()->user()->category == '3'){
										echo 'Supervisor';
									}elseif(auth()->user()->category == '4'){
										echo 'Branch Manager';
									}
									?></div>
                                </div>
								<?php } ?>
                                <div class="p-2">
                                    <a href="{{ route('provider.profile') }}" class="flex items-center block p-2 transition duration-300 ease-in-out hover:bg-theme-1 dark:hover:bg-dark-3 rounded-md"> <i data-feather="user" class="w-4 h-4 mr-2"></i> Profile </a>
									<a href="#" class="flex items-center block p-2 transition duration-300 ease-in-out hover:bg-theme-1 dark:hover:bg-dark-3 rounded-md"> <i data-feather="settings" class="w-4 h-4 mr-2"></i> Settings </a>
                                    <a href="#" class="flex items-center block p-2 transition duration-300 ease-in-out hover:bg-theme-1 dark:hover:bg-dark-3 rounded-md"> <i data-feather="user-plus" class="w-4 h-4 mr-2"></i> Membership </a>
									 <a href="#" class="flex items-center block p-2 transition duration-300 ease-in-out hover:bg-theme-1 dark:hover:bg-dark-3 rounded-md"> <i data-feather="tablet" class="w-4 h-4 mr-2"></i> Advertising </a>

                                    <a href="#" class="flex items-center block p-2 transition duration-300 ease-in-out hover:bg-theme-1 dark:hover:bg-dark-3 rounded-md"> <i data-feather="help-circle" class="w-4 h-4 mr-2"></i> Help </a>
                                </div>
                                <div class="p-2 border-t border-theme-40 dark:border-dark-3">
                                    <a href="{{ route('logout') }}" class="flex items-center block p-2 transition duration-300 ease-in-out hover:bg-theme-1 dark:hover:bg-dark-3 rounded-md" onclick="event.preventDefault();document.getElementById('logout-form').submit();"> <i data-feather="toggle-right" class="w-4 h-4 mr-2"></i> Logout </a>
									<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">@csrf</form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END: Account Menu -->
                </div>
                <!-- END: Top Bar -->

				@yield('content')

            </div>
            <!-- END: Content -->
        </div>
        <!-- BEGIN: Dark Mode Switcher-->
        <div style="display:none;" class="dark-mode-switcher shadow-md fixed bottom-0 right-0 box dark:bg-dark-2 border rounded-full w-40 h-12 flex items-center justify-center z-50 mb-10 mr-10">
            <div class="mr-4 text-gray-700 dark:text-gray-300">Dark Mode</div>
            <input class="input input--switch border" type="checkbox" value="1">
        </div>
        <!-- END: Dark Mode Switcher-->
        <!-- BEGIN: JS Assets-->
        <script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAX8NmXxD-IKH5ZbMBhFCeKPQZs8vRL5wA&libraries=places"></script>

        <!-- BEGIN: JS Assets-->
        <script src="{{asset('dist/js/app.js')}}"></script>
        <script src="{{asset('plugins/sweetalert/sweetalert.min.js')}}"></script>
        <!-- END: JS Assets-->

    </body>
</html>
