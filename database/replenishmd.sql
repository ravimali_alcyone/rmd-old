-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 22, 2020 at 12:17 PM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `replenishmd`
--

-- --------------------------------------------------------

--
-- Table structure for table `all_plan_features`
--

CREATE TABLE `all_plan_features` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `price` float DEFAULT NULL,
  `detail` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `all_plan_features`
--

INSERT INTO `all_plan_features` (`id`, `name`, `price`, `detail`, `status`, `created_at`, `updated_at`) VALUES
(1, 'one month free membership', 0, '4 weeks(one month) access to membership benefits.', 1, '2020-12-21 18:30:00', '2020-12-22 05:38:35'),
(2, '12 months of care and membership', 0, '12 months of care and access to membership benefits.', 1, '2020-12-21 18:30:00', '2020-12-22 05:39:09'),
(3, '4 months of care and membership', 0, 'total months of care and access to membership benefits.', 1, '2020-12-21 18:30:00', '2020-12-22 05:39:18'),
(4, '1 doctor visit (in person or online)', 30, 'The average person spends 15 minutes  with a doctor. Our first visit is 75 minutes during which our doctors map your health biography, discuss your symptoms, and design a personalized health plan for you. After your initial visit, the first follow up is 60 minutes, all other follow ups are 30 minutes.', 1, '2020-12-21 18:30:00', '2020-12-21 18:30:00'),
(5, '2 doctor visit (in person or online)', 60, 'The average person spends 15 minutes with a doctor. Our first visit is 75 minutes during which our doctors map your health biography, discuss your symptoms, and design a personalized health plan for you. After your initial visit, the first follow up is 60 minutes, all other follow ups are 30 minutes.', 1, '2020-12-21 18:30:00', '2020-12-21 18:30:00'),
(6, '5 doctor visit (in person or online)', 150, 'The average person spends 15 minutes with a doctor. Our first visit is 75 minutes during which our doctors map your health biography, discuss your symptoms, and design a personalized health plan for you. After your initial visit, the first follow up is 60 minutes, all other follow ups are 30 minutes.', 1, '2020-12-21 18:30:00', '2020-12-21 18:30:00'),
(7, '10 doctor visits (in person or online)', 300, 'The average person spends 15 minutes  with a doctor. Our first visit is 75 minutes during which our doctors map your health biography, discuss your symptoms, and design a personalized health plan for you. After your initial visit, the first follow up is 60 minutes, all other follow ups are 30 minutes.', 1, '2020-12-21 18:30:00', '2020-12-21 18:30:00'),
(8, 'Unlimited messaging(Only Text)', 0, 'Includes only text message chat.', 1, '2020-12-21 18:30:00', '2020-12-21 18:30:00'),
(9, 'Unlimited messaging(Text & Video)', 50, 'Includes text, audio, video chat to direct providers', 1, '2020-12-21 18:30:00', '2020-12-21 18:30:00'),
(10, 'Introductory health plan', 0, 'Each plan includes an individualized plan for diagnostic testing, nutrition, fitness, mental health, medications, supplementation and coaching support.', 1, '2020-12-21 18:30:00', '2020-12-21 18:30:00'),
(11, 'Long-term health plan', 50, 'Each plan includes an individualized plan for diagnostic testing, nutrition, fitness, mental health, medications, supplementation and coaching support.', 1, '2020-12-21 18:30:00', '2020-12-21 18:30:00'),
(12, 'Targeted health plan', 20, 'Get access to our proprietary clinical expertise through a personalized health plan that’s designed to focus on improving your biggest health concern and acheive results.', 1, '2020-12-21 18:30:00', '2020-12-21 18:30:00'),
(13, 'Advanced testing available', 50, 'Our in-depth diagnostic panel looks at inflammation, hormones, nutrient status, heart health and more. Additional costs may apply depending on insurance.', 1, '2020-12-21 18:30:00', '2020-12-21 18:30:00');

-- --------------------------------------------------------

--
-- Table structure for table `blogs`
--

CREATE TABLE `blogs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(10) NOT NULL DEFAULT 0 COMMENT 'F.K. users table authors',
  `blog_category_id` int(10) NOT NULL DEFAULT 0 COMMENT 'F.K.(blog_categories) table',
  `blog_title` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `blog_content` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `blog_status` enum('published','submitted_for_publish','new','not_published','deleted','blocked') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'not_published',
  `is_featured` tinyint(1) NOT NULL DEFAULT 0,
  `is_monetize` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `blog_categories`
--

CREATE TABLE `blog_categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `parent_id` int(10) NOT NULL DEFAULT 0,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `blog_categories`
--

INSERT INTO `blog_categories` (`id`, `parent_id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 0, 'Gastrointestinal Health', 1, '2020-07-05 18:30:00', '2020-07-05 18:30:00'),
(2, 0, 'Cardiometabolic Health', 1, '2020-07-05 18:30:00', '2020-07-05 18:30:00'),
(3, 0, 'Immune Health', 1, '2020-07-05 18:30:00', '2020-07-05 18:30:00'),
(4, 0, 'Muscle, Bone & Joint Health', 1, '2020-07-05 18:30:00', '2020-07-05 18:30:00'),
(5, 0, 'Neurological Health', 1, '2020-07-05 18:30:00', '2020-07-05 18:30:00'),
(6, 0, 'Stress Management', 1, '2020-07-05 18:30:00', '2020-07-05 18:30:00'),
(7, 0, 'Metabolic Detoxification', 1, '2020-07-05 18:30:00', '2020-07-05 18:30:00'),
(8, 0, 'General Wellness', 1, '2020-07-05 18:30:00', '2020-07-05 18:30:00'),
(9, 0, 'Children\'s Health', 1, '2020-07-05 18:30:00', '2020-07-05 18:30:00'),
(10, 0, 'Medical Foods', 1, '2020-07-05 18:30:00', '2020-07-05 18:30:00');

-- --------------------------------------------------------

--
-- Table structure for table `blog_comments`
--

CREATE TABLE `blog_comments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(10) NOT NULL DEFAULT 0 COMMENT 'F.K. users table authors',
  `blog_id` int(10) NOT NULL DEFAULT 0 COMMENT 'F.K.(blogs) table',
  `comments` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `contact_us`
--

CREATE TABLE `contact_us` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_category` tinyint(1) NOT NULL DEFAULT 1 COMMENT '1=visitor, 2=provider, 3=patient',
  `subject` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `other` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `comments` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0=unread, 1= read',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contact_us`
--

INSERT INTO `contact_us` (`id`, `name`, `user_category`, `subject`, `other`, `email`, `phone`, `comments`, `status`, `created_at`, `updated_at`) VALUES
(1, 'John Doe', 1, 'Order Enquiry', NULL, 'john.d877@yopmail.com', '9809809801', 'My order is still not completed. please help', 0, '2020-08-07 18:30:00', '2020-08-08 05:41:35'),
(2, 'Robert Clark', 2, 'Login Issue', '', 'john.d877@yopmail.com', '9879879871', 'I can\'t login using my credentials and even I can\'t reset my password.', 0, '2020-08-07 18:30:00', '2020-08-07 18:30:00'),
(3, 'Rex Jackson', 1, 'Online Visit', NULL, 'rex.j9090@yopmail.com', '980980802', 'Testing', 1, '2020-09-15 06:32:06', '2020-09-15 06:32:06');

-- --------------------------------------------------------

--
-- Table structure for table `diagnosis_categories`
--

CREATE TABLE `diagnosis_categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `parent_id` int(10) NOT NULL DEFAULT 0,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `diagnosis_categories`
--

INSERT INTO `diagnosis_categories` (`id`, `parent_id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 0, 'Hypertension', 1, '2020-07-22 18:30:00', '2020-07-22 18:30:00'),
(2, 0, 'Hyperlipidemia', 1, '2020-07-22 18:30:00', '2020-07-22 18:30:00'),
(3, 0, 'Anxiety', 1, '2020-07-22 18:30:00', '2020-07-22 18:30:00'),
(4, 0, 'Back pain', 1, '2020-07-22 18:30:00', '2020-07-22 18:30:00'),
(5, 0, 'Urinary tract infection', 1, '2020-07-22 18:30:00', '2020-07-22 18:30:00'),
(6, 0, 'Obesity', 1, '2020-07-22 18:30:00', '2020-07-22 18:30:00'),
(7, 0, 'Allergic rhinitis', 1, '2020-07-22 18:30:00', '2020-07-22 18:30:00'),
(8, 0, 'Osteoarthritis', 1, '2020-07-22 18:30:00', '2020-07-22 18:30:00');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `faqs`
--

CREATE TABLE `faqs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category_id` int(10) NOT NULL DEFAULT 0,
  `sub_category_id` int(10) NOT NULL DEFAULT 0,
  `faq_data` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `faqs`
--

INSERT INTO `faqs` (`id`, `category_id`, `sub_category_id`, `faq_data`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 0, '[{\"question\":\"How much does a visit cost?\",\"answer\":\"Unfortunately we cannot accept returns of prescription products for reuse or resale, and all sales are final. However, if you feel we have made an error in the filling of your prescription.\"},{\"question\":\"How much does the medication cost?\",\"answer\":\"Unfortunately we cannot accept returns of prescription products for reuse or resale, and all sales are final. However, if you feel we have made an error in the filling of your prescription.\"},{\"question\":\"Do I need to be home to sign for my order?\",\"answer\":\"Unfortunately we cannot accept returns of prescription products for reuse or resale, and all sales are final. However, if you feel we have made an error in the filling of your prescription.\"},{\"question\":\"When will I receive my order?\",\"answer\":\"Unfortunately we cannot accept returns of prescription products for reuse or resale, and all sales are final. However, if you feel we have made an error in the filling of your prescription.\"},{\"question\":\"Is ReplenishMD covered by insurance?\",\"answer\":\"Unfortunately we cannot accept returns of prescription products for reuse or resale, and all sales are final. However, if you feel we have made an error in the filling of your prescription.\"}]', 1, '2020-07-22 08:03:28', '2020-09-15 02:22:25'),
(2, 2, 0, '[{\"question\":\"How much does a visit cost?\",\"answer\":\"Unfortunately we cannot accept returns of prescription products for reuse or resale, and all sales are final. However, if you feel we have made an error in the filling of your prescription.\"},{\"question\":\"How much does the medication cost?\",\"answer\":\"Unfortunately we cannot accept returns of prescription products for reuse or resale, and all sales are final. However, if you feel we have made an error in the filling of your prescription.\"},{\"question\":\"Do I need to be home to sign for my order?\",\"answer\":\"Unfortunately we cannot accept returns of prescription products for reuse or resale, and all sales are final. However, if you feel we have made an error in the filling of your prescription.\"},{\"question\":\"When will I receive my order?\",\"answer\":\"Unfortunately we cannot accept returns of prescription products for reuse or resale, and all sales are final. However, if you feel we have made an error in the filling of your prescription.\"},{\"question\":\"Is ReplenishMD covered by insurance?\",\"answer\":\"Unfortunately we cannot accept returns of prescription products for reuse or resale, and all sales are final. However, if you feel we have made an error in the filling of your prescription.\"}]', 1, '2020-07-22 08:04:11', '2020-09-15 02:25:39'),
(3, 3, 0, '[{\"question\":\"How much does a visit cost?\",\"answer\":\"Unfortunately we cannot accept returns of prescription products for reuse or resale, and all sales are final. However, if you feel we have made an error in the filling of your prescription.\"},{\"question\":\"How much does the medication cost?\",\"answer\":\"Unfortunately we cannot accept returns of prescription products for reuse or resale, and all sales are final. However, if you feel we have made an error in the filling of your prescription.\"},{\"question\":\"Do I need to be home to sign for my order?\",\"answer\":\"Unfortunately we cannot accept returns of prescription products for reuse or resale, and all sales are final. However, if you feel we have made an error in the filling of your prescription.\"},{\"question\":\"When will I receive my order?\",\"answer\":\"Unfortunately we cannot accept returns of prescription products for reuse or resale, and all sales are final. However, if you feel we have made an error in the filling of your prescription.\"},{\"question\":\"Is ReplenishMD covered by insurance?\",\"answer\":\"Unfortunately we cannot accept returns of prescription products for reuse or resale, and all sales are final. However, if you feel we have made an error in the filling of your prescription.\"}]', 1, '2020-07-23 02:11:54', '2020-09-15 01:35:27'),
(4, 4, 0, '[{\"question\":\"How much does a visit cost?\",\"answer\":\"Unfortunately we cannot accept returns of prescription products for reuse or resale, and all sales are final. However, if you feel we have made an error in the filling of your prescription.\"},{\"question\":\"How much does the medication cost?\",\"answer\":\"Unfortunately we cannot accept returns of prescription products for reuse or resale, and all sales are final. However, if you feel we have made an error in the filling of your prescription.\"},{\"question\":\"Do I need to be home to sign for my order?\",\"answer\":\"Unfortunately we cannot accept returns of prescription products for reuse or resale, and all sales are final. However, if you feel we have made an error in the filling of your prescription.\"},{\"question\":\"When will I receive my order?\",\"answer\":\"Unfortunately we cannot accept returns of prescription products for reuse or resale, and all sales are final. However, if you feel we have made an error in the filling of your prescription.\"},{\"question\":\"Is ReplenishMD covered by insurance?\",\"answer\":\"Unfortunately we cannot accept returns of prescription products for reuse or resale, and all sales are final. However, if you feel we have made an error in the filling of your prescription.\"}]', 1, '2020-07-23 02:13:55', '2020-07-23 02:15:54'),
(5, 5, 0, '[{\"question\":\"How much does a visit cost?\",\"answer\":\"Unfortunately we cannot accept returns of prescription products for reuse or resale, and all sales are final. However, if you feel we have made an error in the filling of your prescription.\"},{\"question\":\"How much does the medication cost?\",\"answer\":\"Unfortunately we cannot accept returns of prescription products for reuse or resale, and all sales are final. However, if you feel we have made an error in the filling of your prescription.\"},{\"question\":\"Do I need to be home to sign for my order?\",\"answer\":\"Unfortunately we cannot accept returns of prescription products for reuse or resale, and all sales are final. However, if you feel we have made an error in the filling of your prescription.\"},{\"question\":\"When will I receive my order?\",\"answer\":\"Unfortunately we cannot accept returns of prescription products for reuse or resale, and all sales are final. However, if you feel we have made an error in the filling of your prescription.\"},{\"question\":\"Is ReplenishMD covered by insurance?\",\"answer\":\"Unfortunately we cannot accept returns of prescription products for reuse or resale, and all sales are final. However, if you feel we have made an error in the filling of your prescription.\"}]', 1, '2020-07-23 02:13:55', '2020-07-23 02:15:54');

-- --------------------------------------------------------

--
-- Table structure for table `faq_categories`
--

CREATE TABLE `faq_categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `parent_id` int(10) NOT NULL DEFAULT 0,
  `category_type` tinyint(1) NOT NULL DEFAULT 1,
  `service_id` int(10) NOT NULL DEFAULT 0,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `faq_categories`
--

INSERT INTO `faq_categories` (`id`, `parent_id`, `category_type`, `service_id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 0, 1, 0, 'Get Started', 1, '2020-07-05 18:30:00', '2020-07-05 18:30:00'),
(2, 0, 1, 0, 'Pricing & Plans', 1, '2020-07-05 18:30:00', '2020-07-05 18:30:00'),
(3, 0, 1, 0, 'Sales Questions', 1, '2020-07-05 18:30:00', '2020-07-05 18:30:00'),
(4, 0, 1, 0, 'User Guide', 1, '2020-07-05 18:30:00', '2020-07-05 18:30:00'),
(5, 0, 1, 0, 'Informations', 1, '2020-07-05 18:30:00', '2020-07-05 18:30:00');

-- --------------------------------------------------------

--
-- Table structure for table `main_categories`
--

CREATE TABLE `main_categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `parent_id` int(10) NOT NULL DEFAULT 0,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `main_categories`
--

INSERT INTO `main_categories` (`id`, `parent_id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 0, 'Aesthetic Medicine', 1, '2020-07-20 18:30:00', '2020-07-20 18:30:00'),
(2, 0, 'Regenerative Medicine', 1, '2020-07-20 18:30:00', '2020-07-20 18:30:00'),
(3, 0, 'Integrative/Functional/Natural Medicine', 1, '2020-07-20 18:30:00', '2020-07-20 18:30:00'),
(4, 0, 'Sexual Wellness', 1, '2020-07-20 18:30:00', '2020-07-20 18:30:00'),
(5, 0, 'Addiction Medicine', 1, '2020-07-20 18:30:00', '2020-07-20 18:30:00'),
(6, 0, 'Emotional Wellness', 1, '2020-07-20 18:30:00', '2020-07-20 18:30:00'),
(7, 0, 'Physical Medicine', 1, '2020-07-20 18:30:00', '2020-07-20 18:30:00'),
(8, 0, 'General Medicine', 1, '2020-07-20 18:30:00', '2020-07-20 18:30:00'),
(9, 0, 'Nutrition', 1, '2020-07-20 18:30:00', '2020-07-20 18:30:00'),
(10, 0, 'Fitness', 1, '2020-07-20 18:30:00', '2020-07-20 18:30:00');

-- --------------------------------------------------------

--
-- Table structure for table `medicine_faqs`
--

CREATE TABLE `medicine_faqs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `medicine_id` int(10) NOT NULL DEFAULT 0,
  `question_data` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `answer_data` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `medicine_faqs`
--

INSERT INTO `medicine_faqs` (`id`, `medicine_id`, `question_data`, `answer_data`, `created_at`, `updated_at`) VALUES
(35, 1, 'Overview', '<p><img alt=\"\" src=\"http://replenishmd.local/images/muscle@2x_1606833237.webp\" style=\"height:200px; width:176px\" /></p>\r\n\r\n<p><strong>Sildenafil should be taken without food 30-60 minutes before sex and its effects last up to 6-8 hours.</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong><img alt=\"\" src=\"http://replenishmd.local/images/viagra_vitals_infographic_02_1606833264.webp\" style=\"height:178px; width:178px\" /></strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Serious side effects include an erection lasting more than 4 hours, sudden vision loss in one or both eyes, and sudden hearing decrease or loss. If these occur, seek medical attention immediately.</strong></p>', '2020-12-01 09:04:58', '2020-12-01 09:04:58'),
(36, 1, 'Question2', '<p>Answers2</p>', '2020-12-01 09:04:58', '2020-12-01 09:04:58'),
(37, 1, 'Questions3', '<p>Answers3</p>', '2020-12-01 09:04:58', '2020-12-01 09:04:58'),
(38, 1, 'Question4', '<p>Answers4</p>', '2020-12-01 09:04:58', '2020-12-01 09:04:58');

-- --------------------------------------------------------

--
-- Table structure for table `medicine_variants`
--

CREATE TABLE `medicine_variants` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `service_id` int(10) NOT NULL DEFAULT 0,
  `medicine_id` int(10) NOT NULL DEFAULT 0,
  `variant_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2019_08_19_000000_create_failed_jobs_table', 1),
(3, '2014_10_12_100000_create_password_resets_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `online_visits`
--

CREATE TABLE `online_visits` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `visit_type` enum('online','concierge','','') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'online',
  `patient_id` int(10) NOT NULL DEFAULT 0 COMMENT 'F.K. users table patients',
  `service_id` int(10) NOT NULL DEFAULT 0 COMMENT 'F.K.(services) table Treatment ooking for',
  `feedback_detail` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `visit_amount` float DEFAULT NULL,
  `visit_status` enum('auto_assigned','in_pool','not_assigned','picked','reviewed','contacted','report_submitted','being_evaluated','complete') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'in_pool',
  `payment_status` enum('null','pending','done','failed','refunded') COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_new` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `online_visits`
--

INSERT INTO `online_visits` (`id`, `visit_type`, `patient_id`, `service_id`, `feedback_detail`, `visit_amount`, `visit_status`, `payment_status`, `is_new`, `created_at`, `updated_at`) VALUES
(1, 'online', 4, 2, 'It is very good.', 100, 'report_submitted', 'done', 0, '2020-08-09 05:59:00', '2020-10-16 01:45:48'),
(2, 'concierge', 17, 5, 'I am feeling good now.', 50, 'reviewed', 'done', 0, '2020-08-10 09:43:08', '2020-12-14 00:47:37'),
(3, 'online', 40, 1, 'It is very good.', 100, 'contacted', 'done', 0, '2020-08-09 05:59:00', '2020-10-16 00:51:28'),
(4, 'concierge', 41, 9, 'I am feeling good now.', 50, 'in_pool', 'done', 0, '2020-08-10 09:43:08', '2020-12-14 00:47:37');

-- --------------------------------------------------------

--
-- Table structure for table `online_visit_provider_assign`
--

CREATE TABLE `online_visit_provider_assign` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `online_visit_id` int(10) NOT NULL DEFAULT 0 COMMENT 'F.K. online visits table',
  `provider_id` int(10) NOT NULL DEFAULT 0 COMMENT 'F.K. of providers(users table)',
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT 'Active/Inactive, differnt from visits status',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `online_visit_provider_assign`
--

INSERT INTO `online_visit_provider_assign` (`id`, `online_visit_id`, `provider_id`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 3, 1, '2020-08-09 05:59:00', '2020-08-09 05:59:00'),
(2, 2, 31, 1, '2020-08-10 09:43:08', '2020-08-10 05:59:00'),
(3, 3, 0, 1, '2020-08-09 05:59:00', '2020-08-09 05:59:00'),
(4, 4, 0, 1, '2020-08-10 09:43:08', '2020-08-10 05:59:00');

-- --------------------------------------------------------

--
-- Table structure for table `online_visit_status_timeline`
--

CREATE TABLE `online_visit_status_timeline` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `online_visit_id` int(10) NOT NULL DEFAULT 0 COMMENT 'F.K. online visits table',
  `old_visit_status` enum('auto_assigned','in_pool','not_assigned','picked','reviewed','contacted','report_submitted') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `new_visit_status` enum('auto_assigned','in_pool','not_assigned','picked','reviewed','contacted','report_submitted') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `online_visit_status_timeline`
--

INSERT INTO `online_visit_status_timeline` (`id`, `online_visit_id`, `old_visit_status`, `new_visit_status`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, 'in_pool', '2020-08-08 22:43:08', '2020-08-08 22:43:08'),
(2, 2, NULL, 'in_pool', '2020-08-08 22:43:08', '2020-08-08 22:43:08'),
(3, 3, NULL, 'in_pool', '2020-08-08 22:43:08', '2020-08-08 22:43:08'),
(4, 4, NULL, 'in_pool', '2020-08-08 22:43:08', '2020-08-08 22:43:08'),
(5, 1, 'in_pool', 'picked', '2020-08-18 04:07:14', '2020-08-18 04:07:14'),
(6, 2, 'in_pool', 'picked', '2020-08-18 04:08:34', '2020-08-18 04:08:34'),
(7, 1, 'picked', 'report_submitted', '2020-10-14 01:58:59', '2020-10-14 01:58:59'),
(8, 3, 'in_pool', 'contacted', '2020-10-14 01:59:07', '2020-10-14 01:59:07'),
(9, 2, 'picked', 'reviewed', '2020-10-16 00:43:26', '2020-10-16 00:43:26');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('admin@gmail.com', '$2y$10$ewPMDnWpbgwH3Nst3WbiQuap.3Tp.Sf95fNWTERg1/.oPZnybL/3S', '2020-07-04 03:37:45');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category` int(10) NOT NULL DEFAULT 0,
  `sub_category` int(10) NOT NULL DEFAULT 0,
  `sub_sub_category` int(10) DEFAULT 0,
  `images` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price_type` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price_value` float NOT NULL DEFAULT 0,
  `discount_price` float NOT NULL DEFAULT 0,
  `stock_status` enum('in','out','','') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'in',
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_info` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `stock_qty` int(10) NOT NULL DEFAULT 0,
  `manufacturer_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `is_direct_purchase` tinyint(1) NOT NULL DEFAULT 1,
  `safety_info` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `associate_problems` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `category`, `sub_category`, `sub_sub_category`, `images`, `price_type`, `price_value`, `discount_price`, `stock_status`, `description`, `details`, `product_info`, `stock_qty`, `manufacturer_name`, `is_direct_purchase`, `safety_info`, `associate_problems`, `status`, `created_at`, `updated_at`) VALUES
(20, 'PhytoMulti®', 3, 0, 0, '[\"uploads\\/products\\/1595855401.test2.jfif\",\"uploads\\/products\\/1595855401.test1.jfif\"]', 'dose', 60, 50, 'in', '<p><strong>PhytoMulti&reg;</strong>&nbsp;takes you beyond basic wellness support. It has a proprietary blend of 13 concentrated extracts and phytonutrients with scientifically tested biological activity to support cellular health and overall wellness.*</p>', '<p><strong>Other Ingredients:&nbsp;</strong>Microcrystalline cellulose, croscarmellose sodium, cellulose, stearic acid (vegetable), silica, and coating [hypromellose, medium-chain triglycerides, hydroxypropylcellulose, and sodium copper chlorophyllin (color)].</p>\n\n<p><strong>Directions:&nbsp;</strong>Take one to two tablets once daily with food or as directed by your healthcare practitioner.</p>\n\n<p><strong>This product is non-GMO and gluten-free.</strong></p>', '<table border=\"0\" style=\"width:100%\">\n	<tbody>\n		<tr>\n			<th>Ingredient</th>\n			<th>Amount Per Serving</th>\n			<th>% Daily Value</th>\n		</tr>\n		<tr>\n			<td>Serving Size</td>\n			<td>2 Tablets</td>\n			<td>&nbsp;</td>\n		</tr>\n		<tr>\n			<td>Servings Per Container</td>\n			<td>60</td>\n			<td>&nbsp;</td>\n		</tr>\n	</tbody>\n	<tbody>\n		<tr>\n			<td>Total Carbohydrate</td>\n			<td>&lt;1 g</td>\n			<td>&lt;1%*</td>\n		</tr>\n		<tr>\n			<td>&nbsp;&nbsp;&nbsp; Dietary Fiber</td>\n			<td>&lt;1 g</td>\n			<td>2%*</td>\n		</tr>\n		<tr>\n			<td>Vitamin A (50% from mixed carotenoids and 50% as retinyl acetate)</td>\n			<td>3,000 mcg</td>\n			<td>333%</td>\n		</tr>\n		<tr>\n			<td>Vitamin C (as ascorbic acid and ascorbyl palmitate)</td>\n			<td>120 mg</td>\n			<td>133%</td>\n		</tr>\n		<tr>\n			<td>Vitamin D (as cholecalciferol)</td>\n			<td>1000 IU</td>\n			<td>125%</td>\n		</tr>\n		<tr>\n			<td>Vitamin E (as d-alpha tocopheryl succinate)</td>\n			<td>67 mg</td>\n			<td>447%</td>\n		</tr>\n		<tr>\n			<td>Vitamin K (as phytonadione)</td>\n			<td>120 mcg</td>\n			<td>100%</td>\n		</tr>\n		<tr>\n			<td>Thiamin (as thiamin mononitrate)</td>\n			<td>25 mg</td>\n			<td>2,083%</td>\n		</tr>\n		<tr>\n			<td>Riboflavin</td>\n			<td>15 mg</td>\n			<td>1,154%</td>\n		</tr>\n		<tr>\n			<td>Niacin (as niacinamide and niacin)</td>\n			<td>50 mg</td>\n			<td>313%</td>\n		</tr>\n		<tr>\n			<td>Vitamin B6&nbsp;(as pyridoxine HCl)</td>\n			<td>25 mg</td>\n			<td>1,471%</td>\n		</tr>\n		<tr>\n			<td>Folate (as calcium L-5-methyltetrahydrofolate)&dagger;</td>\n			<td>1,360 mcg DFE</td>\n			<td>340%</td>\n		</tr>\n		<tr>\n			<td>Vitamin B12&nbsp;(as methylcobalamin)</td>\n			<td>200 mcg</td>\n			<td>8,333%</td>\n		</tr>\n		<tr>\n			<td>Biotin</td>\n			<td>500 mcg</td>\n			<td>1,667%</td>\n		</tr>\n		<tr>\n			<td>Pantothenic Acid (as calcium D-pantothenate)</td>\n			<td>75 mg</td>\n			<td>1,500%</td>\n		</tr>\n	</tbody>\n</table>', 500, 'PhytoMulti.com', 1, '<p><strong>Warning:&nbsp;</strong>Do not use if pregnant or nursing. Excess vitamin A may increase the risk of birth defects. Pregnant women and women who may become pregnant should not exceed 3,000 mcg of preformed vitamin A per day.</p>\n\n<p><strong>Caution:&nbsp;</strong>If taking medications consult your healthcare practitioner before use. Keep out of the reach of children.</p>\n\n<p><strong>Storage:</strong>&nbsp;Keep tightly closed in a cool, dry place.</p>\n\n<p>&dagger;As Metafolin&reg;. Metafolin&reg;&nbsp;is a registered trademark of Merck KGaA, Darmstadt Germany</p>', NULL, 0, '2020-07-25 03:03:23', '2020-07-28 04:54:09'),
(21, 'OmegaGenics® EPA-DHA 1000', 2, 0, 0, '[\"uploads\\/products\\/1595855988.42845.jfif\",\"uploads\\/products\\/1595855988.19981.jfif\"]', 'dose', 150, 140, 'in', '<p><strong>OmegaGenics&reg;&nbsp;EPA-DHA 1000</strong>&nbsp;features a concentrated, purified source of omega-3 fatty acids in triglyceride form from sustainably sourced, cold-water fish. Each softgel provides a total of 710 mg EPA and 290 mg DHA.</p>\n\n<p>All OmegaGenics formulas are tested for purity and quality and stabilized with antioxidants to maintain freshness. Learn more about TruQuality&reg;</p>', '<p><strong>ngredients:</strong>&nbsp;Marine lipid concentrate [fish (anchovy, sardine, and mackerel) oil], softgel shell (gelatin, glycerin, water), contains 2 percent or less of natural lemon flavor, mixed tocopherols (antioxidant), rosemary extract, and ascorbyl palmitate (antioxidant).&nbsp;<strong>Contains: Fish (anchovy, sardine, and mackerel).</strong></p>\n\n<p><strong>Directions:</strong>&nbsp;Take one softgel up to three times daily with food or as directed by your healthcare practitioner.</p>\n\n<p><strong>This product is non-GMO and gluten-free.</strong></p>', '<table border=\"0\" style=\"width:100%\">\n	<tbody>\n		<tr>\n			<th>Ingredients</th>\n			<th>Amount Per Serving</th>\n			<th>% Daily Value</th>\n		</tr>\n		<tr>\n			<td>Serving Size</td>\n			<td>1 Softgel</td>\n			<td>&nbsp;</td>\n		</tr>\n		<tr>\n			<td>Servings Per Container</td>\n			<td>60</td>\n			<td>&nbsp;</td>\n		</tr>\n		<tr>\n			<td>Calories</td>\n			<td>15</td>\n			<td>&nbsp;</td>\n		</tr>\n		<tr>\n			<td>Total Fat</td>\n			<td>1.5 g</td>\n			<td>2%*</td>\n		</tr>\n		<tr>\n			<td>Cholesterol</td>\n			<td>5 mg</td>\n			<td>2%</td>\n		</tr>\n		<tr>\n			<td>Marine Lipid Concentrate</td>\n			<td>1.4 g</td>\n			<td>**</td>\n		</tr>\n		<tr>\n			<td>&nbsp;&nbsp;&nbsp;EPA (Eicosapentaenoic acid triglyceride)</td>\n			<td>710 mg</td>\n			<td>**</td>\n		</tr>\n		<tr>\n			<td>&nbsp;&nbsp;&nbsp;DHA (Docosahexaenoic acid triglyceride)</td>\n			<td>290 mg</td>\n			<td>**</td>\n		</tr>\n		<tr>\n			<td>&nbsp;&nbsp;&nbsp;Other Omega-3 Fatty Acid Triglycerides</td>\n			<td>100 mg</td>\n			<td>**</td>\n		</tr>\n	</tbody>\n</table>', 300, 'Metagenics', 1, '<p>Caution:&nbsp;Consult your healthcare practitioner if pregnant, nursing, or taking other nutritional supplements or medications. Keep out of the reach of children.</p>\n\n<p>Storage:&nbsp;Keep tightly closed in a cool, dry place.</p>\n\n<p>This product is manufactured in a facility that processes soy and fish.</p>\n\n<p>*Percent Daily Values are based on a 2,000 calorie diet.<br />\n**Daily Value not established.</p>', NULL, 1, '2020-07-27 07:49:48', '2020-09-18 02:18:11');

-- --------------------------------------------------------

--
-- Table structure for table `product_categories`
--

CREATE TABLE `product_categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `parent_id` int(10) NOT NULL DEFAULT 0,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_categories`
--

INSERT INTO `product_categories` (`id`, `parent_id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 0, 'Gastrointestinal Health', 1, '2020-07-05 18:30:00', '2020-07-05 18:30:00'),
(2, 0, 'Cardiometabolic Health', 1, '2020-07-05 18:30:00', '2020-07-05 18:30:00'),
(3, 0, 'Immune Health', 1, '2020-07-05 18:30:00', '2020-07-05 18:30:00'),
(4, 0, 'Muscle, Bone & Joint Health', 1, '2020-07-05 18:30:00', '2020-07-05 18:30:00'),
(5, 0, 'Neurological Health', 1, '2020-07-05 18:30:00', '2020-07-05 18:30:00'),
(6, 0, 'Stress Management', 1, '2020-07-05 18:30:00', '2020-07-05 18:30:00'),
(7, 0, 'Metabolic Detoxification', 1, '2020-07-05 18:30:00', '2020-07-05 18:30:00'),
(8, 0, 'General Wellness', 1, '2020-07-05 18:30:00', '2020-07-05 18:30:00'),
(9, 0, 'Children\'s Health', 1, '2020-07-05 18:30:00', '2020-07-05 18:30:00'),
(10, 0, 'Medical Foods', 1, '2020-07-05 18:30:00', '2020-07-05 18:30:00');

-- --------------------------------------------------------

--
-- Table structure for table `provider_categories`
--

CREATE TABLE `provider_categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `parent_id` int(10) NOT NULL DEFAULT 0,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `provider_categories`
--

INSERT INTO `provider_categories` (`id`, `parent_id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 0, 'Medical Doctor', 1, '2020-07-20 18:30:00', '2020-07-20 18:30:00'),
(2, 0, 'Doctor Of Osteopathy', 1, '2020-07-20 18:30:00', '2020-07-20 18:30:00'),
(3, 0, 'Nurse Practitioner', 1, '2020-07-20 18:30:00', '2020-07-20 18:30:00'),
(4, 0, 'Physician Assistant', 1, '2020-07-20 18:30:00', '2020-07-20 18:30:00'),
(5, 0, 'Nurse', 1, '2020-07-20 18:30:00', '2020-07-20 18:30:00'),
(6, 0, 'Naturopathic Doctor', 1, '2020-07-20 18:30:00', '2020-07-20 18:30:00'),
(7, 0, 'Chiropractor', 1, '2020-07-20 18:30:00', '2020-07-20 18:30:00'),
(8, 0, 'Physical Therapist', 1, '2020-07-20 18:30:00', '2020-07-20 18:30:00'),
(9, 0, 'Physical Therapy Assistant', 1, '2020-07-20 18:30:00', '2020-07-20 18:30:00'),
(10, 0, 'Medical Assistant', 1, '2020-07-20 18:30:00', '2020-07-20 18:30:00'),
(11, 0, 'Aesthetician', 1, '2020-07-20 18:30:00', '2020-07-20 18:30:00'),
(12, 0, 'Psychologist', 1, '2020-07-20 18:30:00', '2020-07-20 18:30:00'),
(13, 0, 'Therapist', 1, '2020-07-20 18:30:00', '2020-07-20 18:30:00'),
(14, 0, 'Nutritionist / Dietitian', 1, '2020-07-20 18:30:00', '2020-07-20 18:30:00'),
(15, 0, 'Personal Trainer / Fitness Expert', 1, '2020-07-20 18:30:00', '2020-07-20 18:30:00'),
(16, 0, 'Private Chefs', 1, '2020-07-20 18:30:00', '2020-07-20 18:30:00');

-- --------------------------------------------------------

--
-- Table structure for table `question_options`
--

CREATE TABLE `question_options` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `visit_form_id` int(10) NOT NULL DEFAULT 0,
  `question_id` int(10) NOT NULL DEFAULT 0,
  `option_text` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_conditional` tinyint(1) DEFAULT NULL,
  `next_question_id` int(10) DEFAULT NULL,
  `positive` tinyint(1) DEFAULT NULL COMMENT 'for yes/no questions',
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `question_options`
--

INSERT INTO `question_options` (`id`, `visit_form_id`, `question_id`, `option_text`, `is_conditional`, `next_question_id`, `positive`, `status`, `created_at`, `updated_at`) VALUES
(1, 4, 1, 'Yes, every time', 0, 0, NULL, 1, '2020-10-27 05:33:55', '2020-10-27 05:39:51'),
(2, 4, 1, 'Yes, more than half the time', 0, 0, NULL, 1, '2020-10-27 05:33:55', '2020-10-27 05:39:51'),
(3, 4, 1, 'Yes, on occasion', 0, 0, NULL, 1, '2020-10-27 05:33:55', '2020-10-27 05:39:51'),
(4, 4, 1, 'Yes, but rarely', 0, 0, NULL, 1, '2020-10-27 05:33:55', '2020-10-27 05:39:51'),
(5, 4, 1, 'I NEVER have a problem getting or maintaining an erection for as long as I want', NULL, NULL, NULL, 1, '2020-10-27 05:33:55', '2020-10-27 05:39:51'),
(6, 4, 2, 'Gradually but has worsened over time', 0, 0, NULL, 1, '2020-10-27 05:33:55', '2020-10-27 05:39:51'),
(7, 4, 2, 'Suddenly, but not with a new partner', 0, 0, NULL, 1, '2020-10-27 05:33:55', '2020-10-27 05:39:51'),
(8, 4, 2, 'Suddenly, with a new partner', 0, 0, NULL, 1, '2020-10-27 05:33:55', '2020-10-27 05:39:51'),
(9, 4, 2, 'I do not recall how it began', 0, 0, NULL, 1, '2020-10-27 05:33:55', '2020-10-27 05:39:51'),
(10, 4, 3, 'When masturbating', 1, 4, NULL, 1, '2020-10-27 05:33:55', '2020-10-27 05:39:51'),
(11, 4, 3, 'When you wake up', 1, 5, NULL, 1, '2020-10-27 05:33:55', '2020-10-27 05:39:51'),
(12, 4, 3, 'Neither', 0, 0, NULL, 1, '2020-10-27 05:33:55', '2020-10-27 05:39:51'),
(13, 4, 4, 'No, it starts hard but never remains hard', 0, 0, NULL, 1, '2020-10-27 05:33:55', '2020-10-27 05:39:51'),
(14, 4, 4, 'Yes, but only rarely', 0, 0, NULL, 1, '2020-10-27 05:33:55', '2020-10-27 05:39:51'),
(15, 4, 4, 'Yes, on occasion', 0, 0, NULL, 1, '2020-10-27 05:33:55', '2020-10-27 05:39:51'),
(16, 4, 4, 'Yes, more than half the time', 0, 0, NULL, 1, '2020-10-27 05:33:55', '2020-10-27 05:39:51'),
(17, 4, 4, 'Yes, always', NULL, NULL, NULL, 1, '2020-10-27 05:33:55', '2020-10-27 05:39:51'),
(18, 4, 5, 'Rarely', 0, 0, NULL, 1, '2020-10-27 05:33:55', '2020-10-27 05:39:51'),
(19, 4, 5, 'Sometimes', 0, 0, NULL, 1, '2020-10-27 05:33:55', '2020-10-27 05:39:51'),
(20, 4, 5, 'Everytime', 0, 0, NULL, 1, '2020-10-27 05:33:55', '2020-10-27 05:39:51'),
(21, 4, 6, 'Not at all', 0, 0, NULL, 1, '2020-10-27 05:33:55', '2020-10-27 05:39:51'),
(22, 4, 6, 'A little bit', 0, 0, NULL, 1, '2020-10-27 05:33:55', '2020-10-27 05:39:51'),
(23, 4, 6, 'Somewhat', 0, 0, NULL, 1, '2020-10-27 05:33:55', '2020-10-27 05:39:51'),
(24, 4, 6, 'Quite a bit', 0, 0, NULL, 1, '2020-10-27 05:33:55', '2020-10-27 05:39:51'),
(25, 4, 6, 'Very', NULL, NULL, NULL, 1, '2020-10-27 05:33:55', '2020-10-27 05:39:51'),
(26, 4, 6, 'Prefer not to answer', NULL, NULL, NULL, 1, '2020-10-27 05:33:55', '2020-10-27 05:39:51'),
(27, 4, 7, 'Low', 1, 8, NULL, 1, '2020-10-27 05:33:55', '2020-10-27 05:39:51'),
(28, 4, 7, 'Medium', 0, 0, NULL, 1, '2020-10-27 05:33:55', '2020-10-27 05:39:51'),
(29, 4, 7, 'High', 0, 0, NULL, 1, '2020-10-27 05:33:55', '2020-10-27 05:39:51'),
(30, 4, 9, 'Yes', 0, 0, 1, 1, '2020-10-27 05:33:55', '2020-10-27 05:39:51'),
(31, 4, 9, 'No', 0, 0, 0, 1, '2020-10-27 05:33:55', '2020-10-27 05:39:51'),
(32, 5, 11, 'Yes', 1, 12, 1, 1, '2020-11-23 04:02:50', '2020-11-23 04:02:50'),
(33, 5, 11, 'No', 1, 13, 0, 1, '2020-11-23 04:02:50', '2020-11-23 04:02:50'),
(34, 5, 13, 'I don\'t want to tell you anything.', 0, 0, NULL, 1, '2020-11-23 04:02:50', '2020-11-23 04:02:50'),
(35, 5, 13, 'Not Interested', 0, 0, NULL, 1, '2020-11-23 04:02:50', '2020-11-23 04:02:50'),
(36, 5, 13, 'Useless', 0, 0, NULL, 1, '2020-11-23 04:02:50', '2020-11-23 04:02:50'),
(37, 5, 13, 'None of the above', 0, 0, NULL, 1, '2020-11-23 04:02:50', '2020-11-23 04:02:50'),
(38, 6, 16, 'Yes', 1, 17, 1, 1, '2020-11-23 06:58:04', '2020-11-23 06:58:04'),
(39, 6, 16, 'No', 1, 18, 0, 1, '2020-11-23 06:58:04', '2020-11-23 06:58:04'),
(40, 6, 17, '0-2 Years', 0, 14, NULL, 1, '2020-11-23 06:58:04', '2020-11-23 06:58:04'),
(41, 6, 17, '2-4 Years', 0, 14, NULL, 1, '2020-11-23 06:58:04', '2020-11-23 06:58:04'),
(42, 6, 17, '4-6 Years', 0, 14, NULL, 1, '2020-11-23 06:58:04', '2020-11-23 06:58:04'),
(43, 6, 17, '6 Years+', 0, 14, NULL, 1, '2020-11-23 06:58:04', '2020-11-23 06:58:04');

-- --------------------------------------------------------

--
-- Table structure for table `sections`
--

CREATE TABLE `sections` (
  `id` int(11) NOT NULL,
  `section_name` varchar(250) DEFAULT NULL,
  `page_name` varchar(250) DEFAULT NULL,
  `section_content` longtext DEFAULT NULL,
  `language` varchar(10) DEFAULT NULL,
  `section_order` varchar(10) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `selected_plan_features`
--

CREATE TABLE `selected_plan_features` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `plan_id` int(10) NOT NULL COMMENT 'P.K. of service_pricing',
  `plan_feature_Id` int(10) NOT NULL COMMENT 'P.K. of all_plan_features',
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `selected_plan_features`
--

INSERT INTO `selected_plan_features` (`id`, `plan_id`, `plan_feature_Id`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, '2020-12-22 05:19:25', '2020-12-22 05:19:25'),
(2, 1, 4, 1, '2020-12-22 05:19:25', '2020-12-22 05:19:25'),
(3, 1, 8, 1, '2020-12-22 05:19:25', '2020-12-22 05:19:25'),
(4, 1, 10, 1, '2020-12-22 05:19:25', '2020-12-22 05:19:25'),
(5, 2, 2, 1, '2020-12-22 05:20:36', '2020-12-22 05:20:36'),
(6, 2, 6, 1, '2020-12-22 05:20:36', '2020-12-22 05:20:36'),
(7, 2, 9, 1, '2020-12-22 05:20:36', '2020-12-22 05:20:36'),
(8, 2, 11, 1, '2020-12-22 05:20:36', '2020-12-22 05:20:36'),
(9, 2, 13, 1, '2020-12-22 05:20:36', '2020-12-22 05:20:36'),
(10, 3, 3, 1, '2020-12-22 05:21:30', '2020-12-22 05:21:30'),
(11, 3, 5, 1, '2020-12-22 05:21:30', '2020-12-22 05:21:30'),
(12, 3, 9, 1, '2020-12-22 05:21:30', '2020-12-22 05:21:30'),
(13, 3, 12, 1, '2020-12-22 05:21:30', '2020-12-22 05:21:30'),
(14, 3, 13, 1, '2020-12-22 05:21:30', '2020-12-22 05:21:30');

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `visit_type` enum('online','concierge','','') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'online',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `short_info` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `available_for` enum('male','female','both','') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'male',
  `is_free` tinyint(1) NOT NULL DEFAULT 1,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `visit_type`, `name`, `short_info`, `image`, `description`, `available_for`, `is_free`, `status`, `created_at`, `updated_at`) VALUES
(1, 'online', 'General', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et sadipscing elitr, sed diam nonumy eirmod.', 'uploads/services/1604127809.service_1.jpg', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Volutpat lacus laoreet non curabitur gravida. Vitae proin sagittis nisl rhoncus mattis rhoncus urna neque viverra. Mauris in aliquam sem fringilla ut morbi tincidunt augue. Interdum velit euismod in pellentesque massa placerat duis ultricies. Habitasse platea dictumst vestibulum rhoncus. Erat pellentesque adipiscing commodo elit at imperdiet dui. Nunc vel risus commodo viverra maecenas. Dolor magna eget est lorem ipsum dolor sit amet. Et leo duis ut diam quam. Montes nascetur ridiculus mus mauris vitae ultricies. Arcu bibendum at varius vel pharetra vel turpis. Dui nunc mattis enim ut tellus elementum sagittis. Molestie ac feugiat sed lectus vestibulum mattis ullamcorper velit. Feugiat nisl pretium fusce id velit ut tortor pretium viverra. Quis varius quam quisque id.</p>\n\n<p>Morbi tristique senectus et netus et malesuada fames ac. Turpis egestas pretium aenean pharetra magna ac placerat vestibulum. Tincidunt ornare massa eget egestas purus viverra accumsan. Semper auctor neque vitae tempus. Sed egestas egestas fringilla phasellus faucibus scelerisque eleifend donec. Nibh sit amet commodo nulla facilisi nullam vehicula. At varius vel pharetra vel turpis nunc eget lorem. Nunc consequat interdum varius sit amet mattis vulputate enim nulla. Augue neque gravida in fermentum et. Egestas sed tempus urna et pharetra pharetra massa. Leo vel fringilla est ullamcorper eget nulla facilisi etiam. In pellentesque massa placerat duis ultricies. Quam nulla porttitor massa id neque aliquam vestibulum morbi blandit. Aliquet porttitor lacus luctus accumsan tortor posuere ac ut consequat. Lacus viverra vitae congue eu consequat.</p>', 'both', 1, 1, '2020-07-22 18:30:00', '2020-10-31 03:09:47'),
(2, 'online', 'Erectile dysfunction', 'When ReplenishMD started, 80% of Viagra sold online was counterfeit. It became our mission to make it safe and simple for people to get medication they can trust from a licensed pharmacy, prescribed by a real, US-licensed healthcare professional. All from the comfort of home.', 'uploads/services/1603805072.condition_ED_vertical.webp', '<h2>What is ED?</h2>\n\n<p>Erectile dysfunction is the inability to get or maintain an erection adequate for sex. There are many different causes, but ED ultimately occurs when there is too little blood flow into the penis and too much blood flow out, partially due to an enzyme called PDE5. Oral ED medications block PDE5 to increase blood flow into the penis and decrease blood flow out.</p>\n\n<h3>Can it be treated?</h3>\n\n<p>Yes. ED medication interferes with the breakdown of chemicals required to obtain and maintain an erection. This interference keeps more of those chemicals in action longer, improving the ability to get and maintain an erection.</p>', 'male', 1, 1, '2020-07-22 18:30:00', '2020-10-29 06:34:24'),
(3, 'online', 'Nutrition', NULL, 'uploads/services/1604127591.service_1.jpg', NULL, 'male', 1, 1, '2020-07-22 18:30:00', '2020-07-22 18:30:00'),
(4, 'online', 'Counseling', NULL, 'uploads/services/1604127591.service_1.jpg', NULL, 'male', 1, 1, '2020-07-22 18:30:00', '2020-07-22 18:30:00'),
(5, 'concierge', 'Botox', NULL, 'uploads/services/1604127591.service_1.jpg', NULL, 'male', 1, 1, '2020-07-22 18:30:00', '2020-07-22 18:30:00'),
(6, 'concierge', 'Dermal Fillers', NULL, 'uploads/services/1604127591.service_1.jpg', NULL, 'male', 1, 1, '2020-07-22 18:30:00', '2020-07-22 18:30:00'),
(7, 'concierge', 'Weight Loss Consult', NULL, 'uploads/services/1604127591.service_1.jpg', NULL, 'male', 1, 1, '2020-07-22 18:30:00', '2020-07-22 18:30:00'),
(8, 'concierge', 'Bio Identical Harmones', NULL, 'uploads/services/1604127591.service_1.jpg', NULL, 'male', 1, 1, '2020-07-22 18:30:00', '2020-07-22 18:30:00'),
(9, 'concierge', 'Testosterone', NULL, 'uploads/services/1604127591.service_1.jpg', NULL, 'male', 1, 1, '2020-07-22 18:30:00', '2020-07-22 18:30:00'),
(10, 'online', 'Hair loss', 'The truth is, hair loss isn’t a physical health concern. But it can be very emotional; whether you embrace it, shave it, or treat it. If hair loss is bothering you, we want to help with FDA-approved medication and a doctor or nurse practitioner who can put you on the right treatment plan.', 'uploads/services/1604127276.condition_HL_vertical.webp', '<p>Hair loss, also known as alopecia or baldness, refers to a loss of hair from part of the head or body.[1] Typically at least the head is involved. The severity of hair loss can vary from a small area to the entire body. Inflammation or scarring is not usually present. Hair loss in some people causes psychological distress.</p>\n\n<p>Common types include male-pattern hair loss, female-pattern hair loss, alopecia areata, and a thinning of hair known as telogen effluvium. The cause of male-pattern hair loss is a combination of genetics and male hormones; the cause of female pattern hair loss is unclear; the cause of alopecia areata is autoimmune; and the cause of telogen effluvium is typically a physically or psychologically stressful event. Telogen effluvium is very common following pregnancy.</p>\n\n<p>Less common causes of hair loss without inflammation or scarring include the pulling out of hair, certain medications including chemotherapy, HIV/AIDS, hypothyroidism, and malnutrition including iron deficiency. Causes of hair loss that occurs with scarring or inflammation include fungal infection, lupus erythematosus, radiation therapy, and sarcoidosis. Diagnosis of hair loss is partly based on the areas affected.</p>\n\n<p>Treatment of pattern hair loss may simply involve accepting the condition, which can also include shaving one&#39;s head. Interventions that can be tried include the medications minoxidil (or finasteride) and hair transplant surgery. Alopecia areata may be treated by steroid injections in the affected area, but these need to be frequently repeated to be effective. Hair loss is a common problem. Pattern hair loss by age 50 affects about half of men and a quarter of women. About 2% of people develop alopecia areata at some point in time.</p>', 'both', 1, 1, '2020-10-31 01:24:36', '2020-10-31 01:24:36'),
(11, 'online', 'Female sexual dysfunction', 'This most common of female sexual dysfunctions involves a lack of sexual interest and willingness to be sexual. Sexual arousal disorder. Your desire for sex might be intact, but you have difficulty with arousal or are unable to become aroused or maintain arousal during sexual activity. Orgasmic disorder', 'uploads/services/1604127591.service_1.jpg', '<p>A sexual problem, or sexual dysfunction, refers to a problem during any phase of the sexual response cycle that prevents the individual or couple from experiencing satisfaction from the sexual activity. The sexual response cycle has four phases: excitement, plateau, orgasm, and resolution.</p>\n\n<p>While research suggests that sexual dysfunction is common (43% of women and 31% of men report some degree of difficulty), it is a topic that many people are hesitant or embarrassed to discuss. Fortunately, most cases of sexual dysfunction are treatable, so it is important to share your concerns with your partner and doctor.</p>', 'female', 1, 1, '2020-10-31 01:29:51', '2020-10-31 01:29:51');

-- --------------------------------------------------------

--
-- Table structure for table `service_pricing`
--

CREATE TABLE `service_pricing` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `is_monthly` tinyint(1) NOT NULL DEFAULT 1,
  `total_months` int(5) NOT NULL,
  `monthly_price` float DEFAULT NULL,
  `is_total` tinyint(1) NOT NULL DEFAULT 0,
  `total_price` float DEFAULT 0,
  `monthly_plan_text` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `total_plan_text` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `special` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `detail` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_most_popular` tinyint(1) NOT NULL DEFAULT 0,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `service_pricing`
--

INSERT INTO `service_pricing` (`id`, `name`, `is_monthly`, `total_months`, `monthly_price`, `is_total`, `total_price`, `monthly_plan_text`, `total_plan_text`, `special`, `detail`, `is_most_popular`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Free Trial', 0, 1, 0, 0, 0, 'Try for free for first month', NULL, NULL, 'Work with our ReplenishMD Team online to understand your health concern and start a personalized health plan.', 0, 1, '2020-12-22 01:16:24', '2020-12-22 01:26:16'),
(2, '12 Month Plan', 1, 12, 150, 1, 1600, 'Monthly for 12 months', 'One upfront payment', 'Pay upfront and save $200', 'Ongoing medical care designed to manage and resolve persistent concerns, helping you achieve your best possible health.', 1, 1, '2020-08-04 18:30:00', '2020-08-05 07:41:41'),
(3, '4 Month Plan', 1, 4, 175, 1, 595, 'Monthly for 4 months', 'One upfront payment', 'Pay upfront and save $100', 'Focused online care designed to improve your biggest health concerns and transform how you feel in a shorter period of time.', 0, 1, '2020-08-04 18:30:00', '2020-08-05 07:59:29');

-- --------------------------------------------------------

--
-- Table structure for table `sponsors`
--

CREATE TABLE `sponsors` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sponsors`
--

INSERT INTO `sponsors` (`id`, `name`, `image`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Metagenics', 'uploads/sponsors/partner1.png', 1, '2020-08-31 00:46:12', '2020-08-31 00:51:17'),
(2, 'eVitamins', 'uploads/sponsors/partner2.png', 1, '2020-08-31 00:46:12', '2020-08-31 00:51:17'),
(3, 'Lucky Vitamin', 'uploads/sponsors/partner3.png', 1, '2020-08-31 00:46:12', '2020-08-31 00:51:17'),
(4, 'Wellevate', 'uploads/sponsors/partner4.png', 1, '2020-08-31 00:46:12', '2020-08-31 00:51:17'),
(5, 'PLM', 'uploads/sponsors/partner5.png', 1, '2020-08-31 00:46:12', '2020-08-31 00:51:17');

-- --------------------------------------------------------

--
-- Table structure for table `testimonials`
--

CREATE TABLE `testimonials` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `star_rating` int(1) NOT NULL DEFAULT 5,
  `comments` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `testimonials`
--

INSERT INTO `testimonials` (`id`, `name`, `image`, `star_rating`, `comments`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Mariya Brown', 'uploads/testimonials/c1.png', 4, 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.', 1, '2020-08-31 00:46:12', '2020-08-31 00:51:17'),
(2, 'Ethen Haddox', 'uploads/testimonials/c2.png', 5, 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.', 1, '2020-08-31 00:46:12', '2020-08-31 00:51:17'),
(3, 'Calicadoo', 'uploads/testimonials/c3.png', 4, 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.', 1, '2020-08-31 00:46:12', '2020-08-31 00:51:17'),
(4, 'Riyaz Khan', 'uploads/testimonials/c1.png', 5, 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.', 1, '2020-08-31 00:46:12', '2020-08-31 00:51:17'),
(5, 'Carol Potts', 'uploads/testimonials/c2.png', 3, 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.', 1, '2020-08-31 00:46:12', '2020-08-31 00:51:17'),
(6, 'Natasha', 'uploads/testimonials/c3.png', 5, 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.', 1, '2020-08-31 00:46:12', '2020-08-31 00:51:17');

-- --------------------------------------------------------

--
-- Table structure for table `treatment_medicines`
--

CREATE TABLE `treatment_medicines` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `service_id` int(10) NOT NULL DEFAULT 0,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `other_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `chemical_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `images` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `start_price` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `treatment_medicines`
--

INSERT INTO `treatment_medicines` (`id`, `service_id`, `name`, `other_name`, `chemical_name`, `images`, `description`, `start_price`, `status`, `created_at`, `updated_at`) VALUES
(1, 2, 'Sildenafil', 'Sildenafil citrate', NULL, '[\"uploads\\/treatment_medicines\\/1603809779.drug_branded_viagra.webp\",\"uploads\\/treatment_medicines\\/1603809779.SIL_Satchet_sm.webp\"]', '<p><strong>Sildenafil</strong> is the generic form of the brand <strong>Viagra&reg;</strong>. Both medications work similarly in the body.<br />\n<br />\n<strong>Viagra&reg;</strong> is a type of medication known as a PDE5 inhibitor that is used to treat erectile dysfunction. It comes in several different doses and is taken as-needed.</p>', '2', 1, '2020-10-07 07:27:37', '2020-12-01 09:04:58'),
(2, 2, 'Viagra', 'Viagra', 'Sildenafil citrate', '[\"uploads\\/treatment_medicines\\/1603809479.drug_branded_viagra.webp\",\"uploads\\/treatment_medicines\\/1603809479.VIA_Satchet_sm.webp\"]', '<p>Sildenafil is the generic form of the brand Viagra&reg;. Both medications work similarly in the body.<br />\n<br />\nViagra&reg; is a type of medication known as a PDE5 inhibitor that is used to treat erectile dysfunction. It comes in several different doses and is taken as-needed.</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<h3>What is Viagra?</h3>\n\n<p>Viagra&reg; (nicknamed &ldquo;the little blue pill&rdquo;) is a drug that is used to treat erectile dysfunction (ED). It comes in three doses, 25 mg, 50 mg, and 100 mg. The 50 mg dose is the most commonly used dose, although some people may benefit from higher or lower doses. Viagra&reg; is part of a &ldquo;family&rdquo; of medicines used to treat ED called phosphodiesterase-5 inhibitors (PDE5i). Other drugs in this family include Cialis (tadalafil), Levitra (vardenafil), and Stendra (avanafil). All of these drugs have the same mechanism of action, but there are subtle differences that may lead the doctor to recommend one drug over the other based on your treatment goals.</p>\n\n<h3>What is generic Viagra?</h3>\n\n<p>Viagra&reg; was originally patented by Pfizer and brought to the US market in 1998. It is FDA-approved to treat erectile dysfunction (ED). After the patent expired, the FDA gave permission to other drug companies to market a generic version of Viagra&reg;. The FDA requires generic drugs to be bioequivalent to the brand name version. This means that they act the same way in the body and are expected to produce the same effects as the original brand name drug. Generic drugs are generally much cheaper than the original brand name drug, and most drugs prescribed in the US are generic.</p>\n\n<h3>What is sildenafil citrate?</h3>\n\n<p>Sildenafil citrate is the chemical name of the active ingredient in Viagra&reg;. When sildenafil is sold in 25, 50, or 100 mg dosages, it&rsquo;s called Viagra or it is the generic for Viagra. When it is sold in a 20 mg dosage, it is called Revatio or it is the generic for Revatio. Revatio is FDA approved for treating pulmonary hypertension. Both Viagra and Revatio contain the exact same ingredient (i.e., sildenafil citrate), they simply differ in the strength in which they are produced. Physicians on the Roman platform have the discretion to prescribe 20 mg sildenafil citrate tablets for ED in doses ranging between 20 mg and 100 mg if they believe that it is an appropriate course of treatment for a particular patient. Sildenafil citrate 20 mg tablets for ED is an&nbsp;<a href=\"https://www.getroman.com/drugs/viagra/#ro-modal-893\">off-label</a>&nbsp;usage of the medication. It is up to the medical judgment of the doctor to decide if off-label treatment is appropriate for a patient based on his unique medical history, symptoms, and preferences. The Roman Pharmacy Network is able to fill prescriptions for sildenafil citrate 20 mg tablets that are issued by a physician on the Roman platform.</p>\n\n<h3>Does Roman offer genuine Viagra?</h3>\n\n<p>Roman offers genuine, branded Viagra&reg; and generic Viagra&reg; to treat erectile dysfunction (ED) in 25, 50, or 100 mg dosages. Roman also offers sildenafil citrate in 20 mg tablets, which can be prescribed by doctors off-label to treat ED in doses ranging between 20 mg and 100 mg if they find it medically appropriate to do so. The active ingredient, sildenafil citrate, is identical in both medications.</p>', '70', 1, '2020-10-27 08:00:56', '2020-10-27 09:07:59'),
(3, 2, 'Cialis', 'generic Cialis', 'Tadalafil', '[\"uploads\\/treatment_medicines\\/1603809449.drug_generic_cialis-1.webp\",\"uploads\\/treatment_medicines\\/1603809449.CIA_Satchet_sm.webp\"]', '<p>Tadalafil is the generic form of the brand Cialis&reg;. Both medications work similarly in the body.<br />\n<br />\nCialis&reg; is a type of medication known as a PDE5 inhibitor that is used to treat erectile dysfunction and benign prostatic hyperplasia. It comes in several different doses and can be taken either as-needed or daily for erectile dysfunction.</p>\n\n<p>&nbsp;</p>\n\n<h3>What is Cialis?</h3>\n\n<p>Cialis&reg; is a drug that is used to treat erectile dysfunction (ED). It comes in four doses, 2.5 mg, 5 mg, 10 mg, and 20 mg. Cialis&reg; can be taken as needed in any of the four doses. The 2.5 mg and 5 mg doses are also approved for daily use for ED. Cialis&reg; in the 2.5 mg and 5 mg doses is also FDA-approved for men suffering from urinary symptoms due to benign prostatic hyperplasia (BPH) with or without ED. Physicians on the Roman platform do not currently treat BPH. If you feel you need Cialis for the treatment of BPH, please seek in-person care. For as needed dosing, the 10 mg dose is the most commonly used dose, although some people may benefit from higher or lower doses. Cialis&reg; is part of a &ldquo;family&rdquo; of medicines used to treat ED called phosphodiesterase-5 inhibitors (PDE5i). Other drugs in this family include Viagra (sildenafil), Levitra (vardenafil), and Stendra (avanafil). All of these drugs have the same mechanism of action, but there are subtle differences that may lead the doctor or nurse practitioner to recommend one drug over the other based on your treatment goals.</p>\n\n<h3>What is tadalafil?</h3>\n\n<p>Tadalafil is the chemical name of the active ingredient in Cialis&reg;. Cialis&reg; was originally patented and sold by Eli Lilly. It is FDA-approved to treat ED and urinary symptoms due to BPH. After the patent expired in 2018, the FDA gave permission to other drug companies to manufacture a generic version of Cialis&reg;. The FDA requires generic drugs to be bioequivalent to the brand name version. This means that they act the same way in the body and are expected to produce the same effects as the original brand name drug. Generic drugs are generally much cheaper than the original brand name drug, and most drugs prescribed in the US are generic.</p>\n\n<h3>Does Roman offer genuine Cialis?</h3>\n\n<p>Roman offers branded Cialis&reg; and its generic, tadalafil, to treat ED. They are both FDA-approved to treat ED. They can be taken as needed (from 2.5 mg to 20 mg) or daily (in the 2.5 mg and 5 mg doses) to treat ED.</p>\n\n<h3>How does Cialis work?</h3>\n\n<p>In order to understand how Cialis&reg; works to treat ED, it&rsquo;s important to first understand the basics of how erections work. Erotic stimulation (by physical touch, erotic thoughts, smells, etc.) causes the local tissues to release nitric oxide (NO), which increases the amount of a chemical called cyclic guanosine monophosphate (cGMP). cGMP causes the muscles on the sides of the penis, the corpora cavernosa, to relax allowing blood to rush into the penis. At the same time veins that drain blood from the penis get compressed causing the increased blood to be trapped in the penis causing an erection. An enzyme called phosphodiesterase-5 breaks down cGMP leading the penis to return to its flaccid state. PDE5i, like Cialis&reg;, block this enzyme leading to higher levels of cGMP and improving the ability to obtain and maintain an erection.</p>', '17', 1, '2020-10-27 08:25:55', '2020-10-27 09:07:29');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL COMMENT 'all',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'admin and provider',
  `is_admin` tinyint(1) DEFAULT 0 COMMENT 'all',
  `user_role` tinyint(1) NOT NULL DEFAULT 4 COMMENT 'all',
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'all',
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'all',
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT 'all',
  `menu_permissions` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'admin only',
  `first_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'patient',
  `last_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'patient',
  `image` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT 'provider and patient',
  `address` text COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'patient',
  `zip_code` int(5) DEFAULT NULL,
  `gender` enum('male','female','other','') COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT 'patient',
  `dob` date DEFAULT NULL COMMENT 'patient',
  `age` int(3) DEFAULT NULL COMMENT 'patient',
  `short_info` text COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'only for providers',
  `clinical_expertise` text COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'only for providers',
  `credentials` text COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'only for providers',
  `awards` text COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'only for providers',
  `household_income` float DEFAULT 0 COMMENT 'patient',
  `dmm_fund` float DEFAULT 0 COMMENT 'patient, Discretionary monthly maintenance fund',
  `monthly_budget` float DEFAULT 0 COMMENT 'only patient',
  `email_verified_at` timestamp NULL DEFAULT NULL COMMENT 'all',
  `category` text COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'only providers, multiple entries',
  `diagnosis` text COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'for patients f.k. ids in comma separated values',
  `insurance` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT 'patient',
  `allergies` text COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'patient',
  `smokers` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT 'patient',
  `google_adsense_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT 'all',
  `amazon_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT 'all',
  `rmd_ad_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT 'all',
  `provider_title` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT 'provider',
  `license_number` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT 'provider',
  `provider_licenses` text COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'provider',
  `npi` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT 'provider',
  `dea` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT 'provider, for physician, NP, PA',
  `practice_address` text COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'provider',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'all',
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'all',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'all'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `is_admin`, `user_role`, `email`, `password`, `status`, `menu_permissions`, `first_name`, `last_name`, `image`, `phone`, `address`, `zip_code`, `gender`, `dob`, `age`, `short_info`, `clinical_expertise`, `credentials`, `awards`, `household_income`, `dmm_fund`, `monthly_budget`, `email_verified_at`, `category`, `diagnosis`, `insurance`, `allergies`, `smokers`, `google_adsense_id`, `amazon_id`, `rmd_ad_id`, `provider_title`, `license_number`, `provider_licenses`, `npi`, `dea`, `practice_address`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Rhea Watson', 1, 1, 'admin@gmail.com', '$2y$10$91.5BbINQoSfjNoN7iClHO0vZbMRpFbkkYPK/ILwNSThMfn86C/rm', 1, '[\"manage_online_visits\",\"visit_pool\",\"manage_orders\",\"manage_products\",\"manage_doctors\",\"manage_patients\",\"manage_contact_us\",\"finances\",\"manage_website_content\",\"manage_sub_admin\",\"manage_questions\",\"manage_faq\"]', '', '', 'uploads/users/1598013593.profile-4.jpg', '+18534324423', '183, Silver street, Manhattan, New York 55432', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, '0', NULL, '', NULL, '', '', '', '', '', '', NULL, '', '', '', NULL, '2020-07-04 02:23:12', '2020-08-21 07:09:53'),
(2, 'Nick Barton', 1, 2, 'subadmin@gmail.com', '$2y$10$4VsmTatx262jDXXTjIpqi.Qd2aafSDrOvYhcPmZ.FC.mo8PuKIgFi', 1, '[\"manage_providers\",\"manage_patients\",\"manage_contact_us\",\"manage_website_content\",\"manage_questions\",\"manage_faq\"]', 'Nick', 'Barton', 'uploads/users/1598013048.profile-9.jpg', '+1832323424542', '70 Wast 56th Street, 12th Floor, Manhattan, New York 10022', NULL, 'male', '1990-07-30', 30, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, '3', NULL, '', NULL, '', '2342423', '42342234', '313243532', '', '', NULL, '', '', '', NULL, '2020-07-04 02:23:12', '2020-08-21 07:00:48'),
(3, 'Dr. Gaby', 0, 3, 'provider@gmail.com', '$2y$10$wHTlxAEtvUol5RrmBhsWF.GApk.Calxblr9IGp77V6RpnJ/R04Kp6', 1, '[\"manage_visit_requests\",\"visit_pool\",\"availability\",\"my_account\"]', '', '', 'uploads/users/team2.png', '7898546525', '333 Washington Avenue North, Suite 300 - 9033 Minneapolis, MN 55401, USA', NULL, 'male', '1992-03-08', 28, 'Dr. Gaby is a board-certified Family Medicine doctor with over 10 years of experience in Functional and Integrative Medicine. He has received training through the Institute for Functional Medicine, the School of Applied Functional Medicine, and has worked under some of the most experienced functional clinicians in West Michigan where he focused on heart and blood vessel disease, diabetes, obesity, digestive health and balancing hormones, among other things.', 'Thyroid & Adrenal Health, Women’s Health, Autoimmunity, Metabolic Syndrome, Digestive Disorders & Gastrointestinal Health, Cardiometabolic Health, Longevity', NULL, 'Gold Medalist, Doctor of the Year', 0, 0, 0, NULL, '1,2', NULL, '', NULL, '', '58978456', '4526-DB-45672', '579864', 'Doctor', '985469874565E', '[\"uploads\\/licenses\\/license1001.jpg\",\"uploads\\/licenses\\/license1002.jpg\",\"uploads\\/licenses\\/license_3.jpg\",\"uploads\\/licenses\\/license_4.jpg\",\"uploads\\/licenses\\/license_5.png\",\"uploads\\/licenses\\/license_6.jpg\"]', '8975445670', 'F91234563', '333 Washington Avenue North, USA', NULL, '2020-07-04 02:23:12', '2020-08-24 03:05:07'),
(4, 'John Doe', 0, 4, 'patient@gmail.com', '$2y$10$wHTlxAEtvUol5RrmBhsWF.GApk.Calxblr9IGp77V6RpnJ/R04Kp6', 1, '', 'John', 'Doe', NULL, '598-443-2805', '180 Wast 56th Street, 12th Floor, Meldegam, Belgium 10022', NULL, 'male', '1990-07-09', 30, NULL, NULL, NULL, NULL, 50000, 2000, 15000, NULL, '0', '1,2,3', 'Yes', 'Red eyes, itchy rash, runny nose, shortness of breath, swelling, sneezing', 'Yes', '1234', '1234', '1234', '', '', 'test000.jpg', '', '', '', NULL, '2020-07-04 03:15:28', '2020-07-04 03:15:28'),
(5, 'Sam Barton', 1, 2, 'subadmin2@gmail.com', '$2y$10$HlTAd/GjdBK3Qbh7x2JzVu0GhA2jvjD/pinadAnK.thpQvAtr0uEW', 1, '[\"manage_online_visits\",\"visit_pool\",\"manage_orders\",\"manage_products\",\"manage_providers\",\"manage_patients\",\"manage_contact_us\",\"finances\",\"manage_website_content\",\"manage_questions\",\"manage_faq\"]', '', '', NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, '2', NULL, '', NULL, '', '', '', '', '', '', NULL, '', '', '', NULL, '2020-07-04 02:23:12', '2020-07-04 02:23:12'),
(6, 'Nisha Roy', 1, 2, 'subadmin3@gmail.com', '$2y$10$HlTAd/GjdBK3Qbh7x2JzVu0GhA2jvjD/pinadAnK.thpQvAtr0uEW', 1, '[\"manage_online_visits\",\"visit_pool\",\"manage_orders\",\"manage_products\",\"manage_doctors\",\"manage_patients\",\"manage_contact_us\",\"finances\",\"manage_website_content\",\"manage_questions\",\"manage_faq\"]', '', '', NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, '4', NULL, '', NULL, '', '', '', '', '', '', NULL, '', '', '', NULL, '2020-07-04 02:23:12', '2020-07-04 02:23:12'),
(7, 'Thomas Smith', 1, 2, 'subadmin4@gmail.com', '$2y$10$HlTAd/GjdBK3Qbh7x2JzVu0GhA2jvjD/pinadAnK.thpQvAtr0uEW', 1, '[\"manage_online_visits\",\"visit_pool\",\"manage_orders\",\"manage_products\",\"manage_providers\",\"manage_patients\",\"manage_contact_us\",\"finances\",\"manage_website_content\",\"manage_questions\",\"manage_faq\"]', '', '', NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, '4', NULL, '', NULL, '', '', '', '', '', '', NULL, '', '', '', NULL, '2020-07-04 02:23:12', '2020-07-04 02:23:12'),
(8, 'Josh Anderson', 1, 2, 'subadmin5@gmail.com', '$2y$10$HlTAd/GjdBK3Qbh7x2JzVu0GhA2jvjD/pinadAnK.thpQvAtr0uEW', 0, '[\"manage_online_visits\",\"visit_pool\",\"manage_orders\",\"manage_products\",\"manage_doctors\",\"manage_patients\",\"manage_contact_us\",\"finances\",\"manage_website_content\",\"manage_questions\",\"manage_faq\"]', '', '', NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, '2', NULL, '', NULL, '', '', '', '', '', '', NULL, '', '', '', NULL, '2020-07-04 02:23:12', '2020-07-04 02:23:12'),
(9, 'Robert Broad', 1, 2, 'subadmin6@gmail.com', '$2y$10$HlTAd/GjdBK3Qbh7x2JzVu0GhA2jvjD/pinadAnK.thpQvAtr0uEW', 1, '[\"manage_online_visits\",\"visit_pool\",\"manage_orders\",\"manage_products\",\"manage_doctors\",\"manage_patients\",\"manage_contact_us\",\"finances\",\"manage_website_content\",\"manage_questions\",\"manage_faq\"]', '', '', NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, '2', NULL, '', NULL, '', '', '', '', '', '', NULL, '', '', '', NULL, '2020-07-04 02:23:12', '2020-07-04 02:23:12'),
(10, 'Priyanka Rai', 1, 2, 'subadmin7@gmail.com', '$2y$10$HlTAd/GjdBK3Qbh7x2JzVu0GhA2jvjD/pinadAnK.thpQvAtr0uEW', 1, '[\"manage_online_visits\",\"visit_pool\",\"manage_orders\",\"manage_products\",\"manage_providers\",\"manage_patients\",\"manage_contact_us\",\"finances\",\"manage_website_content\",\"manage_questions\",\"manage_faq\"]', '', '', NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, '4', NULL, '', NULL, '', '', '', '', '', '', NULL, '', '', '', NULL, '2020-07-04 02:23:12', '2020-07-04 02:23:12'),
(11, 'Ayaz Khan', 1, 2, 'subadmin8@gmail.com', '$2y$10$HlTAd/GjdBK3Qbh7x2JzVu0GhA2jvjD/pinadAnK.thpQvAtr0uEW', 0, '[\"manage_online_visits\",\"visit_pool\",\"manage_orders\",\"manage_products\",\"manage_providers\",\"manage_patients\",\"manage_contact_us\",\"finances\",\"manage_website_content\",\"manage_questions\",\"manage_faq\"]', '', '', NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, '1', NULL, '', NULL, '', '', '', '', '', '', NULL, '', '', '', NULL, '2020-07-04 02:23:12', '2020-07-04 02:23:12'),
(12, 'Steve Anderson', 1, 2, 'subadmin9@gmail.com', '$2y$10$HlTAd/GjdBK3Qbh7x2JzVu0GhA2jvjD/pinadAnK.thpQvAtr0uEW', 1, '[\"manage_online_visits\",\"visit_pool\",\"manage_orders\",\"manage_products\",\"manage_providers\",\"manage_patients\",\"manage_contact_us\",\"finances\",\"manage_website_content\",\"manage_questions\",\"manage_faq\"]', '', '', NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, '2', NULL, '', NULL, '', '', '', '', '', '', NULL, '', '', '', NULL, '2020-07-04 02:23:12', '2020-07-04 02:23:12'),
(13, 'John Parker', 1, 2, 'subadmin10@gmail.com', '$2y$10$HlTAd/GjdBK3Qbh7x2JzVu0GhA2jvjD/pinadAnK.thpQvAtr0uEW', 1, '[\"manage_online_visits\",\"visit_pool\",\"manage_orders\",\"manage_products\",\"manage_providers\",\"manage_patients\",\"manage_contact_us\",\"finances\",\"manage_website_content\",\"manage_questions\",\"manage_faq\"]', '', '', NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, '3', NULL, '', NULL, '', '', '', '', '', '', NULL, '', '', '', NULL, '2020-07-04 02:23:12', '2020-07-04 02:23:12'),
(14, 'Rex Doe', 1, 2, 'subadmin11@gmail.com', '$2y$10$HlTAd/GjdBK3Qbh7x2JzVu0GhA2jvjD/pinadAnK.thpQvAtr0uEW', 0, '[\"manage_online_visits\",\"visit_pool\",\"manage_orders\",\"manage_products\",\"manage_providers\",\"manage_patients\",\"manage_contact_us\",\"finances\",\"manage_website_content\",\"manage_questions\",\"manage_faq\"]', '', '', NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, '3', NULL, '', NULL, '', '', '', '', '', '', NULL, '', '', '', NULL, '2020-07-04 02:23:12', '2020-07-04 02:23:12'),
(15, 'Clint Rogers', 1, 2, 'subadmin12@gmail.com', '$2y$10$HlTAd/GjdBK3Qbh7x2JzVu0GhA2jvjD/pinadAnK.thpQvAtr0uEW', 1, '[\"manage_online_visits\",\"visit_pool\",\"manage_orders\",\"manage_products\",\"manage_providers\",\"manage_patients\",\"manage_contact_us\",\"finances\",\"manage_website_content\",\"manage_questions\",\"manage_faq\"]', '', '', NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, '2', NULL, '', NULL, '', '', '', '', '', '', NULL, '', '', '', NULL, '2020-07-04 02:23:12', '2020-07-04 02:23:12'),
(16, 'Steve Banner', 1, 2, 'subadmin13@gmail.com', '$2y$10$HlTAd/GjdBK3Qbh7x2JzVu0GhA2jvjD/pinadAnK.thpQvAtr0uEW', 1, '[\"manage_online_visits\",\"visit_pool\",\"manage_orders\",\"manage_products\",\"manage_providers\",\"manage_patients\",\"manage_contact_us\",\"finances\",\"manage_website_content\",\"manage_questions\",\"manage_faq\"]', '', '', NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, '3', NULL, '', NULL, '', '', '', '', '', '', NULL, '', '', '', NULL, '2020-07-04 02:23:12', '2020-07-04 02:23:12'),
(17, 'Steve Johnson', 0, 4, 'test@gmail.com', '$2y$10$Z5aYTOxrwcmJPX6tWBU8oeovC4UHDif5YaKrIhlACoAjwXa.OWXXq', 1, '', 'Steve', 'Johnson', 'profile-1.jpg', '832-443-2909', '160 East 56th Street, 12th Floor, New York, New York 10022', NULL, 'male', '1994-06-10', 26, NULL, NULL, NULL, NULL, 40000, 3000, 13000, NULL, '0', '5,6,7', 'No', 'Hay fever, food allergies, atopic dermatitis, allergic asthma, anaphylaxis', 'No', '2345', '2345', '2345', '', '', 'test0002.jpg', '', '', '', NULL, '2020-07-06 11:29:19', '2020-07-28 06:11:11'),
(23, 'Mark Smith', 1, 2, 'abc@gmail.com', '$2y$10$.65NcdpEGPzQMUaxPG.4d.CZTfvc0fm3piOyGEDeRdkw3nl34TeOW', 0, '[\"manage_online_visits\",\"visit_pool\",\"manage_orders\",\"manage_products\",\"manage_providers\",\"manage_patients\",\"manage_contact_us\",\"finances\",\"manage_questions\"]', '', '', NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, '1', NULL, '', NULL, '', '', '', '', '', '', NULL, '', '', '', NULL, '2020-07-07 01:39:57', '2020-10-08 07:33:16'),
(31, 'Dr. Alice Banton', 0, 3, 'doctor2@gmail.com', '$2y$10$0xAUZmlfOIktsuSZVqlQpe8vg3A8H8PZtsuOifFOo9W1JeAjV68bO', 1, '[\"manage_visit_requests\",\"visit_pool\",\"availability\",\"my_account\"]', '', '', 'uploads/users/team3.png', '', '180 Wast 56th Street, 12th Floor, Meldegam, Belgium 10022', NULL, '', NULL, NULL, 'Dr. Alice Banton is the founder and CEO of Parsley Health. A Summa Cum Laude graduate of the University of Pennsylvania, Robin completed medical school at Columbia University’s College of Physicians and Surgeons, and trained in Internal Medicine at Mount Sinai Hospital in New York City.', 'Hormone Balancing & Optimization, Epigenetic & Nutrigenomics, Clinical Cannabinoid Medicine, Cancer Prevention & Nutrition, Men’s & Women’s Health', NULL, 'Gold Medalist, Doctor of the Year', 0, 0, 0, NULL, '3,4', NULL, '', NULL, '', '', '', '', '', '', NULL, '', '', '', NULL, '2020-07-08 06:29:14', '2020-09-03 10:08:12'),
(32, 'Dr. Bethany Perrin', 0, 3, 'doctor3@gmail.com', '$2y$10$l8MGpAP9e.cC7rOmMl/4pugiGYLa9J305jSx.QyiZkbdYmcxtjuhC', 1, '[\"manage_visit_requests\",\"visit_pool\",\"availability\",\"my_account\"]', '', '', 'uploads/users/team4.png', '', '180 Wast 56th Street, 12th Floor, Meldegam, Belgium 10022', NULL, '', NULL, NULL, 'Dr. Bethany Perrin is a new to Nashville-based Registered Dietitian Nutritionist. She recently moved from Los Angeles to middle Tennessee. She holds a Masters of Science in Nutritional Sciences from Texas Tech University and obtained her credentials after completing her internship through Texas State University.', 'Thyroid & Adrenal Health, Hormones, Autoimmune Disorders, Gastrointestinal Health, Biology of Stress, Cancer Prevention, Fertility Optimization', NULL, 'Gold Medalist, Doctor of the Year', 0, 0, 0, NULL, '1,2', NULL, '', NULL, '', '', '', '', '', '', NULL, '', '', '', NULL, '2020-07-08 06:42:39', '2020-07-08 06:42:39'),
(33, 'Dr. Frank', 0, 3, 'doctor4@gmail.com', '$2y$10$pnu.8EBAfw95bEuHMlPfsu78SOADwsVyoaarl.nNliEfA7Ocya6gG', 1, '[\"manage_visit_requests\",\"visit_pool\",\"availability\",\"my_account\"]', '', '', 'uploads/users/team1.png', '', '180 Wast 56th Street, 12th Floor, Meldegam, Belgium 10022', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, '1', NULL, '', NULL, '', '', '', '', '', '', NULL, '', '', '', NULL, '2020-07-08 06:45:40', '2020-09-03 10:08:21'),
(35, 'Dr. Richards', 0, 3, 'doctor111@gmail.com', '$2y$10$wHTlxAEtvUol5RrmBhsWF.GApk.Calxblr9IGp77V6RpnJ/R04Kp6', 1, '', '', '', 'uploads/users/team4.png', '', '180 Wast 56th Street, 12th Floor, Meldegam, Belgium 10022', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, '6', NULL, '', NULL, '', '', '', '', '', '', NULL, '', '', '', NULL, '2020-07-04 02:23:12', '2020-07-04 02:23:12'),
(36, 'Dr. Gwen Nolan', 0, 3, 'doctor222@gmail.com', '$2y$10$0xAUZmlfOIktsuSZVqlQpe8vg3A8H8PZtsuOifFOo9W1JeAjV68bO', 1, '[\"manage_visit_requests\",\"visit_pool\",\"availability\",\"my_account\"]', '', '', 'uploads/users/team3.png', '', '180 Wast 56th Street, 12th Floor, Meldegam, Belgium 10022', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, '6', NULL, '', NULL, '', '', '', '', '', '', NULL, '', '', '', NULL, '2020-07-08 06:29:14', '2020-09-03 10:08:17'),
(37, 'Dr. R.S. Kapoor', 0, 3, 'doctor333@gmail.com', '$2y$10$l8MGpAP9e.cC7rOmMl/4pugiGYLa9J305jSx.QyiZkbdYmcxtjuhC', 1, '[\"manage_visit_requests\",\"visit_pool\",\"availability\",\"my_account\"]', '', '', 'uploads/users/team4.png', '', '180 Wast 56th Street, 12th Floor, Meldegam, Belgium 10022', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, '4', NULL, '', NULL, '', '', '', '', '', '', NULL, '', '', '', NULL, '2020-07-08 06:42:39', '2020-09-03 10:08:16'),
(38, 'Dr. Sara ', 0, 3, 'doctor444@gmail.com', '$2y$10$pnu.8EBAfw95bEuHMlPfsu78SOADwsVyoaarl.nNliEfA7Ocya6gG', 1, '[\"manage_visit_requests\",\"visit_pool\",\"availability\",\"my_account\"]', '', '', 'uploads/users/team3.png', '', '180 Wast 56th Street, 12th Floor, Meldegam, Belgium 10022', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, '2', NULL, '', NULL, '', '', '', '', '', '', NULL, '', '', '', NULL, '2020-07-08 06:45:40', '2020-07-20 02:21:04'),
(39, 'Dr. Robert', 0, 3, 'doctor555@gmail.com', '$2y$10$xuEtJvPiD90rJlp.oaVqVujiZoHDWc7jMnAjMy0Ba8qcgkv2ySuD.', 0, '[\"manage_visit_requests\",\"visit_pool\",\"availability\",\"my_account\"]', '', '', 'uploads/users/team1.png', '', '180 Wast 56th Street, 12th Floor, Meldegam, Belgium 10022', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, '5', NULL, '', NULL, '', '', '', '', '', '', NULL, '', '', '', NULL, '2020-07-08 06:49:17', '2020-07-08 06:49:17'),
(40, 'John Wick', 0, 4, 'patient123@gmail.com', '$2y$10$wHTlxAEtvUol5RrmBhsWF.GApk.Calxblr9IGp77V6RpnJ/R04Kp6', 1, '', 'John', 'Wick', NULL, '598-443-2805', '180 Wast 56th Street, 12th Floor, Meldegam, Belgium 10022', NULL, 'female', '1990-07-09', 30, NULL, NULL, NULL, NULL, 50000, 2000, 15000, NULL, '0', '1,2,3', 'Yes', 'Red eyes, itchy rash, runny nose, shortness of breath, swelling, sneezing', 'Yes', '1234', '1234', '1234', '', '', 'test000.jpg', '', '', '', NULL, '2020-07-04 03:15:28', '2020-07-04 03:15:28'),
(41, 'Michel Johnson', 0, 4, 'test2345@gmail.com', '$2y$10$Z5aYTOxrwcmJPX6tWBU8oeovC4UHDif5YaKrIhlACoAjwXa.OWXXq', 1, '', 'Michel', 'Johnson', 'profile-1.jpg', '832-443-2909', '160 East 56th Street, 12th Floor, New York, New York 10022', NULL, 'male', '1994-06-10', 26, NULL, NULL, NULL, NULL, 40000, 3000, 13000, NULL, '0', '5,6,7', 'No', 'Hay fever, food allergies, atopic dermatitis, allergic asthma, anaphylaxis', 'No', '2345', '2345', '2345', '', '', 'test0002.jpg', '', '', '', NULL, '2020-07-06 11:29:19', '2020-07-28 06:11:11'),
(42, 'Rajesh Kumar', 0, 3, 'rajesh@gmail.com', '$2y$10$Xy/7foN390AZAaw/JkcmyefhsZrLaDcFbK7i5tUoQSHXRW.xn1MQW', 0, '[\"manage_visit_requests\",\"visit_pool\",\"availability\",\"my_account\"]', 'Rajesh', 'Kumar', 'uploads/users/team4.png', '', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, '', NULL, '', '', '', '', '', '', NULL, '', '', NULL, NULL, '2020-09-02 05:11:48', '2020-09-02 05:11:48'),
(43, 'Atish Kumar', 0, 3, 'atish@gmail.com', '$2y$10$926pY/OWGlGrTRSRxU851.EnhenmLZwwjsGy11DYngWN3STcBIx6m', 0, '[\"manage_visit_requests\",\"visit_pool\",\"availability\",\"my_account\"]', 'Atish', 'Kumar', 'uploads/users/team1.png', '', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, '', NULL, '', '', '', '', '', '', NULL, '', '', NULL, NULL, '2020-09-02 05:46:50', '2020-09-02 05:46:50'),
(44, 'Dr. Krish Sinha', 0, 3, 'krish.md100@gmail.com', '$2y$10$ve6F6BRZZpu9AGSmpdNIiO9h4ReV.IWc8uxqTje92P4weqzK6Cd4i', 0, '[\"manage_visit_requests\",\"visit_pool\",\"availability\",\"my_account\"]', 'Dr. Krish', 'Sinha', 'uploads/users/team4.png', '', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, '', NULL, '', '', '', '', '', '', NULL, '', '', NULL, NULL, '2020-09-02 05:49:19', '2020-09-02 05:49:19'),
(46, 'Ayaz Khan', 0, 4, 'user@gmail.com', '$2y$10$NFBQh6RkAWhIx3PF5MuFwu2WDYcanHTI8GPT2/43fsdDW2JFMTnr.', 1, '', 'Ayaz', 'Khan', NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, '', NULL, '', '', '', '', '', '', NULL, '', '', NULL, NULL, '2020-10-29 05:27:24', '2020-10-29 05:27:24'),
(47, 'Ayaz Khan', 0, 4, 'user2@gmail.com', '$2y$10$HxJasvFKbZaj28wPDeBCR.nTx0BxG6b.Pk1oS1q6fR1QXvtAq04/q', 1, '', 'Ayaz', 'Khan', NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, '', NULL, '', '', '', '', '', '', NULL, '', '', NULL, NULL, '2020-10-29 05:37:08', '2020-10-29 05:37:08'),
(48, 'Sandeep Shukla', 0, 4, 'user10@gmail.com', '$2y$10$0jVoy2L0V5YODynujZicPuKo3EuZt6RGhcrtAICI9RMaB1X3eEywu', 1, '', 'Sandeep', 'Shukla', NULL, '8328328321', NULL, 23231, 'male', '2002-01-01', NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, '', NULL, '', '', '', '', '', '', NULL, '', '', NULL, NULL, '2020-10-29 06:43:10', '2020-10-29 09:09:19'),
(49, 'Jenny Roy', 0, 4, 'jennyroy@gmail.com', '$2y$10$pp02FwqwrBz6xAoSsat5ceNKcku/Tzf97OKVfiZWw/MsSzecrNyLO', 1, '', 'Jenny', 'Roy', NULL, '8328328321', NULL, 34323, 'female', '2002-01-01', NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, '', NULL, '', '', '', '', '', '', NULL, '', '', NULL, NULL, '2020-10-31 04:55:51', '2020-10-31 05:01:11'),
(50, 'Raja Dubey', 0, 4, 'raja1234@yopmail.com', '$2y$10$/bWgKPA2ZbU09nyqhcAJbOd349ZnNypcTmU.tc.4USCVZ6FZ0AvEa', 1, '', 'Raja', 'Dubey', NULL, '8325330201', NULL, 23123, 'male', '2001-06-07', NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, '', NULL, '', '', '', '', '', '', NULL, '', '', NULL, NULL, '2020-10-31 05:21:01', '2020-10-31 05:21:37'),
(51, 'Ayaz Khan', 0, 4, 'ayazkhan@gmail.com', '$2y$10$pZQMDDfOADkwLzgTdCr4hOtV8g0QO7T5PCpf2R2CZzQlTuvmTgJM2', 1, '', 'Ayaz', 'Khan', NULL, '8328328321', NULL, 23456, 'male', '1999-02-10', NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, '', NULL, '', '', '', '', '', '', NULL, '', '', NULL, NULL, '2020-11-02 00:59:20', '2020-11-02 05:44:24'),
(52, 'Aman Khan', 0, 4, 'amankhan@gmail.com', '$2y$10$BqdqmYsFSGpR/8jMMONiG.rBFLmJXRrHhg8HvbgFzsBx3ZZX3/nhm', 1, '', 'Aman', 'Khan', NULL, '1234567890', NULL, 12345, 'male', '2001-09-01', NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, '', NULL, '', '', '', '', '', '', NULL, '', '', NULL, NULL, '2020-11-09 01:45:32', '2020-11-09 01:45:55'),
(53, 'Rajat Sharma', 0, 4, 'rajatsharma@gmail.com', '$2y$10$qZ8FwyrN43ereG4BNa.6.eHyqCrIIodd9tdzonyNcMFyzd/ZS70RO', 1, '', 'Rajat', 'Sharma', NULL, '1234567890', NULL, 12345, 'male', '2002-01-01', NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, '', NULL, '', '', '', '', '', '', NULL, '', '', NULL, NULL, '2020-11-11 00:57:32', '2020-11-11 04:27:50'),
(54, 'Dr. Gaby', 0, 3, 'provider@gmail.com', '$2y$10$wHTlxAEtvUol5RrmBhsWF.GApk.Calxblr9IGp77V6RpnJ/R04Kp6', 1, '[\"manage_visit_requests\",\"visit_pool\",\"availability\",\"my_account\"]', '', '', 'uploads/users/team2.png', '7898546525', '333 Washington Avenue North, Suite 300 - 9033 Minneapolis, MN 55401, USA', NULL, 'male', '1992-03-08', 28, 'Dr. Gaby is a board-certified Family Physician and a Functional Medicine Practitioner certified through the Institute for Functional Medicine. She graduated cum laude with a BA in biology from the University of Delaware, attended Jefferson Medical College, completed her residency at Crozer-Chester Medical Center, and served in the Medical Corps of the US Navy.', NULL, NULL, NULL, 0, 0, 0, NULL, '1,2', NULL, '', NULL, '', '58978456', '4526-DB-45672', '579864', 'Doctor', '985469874565E', '[\"uploads\\/licenses\\/license1001.jpg\",\"uploads\\/licenses\\/license1002.jpg\",\"uploads\\/licenses\\/license_3.jpg\",\"uploads\\/licenses\\/license_4.jpg\",\"uploads\\/licenses\\/license_5.png\",\"uploads\\/licenses\\/license_6.jpg\"]', '8975445670', 'F91234563', '333 Washington Avenue North, USA', NULL, '2020-07-04 02:23:12', '2020-08-24 03:05:07'),
(55, 'Amit choudhary', 0, 4, 'amit@gmail.com', '$2y$10$ggKXUHC4IkQXicdBlWZG2OQWqQ3BSDwBvEsHrwTjksqzzg560X/Gq', 1, '', 'Amit', 'choudhary', NULL, '9809809801', NULL, 12345, 'male', '2002-01-01', NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, '', NULL, '', '', '', '', '', '', NULL, '', '', NULL, NULL, '2020-11-23 00:48:59', '2020-11-23 00:58:48'),
(56, 'Raj Kumar', 0, 4, 'rajkumar@gmail.com', '$2y$10$ptatQEuEGcEnBzqj5/78LuxJuNTfoX4HA8qY1/olUUGk46mByE3su', 1, '', 'Raj', 'Kumar', NULL, '8320988678', NULL, 78358, 'male', '2002-11-26', NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, '', NULL, '', '', '', '', '', '', NULL, '', '', NULL, NULL, '2020-11-23 04:04:34', '2020-11-26 03:41:09'),
(57, 'Test test', 0, 4, 'subadmin33@gmail.com', '$2y$10$pNmfjxoShW8OA8NmxtNnB.uuTBeZhQosFl74gML8P9Sqf2V.oelvC', 1, '', 'Test', 'test', NULL, '8325423423', NULL, 75050, 'male', '2002-12-14', NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, '', NULL, '', '', '', '', '', '', NULL, '', '', NULL, NULL, '2020-12-14 00:38:32', '2020-12-14 00:39:10'),
(58, 'Kunal Singh', 0, 4, 'kunalsingh@gmail.com', '$2y$10$/xg4GvzUleis7nIilSa.Lu4RJrSzsEjnqOcFRMcbmoWXNKK5hQuB.', 1, '', 'Kunal', 'Singh', NULL, '8332343242', NULL, 75006, 'male', '1998-01-01', NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, '', NULL, '', '', '', '', '', '', NULL, '', '', NULL, NULL, '2020-12-18 05:11:53', '2020-12-18 05:12:46'),
(59, 'Vishal Shukla', 0, 4, 'patient1@gmail.com', '$2y$10$yQOmMWPtRDFT0kLhnRwOPON2mzMO7SUrjO/T2pSj.QgQ0PWK/ICzK', 1, '', 'Vishal', 'Shukla', NULL, '8325432432', NULL, 75501, 'male', '2002-09-06', NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, '', NULL, '', '', '', '', '', '', NULL, '', '', NULL, NULL, '2020-12-19 01:06:31', '2020-12-19 01:07:44'),
(60, 'Ajay Singh', 0, 4, 'ajay@gmail.com', '$2y$10$5SOBWX2bVE.5JBwjIp3GUuX122Mbkbmf1RtNwlWxl9kpe4zFOYNn2', 1, '', 'Ajay', 'Singh', NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, '', NULL, '', '', '', '', '', '', NULL, '', '', NULL, NULL, '2020-12-21 05:28:20', '2020-12-21 05:28:20'),
(61, 'Aditya Shukla', 0, 4, 'aditya@gmail.com', '$2y$10$WqMyn1pQ5pNdAuB4xb.AVemfubbp7Wr6jQsABPPPYU1P6krjsJek2', 1, '', 'Aditya', 'Shukla', NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, '', NULL, '', '', '', '', '', '', NULL, '', '', NULL, NULL, '2020-12-21 05:32:24', '2020-12-21 05:32:24'),
(62, 'Avinash Shukla', 0, 4, 'avinash@gmail.com', '$2y$10$iYuThfsBkcmtmILC9IlQGuL79s3v4AYcoimodu/0w8popESL6q80.', 1, '', 'Avinash', 'Shukla', NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, '', NULL, '', '', '', '', '', '', NULL, '', '', NULL, NULL, '2020-12-21 05:32:59', '2020-12-21 05:32:59');

-- --------------------------------------------------------

--
-- Table structure for table `visit_answers`
--

CREATE TABLE `visit_answers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(10) NOT NULL,
  `service_id` int(10) NOT NULL,
  `online_visit_form_id` int(10) NOT NULL DEFAULT 0,
  `question_id` int(10) NOT NULL,
  `answer_type` enum('text','option') COLLATE utf8mb4_unicode_ci DEFAULT 'text',
  `answer_text` text COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'only for text answer',
  `answer_option_id` int(10) DEFAULT NULL COMMENT 'only for option type answer',
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `visit_answers`
--

INSERT INTO `visit_answers` (`id`, `user_id`, `service_id`, `online_visit_form_id`, `question_id`, `answer_type`, `answer_text`, `answer_option_id`, `status`, `created_at`, `updated_at`) VALUES
(10, 55, 2, 4, 1, 'option', NULL, 2, 1, '2020-11-23 03:52:44', '2020-11-23 03:52:44'),
(11, 55, 2, 4, 2, 'option', NULL, 8, 1, '2020-11-23 03:52:44', '2020-11-23 03:52:44'),
(12, 55, 2, 4, 3, 'option', NULL, 11, 1, '2020-11-23 03:52:44', '2020-11-23 03:52:44'),
(13, 55, 2, 4, 5, 'option', NULL, 19, 1, '2020-11-23 03:52:44', '2020-11-23 03:52:44'),
(14, 55, 2, 4, 6, 'option', NULL, 24, 1, '2020-11-23 03:52:44', '2020-11-23 03:52:44'),
(15, 55, 2, 4, 7, 'option', NULL, 27, 1, '2020-11-23 03:52:44', '2020-11-23 03:52:44'),
(16, 55, 2, 4, 9, 'option', NULL, 31, 1, '2020-11-23 03:52:44', '2020-11-23 03:52:44'),
(17, 55, 2, 4, 8, 'text', 'dont know', NULL, 1, '2020-11-23 03:52:44', '2020-11-23 03:52:44'),
(18, 56, 5, 6, 16, 'option', NULL, 39, 1, '2020-11-23 07:21:21', '2020-11-23 07:21:21'),
(19, 56, 5, 6, 14, 'text', 'Rajkumar', NULL, 1, '2020-11-23 07:21:21', '2020-11-23 07:21:21'),
(20, 56, 5, 6, 15, 'text', '14/3, Manhattan, Newyork', NULL, 1, '2020-11-23 07:21:21', '2020-11-23 07:21:21'),
(21, 56, 5, 6, 18, 'text', 'Next Year', NULL, 1, '2020-11-23 07:21:21', '2020-11-23 07:21:21'),
(22, 57, 2, 4, 1, 'option', NULL, 1, 1, '2020-12-14 00:42:13', '2020-12-14 00:42:13'),
(23, 57, 2, 4, 2, 'option', NULL, 6, 1, '2020-12-14 00:42:13', '2020-12-14 00:42:13'),
(24, 57, 2, 4, 3, 'option', NULL, 11, 1, '2020-12-14 00:42:13', '2020-12-14 00:42:13'),
(25, 57, 2, 4, 5, 'option', NULL, 19, 1, '2020-12-14 00:42:13', '2020-12-14 00:42:13'),
(26, 57, 2, 4, 6, 'option', NULL, 23, 1, '2020-12-14 00:42:13', '2020-12-14 00:42:13'),
(27, 57, 2, 4, 7, 'option', NULL, 28, 1, '2020-12-14 00:42:13', '2020-12-14 00:42:13'),
(28, 57, 2, 4, 9, 'option', NULL, 31, 1, '2020-12-14 00:42:13', '2020-12-14 00:42:13'),
(29, 58, 2, 4, 1, 'option', NULL, 1, 1, '2020-12-18 05:13:06', '2020-12-18 05:13:06'),
(30, 58, 2, 4, 2, 'option', NULL, 7, 1, '2020-12-18 05:13:06', '2020-12-18 05:13:06'),
(31, 58, 2, 4, 3, 'option', NULL, 11, 1, '2020-12-18 05:13:06', '2020-12-18 05:13:06'),
(32, 58, 2, 4, 5, 'option', NULL, 20, 1, '2020-12-18 05:13:06', '2020-12-18 05:13:06'),
(33, 58, 2, 4, 6, 'option', NULL, 26, 1, '2020-12-18 05:13:06', '2020-12-18 05:13:06'),
(34, 58, 2, 4, 7, 'option', NULL, 29, 1, '2020-12-18 05:13:06', '2020-12-18 05:13:06'),
(35, 58, 2, 4, 9, 'option', NULL, 31, 1, '2020-12-18 05:13:06', '2020-12-18 05:13:06'),
(36, 59, 2, 4, 1, 'option', NULL, 1, 1, '2020-12-19 01:09:12', '2020-12-19 01:09:12'),
(37, 59, 2, 4, 2, 'option', NULL, 9, 1, '2020-12-19 01:09:12', '2020-12-19 01:09:12'),
(38, 59, 2, 4, 3, 'option', NULL, 12, 1, '2020-12-19 01:09:12', '2020-12-19 01:09:12'),
(39, 59, 2, 4, 6, 'option', NULL, 25, 1, '2020-12-19 01:09:12', '2020-12-19 01:09:12'),
(40, 59, 2, 4, 7, 'option', NULL, 28, 1, '2020-12-19 01:09:12', '2020-12-19 01:09:12'),
(41, 59, 2, 4, 9, 'option', NULL, 31, 1, '2020-12-19 01:09:12', '2020-12-19 01:09:12');

-- --------------------------------------------------------

--
-- Table structure for table `visit_forms`
--

CREATE TABLE `visit_forms` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `service_id` int(10) NOT NULL DEFAULT 0,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `short_info` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instructions` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `visit_forms`
--

INSERT INTO `visit_forms` (`id`, `service_id`, `name`, `short_info`, `instructions`, `status`, `created_at`, `updated_at`) VALUES
(4, 2, 'ED (Erectile Dysfunction) Consultation', 'ED Questions', 'ED Questions', 1, '2020-10-27 05:34:44', '2020-12-14 00:51:11'),
(5, 1, 'General Test Form', 'TEST', 'TEST', 1, '2020-10-27 05:44:31', '2020-11-23 05:24:14'),
(6, 5, 'Botox Visit Form', 'Botox Visit Form', 'Botox Visit Form', 1, '2020-11-23 05:29:37', '2020-11-23 05:29:37');

-- --------------------------------------------------------

--
-- Table structure for table `visit_form_field_types`
--

CREATE TABLE `visit_form_field_types` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `field_type_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `short_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `icon` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `visit_form_field_types`
--

INSERT INTO `visit_form_field_types` (`id`, `field_type_name`, `short_name`, `icon`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Short Text Question', 'short_text', 'file-text', 1, '2020-08-23 18:30:00', '2020-08-23 18:30:00'),
(2, 'Long Text Question', 'long_text', 'file-plus', 1, '2020-08-23 18:30:00', '2020-08-23 18:30:00'),
(3, 'Yes/No Question', 'yes_no', 'thumbs-up', 1, '2020-08-23 18:30:00', '2020-08-23 18:30:00'),
(4, 'Single Choice Question', 'single', 'stop-circle', 1, '2020-08-23 18:30:00', '2020-08-23 18:30:00'),
(5, 'Multiple Choice Question', 'multiple', 'check-square', 1, '2020-08-23 18:30:00', '2020-08-23 18:30:00');

-- --------------------------------------------------------

--
-- Table structure for table `visit_form_patient_details`
--

CREATE TABLE `visit_form_patient_details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `visit_form_id` int(10) NOT NULL DEFAULT 0,
  `first_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `last_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `gender` enum('male','female','','') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'male',
  `birth_date` date NOT NULL,
  `zip_code` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_no` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `visit_questions`
--

CREATE TABLE `visit_questions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `visit_form_id` int(10) NOT NULL DEFAULT 0,
  `question_type` enum('short_text','long_text','yes_no','single','multiple') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `question_text` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_parent` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'if 1 then it is parent of other question',
  `parent_id` int(10) DEFAULT NULL COMMENT 'if 0 then NOT child',
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `visit_questions`
--

INSERT INTO `visit_questions` (`id`, `visit_form_id`, `question_type`, `question_text`, `is_parent`, `parent_id`, `status`, `created_at`, `updated_at`) VALUES
(1, 4, 'single', 'Do you ever have a problem getting or maintaining an erection that is satisfying enough for sex?', 0, NULL, 1, '2020-10-27 05:33:55', '2020-10-27 05:39:51'),
(2, 4, 'single', 'How did your ED begin? Select the one that best describes your ED.', 0, NULL, 1, '2020-10-27 05:33:55', '2020-10-27 05:39:51'),
(3, 4, 'multiple', 'Do you get erections...', 1, NULL, 1, '2020-10-27 05:33:55', '2020-10-27 05:39:51'),
(4, 4, 'single', 'When masturbating, does your erection remain hard until orgasm or as long as you would like?', 0, 3, 1, '2020-10-27 05:33:55', '2020-10-27 05:39:51'),
(5, 4, 'single', 'How often do you wake up with an erection?', 0, 3, 1, '2020-10-27 05:33:55', '2020-10-27 05:39:51'),
(6, 4, 'single', 'In the past month, how satisfied have you been with your sex life?', 0, NULL, 1, '2020-10-27 05:33:55', '2020-10-27 05:39:51'),
(7, 4, 'single', 'How would you describe your sex drive over the past year?', 1, NULL, 1, '2020-10-27 05:33:55', '2020-10-27 05:39:51'),
(8, 4, 'long_text', 'Tell us why you think your sex drive has been low.', 0, 7, 1, '2020-10-27 05:33:55', '2020-10-27 05:39:51'),
(9, 4, 'yes_no', 'Have you ever been formally treated for ED or tried any medicines, vitamins, or supplements to treat it?', 0, NULL, 1, '2020-10-27 05:33:55', '2020-10-27 05:39:51'),
(10, 5, 'short_text', 'Question 1', 0, NULL, 1, '2020-11-23 04:02:50', '2020-11-23 04:02:50'),
(11, 5, 'yes_no', 'We want some details, are you ready?', 1, NULL, 1, '2020-11-23 04:02:50', '2020-11-23 04:02:50'),
(12, 5, 'short_text', 'Tell me your name?', 0, 1, 1, '2020-11-23 04:02:50', '2020-11-23 04:02:50'),
(13, 5, 'single', 'Why did you choose \'NO\'?', 0, 1, 1, '2020-11-23 04:02:50', '2020-11-23 04:02:50'),
(14, 6, 'short_text', 'What is your good name?', 1, NULL, 1, '2020-11-23 06:58:04', '2020-11-23 06:58:04'),
(15, 6, 'long_text', 'Tell me your address.', 1, NULL, 1, '2020-11-23 06:58:04', '2020-11-23 06:58:04'),
(16, 6, 'yes_no', 'Are you married?', 1, NULL, 1, '2020-11-23 06:58:04', '2020-11-23 06:58:04'),
(17, 6, 'single', 'How long your marriage life is?', 0, 2, 1, '2020-11-23 06:58:04', '2020-11-23 06:58:04'),
(18, 6, 'short_text', 'When will you be married?', 0, 2, 1, '2020-11-23 06:58:04', '2020-11-23 06:58:04');

-- --------------------------------------------------------

--
-- Table structure for table `zip_codes`
--

CREATE TABLE `zip_codes` (
  `id` bigint(20) NOT NULL,
  `zipcode` char(5) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `state` varchar(50) DEFAULT NULL,
  `state_abbr` char(2) DEFAULT NULL,
  `county_area` varchar(50) DEFAULT NULL,
  `code` char(3) DEFAULT NULL,
  `latitude` double(15,5) DEFAULT NULL,
  `longitude` double(15,5) DEFAULT NULL,
  `some_field` tinyint(4) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `zip_codes`
--

INSERT INTO `zip_codes` (`id`, `zipcode`, `city`, `state`, `state_abbr`, `county_area`, `code`, `latitude`, `longitude`, `some_field`, `status`, `created_at`, `updated_at`) VALUES
(1, '75763', 'Frankston', 'Texas', 'TX', 'Anderson', '001', 32.05350, -95.51630, 4, 1, NULL, NULL),
(2, '75779', 'Neches', 'Texas', 'TX', 'Anderson', '001', 31.86680, -95.49580, 4, 1, NULL, NULL),
(3, '75801', 'Palestine', 'Texas', 'TX', 'Anderson', '001', 31.75880, -95.63420, 4, 1, NULL, NULL),
(4, '75802', 'Palestine', 'Texas', 'TX', 'Anderson', '001', 31.76210, -95.63080, 4, 1, NULL, NULL),
(5, '75803', 'Palestine', 'Texas', 'TX', 'Anderson', '001', 31.75710, -95.65450, 4, 1, NULL, NULL),
(6, '75832', 'Cayuga', 'Texas', 'TX', 'Anderson', '001', 31.95710, -95.97470, 4, 1, NULL, NULL),
(7, '75839', 'Elkhart', 'Texas', 'TX', 'Anderson', '001', 31.64800, -95.55510, 4, 1, NULL, NULL),
(8, '75853', 'Montalba', 'Texas', 'TX', 'Anderson', '001', 31.87660, -95.73270, 4, 1, NULL, NULL),
(9, '75861', 'Tennessee Colony', 'Texas', 'TX', 'Anderson', '001', 31.79290, -95.89980, 4, 1, NULL, NULL),
(10, '75880', 'Tennessee Colony', 'Texas', 'TX', 'Anderson', '001', 31.83540, -95.83890, 4, 1, NULL, NULL),
(11, '75882', 'Palestine', 'Texas', 'TX', 'Anderson', '001', 31.79420, -95.66200, 4, 1, NULL, NULL),
(12, '75884', 'Tennessee Colony', 'Texas', 'TX', 'Anderson', '001', 31.83540, -95.83890, 4, 1, NULL, NULL),
(13, '75886', 'Tennessee Colony', 'Texas', 'TX', 'Anderson', '001', 31.83540, -95.83890, 4, 1, NULL, NULL),
(14, '79714', 'Andrews', 'Texas', 'TX', 'Andrews', '003', 32.32010, -102.54090, 4, 1, NULL, NULL),
(15, '75901', 'Lufkin', 'Texas', 'TX', 'Angelina', '005', 31.27000, -94.64690, 4, 1, NULL, NULL),
(16, '75902', 'Lufkin', 'Texas', 'TX', 'Angelina', '005', 31.36230, -94.76110, 4, 1, NULL, NULL),
(17, '75903', 'Lufkin', 'Texas', 'TX', 'Angelina', '005', 31.27660, -94.56760, 4, 1, NULL, NULL),
(18, '75904', 'Lufkin', 'Texas', 'TX', 'Angelina', '005', 31.34890, -94.83270, 4, 1, NULL, NULL),
(19, '75915', 'Lufkin', 'Texas', 'TX', 'Angelina', '005', 31.33820, -94.72910, 4, 1, NULL, NULL),
(20, '75941', 'Diboll', 'Texas', 'TX', 'Angelina', '005', 31.19500, -94.77290, 4, 1, NULL, NULL),
(21, '75949', 'Huntington', 'Texas', 'TX', 'Angelina', '005', 31.28370, -94.56620, 4, 1, NULL, NULL),
(22, '75969', 'Pollok', 'Texas', 'TX', 'Angelina', '005', 31.42910, -94.82540, 4, 1, NULL, NULL),
(23, '75980', 'Zavalla', 'Texas', 'TX', 'Angelina', '005', 31.15690, -94.38710, 4, 1, NULL, NULL),
(24, '78358', 'Fulton', 'Texas', 'TX', 'Aransas', '007', 28.08650, -97.04160, 4, 1, NULL, NULL),
(25, '78381', 'Rockport', 'Texas', 'TX', 'Aransas', '007', 28.01310, -97.09360, 4, 1, NULL, NULL),
(26, '78382', 'Rockport', 'Texas', 'TX', 'Aransas', '007', 28.03080, -97.06880, 4, 1, NULL, NULL),
(27, '76351', 'Archer City', 'Texas', 'TX', 'Archer', '009', 33.55620, -98.62490, 4, 1, NULL, NULL),
(28, '76366', 'Holliday', 'Texas', 'TX', 'Archer', '009', 33.81620, -98.69510, 4, 1, NULL, NULL),
(29, '76370', 'Megargel', 'Texas', 'TX', 'Archer', '009', 33.45330, -98.92970, 4, 1, NULL, NULL),
(30, '76379', 'Scotland', 'Texas', 'TX', 'Archer', '009', 33.65350, -98.46500, 4, 1, NULL, NULL),
(31, '76389', 'Windthorst', 'Texas', 'TX', 'Archer', '009', 33.57960, -98.43760, 4, 1, NULL, NULL),
(32, '79019', 'Claude', 'Texas', 'TX', 'Armstrong', '011', 35.09670, -101.38130, 4, 1, NULL, NULL),
(33, '79094', 'Wayside', 'Texas', 'TX', 'Armstrong', '011', 34.96530, -101.35780, 4, 1, NULL, NULL),
(34, '78008', 'Campbellton', 'Texas', 'TX', 'Atascosa', '013', 28.76700, -98.25660, 4, 1, NULL, NULL),
(35, '78011', 'Charlotte', 'Texas', 'TX', 'Atascosa', '013', 28.86490, -98.70700, 4, 1, NULL, NULL),
(36, '78012', 'Christine', 'Texas', 'TX', 'Atascosa', '013', 28.78580, -98.48860, 4, 1, NULL, NULL),
(37, '78026', 'Jourdanton', 'Texas', 'TX', 'Atascosa', '013', 28.90300, -98.54400, 4, 1, NULL, NULL),
(38, '78050', 'Leming', 'Texas', 'TX', 'Atascosa', '013', 29.07300, -98.48420, 4, 1, NULL, NULL),
(39, '78052', 'Lytle', 'Texas', 'TX', 'Atascosa', '013', 29.23660, -98.79450, 4, 1, NULL, NULL),
(40, '78062', 'Peggy', 'Texas', 'TX', 'Atascosa', '013', 28.73970, -98.17860, 4, 1, NULL, NULL),
(41, '78064', 'Pleasanton', 'Texas', 'TX', 'Atascosa', '013', 28.99240, -98.48310, 4, 1, NULL, NULL),
(42, '78065', 'Poteet', 'Texas', 'TX', 'Atascosa', '013', 29.09940, -98.62410, 4, 1, NULL, NULL),
(43, '77418', 'Bellville', 'Texas', 'TX', 'Austin', '015', 29.96580, -96.25310, 4, 1, NULL, NULL),
(44, '77452', 'Kenney', 'Texas', 'TX', 'Austin', '015', 30.04770, -96.32690, 4, 1, NULL, NULL),
(45, '77473', 'San Felipe', 'Texas', 'TX', 'Austin', '015', 29.80190, -96.10140, 4, 1, NULL, NULL),
(46, '77474', 'Sealy', 'Texas', 'TX', 'Austin', '015', 29.78260, -96.15920, 4, 1, NULL, NULL),
(47, '77485', 'Wallis', 'Texas', 'TX', 'Austin', '015', 29.63970, -96.04560, 4, 1, NULL, NULL),
(48, '78931', 'Bleiblerville', 'Texas', 'TX', 'Austin', '015', 30.02920, -96.44480, 4, 1, NULL, NULL),
(49, '78933', 'Cat Spring', 'Texas', 'TX', 'Austin', '015', 29.75120, -96.39000, 4, 1, NULL, NULL),
(50, '78944', 'Industry', 'Texas', 'TX', 'Austin', '015', 29.99040, -96.48430, 4, 1, NULL, NULL),
(51, '78950', 'New Ulm', 'Texas', 'TX', 'Austin', '015', 29.91340, -96.49610, 4, 1, NULL, NULL),
(52, '79324', 'Enochs', 'Texas', 'TX', 'Bailey', '017', 33.87310, -102.75990, 4, 1, NULL, NULL),
(53, '79344', 'Maple', 'Texas', 'TX', 'Bailey', '017', 33.84870, -102.89850, 4, 1, NULL, NULL),
(54, '79347', 'Muleshoe', 'Texas', 'TX', 'Bailey', '017', 34.21930, -102.74960, 4, 1, NULL, NULL),
(55, '78003', 'Bandera', 'Texas', 'TX', 'Bandera', '019', 29.72760, -99.04530, 4, 1, NULL, NULL),
(56, '78055', 'Medina', 'Texas', 'TX', 'Bandera', '019', 29.84750, -99.32150, 4, 1, NULL, NULL),
(57, '78063', 'Pipe Creek', 'Texas', 'TX', 'Bandera', '019', 29.67960, -98.94840, 4, 1, NULL, NULL),
(58, '78883', 'Tarpley', 'Texas', 'TX', 'Bandera', '019', 29.64550, -99.24690, 4, 1, NULL, NULL),
(59, '78885', 'Vanderpool', 'Texas', 'TX', 'Bandera', '019', 29.74520, -99.55510, 4, 1, NULL, NULL),
(60, '78602', 'Bastrop', 'Texas', 'TX', 'Bastrop', '021', 30.13880, -97.29210, 4, 1, NULL, NULL),
(61, '78612', 'Cedar Creek', 'Texas', 'TX', 'Bastrop', '021', 30.09660, -97.49760, 4, 1, NULL, NULL),
(62, '78621', 'Elgin', 'Texas', 'TX', 'Bastrop', '021', 30.32310, -97.37370, 4, 1, NULL, NULL),
(63, '78650', 'Mc Dade', 'Texas', 'TX', 'Bastrop', '021', 30.29680, -97.23860, 1, 1, NULL, NULL),
(64, '78659', 'Paige', 'Texas', 'TX', 'Bastrop', '021', 30.18580, -97.11980, 4, 1, NULL, NULL),
(65, '78662', 'Red Rock', 'Texas', 'TX', 'Bastrop', '021', 29.99060, -97.40810, 4, 1, NULL, NULL),
(66, '78953', 'Rosanky', 'Texas', 'TX', 'Bastrop', '021', 29.92410, -97.31190, 4, 1, NULL, NULL),
(67, '78957', 'Smithville', 'Texas', 'TX', 'Bastrop', '021', 30.01780, -97.14940, 4, 1, NULL, NULL),
(68, '76380', 'Seymour', 'Texas', 'TX', 'Baylor', '023', 33.59140, -99.25870, 4, 1, NULL, NULL),
(69, '78102', 'Beeville', 'Texas', 'TX', 'Bee', '025', 28.42220, -97.76160, 4, 1, NULL, NULL),
(70, '78104', 'Beeville', 'Texas', 'TX', 'Bee', '025', 28.39310, -97.77600, 4, 1, NULL, NULL),
(71, '78125', 'Mineral', 'Texas', 'TX', 'Bee', '025', 28.55520, -97.94070, 4, 1, NULL, NULL),
(72, '78142', 'Normanna', 'Texas', 'TX', 'Bee', '025', 28.52780, -97.78310, 4, 1, NULL, NULL),
(73, '78145', 'Pawnee', 'Texas', 'TX', 'Bee', '025', 28.65260, -97.99210, 4, 1, NULL, NULL),
(74, '78146', 'Pettus', 'Texas', 'TX', 'Bee', '025', 28.61640, -97.80820, 4, 1, NULL, NULL),
(75, '78162', 'Tuleta', 'Texas', 'TX', 'Bee', '025', 28.57080, -97.79720, 4, 1, NULL, NULL),
(76, '78389', 'Skidmore', 'Texas', 'TX', 'Bee', '025', 28.21210, -97.71270, 4, 1, NULL, NULL),
(77, '78391', 'Tynan', 'Texas', 'TX', 'Bee', '025', 28.16930, -97.75490, 4, 1, NULL, NULL),
(78, '76501', 'Temple', 'Texas', 'TX', 'Bell', '027', 31.08950, -97.33430, 4, 1, NULL, NULL),
(79, '76502', 'Temple', 'Texas', 'TX', 'Bell', '027', 31.07100, -97.38980, 4, 1, NULL, NULL),
(80, '76503', 'Temple', 'Texas', 'TX', 'Bell', '027', 31.05400, -97.32030, 4, 1, NULL, NULL),
(81, '76504', 'Temple', 'Texas', 'TX', 'Bell', '027', 31.09170, -97.36480, 4, 1, NULL, NULL),
(82, '76508', 'Temple', 'Texas', 'TX', 'Bell', '027', 31.03630, -97.49200, 4, 1, NULL, NULL),
(83, '76511', 'Bartlett', 'Texas', 'TX', 'Bell', '027', 30.79910, -97.42630, 4, 1, NULL, NULL),
(84, '76513', 'Belton', 'Texas', 'TX', 'Bell', '027', 31.07230, -97.47200, 4, 1, NULL, NULL),
(85, '76533', 'Heidenheimer', 'Texas', 'TX', 'Bell', '027', 31.04960, -97.49360, 4, 1, NULL, NULL),
(86, '76534', 'Holland', 'Texas', 'TX', 'Bell', '027', 30.88000, -97.38570, 4, 1, NULL, NULL),
(87, '76540', 'Killeen', 'Texas', 'TX', 'Bell', '027', 31.11710, -97.72780, 4, 1, NULL, NULL),
(88, '76541', 'Killeen', 'Texas', 'TX', 'Bell', '027', 31.11640, -97.72780, 4, 1, NULL, NULL),
(89, '76542', 'Killeen', 'Texas', 'TX', 'Bell', '027', 31.03760, -97.68090, 4, 1, NULL, NULL),
(90, '76543', 'Killeen', 'Texas', 'TX', 'Bell', '027', 31.11720, -97.66510, 4, 1, NULL, NULL),
(91, '76544', 'Killeen', 'Texas', 'TX', 'Bell', '027', 31.12820, -97.74690, 4, 1, NULL, NULL),
(92, '76547', 'Killeen', 'Texas', 'TX', 'Bell', '027', 31.11710, -97.72780, 4, 1, NULL, NULL),
(93, '76548', 'Harker Heights', 'Texas', 'TX', 'Bell', '027', 31.02860, -97.61150, 4, 1, NULL, NULL),
(94, '76549', 'Killeen', 'Texas', 'TX', 'Bell', '027', 31.00650, -97.84100, 4, 1, NULL, NULL),
(95, '76554', 'Little River', 'Texas', 'TX', 'Bell', '027', 30.96300, -97.36160, 4, 1, NULL, NULL),
(96, '76559', 'Nolanville', 'Texas', 'TX', 'Bell', '027', 31.08330, -97.59410, 4, 1, NULL, NULL),
(97, '76564', 'Pendleton', 'Texas', 'TX', 'Bell', '027', 31.19520, -97.34860, 4, 1, NULL, NULL),
(98, '76569', 'Rogers', 'Texas', 'TX', 'Bell', '027', 30.95500, -97.22280, 4, 1, NULL, NULL),
(99, '76571', 'Salado', 'Texas', 'TX', 'Bell', '027', 30.94940, -97.53300, 4, 1, NULL, NULL),
(100, '76579', 'Troy', 'Texas', 'TX', 'Bell', '027', 31.17590, -97.28520, 4, 1, NULL, NULL),
(101, '78002', 'Atascosa', 'Texas', 'TX', 'Bexar', '029', 29.27050, -98.74210, 4, 1, NULL, NULL),
(102, '78015', 'Boerne', 'Texas', 'TX', 'Bexar', '029', 29.73270, -98.66460, 4, 1, NULL, NULL),
(103, '78023', 'Helotes', 'Texas', 'TX', 'Bexar', '029', 29.63210, -98.75420, 4, 1, NULL, NULL),
(104, '78054', 'Macdona', 'Texas', 'TX', 'Bexar', '029', 29.32580, -98.69110, 4, 1, NULL, NULL),
(105, '78069', 'Somerset', 'Texas', 'TX', 'Bexar', '029', 29.22050, -98.66780, 4, 1, NULL, NULL),
(106, '78073', 'Von Ormy', 'Texas', 'TX', 'Bexar', '029', 29.27500, -98.66770, 4, 1, NULL, NULL),
(107, '78101', 'Adkins', 'Texas', 'TX', 'Bexar', '029', 29.38050, -98.26500, 4, 1, NULL, NULL),
(108, '78109', 'Converse', 'Texas', 'TX', 'Bexar', '029', 29.51730, -98.32170, 4, 1, NULL, NULL),
(109, '78112', 'Elmendorf', 'Texas', 'TX', 'Bexar', '029', 29.23080, -98.37200, 4, 1, NULL, NULL),
(110, '78148', 'Universal City', 'Texas', 'TX', 'Bexar', '029', 29.54650, -98.29540, 4, 1, NULL, NULL),
(111, '78150', 'Universal City', 'Texas', 'TX', 'Bexar', '029', 29.43750, -98.46160, 4, 1, NULL, NULL),
(112, '78152', 'Saint Hedwig', 'Texas', 'TX', 'Bexar', '029', 29.43530, -98.19520, 4, 1, NULL, NULL),
(113, '78201', 'San Antonio', 'Texas', 'TX', 'Bexar', '029', 29.47110, -98.53560, 4, 1, NULL, NULL),
(114, '78202', 'San Antonio', 'Texas', 'TX', 'Bexar', '029', 29.42750, -98.46010, 4, 1, NULL, NULL),
(115, '78203', 'San Antonio', 'Texas', 'TX', 'Bexar', '029', 29.41480, -98.46010, 4, 1, NULL, NULL),
(116, '78204', 'San Antonio', 'Texas', 'TX', 'Bexar', '029', 29.40590, -98.50780, 4, 1, NULL, NULL),
(117, '78205', 'San Antonio', 'Texas', 'TX', 'Bexar', '029', 29.42370, -98.49250, 4, 1, NULL, NULL),
(118, '78206', 'San Antonio', 'Texas', 'TX', 'Bexar', '029', 29.43750, -98.46160, 4, 1, NULL, NULL),
(119, '78207', 'San Antonio', 'Texas', 'TX', 'Bexar', '029', 29.42290, -98.52600, 4, 1, NULL, NULL),
(120, '78208', 'San Antonio', 'Texas', 'TX', 'Bexar', '029', 29.44000, -98.45900, 4, 1, NULL, NULL),
(121, '78209', 'San Antonio', 'Texas', 'TX', 'Bexar', '029', 29.48210, -98.45540, 4, 1, NULL, NULL),
(122, '78210', 'San Antonio', 'Texas', 'TX', 'Bexar', '029', 29.39770, -98.46580, 4, 1, NULL, NULL),
(123, '78211', 'San Antonio', 'Texas', 'TX', 'Bexar', '029', 29.34990, -98.56380, 4, 1, NULL, NULL),
(124, '78212', 'San Antonio', 'Texas', 'TX', 'Bexar', '029', 29.43880, -98.49350, 4, 1, NULL, NULL),
(125, '78213', 'San Antonio', 'Texas', 'TX', 'Bexar', '029', 29.52340, -98.52730, 4, 1, NULL, NULL),
(126, '78214', 'San Antonio', 'Texas', 'TX', 'Bexar', '029', 29.36410, -98.49240, 4, 1, NULL, NULL),
(127, '78215', 'San Antonio', 'Texas', 'TX', 'Bexar', '029', 29.44130, -98.47930, 4, 1, NULL, NULL),
(128, '78216', 'San Antonio', 'Texas', 'TX', 'Bexar', '029', 29.53340, -98.49750, 4, 1, NULL, NULL),
(129, '78217', 'San Antonio', 'Texas', 'TX', 'Bexar', '029', 29.53950, -98.41940, 4, 1, NULL, NULL),
(130, '78218', 'San Antonio', 'Texas', 'TX', 'Bexar', '029', 29.49690, -98.40320, 4, 1, NULL, NULL),
(131, '78219', 'San Antonio', 'Texas', 'TX', 'Bexar', '029', 29.44920, -98.34460, 4, 1, NULL, NULL),
(132, '78220', 'San Antonio', 'Texas', 'TX', 'Bexar', '029', 29.41060, -98.41280, 4, 1, NULL, NULL),
(133, '78221', 'San Antonio', 'Texas', 'TX', 'Bexar', '029', 29.33090, -98.50540, 4, 1, NULL, NULL),
(134, '78222', 'San Antonio', 'Texas', 'TX', 'Bexar', '029', 29.38310, -98.39600, 4, 1, NULL, NULL),
(135, '78223', 'San Antonio', 'Texas', 'TX', 'Bexar', '029', 29.35790, -98.43560, 4, 1, NULL, NULL),
(136, '78224', 'San Antonio', 'Texas', 'TX', 'Bexar', '029', 29.33740, -98.53930, 4, 1, NULL, NULL),
(137, '78225', 'San Antonio', 'Texas', 'TX', 'Bexar', '029', 29.38750, -98.52450, 4, 1, NULL, NULL),
(138, '78226', 'San Antonio', 'Texas', 'TX', 'Bexar', '029', 29.39300, -98.55110, 4, 1, NULL, NULL),
(139, '78227', 'San Antonio', 'Texas', 'TX', 'Bexar', '029', 29.40270, -98.64330, 4, 1, NULL, NULL),
(140, '78228', 'San Antonio', 'Texas', 'TX', 'Bexar', '029', 29.45890, -98.56990, 4, 1, NULL, NULL),
(141, '78229', 'San Antonio', 'Texas', 'TX', 'Bexar', '029', 29.50420, -98.56970, 4, 1, NULL, NULL),
(142, '78230', 'San Antonio', 'Texas', 'TX', 'Bexar', '029', 29.54070, -98.55210, 4, 1, NULL, NULL),
(143, '78231', 'San Antonio', 'Texas', 'TX', 'Bexar', '029', 29.57140, -98.54140, 4, 1, NULL, NULL),
(144, '78232', 'San Antonio', 'Texas', 'TX', 'Bexar', '029', 29.58630, -98.47690, 4, 1, NULL, NULL),
(145, '78233', 'San Antonio', 'Texas', 'TX', 'Bexar', '029', 29.55630, -98.36430, 4, 1, NULL, NULL),
(146, '78234', 'San Antonio', 'Texas', 'TX', 'Bexar', '029', 29.46220, -98.44230, 4, 1, NULL, NULL),
(147, '78235', 'San Antonio', 'Texas', 'TX', 'Bexar', '029', 29.34940, -98.44220, 4, 1, NULL, NULL),
(148, '78236', 'San Antonio', 'Texas', 'TX', 'Bexar', '029', 29.38870, -98.61120, 4, 1, NULL, NULL),
(149, '78237', 'San Antonio', 'Texas', 'TX', 'Bexar', '029', 29.42080, -98.56450, 4, 1, NULL, NULL),
(150, '78238', 'San Antonio', 'Texas', 'TX', 'Bexar', '029', 29.45100, -98.61690, 4, 1, NULL, NULL),
(151, '78239', 'San Antonio', 'Texas', 'TX', 'Bexar', '029', 29.51820, -98.36270, 4, 1, NULL, NULL),
(152, '78240', 'San Antonio', 'Texas', 'TX', 'Bexar', '029', 29.51890, -98.60060, 4, 1, NULL, NULL),
(153, '78241', 'San Antonio', 'Texas', 'TX', 'Bexar', '029', 29.43750, -98.46160, 4, 1, NULL, NULL),
(154, '78242', 'San Antonio', 'Texas', 'TX', 'Bexar', '029', 29.35090, -98.61090, 4, 1, NULL, NULL),
(155, '78243', 'San Antonio', 'Texas', 'TX', 'Bexar', '029', 29.43750, -98.46160, 4, 1, NULL, NULL),
(156, '78244', 'San Antonio', 'Texas', 'TX', 'Bexar', '029', 29.47930, -98.34760, 4, 1, NULL, NULL),
(157, '78245', 'San Antonio', 'Texas', 'TX', 'Bexar', '029', 29.41890, -98.68950, 4, 1, NULL, NULL),
(158, '78246', 'San Antonio', 'Texas', 'TX', 'Bexar', '029', 29.43750, -98.46160, 4, 1, NULL, NULL),
(159, '78247', 'San Antonio', 'Texas', 'TX', 'Bexar', '029', 29.58550, -98.40710, 4, 1, NULL, NULL),
(160, '78248', 'San Antonio', 'Texas', 'TX', 'Bexar', '029', 29.58940, -98.52010, 4, 1, NULL, NULL),
(161, '78249', 'San Antonio', 'Texas', 'TX', 'Bexar', '029', 29.56120, -98.61170, 4, 1, NULL, NULL),
(162, '78250', 'San Antonio', 'Texas', 'TX', 'Bexar', '029', 29.50540, -98.66880, 4, 1, NULL, NULL),
(163, '78251', 'San Antonio', 'Texas', 'TX', 'Bexar', '029', 29.45970, -98.65550, 4, 1, NULL, NULL),
(164, '78252', 'San Antonio', 'Texas', 'TX', 'Bexar', '029', 29.34600, -98.64640, 4, 1, NULL, NULL),
(165, '78253', 'San Antonio', 'Texas', 'TX', 'Bexar', '029', 29.45990, -98.74790, 4, 1, NULL, NULL),
(166, '78254', 'San Antonio', 'Texas', 'TX', 'Bexar', '029', 29.55510, -98.74420, 4, 1, NULL, NULL),
(167, '78255', 'San Antonio', 'Texas', 'TX', 'Bexar', '029', 29.67010, -98.68730, 4, 1, NULL, NULL),
(168, '78256', 'San Antonio', 'Texas', 'TX', 'Bexar', '029', 29.61690, -98.62520, 4, 1, NULL, NULL),
(169, '78257', 'San Antonio', 'Texas', 'TX', 'Bexar', '029', 29.64950, -98.61370, 4, 1, NULL, NULL),
(170, '78258', 'San Antonio', 'Texas', 'TX', 'Bexar', '029', 29.65620, -98.49670, 4, 1, NULL, NULL),
(171, '78259', 'San Antonio', 'Texas', 'TX', 'Bexar', '029', 29.62830, -98.44450, 4, 1, NULL, NULL),
(172, '78260', 'San Antonio', 'Texas', 'TX', 'Bexar', '029', 29.70260, -98.47590, 4, 1, NULL, NULL),
(173, '78261', 'San Antonio', 'Texas', 'TX', 'Bexar', '029', 29.70550, -98.41910, 4, 1, NULL, NULL),
(174, '78263', 'San Antonio', 'Texas', 'TX', 'Bexar', '029', 29.36140, -98.31740, 4, 1, NULL, NULL),
(175, '78264', 'San Antonio', 'Texas', 'TX', 'Bexar', '029', 29.17330, -98.47230, 4, 1, NULL, NULL),
(176, '78265', 'San Antonio', 'Texas', 'TX', 'Bexar', '029', 29.43750, -98.46160, 4, 1, NULL, NULL),
(177, '78268', 'San Antonio', 'Texas', 'TX', 'Bexar', '029', 29.43750, -98.46160, 4, 1, NULL, NULL),
(178, '78269', 'San Antonio', 'Texas', 'TX', 'Bexar', '029', 29.43750, -98.46160, 4, 1, NULL, NULL),
(179, '78270', 'San Antonio', 'Texas', 'TX', 'Bexar', '029', 29.43750, -98.46160, 4, 1, NULL, NULL),
(180, '78278', 'San Antonio', 'Texas', 'TX', 'Bexar', '029', 29.43750, -98.46160, 4, 1, NULL, NULL),
(181, '78279', 'San Antonio', 'Texas', 'TX', 'Bexar', '029', 29.43750, -98.46160, 4, 1, NULL, NULL),
(182, '78280', 'San Antonio', 'Texas', 'TX', 'Bexar', '029', 29.43750, -98.46160, 4, 1, NULL, NULL),
(183, '78283', 'San Antonio', 'Texas', 'TX', 'Bexar', '029', 29.43750, -98.46160, 4, 1, NULL, NULL),
(184, '78284', 'San Antonio', 'Texas', 'TX', 'Bexar', '029', 29.44260, -98.49130, 4, 1, NULL, NULL),
(185, '78285', 'San Antonio', 'Texas', 'TX', 'Bexar', '029', 29.43750, -98.46160, 4, 1, NULL, NULL),
(186, '78288', 'San Antonio', 'Texas', 'TX', 'Bexar', '029', 29.42410, -98.49360, 4, 1, NULL, NULL),
(187, '78289', 'San Antonio', 'Texas', 'TX', 'Bexar', '029', 29.43750, -98.46160, 4, 1, NULL, NULL),
(188, '78291', 'San Antonio', 'Texas', 'TX', 'Bexar', '029', 29.43750, -98.46160, 4, 1, NULL, NULL),
(189, '78292', 'San Antonio', 'Texas', 'TX', 'Bexar', '029', 29.43750, -98.46160, 4, 1, NULL, NULL),
(190, '78293', 'San Antonio', 'Texas', 'TX', 'Bexar', '029', 29.43750, -98.46160, 4, 1, NULL, NULL),
(191, '78294', 'San Antonio', 'Texas', 'TX', 'Bexar', '029', 29.43750, -98.46160, 4, 1, NULL, NULL),
(192, '78295', 'San Antonio', 'Texas', 'TX', 'Bexar', '029', 29.43750, -98.46160, 4, 1, NULL, NULL),
(193, '78296', 'San Antonio', 'Texas', 'TX', 'Bexar', '029', 29.43750, -98.46160, 4, 1, NULL, NULL),
(194, '78297', 'San Antonio', 'Texas', 'TX', 'Bexar', '029', 29.43750, -98.46160, 4, 1, NULL, NULL),
(195, '78298', 'San Antonio', 'Texas', 'TX', 'Bexar', '029', 29.43750, -98.46160, 4, 1, NULL, NULL),
(196, '78299', 'San Antonio', 'Texas', 'TX', 'Bexar', '029', 29.43750, -98.46160, 4, 1, NULL, NULL),
(197, '78606', 'Blanco', 'Texas', 'TX', 'Blanco', '031', 30.08740, -98.41070, 4, 1, NULL, NULL),
(198, '78635', 'Hye', 'Texas', 'TX', 'Blanco', '031', 30.22690, -98.53960, 4, 1, NULL, NULL),
(199, '78636', 'Johnson City', 'Texas', 'TX', 'Blanco', '031', 30.29480, -98.36910, 4, 1, NULL, NULL),
(200, '78663', 'Round Mountain', 'Texas', 'TX', 'Blanco', '031', 30.44290, -98.43650, 4, 1, NULL, NULL),
(201, '79738', 'Gail', 'Texas', 'TX', 'Borden', '033', 32.75230, -101.45000, 4, 1, NULL, NULL),
(202, '76634', 'Clifton', 'Texas', 'TX', 'Bosque', '035', 31.79180, -97.52030, 4, 1, NULL, NULL),
(203, '76637', 'Cranfills Gap', 'Texas', 'TX', 'Bosque', '035', 31.74860, -97.81130, 4, 1, NULL, NULL),
(204, '76644', 'Laguna Park', 'Texas', 'TX', 'Bosque', '035', 31.85930, -97.37970, 4, 1, NULL, NULL),
(205, '76649', 'Iredell', 'Texas', 'TX', 'Bosque', '035', 31.97220, -97.87930, 4, 1, NULL, NULL),
(206, '76652', 'Kopperl', 'Texas', 'TX', 'Bosque', '035', 32.10350, -97.54210, 4, 1, NULL, NULL),
(207, '76665', 'Meridian', 'Texas', 'TX', 'Bosque', '035', 31.92900, -97.64430, 4, 1, NULL, NULL),
(208, '76671', 'Morgan', 'Texas', 'TX', 'Bosque', '035', 32.01950, -97.56080, 4, 1, NULL, NULL),
(209, '76689', 'Valley Mills', 'Texas', 'TX', 'Bosque', '035', 31.65990, -97.49350, 4, 1, NULL, NULL),
(210, '76690', 'Walnut Springs', 'Texas', 'TX', 'Bosque', '035', 32.05930, -97.75140, 4, 1, NULL, NULL),
(211, '75501', 'Texarkana', 'Texas', 'TX', 'Bowie', '037', 33.41130, -94.17740, 4, 1, NULL, NULL),
(212, '75503', 'Texarkana', 'Texas', 'TX', 'Bowie', '037', 33.46690, -94.07740, 4, 1, NULL, NULL),
(213, '75504', 'Texarkana', 'Texas', 'TX', 'Bowie', '037', 33.42510, -94.04770, 4, 1, NULL, NULL),
(214, '75505', 'Texarkana', 'Texas', 'TX', 'Bowie', '037', 33.46240, -94.07150, 4, 1, NULL, NULL),
(215, '75507', 'Texarkana', 'Texas', 'TX', 'Bowie', '037', 33.42510, -94.04770, 4, 1, NULL, NULL),
(216, '75559', 'De Kalb', 'Texas', 'TX', 'Bowie', '037', 33.50870, -94.61630, 4, 1, NULL, NULL),
(217, '75561', 'Hooks', 'Texas', 'TX', 'Bowie', '037', 33.52530, -94.26570, 4, 1, NULL, NULL),
(218, '75567', 'Maud', 'Texas', 'TX', 'Bowie', '037', 33.34090, -94.31750, 4, 1, NULL, NULL),
(219, '75569', 'Nash', 'Texas', 'TX', 'Bowie', '037', 33.43440, -94.12190, 4, 1, NULL, NULL),
(220, '75570', 'New Boston', 'Texas', 'TX', 'Bowie', '037', 33.46190, -94.43750, 4, 1, NULL, NULL),
(221, '75573', 'Redwater', 'Texas', 'TX', 'Bowie', '037', 33.35800, -94.25710, 4, 1, NULL, NULL),
(222, '75574', 'Simms', 'Texas', 'TX', 'Bowie', '037', 33.33590, -94.55200, 4, 1, NULL, NULL),
(223, '75599', 'Texarkana', 'Texas', 'TX', 'Bowie', '037', 33.42510, -94.04770, 4, 1, NULL, NULL),
(224, '77422', 'Brazoria', 'Texas', 'TX', 'Brazoria', '039', 29.02360, -95.58670, 4, 1, NULL, NULL),
(225, '77430', 'Damon', 'Texas', 'TX', 'Brazoria', '039', 29.30140, -95.70360, 4, 1, NULL, NULL),
(226, '77431', 'Danciger', 'Texas', 'TX', 'Brazoria', '039', 29.17370, -95.82070, 4, 1, NULL, NULL),
(227, '77463', 'Old Ocean', 'Texas', 'TX', 'Brazoria', '039', 29.08000, -95.74970, 4, 1, NULL, NULL),
(228, '77480', 'Sweeny', 'Texas', 'TX', 'Brazoria', '039', 29.04150, -95.70040, 4, 1, NULL, NULL),
(229, '77486', 'West Columbia', 'Texas', 'TX', 'Brazoria', '039', 29.14080, -95.66940, 4, 1, NULL, NULL),
(230, '77511', 'Alvin', 'Texas', 'TX', 'Brazoria', '039', 29.41200, -95.25150, 4, 1, NULL, NULL),
(231, '77512', 'Alvin', 'Texas', 'TX', 'Brazoria', '039', 29.42380, -95.24410, 4, 1, NULL, NULL),
(232, '77515', 'Angleton', 'Texas', 'TX', 'Brazoria', '039', 29.18100, -95.44670, 4, 1, NULL, NULL),
(233, '77516', 'Angleton', 'Texas', 'TX', 'Brazoria', '039', 29.16940, -95.43190, 4, 1, NULL, NULL),
(234, '77531', 'Clute', 'Texas', 'TX', 'Brazoria', '039', 29.03250, -95.40260, 4, 1, NULL, NULL),
(235, '77534', 'Danbury', 'Texas', 'TX', 'Brazoria', '039', 29.22910, -95.34350, 4, 1, NULL, NULL),
(236, '77541', 'Freeport', 'Texas', 'TX', 'Brazoria', '039', 29.03570, -95.33790, 4, 1, NULL, NULL),
(237, '77542', 'Freeport', 'Texas', 'TX', 'Brazoria', '039', 28.95410, -95.35970, 4, 1, NULL, NULL),
(238, '77566', 'Lake Jackson', 'Texas', 'TX', 'Brazoria', '039', 29.03930, -95.44010, 4, 1, NULL, NULL),
(239, '77577', 'Liverpool', 'Texas', 'TX', 'Brazoria', '039', 29.26670, -95.28890, 4, 1, NULL, NULL),
(240, '77578', 'Manvel', 'Texas', 'TX', 'Brazoria', '039', 29.46940, -95.35030, 4, 1, NULL, NULL),
(241, '77581', 'Pearland', 'Texas', 'TX', 'Brazoria', '039', 29.56170, -95.27210, 4, 1, NULL, NULL),
(242, '77583', 'Rosharon', 'Texas', 'TX', 'Brazoria', '039', 29.42030, -95.45370, 4, 1, NULL, NULL),
(243, '77584', 'Pearland', 'Texas', 'TX', 'Brazoria', '039', 29.54050, -95.32080, 4, 1, NULL, NULL),
(244, '77588', 'Pearland', 'Texas', 'TX', 'Brazoria', '039', 29.51270, -95.25420, 4, 1, NULL, NULL),
(245, '77801', 'Bryan', 'Texas', 'TX', 'Brazos', '041', 30.63270, -96.36620, 4, 1, NULL, NULL),
(246, '77802', 'Bryan', 'Texas', 'TX', 'Brazos', '041', 30.65820, -96.33510, 4, 1, NULL, NULL),
(247, '77803', 'Bryan', 'Texas', 'TX', 'Brazos', '041', 30.69130, -96.37140, 4, 1, NULL, NULL),
(248, '77805', 'Bryan', 'Texas', 'TX', 'Brazos', '041', 30.65210, -96.34100, 4, 1, NULL, NULL),
(249, '77806', 'Bryan', 'Texas', 'TX', 'Brazos', '041', 30.67440, -96.37000, 4, 1, NULL, NULL),
(250, '77807', 'Bryan', 'Texas', 'TX', 'Brazos', '041', 30.67110, -96.47990, 4, 1, NULL, NULL),
(251, '77808', 'Bryan', 'Texas', 'TX', 'Brazos', '041', 30.82020, -96.30590, 4, 1, NULL, NULL),
(252, '77840', 'College Station', 'Texas', 'TX', 'Brazos', '041', 30.60450, -96.31230, 4, 1, NULL, NULL),
(253, '77841', 'College Station', 'Texas', 'TX', 'Brazos', '041', 30.57260, -96.32700, 4, 1, NULL, NULL),
(254, '77842', 'College Station', 'Texas', 'TX', 'Brazos', '041', 30.65210, -96.34100, 4, 1, NULL, NULL),
(255, '77843', 'College Station', 'Texas', 'TX', 'Brazos', '041', 30.65210, -96.34100, 4, 1, NULL, NULL),
(256, '77844', 'College Station', 'Texas', 'TX', 'Brazos', '041', 30.65210, -96.34100, 4, 1, NULL, NULL),
(257, '77845', 'College Station', 'Texas', 'TX', 'Brazos', '041', 30.51180, -96.31710, 4, 1, NULL, NULL),
(258, '77862', 'Kurten', 'Texas', 'TX', 'Brazos', '041', 30.78710, -96.26410, 4, 1, NULL, NULL),
(259, '77866', 'Millican', 'Texas', 'TX', 'Brazos', '041', 30.44910, -96.21700, 4, 1, NULL, NULL),
(260, '77881', 'Wellborn', 'Texas', 'TX', 'Brazos', '041', 30.53340, -96.30320, 4, 1, NULL, NULL),
(261, '79830', 'Alpine', 'Texas', 'TX', 'Brewster', '043', 30.26310, -103.65410, 1, 1, NULL, NULL),
(262, '79831', 'Alpine', 'Texas', 'TX', 'Brewster', '043', 30.30680, -103.67040, 1, 1, NULL, NULL),
(263, '79832', 'Alpine', 'Texas', 'TX', 'Brewster', '043', 30.36310, -103.65390, 1, 1, NULL, NULL),
(264, '79834', 'Big Bend National Park', 'Texas', 'TX', 'Brewster', '043', 29.29240, -103.29630, 1, 1, NULL, NULL),
(265, '79842', 'Marathon', 'Texas', 'TX', 'Brewster', '043', 30.18860, -103.22140, 4, 1, NULL, NULL),
(266, '79852', 'Terlingua', 'Texas', 'TX', 'Brewster', '043', 29.31650, -103.55970, 1, 1, NULL, NULL),
(267, '79255', 'Quitaque', 'Texas', 'TX', 'Briscoe', '045', 34.37960, -101.04650, 4, 1, NULL, NULL),
(268, '79257', 'Silverton', 'Texas', 'TX', 'Briscoe', '045', 34.46410, -101.31690, 4, 1, NULL, NULL),
(269, '78353', 'Encino', 'Texas', 'TX', 'Brooks', '047', 26.92490, -98.19220, 4, 1, NULL, NULL),
(270, '78355', 'Falfurrias', 'Texas', 'TX', 'Brooks', '047', 27.22420, -98.14080, 4, 1, NULL, NULL),
(271, '76432', 'Blanket', 'Texas', 'TX', 'Brown', '049', 31.78820, -98.83110, 4, 1, NULL, NULL),
(272, '76801', 'Brownwood', 'Texas', 'TX', 'Brown', '049', 31.77540, -98.99150, 4, 1, NULL, NULL),
(273, '76802', 'Early', 'Texas', 'TX', 'Brown', '049', 31.78740, -98.92290, 4, 1, NULL, NULL),
(274, '76803', 'Brownwood', 'Texas', 'TX', 'Brown', '049', 31.76390, -98.93600, 4, 1, NULL, NULL),
(275, '76804', 'Brownwood', 'Texas', 'TX', 'Brown', '049', 31.77420, -99.09210, 4, 1, NULL, NULL),
(276, '76823', 'Bangs', 'Texas', 'TX', 'Brown', '049', 31.75030, -99.15680, 4, 1, NULL, NULL),
(277, '76827', 'Brookesmith', 'Texas', 'TX', 'Brown', '049', 31.51760, -99.12770, 4, 1, NULL, NULL),
(278, '76857', 'May', 'Texas', 'TX', 'Brown', '049', 31.95710, -98.96560, 4, 1, NULL, NULL),
(279, '76890', 'Zephyr', 'Texas', 'TX', 'Brown', '049', 31.66940, -98.81820, 4, 1, NULL, NULL),
(280, '77836', 'Caldwell', 'Texas', 'TX', 'Burleson', '051', 30.52980, -96.71430, 4, 1, NULL, NULL),
(281, '77838', 'Chriesman', 'Texas', 'TX', 'Burleson', '051', 30.59940, -96.77080, 4, 1, NULL, NULL),
(282, '77852', 'Deanville', 'Texas', 'TX', 'Burleson', '051', 30.43220, -96.75610, 4, 1, NULL, NULL),
(283, '77863', 'Lyons', 'Texas', 'TX', 'Burleson', '051', 30.38630, -96.56330, 4, 1, NULL, NULL),
(284, '77878', 'Snook', 'Texas', 'TX', 'Burleson', '051', 30.47020, -96.48040, 4, 1, NULL, NULL),
(285, '77879', 'Somerville', 'Texas', 'TX', 'Burleson', '051', 30.40760, -96.53580, 4, 1, NULL, NULL),
(286, '78605', 'Bertram', 'Texas', 'TX', 'Burnet', '053', 30.74110, -98.05290, 4, 1, NULL, NULL),
(287, '78608', 'Briggs', 'Texas', 'TX', 'Burnet', '053', 30.93250, -97.97020, 4, 1, NULL, NULL),
(288, '78611', 'Burnet', 'Texas', 'TX', 'Burnet', '053', 30.77660, -98.26420, 4, 1, NULL, NULL),
(289, '78654', 'Marble Falls', 'Texas', 'TX', 'Burnet', '053', 30.57840, -98.27510, 4, 1, NULL, NULL),
(290, '78657', 'Marble Falls', 'Texas', 'TX', 'Burnet', '053', 30.53730, -98.37370, 4, 1, NULL, NULL),
(291, '78616', 'Dale', 'Texas', 'TX', 'Caldwell', '055', 29.95280, -97.58100, 4, 1, NULL, NULL),
(292, '78622', 'Fentress', 'Texas', 'TX', 'Caldwell', '055', 29.75610, -97.77640, 4, 1, NULL, NULL),
(293, '78644', 'Lockhart', 'Texas', 'TX', 'Caldwell', '055', 29.88680, -97.67690, 4, 1, NULL, NULL),
(294, '78648', 'Luling', 'Texas', 'TX', 'Caldwell', '055', 29.68260, -97.64990, 4, 1, NULL, NULL),
(295, '78655', 'Martindale', 'Texas', 'TX', 'Caldwell', '055', 29.83940, -97.85080, 4, 1, NULL, NULL),
(296, '78656', 'Maxwell', 'Texas', 'TX', 'Caldwell', '055', 29.89530, -97.81440, 4, 1, NULL, NULL),
(297, '78661', 'Prairie Lea', 'Texas', 'TX', 'Caldwell', '055', 29.72390, -97.74490, 4, 1, NULL, NULL),
(298, '77978', 'Point Comfort', 'Texas', 'TX', 'Calhoun', '057', 28.66510, -96.55060, 4, 1, NULL, NULL),
(299, '77979', 'Port Lavaca', 'Texas', 'TX', 'Calhoun', '057', 28.60110, -96.62590, 4, 1, NULL, NULL),
(300, '77982', 'Port O Connor', 'Texas', 'TX', 'Calhoun', '057', 28.44830, -96.40580, 4, 1, NULL, NULL),
(301, '77983', 'Seadrift', 'Texas', 'TX', 'Calhoun', '057', 28.41010, -96.70230, 4, 1, NULL, NULL),
(302, '76443', 'Cross Plains', 'Texas', 'TX', 'Callahan', '059', 32.14820, -99.18720, 4, 1, NULL, NULL),
(303, '76469', 'Putnam', 'Texas', 'TX', 'Callahan', '059', 32.37400, -99.19570, 4, 1, NULL, NULL),
(304, '79504', 'Baird', 'Texas', 'TX', 'Callahan', '059', 32.39160, -99.37770, 4, 1, NULL, NULL),
(305, '79510', 'Clyde', 'Texas', 'TX', 'Callahan', '059', 32.38030, -99.51840, 4, 1, NULL, NULL),
(306, '78520', 'Brownsville', 'Texas', 'TX', 'Cameron', '061', 25.93370, -97.51740, 4, 1, NULL, NULL),
(307, '78521', 'Brownsville', 'Texas', 'TX', 'Cameron', '061', 25.92210, -97.46120, 4, 1, NULL, NULL),
(308, '78522', 'Brownsville', 'Texas', 'TX', 'Cameron', '061', 25.90170, -97.49750, 4, 1, NULL, NULL),
(309, '78523', 'Brownsville', 'Texas', 'TX', 'Cameron', '061', 25.98100, -97.52090, 4, 1, NULL, NULL),
(310, '78526', 'Brownsville', 'Texas', 'TX', 'Cameron', '061', 25.97170, -97.46990, 4, 1, NULL, NULL),
(311, '78535', 'Combes', 'Texas', 'TX', 'Cameron', '061', 26.24510, -97.74160, 4, 1, NULL, NULL),
(312, '78550', 'Harlingen', 'Texas', 'TX', 'Cameron', '061', 26.19510, -97.68900, 4, 1, NULL, NULL),
(313, '78551', 'Harlingen', 'Texas', 'TX', 'Cameron', '061', 26.24470, -97.72060, 4, 1, NULL, NULL),
(314, '78552', 'Harlingen', 'Texas', 'TX', 'Cameron', '061', 26.18310, -97.74680, 4, 1, NULL, NULL),
(315, '78553', 'Harlingen', 'Texas', 'TX', 'Cameron', '061', 26.19060, -97.69610, 4, 1, NULL, NULL),
(316, '78559', 'La Feria', 'Texas', 'TX', 'Cameron', '061', 26.16660, -97.82610, 4, 1, NULL, NULL),
(317, '78566', 'Los Fresnos', 'Texas', 'TX', 'Cameron', '061', 26.11580, -97.41070, 4, 1, NULL, NULL),
(318, '78567', 'Los Indios', 'Texas', 'TX', 'Cameron', '061', 26.04920, -97.74500, 4, 1, NULL, NULL),
(319, '78568', 'Lozano', 'Texas', 'TX', 'Cameron', '061', 26.19040, -97.54230, 4, 1, NULL, NULL),
(320, '78575', 'Olmito', 'Texas', 'TX', 'Cameron', '061', 26.03530, -97.54960, 4, 1, NULL, NULL),
(321, '78578', 'Port Isabel', 'Texas', 'TX', 'Cameron', '061', 26.05390, -97.31250, 4, 1, NULL, NULL),
(322, '78583', 'Rio Hondo', 'Texas', 'TX', 'Cameron', '061', 26.23390, -97.55130, 4, 1, NULL, NULL),
(323, '78586', 'San Benito', 'Texas', 'TX', 'Cameron', '061', 26.13370, -97.64470, 4, 1, NULL, NULL),
(324, '78592', 'Santa Maria', 'Texas', 'TX', 'Cameron', '061', 26.07870, -97.84940, 4, 1, NULL, NULL),
(325, '78593', 'Santa Rosa', 'Texas', 'TX', 'Cameron', '061', 26.25550, -97.82570, 4, 1, NULL, NULL),
(326, '78597', 'South Padre Island', 'Texas', 'TX', 'Cameron', '061', 26.23930, -97.19360, 4, 1, NULL, NULL),
(327, '75451', 'Leesburg', 'Texas', 'TX', 'Camp', '063', 32.97630, -95.10790, 4, 1, NULL, NULL),
(328, '75686', 'Pittsburg', 'Texas', 'TX', 'Camp', '063', 32.96230, -94.96030, 4, 1, NULL, NULL),
(329, '79039', 'Groom', 'Texas', 'TX', 'Carson', '065', 35.21680, -101.12850, 4, 1, NULL, NULL),
(330, '79068', 'Panhandle', 'Texas', 'TX', 'Carson', '065', 35.38080, -101.43040, 4, 1, NULL, NULL),
(331, '79080', 'Skellytown', 'Texas', 'TX', 'Carson', '065', 35.56850, -101.17210, 4, 1, NULL, NULL),
(332, '79097', 'White Deer', 'Texas', 'TX', 'Carson', '065', 35.42780, -101.17400, 4, 1, NULL, NULL),
(333, '75551', 'Atlanta', 'Texas', 'TX', 'Cass', '067', 33.10900, -94.16460, 4, 1, NULL, NULL),
(334, '75555', 'Bivins', 'Texas', 'TX', 'Cass', '067', 32.96600, -94.14040, 4, 1, NULL, NULL),
(335, '75556', 'Bloomburg', 'Texas', 'TX', 'Cass', '067', 33.13390, -94.06470, 4, 1, NULL, NULL),
(336, '75560', 'Douglassville', 'Texas', 'TX', 'Cass', '067', 33.17580, -94.34670, 4, 1, NULL, NULL),
(337, '75562', 'Kildare', 'Texas', 'TX', 'Cass', '067', 32.94680, -94.25390, 4, 1, NULL, NULL),
(338, '75563', 'Linden', 'Texas', 'TX', 'Cass', '067', 33.00480, -94.36050, 4, 1, NULL, NULL),
(339, '75565', 'Mc Leod', 'Texas', 'TX', 'Cass', '067', 32.95110, -94.08080, 1, 1, NULL, NULL),
(340, '75566', 'Marietta', 'Texas', 'TX', 'Cass', '067', 33.17350, -94.54240, 4, 1, NULL, NULL),
(341, '75572', 'Queen City', 'Texas', 'TX', 'Cass', '067', 33.15570, -94.15370, 4, 1, NULL, NULL),
(342, '75630', 'Avinger', 'Texas', 'TX', 'Cass', '067', 32.84850, -94.57950, 4, 1, NULL, NULL),
(343, '75656', 'Hughes Springs', 'Texas', 'TX', 'Cass', '067', 33.01680, -94.62280, 4, 1, NULL, NULL),
(344, '79027', 'Dimmitt', 'Texas', 'TX', 'Castro', '069', 34.53410, -102.30460, 4, 1, NULL, NULL),
(345, '79043', 'Hart', 'Texas', 'TX', 'Castro', '069', 34.38780, -102.11550, 4, 1, NULL, NULL),
(346, '79063', 'Nazareth', 'Texas', 'TX', 'Castro', '069', 34.54440, -102.10690, 4, 1, NULL, NULL),
(347, '79085', 'Summerfield', 'Texas', 'TX', 'Castro', '069', 34.74370, -102.50640, 4, 1, NULL, NULL),
(348, '77514', 'Anahuac', 'Texas', 'TX', 'Chambers', '071', 29.66200, -94.59300, 4, 1, NULL, NULL),
(349, '77523', 'Baytown', 'Texas', 'TX', 'Chambers', '071', 29.77000, -94.86080, 4, 1, NULL, NULL),
(350, '77560', 'Hankamer', 'Texas', 'TX', 'Chambers', '071', 29.87520, -94.59380, 4, 1, NULL, NULL),
(351, '77580', 'Mont Belvieu', 'Texas', 'TX', 'Chambers', '071', 29.85610, -94.84290, 4, 1, NULL, NULL),
(352, '77597', 'Wallisville', 'Texas', 'TX', 'Chambers', '071', 29.85910, -94.67590, 4, 1, NULL, NULL),
(353, '77661', 'Stowell', 'Texas', 'TX', 'Chambers', '071', 29.78090, -94.39000, 4, 1, NULL, NULL),
(354, '77665', 'Winnie', 'Texas', 'TX', 'Chambers', '071', 29.81570, -94.33950, 4, 1, NULL, NULL),
(355, '75759', 'Cuney', 'Texas', 'TX', 'Cherokee', '073', 32.04150, -95.42000, 4, 1, NULL, NULL),
(356, '75764', 'Gallatin', 'Texas', 'TX', 'Cherokee', '073', 31.89100, -95.14550, 4, 1, NULL, NULL),
(357, '75766', 'Jacksonville', 'Texas', 'TX', 'Cherokee', '073', 31.96180, -95.27030, 4, 1, NULL, NULL),
(358, '75772', 'Maydelle', 'Texas', 'TX', 'Cherokee', '073', 31.80080, -95.30010, 4, 1, NULL, NULL),
(359, '75780', 'New Summerfield', 'Texas', 'TX', 'Cherokee', '073', 31.78200, -95.16400, 4, 1, NULL, NULL),
(360, '75784', 'Reklaw', 'Texas', 'TX', 'Cherokee', '073', 31.88590, -95.01180, 4, 1, NULL, NULL),
(361, '75785', 'Rusk', 'Texas', 'TX', 'Cherokee', '073', 31.81360, -95.09650, 4, 1, NULL, NULL),
(362, '75925', 'Alto', 'Texas', 'TX', 'Cherokee', '073', 31.61030, -95.07620, 4, 1, NULL, NULL),
(363, '75976', 'Wells', 'Texas', 'TX', 'Cherokee', '073', 31.48850, -94.93990, 4, 1, NULL, NULL),
(364, '79201', 'Childress', 'Texas', 'TX', 'Childress', '075', 34.41040, -100.23640, 4, 1, NULL, NULL),
(365, '79259', 'Tell', 'Texas', 'TX', 'Childress', '075', 34.37590, -100.39480, 4, 1, NULL, NULL),
(366, '76228', 'Bellevue', 'Texas', 'TX', 'Clay', '077', 33.58790, -98.15740, 4, 1, NULL, NULL),
(367, '76352', 'Bluegrove', 'Texas', 'TX', 'Clay', '077', 33.67400, -98.23000, 4, 1, NULL, NULL),
(368, '76357', 'Byers', 'Texas', 'TX', 'Clay', '077', 34.07280, -98.18390, 4, 1, NULL, NULL),
(369, '76365', 'Henrietta', 'Texas', 'TX', 'Clay', '077', 33.81960, -98.26000, 4, 1, NULL, NULL),
(370, '76377', 'Petrolia', 'Texas', 'TX', 'Clay', '077', 34.01320, -98.23230, 4, 1, NULL, NULL),
(371, '79314', 'Bledsoe', 'Texas', 'TX', 'Cochran', '079', 33.59970, -103.01680, 4, 1, NULL, NULL),
(372, '79346', 'Morton', 'Texas', 'TX', 'Cochran', '079', 33.72510, -102.75940, 4, 1, NULL, NULL),
(373, '79379', 'Whiteface', 'Texas', 'TX', 'Cochran', '079', 33.59880, -102.61990, 4, 1, NULL, NULL),
(374, '76933', 'Bronte', 'Texas', 'TX', 'Coke', '081', 31.87890, -100.29880, 4, 1, NULL, NULL),
(375, '76945', 'Robert Lee', 'Texas', 'TX', 'Coke', '081', 31.89510, -100.51040, 4, 1, NULL, NULL),
(376, '76949', 'Silver', 'Texas', 'TX', 'Coke', '081', 32.04840, -100.69220, 4, 1, NULL, NULL),
(377, '76953', 'Tennyson', 'Texas', 'TX', 'Coke', '081', 31.73970, -100.28840, 4, 1, NULL, NULL),
(378, '76828', 'Burkett', 'Texas', 'TX', 'Coleman', '083', 31.99860, -99.25530, 4, 1, NULL, NULL),
(379, '76834', 'Coleman', 'Texas', 'TX', 'Coleman', '083', 31.82870, -99.42700, 4, 1, NULL, NULL),
(380, '76845', 'Gouldbusk', 'Texas', 'TX', 'Coleman', '083', 31.55490, -99.47670, 4, 1, NULL, NULL),
(381, '76873', 'Rockwood', 'Texas', 'TX', 'Coleman', '083', 31.50370, -99.37460, 4, 1, NULL, NULL),
(382, '76878', 'Santa Anna', 'Texas', 'TX', 'Coleman', '083', 31.72150, -99.32120, 4, 1, NULL, NULL),
(383, '76882', 'Talpa', 'Texas', 'TX', 'Coleman', '083', 31.80340, -99.67470, 4, 1, NULL, NULL),
(384, '76884', 'Valera', 'Texas', 'TX', 'Coleman', '083', 31.75290, -99.54730, 4, 1, NULL, NULL),
(385, '76888', 'Voss', 'Texas', 'TX', 'Coleman', '083', 31.58920, -99.63260, 4, 1, NULL, NULL),
(386, '79519', 'Goldsboro', 'Texas', 'TX', 'Coleman', '083', 32.04820, -99.67750, 4, 1, NULL, NULL),
(387, '79538', 'Novice', 'Texas', 'TX', 'Coleman', '083', 31.97810, -99.68090, 4, 1, NULL, NULL),
(388, '75002', 'Allen', 'Texas', 'TX', 'Collin', '085', 33.09340, -96.64540, 4, 1, NULL, NULL),
(389, '75009', 'Celina', 'Texas', 'TX', 'Collin', '085', 33.31030, -96.76730, 4, 1, NULL, NULL),
(390, '75013', 'Allen', 'Texas', 'TX', 'Collin', '085', 33.11860, -96.67730, 4, 1, NULL, NULL),
(391, '75023', 'Plano', 'Texas', 'TX', 'Collin', '085', 33.05500, -96.73650, 4, 1, NULL, NULL),
(392, '75024', 'Plano', 'Texas', 'TX', 'Collin', '085', 33.07520, -96.78430, 4, 1, NULL, NULL),
(393, '75025', 'Plano', 'Texas', 'TX', 'Collin', '085', 33.07840, -96.72910, 4, 1, NULL, NULL),
(394, '75026', 'Plano', 'Texas', 'TX', 'Collin', '085', 33.01980, -96.69890, 4, 1, NULL, NULL),
(395, '75033', 'Frisco', 'Texas', 'TX', 'Collin', '085', 33.25590, -96.88530, 4, 1, NULL, NULL),
(396, '75034', 'Frisco', 'Texas', 'TX', 'Collin', '085', 33.14990, -96.82410, 4, 1, NULL, NULL),
(397, '75035', 'Frisco', 'Texas', 'TX', 'Collin', '085', 33.13770, -96.75240, 4, 1, NULL, NULL),
(398, '75069', 'Mc Kinney', 'Texas', 'TX', 'Collin', '085', 33.19660, -96.60850, 1, 1, NULL, NULL),
(399, '75070', 'Mc Kinney', 'Texas', 'TX', 'Collin', '085', 33.19510, -96.66420, 1, 1, NULL, NULL),
(400, '75071', 'Mc Kinney', 'Texas', 'TX', 'Collin', '085', 33.21410, -96.75330, 1, 1, NULL, NULL),
(401, '75074', 'Plano', 'Texas', 'TX', 'Collin', '085', 33.02770, -96.67770, 4, 1, NULL, NULL),
(402, '75075', 'Plano', 'Texas', 'TX', 'Collin', '085', 33.02500, -96.73970, 4, 1, NULL, NULL),
(403, '75078', 'Prosper', 'Texas', 'TX', 'Collin', '085', 33.23620, -96.79540, 4, 1, NULL, NULL),
(404, '75086', 'Plano', 'Texas', 'TX', 'Collin', '085', 33.01980, -96.69890, 4, 1, NULL, NULL),
(405, '75093', 'Plano', 'Texas', 'TX', 'Collin', '085', 33.02990, -96.78890, 4, 1, NULL, NULL),
(406, '75094', 'Plano', 'Texas', 'TX', 'Collin', '085', 33.01480, -96.61890, 4, 1, NULL, NULL),
(407, '75097', 'Weston', 'Texas', 'TX', 'Collin', '085', 33.35120, -96.66460, 4, 1, NULL, NULL),
(408, '75121', 'Copeville', 'Texas', 'TX', 'Collin', '085', 33.08750, -96.41860, 4, 1, NULL, NULL),
(409, '75164', 'Josephine', 'Texas', 'TX', 'Collin', '085', 33.06120, -96.30720, 4, 1, NULL, NULL),
(410, '75166', 'Lavon', 'Texas', 'TX', 'Collin', '085', 33.01390, -96.43770, 4, 1, NULL, NULL),
(411, '75173', 'Nevada', 'Texas', 'TX', 'Collin', '085', 33.05930, -96.38770, 4, 1, NULL, NULL),
(412, '75407', 'Princeton', 'Texas', 'TX', 'Collin', '085', 33.15550, -96.49810, 4, 1, NULL, NULL),
(413, '75409', 'Anna', 'Texas', 'TX', 'Collin', '085', 33.34450, -96.56390, 4, 1, NULL, NULL),
(414, '75424', 'Blue Ridge', 'Texas', 'TX', 'Collin', '085', 33.30610, -96.39010, 4, 1, NULL, NULL),
(415, '75442', 'Farmersville', 'Texas', 'TX', 'Collin', '085', 33.16590, -96.36860, 4, 1, NULL, NULL),
(416, '75454', 'Melissa', 'Texas', 'TX', 'Collin', '085', 33.28410, -96.57400, 4, 1, NULL, NULL),
(417, '75485', 'Westminster', 'Texas', 'TX', 'Collin', '085', 33.36270, -96.46350, 4, 1, NULL, NULL),
(418, '79077', 'Samnorwood', 'Texas', 'TX', 'Collingsworth', '087', 35.05230, -100.28070, 4, 1, NULL, NULL),
(419, '79095', 'Wellington', 'Texas', 'TX', 'Collingsworth', '087', 34.87170, -100.22070, 4, 1, NULL, NULL),
(420, '79230', 'Dodson', 'Texas', 'TX', 'Collingsworth', '087', 34.76440, -100.02860, 4, 1, NULL, NULL),
(421, '79251', 'Quail', 'Texas', 'TX', 'Collingsworth', '087', 34.91800, -100.42520, 4, 1, NULL, NULL),
(422, '77412', 'Altair', 'Texas', 'TX', 'Colorado', '089', 29.60470, -96.52490, 4, 1, NULL, NULL),
(423, '77434', 'Eagle Lake', 'Texas', 'TX', 'Colorado', '089', 29.58420, -96.33540, 4, 1, NULL, NULL),
(424, '77442', 'Garwood', 'Texas', 'TX', 'Colorado', '089', 29.47600, -96.49190, 4, 1, NULL, NULL),
(425, '77460', 'Nada', 'Texas', 'TX', 'Colorado', '089', 29.40500, -96.38640, 4, 1, NULL, NULL),
(426, '77470', 'Rock Island', 'Texas', 'TX', 'Colorado', '089', 29.53110, -96.57520, 4, 1, NULL, NULL),
(427, '77475', 'Sheridan', 'Texas', 'TX', 'Colorado', '089', 29.49360, -96.67110, 4, 1, NULL, NULL),
(428, '78934', 'Columbus', 'Texas', 'TX', 'Colorado', '089', 29.70320, -96.55270, 4, 1, NULL, NULL),
(429, '78935', 'Alleyton', 'Texas', 'TX', 'Colorado', '089', 29.70920, -96.48650, 4, 1, NULL, NULL),
(430, '78943', 'Glidden', 'Texas', 'TX', 'Colorado', '089', 29.69940, -96.59420, 4, 1, NULL, NULL),
(431, '78951', 'Oakland', 'Texas', 'TX', 'Colorado', '089', 29.60160, -96.83000, 4, 1, NULL, NULL),
(432, '78962', 'Weimar', 'Texas', 'TX', 'Colorado', '089', 29.67870, -96.75500, 4, 1, NULL, NULL),
(433, '78070', 'Spring Branch', 'Texas', 'TX', 'Comal', '091', 29.92380, -98.37880, 4, 1, NULL, NULL),
(434, '78130', 'New Braunfels', 'Texas', 'TX', 'Comal', '091', 29.72290, -98.07420, 4, 1, NULL, NULL),
(435, '78131', 'New Braunfels', 'Texas', 'TX', 'Comal', '091', 29.70300, -98.12450, 4, 1, NULL, NULL),
(436, '78132', 'New Braunfels', 'Texas', 'TX', 'Comal', '091', 29.75650, -98.19830, 4, 1, NULL, NULL),
(437, '78133', 'Canyon Lake', 'Texas', 'TX', 'Comal', '091', 29.91120, -98.23740, 4, 1, NULL, NULL),
(438, '78135', 'New Braunfels', 'Texas', 'TX', 'Comal', '091', 29.70300, -98.12450, 4, 1, NULL, NULL),
(439, '78163', 'Bulverde', 'Texas', 'TX', 'Comal', '091', 29.77670, -98.46260, 4, 1, NULL, NULL),
(440, '78266', 'San Antonio', 'Texas', 'TX', 'Comal', '091', 29.66430, -98.31180, 4, 1, NULL, NULL),
(441, '78623', 'Fischer', 'Texas', 'TX', 'Comal', '091', 29.96270, -98.20880, 4, 1, NULL, NULL),
(442, '76442', 'Comanche', 'Texas', 'TX', 'Comanche', '093', 31.91160, -98.60820, 4, 1, NULL, NULL),
(443, '76444', 'De Leon', 'Texas', 'TX', 'Comanche', '093', 32.10870, -98.54890, 4, 1, NULL, NULL),
(444, '76452', 'Energy', 'Texas', 'TX', 'Comanche', '093', 31.76330, -98.39990, 4, 1, NULL, NULL),
(445, '76455', 'Gustine', 'Texas', 'TX', 'Comanche', '093', 31.84680, -98.40170, 4, 1, NULL, NULL),
(446, '76468', 'Proctor', 'Texas', 'TX', 'Comanche', '093', 31.98740, -98.42980, 4, 1, NULL, NULL),
(447, '76474', 'Sidney', 'Texas', 'TX', 'Comanche', '093', 31.93200, -98.76800, 4, 1, NULL, NULL),
(448, '76837', 'Eden', 'Texas', 'TX', 'Concho', '095', 31.21920, -99.84070, 4, 1, NULL, NULL),
(449, '76855', 'Lowake', 'Texas', 'TX', 'Concho', '095', 31.56630, -100.07590, 4, 1, NULL, NULL),
(450, '76862', 'Millersview', 'Texas', 'TX', 'Concho', '095', 31.41670, -99.71710, 4, 1, NULL, NULL),
(451, '76866', 'Paint Rock', 'Texas', 'TX', 'Concho', '095', 31.50480, -99.91390, 4, 1, NULL, NULL),
(452, '76937', 'Eola', 'Texas', 'TX', 'Concho', '095', 31.36170, -100.09220, 4, 1, NULL, NULL),
(453, '76238', 'Era', 'Texas', 'TX', 'Cooke', '097', 33.49650, -97.38590, 4, 1, NULL, NULL),
(454, '76240', 'Gainesville', 'Texas', 'TX', 'Cooke', '097', 33.65470, -97.15830, 4, 1, NULL, NULL),
(455, '76241', 'Gainesville', 'Texas', 'TX', 'Cooke', '097', 33.61040, -97.03690, 4, 1, NULL, NULL),
(456, '76250', 'Lindsay', 'Texas', 'TX', 'Cooke', '097', 33.63600, -97.22140, 4, 1, NULL, NULL),
(457, '76252', 'Muenster', 'Texas', 'TX', 'Cooke', '097', 33.65950, -97.36240, 4, 1, NULL, NULL),
(458, '76253', 'Myra', 'Texas', 'TX', 'Cooke', '097', 33.61780, -97.30900, 4, 1, NULL, NULL),
(459, '76263', 'Rosston', 'Texas', 'TX', 'Cooke', '097', 33.46740, -97.45340, 4, 1, NULL, NULL),
(460, '76272', 'Valley View', 'Texas', 'TX', 'Cooke', '097', 33.50220, -97.23110, 4, 1, NULL, NULL),
(461, '76522', 'Copperas Cove', 'Texas', 'TX', 'Coryell', '099', 31.20290, -97.93010, 4, 1, NULL, NULL),
(462, '76525', 'Evant', 'Texas', 'TX', 'Coryell', '099', 31.43880, -98.05350, 4, 1, NULL, NULL),
(463, '76526', 'Flat', 'Texas', 'TX', 'Coryell', '099', 31.29560, -97.58220, 4, 1, NULL, NULL),
(464, '76528', 'Gatesville', 'Texas', 'TX', 'Coryell', '099', 31.41770, -97.83300, 4, 1, NULL, NULL),
(465, '76538', 'Jonesboro', 'Texas', 'TX', 'Coryell', '099', 31.55990, -97.84560, 4, 1, NULL, NULL),
(466, '76558', 'Mound', 'Texas', 'TX', 'Coryell', '099', 31.35140, -97.64440, 4, 1, NULL, NULL),
(467, '76561', 'Oglesby', 'Texas', 'TX', 'Coryell', '099', 31.44380, -97.55010, 4, 1, NULL, NULL),
(468, '76566', 'Purmela', 'Texas', 'TX', 'Coryell', '099', 31.48410, -97.99030, 4, 1, NULL, NULL),
(469, '76596', 'Gatesville', 'Texas', 'TX', 'Coryell', '099', 31.39020, -97.79930, 4, 1, NULL, NULL),
(470, '76597', 'Gatesville', 'Texas', 'TX', 'Coryell', '099', 31.39020, -97.79930, 4, 1, NULL, NULL),
(471, '76598', 'Gatesville', 'Texas', 'TX', 'Coryell', '099', 31.39020, -97.79930, 4, 1, NULL, NULL),
(472, '76599', 'Gatesville', 'Texas', 'TX', 'Coryell', '099', 31.47060, -97.73470, 4, 1, NULL, NULL),
(473, '79223', 'Cee Vee', 'Texas', 'TX', 'Cottle', '101', 34.22950, -100.45730, 4, 1, NULL, NULL),
(474, '79248', 'Paducah', 'Texas', 'TX', 'Cottle', '101', 34.02170, -100.29860, 4, 1, NULL, NULL),
(475, '79731', 'Crane', 'Texas', 'TX', 'Crane', '103', 31.39690, -102.35440, 4, 1, NULL, NULL),
(476, '76943', 'Ozona', 'Texas', 'TX', 'Crockett', '105', 30.71640, -101.23880, 4, 1, NULL, NULL),
(477, '79322', 'Crosbyton', 'Texas', 'TX', 'Crosby', '107', 33.65620, -101.22870, 4, 1, NULL, NULL),
(478, '79343', 'Lorenzo', 'Texas', 'TX', 'Crosby', '107', 33.66660, -101.52770, 4, 1, NULL, NULL),
(479, '79357', 'Ralls', 'Texas', 'TX', 'Crosby', '107', 33.68510, -101.38360, 4, 1, NULL, NULL),
(480, '79855', 'Van Horn', 'Texas', 'TX', 'Culberson', '109', 31.03910, -104.82450, 4, 1, NULL, NULL),
(481, '79022', 'Dalhart', 'Texas', 'TX', 'Dallam', '111', 36.07320, -102.51770, 1, 1, NULL, NULL),
(482, '79051', 'Kerrick', 'Texas', 'TX', 'Dallam', '111', 36.56630, -102.34380, 1, 1, NULL, NULL),
(483, '79087', 'Texline', 'Texas', 'TX', 'Dallam', '111', 36.37400, -102.98450, 4, 1, NULL, NULL),
(484, '75001', 'Addison', 'Texas', 'TX', 'Dallas County', '113', 32.96000, -96.83850, 4, 1, NULL, NULL),
(485, '75006', 'Carrollton', 'Texas', 'TX', 'Dallas County', '113', 32.96570, -96.88250, 4, 1, NULL, NULL),
(486, '75011', 'Carrollton', 'Texas', 'TX', 'Dallas County', '113', 32.95370, -96.89030, 4, 1, NULL, NULL),
(487, '75014', 'Irving', 'Texas', 'TX', 'Dallas County', '113', 32.76730, -96.77760, 4, 1, NULL, NULL),
(488, '75015', 'Irving', 'Texas', 'TX', 'Dallas County', '113', 32.89870, -96.97220, 4, 1, NULL, NULL),
(489, '75016', 'Irving', 'Texas', 'TX', 'Dallas County', '113', 32.89870, -96.97220, 4, 1, NULL, NULL),
(490, '75017', 'Irving', 'Texas', 'TX', 'Dallas County', '113', 32.76730, -96.77760, 4, 1, NULL, NULL),
(491, '75019', 'Coppell', 'Texas', 'TX', 'Dallas County', '113', 32.96730, -96.98050, 4, 1, NULL, NULL),
(492, '75030', 'Rowlett', 'Texas', 'TX', 'Dallas County', '113', 32.90290, -96.56390, 4, 1, NULL, NULL),
(493, '75038', 'Irving', 'Texas', 'TX', 'Dallas County', '113', 32.86530, -96.99050, 4, 1, NULL, NULL),
(494, '75039', 'Irving', 'Texas', 'TX', 'Dallas County', '113', 32.86970, -96.93890, 4, 1, NULL, NULL),
(495, '75040', 'Garland', 'Texas', 'TX', 'Dallas County', '113', 32.92270, -96.62480, 4, 1, NULL, NULL),
(496, '75041', 'Garland', 'Texas', 'TX', 'Dallas County', '113', 32.87940, -96.64110, 4, 1, NULL, NULL),
(497, '75042', 'Garland', 'Texas', 'TX', 'Dallas County', '113', 32.91850, -96.67750, 4, 1, NULL, NULL),
(498, '75043', 'Garland', 'Texas', 'TX', 'Dallas County', '113', 32.85650, -96.59990, 4, 1, NULL, NULL),
(499, '75044', 'Garland', 'Texas', 'TX', 'Dallas County', '113', 32.95220, -96.66540, 4, 1, NULL, NULL),
(500, '75045', 'Garland', 'Texas', 'TX', 'Dallas County', '113', 32.91370, -96.62710, 4, 1, NULL, NULL),
(501, '75046', 'Garland', 'Texas', 'TX', 'Dallas County', '113', 32.91260, -96.63890, 4, 1, NULL, NULL),
(502, '75047', 'Garland', 'Texas', 'TX', 'Dallas County', '113', 32.91260, -96.63890, 4, 1, NULL, NULL),
(503, '75048', 'Sachse', 'Texas', 'TX', 'Dallas County', '113', 32.96430, -96.60120, 4, 1, NULL, NULL),
(504, '75048', 'Garland', 'Texas', 'TX', 'Dallas County', '113', 32.95040, -96.57530, 4, 1, NULL, NULL),
(505, '75049', 'Garland', 'Texas', 'TX', 'Dallas County', '113', 32.91260, -96.63890, 4, 1, NULL, NULL),
(506, '75050', 'Grand Prairie', 'Texas', 'TX', 'Dallas County', '113', 32.76490, -97.01120, 4, 1, NULL, NULL),
(507, '75051', 'Grand Prairie', 'Texas', 'TX', 'Dallas County', '113', 32.71150, -97.00690, 4, 1, NULL, NULL),
(508, '75052', 'Grand Prairie', 'Texas', 'TX', 'Dallas County', '113', 32.66050, -97.03110, 4, 1, NULL, NULL);
INSERT INTO `zip_codes` (`id`, `zipcode`, `city`, `state`, `state_abbr`, `county_area`, `code`, `latitude`, `longitude`, `some_field`, `status`, `created_at`, `updated_at`) VALUES
(509, '75053', 'Grand Prairie', 'Texas', 'TX', 'Dallas County', '113', 32.74600, -96.99780, 4, 1, NULL, NULL),
(510, '75054', 'Grand Prairie', 'Texas', 'TX', 'Dallas County', '113', 32.59070, -97.04050, 4, 1, NULL, NULL),
(511, '75060', 'Irving', 'Texas', 'TX', 'Dallas County', '113', 32.80230, -96.95970, 4, 1, NULL, NULL),
(512, '75061', 'Irving', 'Texas', 'TX', 'Dallas County', '113', 32.82670, -96.96330, 4, 1, NULL, NULL),
(513, '75062', 'Irving', 'Texas', 'TX', 'Dallas County', '113', 32.84790, -96.97400, 4, 1, NULL, NULL),
(514, '75063', 'Irving', 'Texas', 'TX', 'Dallas County', '113', 32.92470, -96.95980, 4, 1, NULL, NULL),
(515, '75080', 'Richardson', 'Texas', 'TX', 'Dallas County', '113', 32.96600, -96.74520, 4, 1, NULL, NULL),
(516, '75081', 'Richardson', 'Texas', 'TX', 'Dallas County', '113', 32.94620, -96.70580, 4, 1, NULL, NULL),
(517, '75082', 'Richardson', 'Texas', 'TX', 'Dallas County', '113', 32.98650, -96.68600, 4, 1, NULL, NULL),
(518, '75083', 'Richardson', 'Texas', 'TX', 'Dallas County', '113', 32.76730, -96.77760, 4, 1, NULL, NULL),
(519, '75085', 'Richardson', 'Texas', 'TX', 'Dallas County', '113', 32.76730, -96.77760, 4, 1, NULL, NULL),
(520, '75088', 'Rowlett', 'Texas', 'TX', 'Dallas County', '113', 32.90310, -96.54720, 4, 1, NULL, NULL),
(521, '75089', 'Rowlett', 'Texas', 'TX', 'Dallas County', '113', 32.93460, -96.55440, 4, 1, NULL, NULL),
(522, '75098', 'Wylie', 'Texas', 'TX', 'Dallas County', '113', 33.00410, -96.53940, 4, 1, NULL, NULL),
(523, '75099', 'Coppell', 'Texas', 'TX', 'Dallas County', '113', 32.95460, -97.01500, 4, 1, NULL, NULL),
(524, '75104', 'Cedar Hill', 'Texas', 'TX', 'Dallas County', '113', 32.58850, -96.94380, 4, 1, NULL, NULL),
(525, '75106', 'Cedar Hill', 'Texas', 'TX', 'Dallas County', '113', 32.58850, -96.95610, 4, 1, NULL, NULL),
(526, '75115', 'Desoto', 'Texas', 'TX', 'Dallas County', '113', 32.60410, -96.86530, 4, 1, NULL, NULL),
(527, '75116', 'Duncanville', 'Texas', 'TX', 'Dallas County', '113', 32.65870, -96.91140, 4, 1, NULL, NULL),
(528, '75123', 'Desoto', 'Texas', 'TX', 'Dallas County', '113', 32.58990, -96.85690, 4, 1, NULL, NULL),
(529, '75134', 'Lancaster', 'Texas', 'TX', 'Dallas County', '113', 32.61610, -96.78300, 4, 1, NULL, NULL),
(530, '75137', 'Duncanville', 'Texas', 'TX', 'Dallas County', '113', 32.63470, -96.91130, 4, 1, NULL, NULL),
(531, '75138', 'Duncanville', 'Texas', 'TX', 'Dallas County', '113', 32.76730, -96.77760, 4, 1, NULL, NULL),
(532, '75141', 'Hutchins', 'Texas', 'TX', 'Dallas County', '113', 32.63960, -96.70700, 4, 1, NULL, NULL),
(533, '75146', 'Lancaster', 'Texas', 'TX', 'Dallas County', '113', 32.59140, -96.77280, 4, 1, NULL, NULL),
(534, '75149', 'Mesquite', 'Texas', 'TX', 'Dallas County', '113', 32.76780, -96.60820, 4, 1, NULL, NULL),
(535, '75150', 'Mesquite', 'Texas', 'TX', 'Dallas County', '113', 32.81540, -96.63070, 4, 1, NULL, NULL),
(536, '75159', 'Seagoville', 'Texas', 'TX', 'Dallas County', '113', 32.65250, -96.55800, 4, 1, NULL, NULL),
(537, '75172', 'Wilmer', 'Texas', 'TX', 'Dallas County', '113', 32.59810, -96.68380, 4, 1, NULL, NULL),
(538, '75180', 'Mesquite', 'Texas', 'TX', 'Dallas County', '113', 32.72120, -96.61530, 4, 1, NULL, NULL),
(539, '75181', 'Mesquite', 'Texas', 'TX', 'Dallas County', '113', 32.72720, -96.56690, 4, 1, NULL, NULL),
(540, '75182', 'Sunnyvale', 'Texas', 'TX', 'Dallas County', '113', 32.79700, -96.56160, 4, 1, NULL, NULL),
(541, '75185', 'Mesquite', 'Texas', 'TX', 'Dallas County', '113', 32.74030, -96.56180, 4, 1, NULL, NULL),
(542, '75187', 'Mesquite', 'Texas', 'TX', 'Dallas County', '113', 32.76730, -96.77760, 4, 1, NULL, NULL),
(543, '75201', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.79040, -96.80440, 4, 1, NULL, NULL),
(544, '75202', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.77810, -96.80540, 4, 1, NULL, NULL),
(545, '75203', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.74600, -96.80700, 4, 1, NULL, NULL),
(546, '75204', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.80380, -96.78510, 4, 1, NULL, NULL),
(547, '75205', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.82600, -96.78430, 4, 1, NULL, NULL),
(548, '75206', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.83100, -96.76920, 4, 1, NULL, NULL),
(549, '75207', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.79390, -96.83190, 4, 1, NULL, NULL),
(550, '75208', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.74920, -96.83890, 4, 1, NULL, NULL),
(551, '75209', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.84560, -96.82600, 4, 1, NULL, NULL),
(552, '75210', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.76990, -96.74300, 4, 1, NULL, NULL),
(553, '75211', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.73190, -96.90570, 4, 1, NULL, NULL),
(554, '75212', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.78290, -96.87140, 4, 1, NULL, NULL),
(555, '75214', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.82480, -96.74980, 4, 1, NULL, NULL),
(556, '75215', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.75820, -96.76230, 4, 1, NULL, NULL),
(557, '75216', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.70860, -96.79550, 4, 1, NULL, NULL),
(558, '75217', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.72440, -96.67550, 4, 1, NULL, NULL),
(559, '75218', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.84630, -96.69720, 4, 1, NULL, NULL),
(560, '75219', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.81320, -96.81420, 4, 1, NULL, NULL),
(561, '75220', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.86810, -96.86220, 4, 1, NULL, NULL),
(562, '75221', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.81470, -96.78770, 4, 1, NULL, NULL),
(563, '75222', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.76730, -96.77760, 4, 1, NULL, NULL),
(564, '75223', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.79420, -96.74750, 4, 1, NULL, NULL),
(565, '75224', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.71140, -96.83870, 4, 1, NULL, NULL),
(566, '75225', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.86280, -96.79180, 4, 1, NULL, NULL),
(567, '75226', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.78870, -96.76760, 4, 1, NULL, NULL),
(568, '75227', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.76720, -96.68360, 4, 1, NULL, NULL),
(569, '75228', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.82500, -96.67840, 4, 1, NULL, NULL),
(570, '75229', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.89580, -96.85880, 4, 1, NULL, NULL),
(571, '75230', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.89990, -96.78970, 4, 1, NULL, NULL),
(572, '75231', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.87560, -96.74950, 4, 1, NULL, NULL),
(573, '75232', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.66470, -96.83840, 4, 1, NULL, NULL),
(574, '75233', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.70460, -96.87250, 4, 1, NULL, NULL),
(575, '75234', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.92450, -96.89380, 4, 1, NULL, NULL),
(576, '75235', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.82520, -96.83880, 4, 1, NULL, NULL),
(577, '75236', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.69000, -96.91770, 4, 1, NULL, NULL),
(578, '75237', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.65900, -96.87650, 4, 1, NULL, NULL),
(579, '75238', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.87700, -96.70800, 4, 1, NULL, NULL),
(580, '75240', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.93740, -96.78720, 4, 1, NULL, NULL),
(581, '75241', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.67220, -96.77740, 4, 1, NULL, NULL),
(582, '75242', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.76730, -96.77760, 4, 1, NULL, NULL),
(583, '75243', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.91030, -96.72850, 4, 1, NULL, NULL),
(584, '75244', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.93220, -96.83530, 4, 1, NULL, NULL),
(585, '75246', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.79480, -96.76970, 4, 1, NULL, NULL),
(586, '75247', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.81520, -96.87030, 4, 1, NULL, NULL),
(587, '75248', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.96820, -96.79420, 4, 1, NULL, NULL),
(588, '75249', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.63600, -96.94930, 4, 1, NULL, NULL),
(589, '75250', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.76730, -96.77760, 4, 1, NULL, NULL),
(590, '75251', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.91890, -96.77510, 4, 1, NULL, NULL),
(591, '75252', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.99680, -96.79210, 4, 1, NULL, NULL),
(592, '75253', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.68330, -96.59640, 4, 1, NULL, NULL),
(593, '75254', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.95030, -96.81900, 4, 1, NULL, NULL),
(594, '75260', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.76730, -96.77760, 4, 1, NULL, NULL),
(595, '75261', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.76730, -96.77760, 4, 1, NULL, NULL),
(596, '75262', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.76730, -96.77760, 4, 1, NULL, NULL),
(597, '75263', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.76730, -96.77760, 4, 1, NULL, NULL),
(598, '75264', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.76730, -96.77760, 4, 1, NULL, NULL),
(599, '75265', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.76730, -96.77760, 4, 1, NULL, NULL),
(600, '75266', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.76730, -96.77760, 4, 1, NULL, NULL),
(601, '75267', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.76730, -96.77760, 4, 1, NULL, NULL),
(602, '75270', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.78130, -96.80190, 4, 1, NULL, NULL),
(603, '75275', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.76730, -96.77760, 4, 1, NULL, NULL),
(604, '75277', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.76730, -96.77760, 4, 1, NULL, NULL),
(605, '75283', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.76730, -96.77760, 4, 1, NULL, NULL),
(606, '75284', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.76730, -96.77760, 4, 1, NULL, NULL),
(607, '75285', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.76730, -96.77760, 4, 1, NULL, NULL),
(608, '75287', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 33.00050, -96.83140, 4, 1, NULL, NULL),
(609, '75301', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.76730, -96.77760, 4, 1, NULL, NULL),
(610, '75303', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.76730, -96.77760, 4, 1, NULL, NULL),
(611, '75312', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.76730, -96.77760, 4, 1, NULL, NULL),
(612, '75313', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.76730, -96.77760, 4, 1, NULL, NULL),
(613, '75315', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.76730, -96.77760, 4, 1, NULL, NULL),
(614, '75320', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.76730, -96.77760, 4, 1, NULL, NULL),
(615, '75326', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.76730, -96.77760, 4, 1, NULL, NULL),
(616, '75336', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.76730, -96.77760, 4, 1, NULL, NULL),
(617, '75339', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.76730, -96.77760, 4, 1, NULL, NULL),
(618, '75342', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.76730, -96.77760, 4, 1, NULL, NULL),
(619, '75354', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.76730, -96.77760, 4, 1, NULL, NULL),
(620, '75355', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.76730, -96.77760, 4, 1, NULL, NULL),
(621, '75356', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.76730, -96.77760, 4, 1, NULL, NULL),
(622, '75357', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.76730, -96.77760, 4, 1, NULL, NULL),
(623, '75358', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.79420, -96.76520, 4, 1, NULL, NULL),
(624, '75359', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.76730, -96.77760, 4, 1, NULL, NULL),
(625, '75360', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.76730, -96.77760, 4, 1, NULL, NULL),
(626, '75367', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.76730, -96.77760, 4, 1, NULL, NULL),
(627, '75368', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.76730, -96.77760, 4, 1, NULL, NULL),
(628, '75370', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.76730, -96.77760, 4, 1, NULL, NULL),
(629, '75371', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.76730, -96.77760, 4, 1, NULL, NULL),
(630, '75372', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.76730, -96.77760, 4, 1, NULL, NULL),
(631, '75373', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.76730, -96.77760, 4, 1, NULL, NULL),
(632, '75374', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.76730, -96.77760, 4, 1, NULL, NULL),
(633, '75376', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.76730, -96.77760, 4, 1, NULL, NULL),
(634, '75378', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.76730, -96.77760, 4, 1, NULL, NULL),
(635, '75379', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.76730, -96.77760, 4, 1, NULL, NULL),
(636, '75380', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.76730, -96.77760, 4, 1, NULL, NULL),
(637, '75381', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.76730, -96.77760, 4, 1, NULL, NULL),
(638, '75382', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.76730, -96.77760, 4, 1, NULL, NULL),
(639, '75389', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.76730, -96.77760, 4, 1, NULL, NULL),
(640, '75390', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.76730, -96.77760, 4, 1, NULL, NULL),
(641, '75391', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.76730, -96.77760, 4, 1, NULL, NULL),
(642, '75392', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.76730, -96.77760, 4, 1, NULL, NULL),
(643, '75393', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.76730, -96.77760, 4, 1, NULL, NULL),
(644, '75394', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.76730, -96.77760, 4, 1, NULL, NULL),
(645, '75395', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.76730, -96.77760, 4, 1, NULL, NULL),
(646, '75397', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.76730, -96.77760, 4, 1, NULL, NULL),
(647, '75398', 'Dallas', 'Texas', 'TX', 'Dallas County', '113', 32.76730, -96.77760, 4, 1, NULL, NULL),
(648, '79331', 'Lamesa', 'Texas', 'TX', 'Dawson', '115', 32.73670, -101.95690, 4, 1, NULL, NULL),
(649, '79377', 'Welch', 'Texas', 'TX', 'Dawson', '115', 32.92980, -102.12780, 4, 1, NULL, NULL),
(650, '79713', 'Ackerly', 'Texas', 'TX', 'Dawson', '115', 32.52730, -101.71600, 4, 1, NULL, NULL),
(651, '79025', 'Dawn', 'Texas', 'TX', 'Deaf Smith', '117', 34.91060, -102.20020, 4, 1, NULL, NULL),
(652, '79045', 'Hereford', 'Texas', 'TX', 'Deaf Smith', '117', 34.83700, -102.40500, 4, 1, NULL, NULL),
(653, '75415', 'Ben Franklin', 'Texas', 'TX', 'Delta', '119', 33.47680, -95.76890, 4, 1, NULL, NULL),
(654, '75432', 'Cooper', 'Texas', 'TX', 'Delta', '119', 33.38120, -95.66230, 4, 1, NULL, NULL),
(655, '75441', 'Enloe', 'Texas', 'TX', 'Delta', '119', 33.42910, -95.65180, 4, 1, NULL, NULL),
(656, '75448', 'Klondike', 'Texas', 'TX', 'Delta', '119', 33.30340, -95.80180, 4, 1, NULL, NULL),
(657, '75450', 'Lake Creek', 'Texas', 'TX', 'Delta', '119', 33.41160, -95.45510, 4, 1, NULL, NULL),
(658, '75469', 'Pecan Gap', 'Texas', 'TX', 'Delta', '119', 33.41960, -95.82620, 4, 1, NULL, NULL),
(659, '75007', 'Carrollton', 'Texas', 'TX', 'Denton', '121', 33.00330, -96.88200, 4, 1, NULL, NULL),
(660, '75010', 'Carrollton', 'Texas', 'TX', 'Denton', '121', 33.03040, -96.87770, 4, 1, NULL, NULL),
(661, '75022', 'Flower Mound', 'Texas', 'TX', 'Denton', '121', 33.02680, -97.11930, 4, 1, NULL, NULL),
(662, '75027', 'Flower Mound', 'Texas', 'TX', 'Denton', '121', 33.20740, -97.11630, 4, 1, NULL, NULL),
(663, '75028', 'Flower Mound', 'Texas', 'TX', 'Denton', '121', 33.03830, -97.07450, 4, 1, NULL, NULL),
(664, '75029', 'Lewisville', 'Texas', 'TX', 'Denton', '121', 33.04620, -96.99420, 4, 1, NULL, NULL),
(665, '75056', 'The Colony', 'Texas', 'TX', 'Denton', '121', 33.09400, -96.88360, 4, 1, NULL, NULL),
(666, '75057', 'Lewisville', 'Texas', 'TX', 'Denton', '121', 33.05320, -96.99990, 4, 1, NULL, NULL),
(667, '75065', 'Lake Dallas', 'Texas', 'TX', 'Denton', '121', 33.12190, -97.02370, 4, 1, NULL, NULL),
(668, '75067', 'Lewisville', 'Texas', 'TX', 'Denton', '121', 33.01980, -96.99250, 4, 1, NULL, NULL),
(669, '75068', 'Little Elm', 'Texas', 'TX', 'Denton', '121', 33.17680, -96.95830, 4, 1, NULL, NULL),
(670, '75077', 'Lewisville', 'Texas', 'TX', 'Denton', '121', 33.07750, -97.07040, 4, 1, NULL, NULL),
(671, '76201', 'Denton', 'Texas', 'TX', 'Denton', '121', 33.22890, -97.13140, 4, 1, NULL, NULL),
(672, '76202', 'Denton', 'Texas', 'TX', 'Denton', '121', 33.22550, -97.10850, 4, 1, NULL, NULL),
(673, '76203', 'Denton', 'Texas', 'TX', 'Denton', '121', 33.21480, -97.13310, 4, 1, NULL, NULL),
(674, '76204', 'Denton', 'Texas', 'TX', 'Denton', '121', 33.20740, -97.11630, 4, 1, NULL, NULL),
(675, '76205', 'Denton', 'Texas', 'TX', 'Denton', '121', 33.19030, -97.12820, 4, 1, NULL, NULL),
(676, '76206', 'Denton', 'Texas', 'TX', 'Denton', '121', 33.16940, -97.15060, 4, 1, NULL, NULL),
(677, '76207', 'Denton', 'Texas', 'TX', 'Denton', '121', 33.22850, -97.18130, 4, 1, NULL, NULL),
(678, '76208', 'Denton', 'Texas', 'TX', 'Denton', '121', 33.21170, -97.06120, 4, 1, NULL, NULL),
(679, '76209', 'Denton', 'Texas', 'TX', 'Denton', '121', 33.23460, -97.11310, 4, 1, NULL, NULL),
(680, '76210', 'Denton', 'Texas', 'TX', 'Denton', '121', 33.14280, -97.07270, 4, 1, NULL, NULL),
(681, '76226', 'Argyle', 'Texas', 'TX', 'Denton', '121', 33.10620, -97.16000, 4, 1, NULL, NULL),
(682, '76227', 'Aubrey', 'Texas', 'TX', 'Denton', '121', 33.29200, -96.98790, 4, 1, NULL, NULL),
(683, '76247', 'Justin', 'Texas', 'TX', 'Denton', '121', 33.07340, -97.30930, 4, 1, NULL, NULL),
(684, '76249', 'Krum', 'Texas', 'TX', 'Denton', '121', 33.27340, -97.26750, 4, 1, NULL, NULL),
(685, '76258', 'Pilot Point', 'Texas', 'TX', 'Denton', '121', 33.37100, -96.94460, 4, 1, NULL, NULL),
(686, '76259', 'Ponder', 'Texas', 'TX', 'Denton', '121', 33.17740, -97.28480, 4, 1, NULL, NULL),
(687, '76262', 'Roanoke', 'Texas', 'TX', 'Denton', '121', 33.02110, -97.21270, 4, 1, NULL, NULL),
(688, '76266', 'Sanger', 'Texas', 'TX', 'Denton', '121', 33.35630, -97.18140, 4, 1, NULL, NULL),
(689, '77954', 'Cuero', 'Texas', 'TX', 'DeWitt', '123', 29.09100, -97.28120, 4, 1, NULL, NULL),
(690, '77967', 'Hochheim', 'Texas', 'TX', 'DeWitt', '123', 29.31250, -97.29170, 4, 1, NULL, NULL),
(691, '77974', 'Meyersville', 'Texas', 'TX', 'DeWitt', '123', 28.92290, -97.30420, 4, 1, NULL, NULL),
(692, '77989', 'Thomaston', 'Texas', 'TX', 'DeWitt', '123', 28.99740, -97.15390, 4, 1, NULL, NULL),
(693, '77994', 'Westhoff', 'Texas', 'TX', 'DeWitt', '123', 29.17920, -97.46000, 4, 1, NULL, NULL),
(694, '78141', 'Nordheim', 'Texas', 'TX', 'DeWitt', '123', 28.91420, -97.59460, 4, 1, NULL, NULL),
(695, '78164', 'Yorktown', 'Texas', 'TX', 'DeWitt', '123', 28.98920, -97.51210, 4, 1, NULL, NULL),
(696, '79220', 'Afton', 'Texas', 'TX', 'Dickens', '125', 33.77180, -100.80210, 4, 1, NULL, NULL),
(697, '79229', 'Dickens', 'Texas', 'TX', 'Dickens', '125', 33.62800, -100.81970, 4, 1, NULL, NULL),
(698, '79243', 'Mcadoo', 'Texas', 'TX', 'Dickens', '125', 33.74130, -100.98330, 4, 1, NULL, NULL),
(699, '79370', 'Spur', 'Texas', 'TX', 'Dickens', '125', 33.47900, -100.85710, 4, 1, NULL, NULL),
(700, '78827', 'Asherton', 'Texas', 'TX', 'Dimmit', '127', 28.43640, -99.74860, 4, 1, NULL, NULL),
(701, '78830', 'Big Wells', 'Texas', 'TX', 'Dimmit', '127', 28.56930, -99.57810, 4, 1, NULL, NULL),
(702, '78834', 'Carrizo Springs', 'Texas', 'TX', 'Dimmit', '127', 28.52780, -99.86350, 4, 1, NULL, NULL),
(703, '78836', 'Catarina', 'Texas', 'TX', 'Dimmit', '127', 28.35980, -99.58600, 4, 1, NULL, NULL),
(704, '79226', 'Clarendon', 'Texas', 'TX', 'Donley', '129', 34.95290, -100.89520, 4, 1, NULL, NULL),
(705, '79237', 'Hedley', 'Texas', 'TX', 'Donley', '129', 34.86980, -100.68060, 4, 1, NULL, NULL),
(706, '79240', 'Lelia Lake', 'Texas', 'TX', 'Donley', '129', 34.88430, -100.76740, 4, 1, NULL, NULL),
(707, '78341', 'Benavides', 'Texas', 'TX', 'Duval', '131', 27.59250, -98.41420, 4, 1, NULL, NULL),
(708, '78349', 'Concepcion', 'Texas', 'TX', 'Duval', '131', 27.38490, -98.30900, 4, 1, NULL, NULL),
(709, '78357', 'Freer', 'Texas', 'TX', 'Duval', '131', 27.88000, -98.60610, 4, 1, NULL, NULL),
(710, '78376', 'Realitos', 'Texas', 'TX', 'Duval', '131', 27.41840, -98.51400, 4, 1, NULL, NULL),
(711, '78384', 'San Diego', 'Texas', 'TX', 'Duval', '131', 27.76540, -98.25030, 4, 1, NULL, NULL),
(712, '76435', 'Carbon', 'Texas', 'TX', 'Eastland', '133', 32.27010, -98.83480, 4, 1, NULL, NULL),
(713, '76437', 'Cisco', 'Texas', 'TX', 'Eastland', '133', 32.38000, -98.98650, 4, 1, NULL, NULL),
(714, '76445', 'Desdemona', 'Texas', 'TX', 'Eastland', '133', 32.28190, -98.56730, 4, 1, NULL, NULL),
(715, '76448', 'Eastland', 'Texas', 'TX', 'Eastland', '133', 32.39940, -98.80710, 4, 1, NULL, NULL),
(716, '76454', 'Gorman', 'Texas', 'TX', 'Eastland', '133', 32.22340, -98.68340, 4, 1, NULL, NULL),
(717, '76466', 'Olden', 'Texas', 'TX', 'Eastland', '133', 32.44210, -98.73420, 4, 1, NULL, NULL),
(718, '76470', 'Ranger', 'Texas', 'TX', 'Eastland', '133', 32.46810, -98.67470, 4, 1, NULL, NULL),
(719, '76471', 'Rising Star', 'Texas', 'TX', 'Eastland', '133', 32.12800, -98.98590, 4, 1, NULL, NULL),
(720, '79741', 'Goldsmith', 'Texas', 'TX', 'Ector', '135', 31.95410, -102.62500, 4, 1, NULL, NULL),
(721, '79758', 'Gardendale', 'Texas', 'TX', 'Ector', '135', 32.02450, -102.35720, 4, 1, NULL, NULL),
(722, '79759', 'Notrees', 'Texas', 'TX', 'Ector', '135', 31.85990, -102.74130, 4, 1, NULL, NULL),
(723, '79760', 'Odessa', 'Texas', 'TX', 'Ector', '135', 31.76520, -102.35430, 4, 1, NULL, NULL),
(724, '79761', 'Odessa', 'Texas', 'TX', 'Ector', '135', 31.85790, -102.35230, 4, 1, NULL, NULL),
(725, '79762', 'Odessa', 'Texas', 'TX', 'Ector', '135', 31.88900, -102.35480, 4, 1, NULL, NULL),
(726, '79763', 'Odessa', 'Texas', 'TX', 'Ector', '135', 31.83410, -102.41620, 4, 1, NULL, NULL),
(727, '79764', 'Odessa', 'Texas', 'TX', 'Ector', '135', 31.87670, -102.43750, 4, 1, NULL, NULL),
(728, '79765', 'Odessa', 'Texas', 'TX', 'Ector', '135', 31.93750, -102.39440, 4, 1, NULL, NULL),
(729, '79766', 'Odessa', 'Texas', 'TX', 'Ector', '135', 31.78270, -102.34490, 4, 1, NULL, NULL),
(730, '79768', 'Odessa', 'Texas', 'TX', 'Ector', '135', 31.84570, -102.36760, 4, 1, NULL, NULL),
(731, '79769', 'Odessa', 'Texas', 'TX', 'Ector', '135', 31.84570, -102.36760, 4, 1, NULL, NULL),
(732, '79776', 'Penwell', 'Texas', 'TX', 'Ector', '135', 31.73340, -102.58790, 4, 1, NULL, NULL),
(733, '76883', 'Telegraph', 'Texas', 'TX', 'Edwards', '137', 30.32740, -99.90620, 4, 1, NULL, NULL),
(734, '78828', 'Barksdale', 'Texas', 'TX', 'Edwards', '137', 29.82820, -100.10500, 4, 1, NULL, NULL),
(735, '78880', 'Rocksprings', 'Texas', 'TX', 'Edwards', '137', 30.01880, -100.23100, 4, 1, NULL, NULL),
(736, '75101', 'Bardwell', 'Texas', 'TX', 'Ellis', '139', 32.27310, -96.70300, 4, 1, NULL, NULL),
(737, '75119', 'Ennis', 'Texas', 'TX', 'Ellis', '139', 32.33210, -96.62240, 4, 1, NULL, NULL),
(738, '75120', 'Ennis', 'Texas', 'TX', 'Ellis', '139', 32.33470, -96.63350, 4, 1, NULL, NULL),
(739, '75125', 'Ferris', 'Texas', 'TX', 'Ellis', '139', 32.52230, -96.66430, 4, 1, NULL, NULL),
(740, '75152', 'Palmer', 'Texas', 'TX', 'Ellis', '139', 32.43870, -96.67940, 4, 1, NULL, NULL),
(741, '75154', 'Red Oak', 'Texas', 'TX', 'Ellis', '139', 32.51850, -96.80710, 4, 1, NULL, NULL),
(742, '75155', 'Rice', 'Texas', 'TX', 'Ellis', '139', 32.22580, -96.46060, 4, 1, NULL, NULL),
(743, '75165', 'Waxahachie', 'Texas', 'TX', 'Ellis', '139', 32.38080, -96.83740, 4, 1, NULL, NULL),
(744, '75167', 'Waxahachie', 'Texas', 'TX', 'Ellis', '139', 32.37730, -96.91620, 4, 1, NULL, NULL),
(745, '75168', 'Waxahachie', 'Texas', 'TX', 'Ellis', '139', 32.37490, -96.71660, 4, 1, NULL, NULL),
(746, '76041', 'Forreston', 'Texas', 'TX', 'Ellis', '139', 32.24860, -96.82320, 4, 1, NULL, NULL),
(747, '76064', 'Maypearl', 'Texas', 'TX', 'Ellis', '139', 32.29930, -97.02710, 4, 1, NULL, NULL),
(748, '76065', 'Midlothian', 'Texas', 'TX', 'Ellis', '139', 32.47570, -96.99360, 4, 1, NULL, NULL),
(749, '76623', 'Avalon', 'Texas', 'TX', 'Ellis', '139', 32.20540, -96.79000, 4, 1, NULL, NULL),
(750, '76651', 'Italy', 'Texas', 'TX', 'Ellis', '139', 32.17850, -96.88230, 4, 1, NULL, NULL),
(751, '76670', 'Milford', 'Texas', 'TX', 'Ellis', '139', 32.14820, -96.96120, 4, 1, NULL, NULL),
(752, '79821', 'Anthony', 'Texas', 'TX', 'El Paso', '141', 31.99070, -106.59760, 4, 1, NULL, NULL),
(753, '79835', 'Canutillo', 'Texas', 'TX', 'El Paso', '141', 31.93440, -106.59290, 4, 1, NULL, NULL),
(754, '79836', 'Clint', 'Texas', 'TX', 'El Paso', '141', 31.54940, -106.20380, 4, 1, NULL, NULL),
(755, '79838', 'Fabens', 'Texas', 'TX', 'El Paso', '141', 31.47450, -106.15890, 4, 1, NULL, NULL),
(756, '79849', 'San Elizario', 'Texas', 'TX', 'El Paso', '141', 31.55080, -106.25070, 4, 1, NULL, NULL),
(757, '79853', 'Tornillo', 'Texas', 'TX', 'El Paso', '141', 31.44070, -106.07650, 1, 1, NULL, NULL),
(758, '79901', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.75840, -106.47830, 4, 1, NULL, NULL),
(759, '79902', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.77630, -106.49320, 4, 1, NULL, NULL),
(760, '79903', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.78620, -106.44060, 4, 1, NULL, NULL),
(761, '79904', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.85330, -106.43810, 4, 1, NULL, NULL),
(762, '79905', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.76740, -106.43040, 4, 1, NULL, NULL),
(763, '79906', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.80920, -106.42470, 4, 1, NULL, NULL),
(764, '79907', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.70890, -106.32930, 4, 1, NULL, NULL),
(765, '79908', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.82650, -106.38570, 4, 1, NULL, NULL),
(766, '79910', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.69480, -106.30000, 4, 1, NULL, NULL),
(767, '79911', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.89250, -106.54260, 4, 1, NULL, NULL),
(768, '79912', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.83830, -106.53640, 4, 1, NULL, NULL),
(769, '79913', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.93730, -106.57240, 4, 1, NULL, NULL),
(770, '79914', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.69480, -106.30000, 4, 1, NULL, NULL),
(771, '79915', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.74320, -106.36860, 4, 1, NULL, NULL),
(772, '79916', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.74440, -106.28790, 4, 1, NULL, NULL),
(773, '79917', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.69480, -106.30000, 4, 1, NULL, NULL),
(774, '79918', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.83180, -106.39070, 4, 1, NULL, NULL),
(775, '79920', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.82140, -106.46140, 4, 1, NULL, NULL),
(776, '79922', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.82180, -106.57320, 4, 1, NULL, NULL),
(777, '79923', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.69480, -106.30000, 4, 1, NULL, NULL),
(778, '79924', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.90210, -106.41490, 4, 1, NULL, NULL),
(779, '79925', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.78140, -106.36130, 4, 1, NULL, NULL),
(780, '79926', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.69480, -106.30000, 4, 1, NULL, NULL),
(781, '79927', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.64460, -106.27370, 4, 1, NULL, NULL),
(782, '79928', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.66310, -106.14010, 4, 1, NULL, NULL),
(783, '79929', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.69480, -106.30000, 4, 1, NULL, NULL),
(784, '79930', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.80480, -106.45680, 4, 1, NULL, NULL),
(785, '79931', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.69480, -106.30000, 4, 1, NULL, NULL),
(786, '79932', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.86230, -106.59320, 4, 1, NULL, NULL),
(787, '79934', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.93860, -106.40730, 4, 1, NULL, NULL),
(788, '79935', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.77180, -106.33030, 4, 1, NULL, NULL),
(789, '79936', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.76770, -106.30160, 4, 1, NULL, NULL),
(790, '79937', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.69480, -106.30000, 4, 1, NULL, NULL),
(791, '79938', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.80450, -105.96610, 4, 1, NULL, NULL),
(792, '79940', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.69480, -106.30000, 4, 1, NULL, NULL),
(793, '79941', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.75870, -106.48690, 4, 1, NULL, NULL),
(794, '79942', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.69480, -106.30000, 4, 1, NULL, NULL),
(795, '79943', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.75870, -106.48690, 4, 1, NULL, NULL),
(796, '79944', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.75870, -106.48690, 4, 1, NULL, NULL),
(797, '79945', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.75870, -106.48690, 4, 1, NULL, NULL),
(798, '79946', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.75870, -106.48690, 4, 1, NULL, NULL),
(799, '79947', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.75870, -106.48690, 4, 1, NULL, NULL),
(800, '79948', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.75870, -106.48690, 4, 1, NULL, NULL),
(801, '79949', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.75870, -106.48690, 4, 1, NULL, NULL),
(802, '79950', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.75870, -106.48690, 4, 1, NULL, NULL),
(803, '79951', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.75870, -106.48690, 4, 1, NULL, NULL),
(804, '79952', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.75870, -106.48690, 4, 1, NULL, NULL),
(805, '79953', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.75870, -106.48690, 4, 1, NULL, NULL),
(806, '79954', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.75870, -106.48690, 4, 1, NULL, NULL),
(807, '79955', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.75870, -106.48690, 4, 1, NULL, NULL),
(808, '79958', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.75870, -106.48690, 4, 1, NULL, NULL),
(809, '79960', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.75870, -106.48690, 4, 1, NULL, NULL),
(810, '79961', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.69480, -106.30000, 4, 1, NULL, NULL),
(811, '79968', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.77050, -106.50480, 4, 1, NULL, NULL),
(812, '79976', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.69480, -106.30000, 4, 1, NULL, NULL),
(813, '79978', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.75870, -106.48690, 4, 1, NULL, NULL),
(814, '79980', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.69480, -106.30000, 4, 1, NULL, NULL),
(815, '79990', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.69480, -106.30000, 4, 1, NULL, NULL),
(816, '79995', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.69480, -106.30000, 4, 1, NULL, NULL),
(817, '79996', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.69480, -106.30000, 4, 1, NULL, NULL),
(818, '79997', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.69480, -106.30000, 4, 1, NULL, NULL),
(819, '79998', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.69480, -106.30000, 4, 1, NULL, NULL),
(820, '79999', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.69480, -106.30000, 4, 1, NULL, NULL),
(821, '88510', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.69480, -106.30000, 4, 1, NULL, NULL),
(822, '88511', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.69480, -106.30000, 4, 1, NULL, NULL),
(823, '88512', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.69480, -106.30000, 4, 1, NULL, NULL),
(824, '88513', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.69480, -106.30000, 4, 1, NULL, NULL),
(825, '88514', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.69480, -106.30000, 4, 1, NULL, NULL),
(826, '88515', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.69480, -106.30000, 4, 1, NULL, NULL),
(827, '88517', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.69480, -106.30000, 4, 1, NULL, NULL),
(828, '88518', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.69480, -106.30000, 4, 1, NULL, NULL),
(829, '88519', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.69480, -106.30000, 4, 1, NULL, NULL),
(830, '88520', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.69480, -106.30000, 4, 1, NULL, NULL),
(831, '88521', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.69480, -106.30000, 4, 1, NULL, NULL),
(832, '88523', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.69480, -106.30000, 4, 1, NULL, NULL),
(833, '88524', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.69480, -106.30000, 4, 1, NULL, NULL),
(834, '88525', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.69480, -106.30000, 4, 1, NULL, NULL),
(835, '88526', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.69480, -106.30000, 4, 1, NULL, NULL),
(836, '88527', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.69480, -106.30000, 4, 1, NULL, NULL),
(837, '88528', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.69480, -106.30000, 4, 1, NULL, NULL),
(838, '88529', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.69480, -106.30000, 4, 1, NULL, NULL),
(839, '88530', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.69480, -106.30000, 4, 1, NULL, NULL),
(840, '88531', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.69480, -106.30000, 4, 1, NULL, NULL),
(841, '88532', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.69480, -106.30000, 4, 1, NULL, NULL),
(842, '88533', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.69480, -106.30000, 4, 1, NULL, NULL),
(843, '88534', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.69480, -106.30000, 4, 1, NULL, NULL),
(844, '88535', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.69480, -106.30000, 4, 1, NULL, NULL),
(845, '88536', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.69480, -106.30000, 4, 1, NULL, NULL),
(846, '88538', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.69480, -106.30000, 4, 1, NULL, NULL),
(847, '88539', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.69480, -106.30000, 4, 1, NULL, NULL),
(848, '88540', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.69480, -106.30000, 4, 1, NULL, NULL),
(849, '88541', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.69480, -106.30000, 4, 1, NULL, NULL),
(850, '88542', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.69480, -106.30000, 4, 1, NULL, NULL),
(851, '88543', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.69480, -106.30000, 4, 1, NULL, NULL),
(852, '88544', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.69480, -106.30000, 4, 1, NULL, NULL),
(853, '88545', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.69480, -106.30000, 4, 1, NULL, NULL),
(854, '88546', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.69480, -106.30000, 4, 1, NULL, NULL),
(855, '88547', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.69480, -106.30000, 4, 1, NULL, NULL),
(856, '88548', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.69480, -106.30000, 4, 1, NULL, NULL),
(857, '88549', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.69480, -106.30000, 4, 1, NULL, NULL),
(858, '88550', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.69480, -106.30000, 4, 1, NULL, NULL),
(859, '88553', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.69480, -106.30000, 4, 1, NULL, NULL),
(860, '88554', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.69480, -106.30000, 4, 1, NULL, NULL),
(861, '88555', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.69480, -106.30000, 4, 1, NULL, NULL),
(862, '88556', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.69480, -106.30000, 4, 1, NULL, NULL),
(863, '88557', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.69480, -106.30000, 4, 1, NULL, NULL),
(864, '88558', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.69480, -106.30000, 4, 1, NULL, NULL),
(865, '88559', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.69480, -106.30000, 4, 1, NULL, NULL),
(866, '88560', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.69480, -106.30000, 4, 1, NULL, NULL),
(867, '88561', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.69480, -106.30000, 4, 1, NULL, NULL),
(868, '88562', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.69480, -106.30000, 4, 1, NULL, NULL),
(869, '88563', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.69480, -106.30000, 4, 1, NULL, NULL),
(870, '88565', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.69480, -106.30000, 4, 1, NULL, NULL),
(871, '88566', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.69480, -106.30000, 4, 1, NULL, NULL),
(872, '88567', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.69480, -106.30000, 4, 1, NULL, NULL),
(873, '88568', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.69480, -106.30000, 4, 1, NULL, NULL),
(874, '88569', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.69480, -106.30000, 4, 1, NULL, NULL),
(875, '88570', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.69480, -106.30000, 4, 1, NULL, NULL),
(876, '88571', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.69480, -106.30000, 4, 1, NULL, NULL),
(877, '88572', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.69480, -106.30000, 4, 1, NULL, NULL),
(878, '88573', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.69480, -106.30000, 4, 1, NULL, NULL),
(879, '88574', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.69480, -106.30000, 4, 1, NULL, NULL),
(880, '88575', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.69480, -106.30000, 4, 1, NULL, NULL),
(881, '88576', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.69480, -106.30000, 4, 1, NULL, NULL),
(882, '88577', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.69480, -106.30000, 4, 1, NULL, NULL),
(883, '88578', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.69480, -106.30000, 4, 1, NULL, NULL),
(884, '88579', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.69480, -106.30000, 4, 1, NULL, NULL),
(885, '88580', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.69480, -106.30000, 4, 1, NULL, NULL),
(886, '88581', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.69480, -106.30000, 4, 1, NULL, NULL),
(887, '88582', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.69480, -106.30000, 4, 1, NULL, NULL),
(888, '88583', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.69480, -106.30000, 4, 1, NULL, NULL),
(889, '88584', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.69480, -106.30000, 4, 1, NULL, NULL),
(890, '88585', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.69480, -106.30000, 4, 1, NULL, NULL),
(891, '88586', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.69480, -106.30000, 4, 1, NULL, NULL),
(892, '88587', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.69480, -106.30000, 4, 1, NULL, NULL),
(893, '88588', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.69480, -106.30000, 4, 1, NULL, NULL),
(894, '88589', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.69480, -106.30000, 4, 1, NULL, NULL),
(895, '88590', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.69480, -106.30000, 4, 1, NULL, NULL),
(896, '88595', 'El Paso', 'Texas', 'TX', 'El Paso', '141', 31.69480, -106.30000, 4, 1, NULL, NULL),
(897, '76401', 'Stephenville', 'Texas', 'TX', 'Erath', '143', 32.22140, -98.22240, 4, 1, NULL, NULL),
(898, '76402', 'Stephenville', 'Texas', 'TX', 'Erath', '143', 32.21530, -98.20800, 4, 1, NULL, NULL),
(899, '76433', 'Bluff Dale', 'Texas', 'TX', 'Erath', '143', 32.28840, -98.05390, 4, 1, NULL, NULL),
(900, '76446', 'Dublin', 'Texas', 'TX', 'Erath', '143', 32.09090, -98.34550, 4, 1, NULL, NULL),
(901, '76461', 'Lingleville', 'Texas', 'TX', 'Erath', '143', 32.24460, -98.37750, 4, 1, NULL, NULL),
(902, '76465', 'Morgan Mill', 'Texas', 'TX', 'Erath', '143', 32.38820, -98.16730, 4, 1, NULL, NULL),
(903, '76570', 'Rosebud', 'Texas', 'TX', 'Falls', '145', 31.09220, -96.97550, 4, 1, NULL, NULL),
(904, '76632', 'Chilton', 'Texas', 'TX', 'Falls', '145', 31.31000, -97.09000, 4, 1, NULL, NULL),
(905, '76656', 'Lott', 'Texas', 'TX', 'Falls', '145', 31.19250, -97.05810, 4, 1, NULL, NULL),
(906, '76661', 'Marlin', 'Texas', 'TX', 'Falls', '145', 31.30360, -96.88890, 4, 1, NULL, NULL),
(907, '76680', 'Reagan', 'Texas', 'TX', 'Falls', '145', 31.18710, -96.79980, 4, 1, NULL, NULL),
(908, '76685', 'Satin', 'Texas', 'TX', 'Falls', '145', 31.36010, -97.01080, 4, 1, NULL, NULL),
(909, '75413', 'Bailey', 'Texas', 'TX', 'Fannin', '147', 33.43000, -96.16620, 4, 1, NULL, NULL),
(910, '75418', 'Bonham', 'Texas', 'TX', 'Fannin', '147', 33.58060, -96.18360, 4, 1, NULL, NULL),
(911, '75438', 'Dodd City', 'Texas', 'TX', 'Fannin', '147', 33.56470, -96.06190, 4, 1, NULL, NULL),
(912, '75439', 'Ector', 'Texas', 'TX', 'Fannin', '147', 33.58190, -96.27350, 4, 1, NULL, NULL),
(913, '75443', 'Gober', 'Texas', 'TX', 'Fannin', '147', 33.46970, -96.11110, 4, 1, NULL, NULL),
(914, '75446', 'Honey Grove', 'Texas', 'TX', 'Fannin', '147', 33.59850, -95.91090, 4, 1, NULL, NULL),
(915, '75447', 'Ivanhoe', 'Texas', 'TX', 'Fannin', '147', 33.76120, -96.11930, 4, 1, NULL, NULL),
(916, '75449', 'Ladonia', 'Texas', 'TX', 'Fannin', '147', 33.42450, -95.94550, 4, 1, NULL, NULL),
(917, '75452', 'Leonard', 'Texas', 'TX', 'Fannin', '147', 33.37960, -96.24750, 4, 1, NULL, NULL),
(918, '75475', 'Randolph', 'Texas', 'TX', 'Fannin', '147', 33.48460, -96.25410, 4, 1, NULL, NULL),
(919, '75476', 'Ravenna', 'Texas', 'TX', 'Fannin', '147', 33.70980, -96.14520, 4, 1, NULL, NULL),
(920, '75479', 'Savoy', 'Texas', 'TX', 'Fannin', '147', 33.60660, -96.35020, 4, 1, NULL, NULL),
(921, '75488', 'Telephone', 'Texas', 'TX', 'Fannin', '147', 33.79790, -96.04490, 4, 1, NULL, NULL),
(922, '75490', 'Trenton', 'Texas', 'TX', 'Fannin', '147', 33.42350, -96.33980, 4, 1, NULL, NULL),
(923, '75492', 'Windom', 'Texas', 'TX', 'Fannin', '147', 33.56330, -96.00200, 4, 1, NULL, NULL),
(924, '78932', 'Carmine', 'Texas', 'TX', 'Fayette', '149', 30.14040, -96.68610, 4, 1, NULL, NULL),
(925, '78938', 'Ellinger', 'Texas', 'TX', 'Fayette', '149', 29.84590, -96.69660, 4, 1, NULL, NULL),
(926, '78940', 'Fayetteville', 'Texas', 'TX', 'Fayette', '149', 29.88680, -96.64630, 4, 1, NULL, NULL),
(927, '78941', 'Flatonia', 'Texas', 'TX', 'Fayette', '149', 29.70950, -97.09870, 4, 1, NULL, NULL),
(928, '78945', 'La Grange', 'Texas', 'TX', 'Fayette', '149', 29.90400, -96.88600, 4, 1, NULL, NULL),
(929, '78946', 'Ledbetter', 'Texas', 'TX', 'Fayette', '149', 30.23830, -96.76130, 4, 1, NULL, NULL),
(930, '78949', 'Muldoon', 'Texas', 'TX', 'Fayette', '149', 29.79930, -97.10060, 4, 1, NULL, NULL),
(931, '78952', 'Plum', 'Texas', 'TX', 'Fayette', '149', 29.93490, -96.96750, 4, 1, NULL, NULL),
(932, '78954', 'Round Top', 'Texas', 'TX', 'Fayette', '149', 30.04110, -96.73410, 4, 1, NULL, NULL),
(933, '78956', 'Schulenburg', 'Texas', 'TX', 'Fayette', '149', 29.68820, -96.91060, 4, 1, NULL, NULL),
(934, '78960', 'Warda', 'Texas', 'TX', 'Fayette', '149', 30.07020, -96.90200, 4, 1, NULL, NULL),
(935, '78961', 'Round Top', 'Texas', 'TX', 'Fayette', '149', 30.06520, -96.69610, 4, 1, NULL, NULL),
(936, '78963', 'West Point', 'Texas', 'TX', 'Fayette', '149', 29.95230, -97.03610, 4, 1, NULL, NULL),
(937, '79534', 'Mc Caulley', 'Texas', 'TX', 'Fisher', '151', 32.77870, -100.21680, 1, 1, NULL, NULL),
(938, '79543', 'Roby', 'Texas', 'TX', 'Fisher', '151', 32.72200, -100.40080, 4, 1, NULL, NULL),
(939, '79546', 'Rotan', 'Texas', 'TX', 'Fisher', '151', 32.85550, -100.47050, 4, 1, NULL, NULL),
(940, '79560', 'Sylvester', 'Texas', 'TX', 'Fisher', '151', 32.72090, -100.25430, 4, 1, NULL, NULL),
(941, '79221', 'Aiken', 'Texas', 'TX', 'Floyd', '153', 34.14230, -101.52570, 4, 1, NULL, NULL),
(942, '79231', 'Dougherty', 'Texas', 'TX', 'Floyd', '153', 33.94460, -101.09220, 4, 1, NULL, NULL),
(943, '79235', 'Floydada', 'Texas', 'TX', 'Floyd', '153', 33.97430, -101.33460, 4, 1, NULL, NULL),
(944, '79241', 'Lockney', 'Texas', 'TX', 'Floyd', '153', 34.14580, -101.42590, 4, 1, NULL, NULL),
(945, '79258', 'South Plains', 'Texas', 'TX', 'Floyd', '153', 34.22450, -101.30960, 4, 1, NULL, NULL),
(946, '79227', 'Crowell', 'Texas', 'TX', 'Foard', '155', 33.99120, -99.69830, 4, 1, NULL, NULL),
(947, '77406', 'Richmond', 'Texas', 'TX', 'Fort Bend', '157', 29.64360, -95.79800, 4, 1, NULL, NULL),
(948, '77407', 'Richmond', 'Texas', 'TX', 'Fort Bend', '157', 29.66250, -95.72720, 4, 1, NULL, NULL),
(949, '77417', 'Beasley', 'Texas', 'TX', 'Fort Bend', '157', 29.47900, -95.96810, 4, 1, NULL, NULL),
(950, '77441', 'Fulshear', 'Texas', 'TX', 'Fort Bend', '157', 29.72170, -95.89770, 4, 1, NULL, NULL),
(951, '77444', 'Guy', 'Texas', 'TX', 'Fort Bend', '157', 29.33270, -95.77020, 4, 1, NULL, NULL),
(952, '77451', 'Kendleton', 'Texas', 'TX', 'Fort Bend', '157', 29.44700, -96.00360, 4, 1, NULL, NULL),
(953, '77459', 'Missouri City', 'Texas', 'TX', 'Fort Bend', '157', 29.57040, -95.54230, 4, 1, NULL, NULL),
(954, '77461', 'Needville', 'Texas', 'TX', 'Fort Bend', '157', 29.41170, -95.82730, 4, 1, NULL, NULL),
(955, '77464', 'Orchard', 'Texas', 'TX', 'Fort Bend', '157', 29.59480, -95.97270, 4, 1, NULL, NULL),
(956, '77469', 'Richmond', 'Texas', 'TX', 'Fort Bend', '157', 29.55110, -95.73290, 4, 1, NULL, NULL),
(957, '77471', 'Rosenberg', 'Texas', 'TX', 'Fort Bend', '157', 29.54970, -95.79820, 4, 1, NULL, NULL),
(958, '77476', 'Simonton', 'Texas', 'TX', 'Fort Bend', '157', 29.67940, -95.97720, 4, 1, NULL, NULL),
(959, '77477', 'Stafford', 'Texas', 'TX', 'Fort Bend', '157', 29.62280, -95.56780, 4, 1, NULL, NULL),
(960, '77478', 'Sugar Land', 'Texas', 'TX', 'Fort Bend', '157', 29.61960, -95.60700, 4, 1, NULL, NULL),
(961, '77479', 'Sugar Land', 'Texas', 'TX', 'Fort Bend', '157', 29.57850, -95.60660, 4, 1, NULL, NULL),
(962, '77481', 'Thompsons', 'Texas', 'TX', 'Fort Bend', '157', 29.46830, -95.57760, 4, 1, NULL, NULL),
(963, '77487', 'Sugar Land', 'Texas', 'TX', 'Fort Bend', '157', 29.61970, -95.63490, 4, 1, NULL, NULL),
(964, '77489', 'Missouri City', 'Texas', 'TX', 'Fort Bend', '157', 29.59620, -95.51150, 4, 1, NULL, NULL),
(965, '77496', 'Sugar Land', 'Texas', 'TX', 'Fort Bend', '157', 29.52550, -95.75650, 4, 1, NULL, NULL),
(966, '77497', 'Stafford', 'Texas', 'TX', 'Fort Bend', '157', 29.61610, -95.55770, 4, 1, NULL, NULL),
(967, '77498', 'Sugar Land', 'Texas', 'TX', 'Fort Bend', '157', 29.63960, -95.65000, 4, 1, NULL, NULL),
(968, '77545', 'Fresno', 'Texas', 'TX', 'Fort Bend', '157', 29.52930, -95.46260, 4, 1, NULL, NULL),
(969, '75457', 'Mount Vernon', 'Texas', 'TX', 'Franklin', '159', 33.17020, -95.21810, 4, 1, NULL, NULL),
(970, '75480', 'Scroggins', 'Texas', 'TX', 'Franklin', '159', 33.04780, -95.19620, 4, 1, NULL, NULL),
(971, '75487', 'Talco', 'Texas', 'TX', 'Franklin', '159', 33.33440, -95.04970, 4, 1, NULL, NULL),
(972, '75838', 'Donie', 'Texas', 'TX', 'Freestone', '161', 31.48730, -96.23870, 4, 1, NULL, NULL),
(973, '75840', 'Fairfield', 'Texas', 'TX', 'Freestone', '161', 31.73610, -96.15720, 4, 1, NULL, NULL),
(974, '75848', 'Kirvin', 'Texas', 'TX', 'Freestone', '161', 31.82310, -96.32180, 4, 1, NULL, NULL),
(975, '75859', 'Streetman', 'Texas', 'TX', 'Freestone', '161', 31.88850, -96.29880, 4, 1, NULL, NULL),
(976, '75860', 'Teague', 'Texas', 'TX', 'Freestone', '161', 31.63280, -96.27780, 4, 1, NULL, NULL),
(977, '76693', 'Wortham', 'Texas', 'TX', 'Freestone', '161', 31.78650, -96.42020, 4, 1, NULL, NULL),
(978, '78005', 'Bigfoot', 'Texas', 'TX', 'Frio', '163', 29.05310, -98.85820, 4, 1, NULL, NULL),
(979, '78017', 'Dilley', 'Texas', 'TX', 'Frio', '163', 28.67820, -99.17470, 4, 1, NULL, NULL),
(980, '78057', 'Moore', 'Texas', 'TX', 'Frio', '163', 29.05950, -99.02040, 4, 1, NULL, NULL),
(981, '78061', 'Pearsall', 'Texas', 'TX', 'Frio', '163', 28.89230, -99.09440, 4, 1, NULL, NULL),
(982, '79342', 'Loop', 'Texas', 'TX', 'Gaines', '165', 32.91620, -102.42210, 4, 1, NULL, NULL),
(983, '79359', 'Seagraves', 'Texas', 'TX', 'Gaines', '165', 32.93170, -102.57810, 4, 1, NULL, NULL),
(984, '79360', 'Seminole', 'Texas', 'TX', 'Gaines', '165', 32.72100, -102.68280, 4, 1, NULL, NULL),
(985, '77510', 'Santa Fe', 'Texas', 'TX', 'Galveston', '167', 29.40320, -95.07340, 4, 1, NULL, NULL),
(986, '77517', 'Santa Fe', 'Texas', 'TX', 'Galveston', '167', 29.36730, -95.13610, 4, 1, NULL, NULL),
(987, '77518', 'Bacliff', 'Texas', 'TX', 'Galveston', '167', 29.50550, -94.98930, 4, 1, NULL, NULL),
(988, '77539', 'Dickinson', 'Texas', 'TX', 'Galveston', '167', 29.45850, -95.03450, 4, 1, NULL, NULL),
(989, '77546', 'Friendswood', 'Texas', 'TX', 'Galveston', '167', 29.52240, -95.18790, 4, 1, NULL, NULL),
(990, '77549', 'Friendswood', 'Texas', 'TX', 'Galveston', '167', 29.52940, -95.20100, 4, 1, NULL, NULL),
(991, '77550', 'Galveston', 'Texas', 'TX', 'Galveston', '167', 29.29830, -94.79300, 4, 1, NULL, NULL),
(992, '77551', 'Galveston', 'Texas', 'TX', 'Galveston', '167', 29.27660, -94.83030, 4, 1, NULL, NULL),
(993, '77552', 'Galveston', 'Texas', 'TX', 'Galveston', '167', 29.22050, -94.94440, 4, 1, NULL, NULL),
(994, '77553', 'Galveston', 'Texas', 'TX', 'Galveston', '167', 29.33220, -94.80020, 4, 1, NULL, NULL),
(995, '77554', 'Galveston', 'Texas', 'TX', 'Galveston', '167', 29.22430, -94.96370, 4, 1, NULL, NULL),
(996, '77555', 'Galveston', 'Texas', 'TX', 'Galveston', '167', 29.33050, -94.80020, 4, 1, NULL, NULL),
(997, '77563', 'Hitchcock', 'Texas', 'TX', 'Galveston', '167', 29.33980, -94.99260, 4, 1, NULL, NULL),
(998, '77565', 'Kemah', 'Texas', 'TX', 'Galveston', '167', 29.52360, -95.02760, 4, 1, NULL, NULL),
(999, '77568', 'La Marque', 'Texas', 'TX', 'Galveston', '167', 29.36760, -94.97420, 4, 1, NULL, NULL),
(1000, '77573', 'League City', 'Texas', 'TX', 'Galveston', '167', 29.51730, -95.09630, 4, 1, NULL, NULL),
(1001, '77574', 'League City', 'Texas', 'TX', 'Galveston', '167', 29.51160, -95.05820, 4, 1, NULL, NULL),
(1002, '77590', 'Texas City', 'Texas', 'TX', 'Galveston', '167', 29.39700, -94.92030, 4, 1, NULL, NULL),
(1003, '77591', 'Texas City', 'Texas', 'TX', 'Galveston', '167', 29.38910, -94.99420, 4, 1, NULL, NULL),
(1004, '77592', 'Texas City', 'Texas', 'TX', 'Galveston', '167', 29.33050, -94.80020, 4, 1, NULL, NULL),
(1005, '77617', 'Gilchrist', 'Texas', 'TX', 'Galveston', '167', 29.52300, -94.47550, 4, 1, NULL, NULL),
(1006, '77623', 'High Island', 'Texas', 'TX', 'Galveston', '167', 29.54720, -94.42670, 4, 1, NULL, NULL),
(1007, '77650', 'Port Bolivar', 'Texas', 'TX', 'Galveston', '167', 29.42660, -94.68610, 4, 1, NULL, NULL),
(1008, '79330', 'Justiceburg', 'Texas', 'TX', 'Garza', '169', 33.05940, -101.18810, 4, 1, NULL, NULL),
(1009, '79356', 'Post', 'Texas', 'TX', 'Garza', '169', 33.20170, -101.39220, 4, 1, NULL, NULL),
(1010, '78618', 'Doss', 'Texas', 'TX', 'Gillespie', '171', 30.46130, -99.17070, 4, 1, NULL, NULL);
INSERT INTO `zip_codes` (`id`, `zipcode`, `city`, `state`, `state_abbr`, `county_area`, `code`, `latitude`, `longitude`, `some_field`, `status`, `created_at`, `updated_at`) VALUES
(1011, '78624', 'Fredericksburg', 'Texas', 'TX', 'Gillespie', '171', 30.28170, -98.87990, 4, 1, NULL, NULL),
(1012, '78631', 'Harper', 'Texas', 'TX', 'Gillespie', '171', 30.28160, -99.24100, 4, 1, NULL, NULL),
(1013, '78671', 'Stonewall', 'Texas', 'TX', 'Gillespie', '171', 30.20880, -98.61310, 4, 1, NULL, NULL),
(1014, '78675', 'Willow City', 'Texas', 'TX', 'Gillespie', '171', 30.45490, -98.66460, 4, 1, NULL, NULL),
(1015, '79739', 'Garden City', 'Texas', 'TX', 'Glasscock', '173', 31.84910, -101.52690, 4, 1, NULL, NULL),
(1016, '77960', 'Fannin', 'Texas', 'TX', 'Goliad', '175', 28.67780, -97.26060, 4, 1, NULL, NULL),
(1017, '77963', 'Goliad', 'Texas', 'TX', 'Goliad', '175', 28.69930, -97.37830, 4, 1, NULL, NULL),
(1018, '77993', 'Weesatche', 'Texas', 'TX', 'Goliad', '175', 28.83580, -97.44420, 4, 1, NULL, NULL),
(1019, '78107', 'Berclair', 'Texas', 'TX', 'Goliad', '175', 28.52950, -97.59250, 4, 1, NULL, NULL),
(1020, '78122', 'Leesville', 'Texas', 'TX', 'Gonzales', '177', 29.39610, -97.75660, 4, 1, NULL, NULL),
(1021, '78140', 'Nixon', 'Texas', 'TX', 'Gonzales', '177', 29.30160, -97.75290, 4, 1, NULL, NULL),
(1022, '78159', 'Smiley', 'Texas', 'TX', 'Gonzales', '177', 29.26550, -97.62270, 4, 1, NULL, NULL),
(1023, '78604', 'Belmont', 'Texas', 'TX', 'Gonzales', '177', 29.52330, -97.68390, 4, 1, NULL, NULL),
(1024, '78614', 'Cost', 'Texas', 'TX', 'Gonzales', '177', 29.43210, -97.55310, 4, 1, NULL, NULL),
(1025, '78629', 'Gonzales', 'Texas', 'TX', 'Gonzales', '177', 29.50860, -97.44950, 4, 1, NULL, NULL),
(1026, '78632', 'Harwood', 'Texas', 'TX', 'Gonzales', '177', 29.66610, -97.49060, 4, 1, NULL, NULL),
(1027, '78658', 'Ottine', 'Texas', 'TX', 'Gonzales', '177', 29.59520, -97.59110, 4, 1, NULL, NULL),
(1028, '78677', 'Wrightsboro', 'Texas', 'TX', 'Gonzales', '177', 29.37390, -97.56500, 4, 1, NULL, NULL),
(1029, '78959', 'Waelder', 'Texas', 'TX', 'Gonzales', '177', 29.68680, -97.29580, 4, 1, NULL, NULL),
(1030, '79002', 'Alanreed', 'Texas', 'TX', 'Gray', '179', 35.22730, -100.75920, 4, 1, NULL, NULL),
(1031, '79054', 'Lefors', 'Texas', 'TX', 'Gray', '179', 35.43910, -100.80590, 4, 1, NULL, NULL),
(1032, '79057', 'Mclean', 'Texas', 'TX', 'Gray', '179', 35.23120, -100.60080, 4, 1, NULL, NULL),
(1033, '79065', 'Pampa', 'Texas', 'TX', 'Gray', '179', 35.53800, -100.95790, 4, 1, NULL, NULL),
(1034, '79066', 'Pampa', 'Texas', 'TX', 'Gray', '179', 35.53340, -100.95600, 4, 1, NULL, NULL),
(1035, '75020', 'Denison', 'Texas', 'TX', 'Grayson', '181', 33.74500, -96.54960, 4, 1, NULL, NULL),
(1036, '75021', 'Denison', 'Texas', 'TX', 'Grayson', '181', 33.71690, -96.52350, 4, 1, NULL, NULL),
(1037, '75058', 'Gunter', 'Texas', 'TX', 'Grayson', '181', 33.44950, -96.73410, 4, 1, NULL, NULL),
(1038, '75076', 'Pottsboro', 'Texas', 'TX', 'Grayson', '181', 33.80950, -96.69060, 4, 1, NULL, NULL),
(1039, '75090', 'Sherman', 'Texas', 'TX', 'Grayson', '181', 33.64350, -96.60750, 4, 1, NULL, NULL),
(1040, '75091', 'Sherman', 'Texas', 'TX', 'Grayson', '181', 33.63570, -96.60890, 4, 1, NULL, NULL),
(1041, '75092', 'Sherman', 'Texas', 'TX', 'Grayson', '181', 33.63720, -96.61840, 4, 1, NULL, NULL),
(1042, '75414', 'Bells', 'Texas', 'TX', 'Grayson', '181', 33.61780, -96.42370, 4, 1, NULL, NULL),
(1043, '75459', 'Howe', 'Texas', 'TX', 'Grayson', '181', 33.53110, -96.67770, 4, 1, NULL, NULL),
(1044, '75489', 'Tom Bean', 'Texas', 'TX', 'Grayson', '181', 33.51930, -96.48430, 4, 1, NULL, NULL),
(1045, '75491', 'Whitewright', 'Texas', 'TX', 'Grayson', '181', 33.51900, -96.45100, 4, 1, NULL, NULL),
(1046, '75495', 'Van Alstyne', 'Texas', 'TX', 'Grayson', '181', 33.42920, -96.54860, 4, 1, NULL, NULL),
(1047, '76233', 'Collinsville', 'Texas', 'TX', 'Grayson', '181', 33.55800, -96.90140, 4, 1, NULL, NULL),
(1048, '76245', 'Gordonville', 'Texas', 'TX', 'Grayson', '181', 33.83430, -96.84030, 4, 1, NULL, NULL),
(1049, '76264', 'Sadler', 'Texas', 'TX', 'Grayson', '181', 33.73100, -96.84000, 4, 1, NULL, NULL),
(1050, '76268', 'Southmayd', 'Texas', 'TX', 'Grayson', '181', 33.63040, -96.76920, 4, 1, NULL, NULL),
(1051, '76271', 'Tioga', 'Texas', 'TX', 'Grayson', '181', 33.46750, -96.90970, 4, 1, NULL, NULL),
(1052, '76273', 'Whitesboro', 'Texas', 'TX', 'Grayson', '181', 33.65900, -96.87900, 4, 1, NULL, NULL),
(1053, '75601', 'Longview', 'Texas', 'TX', 'Gregg', '183', 32.51780, -94.73030, 4, 1, NULL, NULL),
(1054, '75602', 'Longview', 'Texas', 'TX', 'Gregg', '183', 32.47240, -94.71010, 4, 1, NULL, NULL),
(1055, '75603', 'Longview', 'Texas', 'TX', 'Gregg', '183', 32.42640, -94.71170, 4, 1, NULL, NULL),
(1056, '75604', 'Longview', 'Texas', 'TX', 'Gregg', '183', 32.52510, -94.79900, 4, 1, NULL, NULL),
(1057, '75605', 'Longview', 'Texas', 'TX', 'Gregg', '183', 32.55470, -94.77670, 4, 1, NULL, NULL),
(1058, '75606', 'Longview', 'Texas', 'TX', 'Gregg', '183', 32.50070, -94.74050, 4, 1, NULL, NULL),
(1059, '75607', 'Longview', 'Texas', 'TX', 'Gregg', '183', 32.51120, -94.78350, 4, 1, NULL, NULL),
(1060, '75608', 'Longview', 'Texas', 'TX', 'Gregg', '183', 32.57010, -94.84810, 4, 1, NULL, NULL),
(1061, '75615', 'Longview', 'Texas', 'TX', 'Gregg', '183', 32.51120, -94.78350, 4, 1, NULL, NULL),
(1062, '75641', 'Easton', 'Texas', 'TX', 'Gregg', '183', 32.38740, -94.58300, 4, 1, NULL, NULL),
(1063, '75647', 'Gladewater', 'Texas', 'TX', 'Gregg', '183', 32.55590, -94.93200, 4, 1, NULL, NULL),
(1064, '75660', 'Judson', 'Texas', 'TX', 'Gregg', '183', 32.58260, -94.75350, 4, 1, NULL, NULL),
(1065, '75662', 'Kilgore', 'Texas', 'TX', 'Gregg', '183', 32.38360, -94.86530, 4, 1, NULL, NULL),
(1066, '75663', 'Kilgore', 'Texas', 'TX', 'Gregg', '183', 32.38700, -94.89510, 4, 1, NULL, NULL),
(1067, '75693', 'White Oak', 'Texas', 'TX', 'Gregg', '183', 32.53830, -94.86220, 4, 1, NULL, NULL),
(1068, '77363', 'Plantersville', 'Texas', 'TX', 'Grimes', '185', 30.29690, -95.84980, 4, 1, NULL, NULL),
(1069, '77830', 'Anderson', 'Texas', 'TX', 'Grimes', '185', 30.54430, -96.00180, 4, 1, NULL, NULL),
(1070, '77831', 'Bedias', 'Texas', 'TX', 'Grimes', '185', 30.70650, -95.95460, 4, 1, NULL, NULL),
(1071, '77861', 'Iola', 'Texas', 'TX', 'Grimes', '185', 30.73260, -96.09110, 4, 1, NULL, NULL),
(1072, '77868', 'Navasota', 'Texas', 'TX', 'Grimes', '185', 30.35760, -96.05940, 4, 1, NULL, NULL),
(1073, '77873', 'Richards', 'Texas', 'TX', 'Grimes', '185', 30.57960, -95.89590, 4, 1, NULL, NULL),
(1074, '77875', 'Roans Prairie', 'Texas', 'TX', 'Grimes', '185', 30.60750, -95.95790, 4, 1, NULL, NULL),
(1075, '77876', 'Shiro', 'Texas', 'TX', 'Grimes', '185', 30.67520, -95.82850, 4, 1, NULL, NULL),
(1076, '78108', 'Cibolo', 'Texas', 'TX', 'Guadalupe', '187', 29.57500, -98.22800, 4, 1, NULL, NULL),
(1077, '78115', 'Geronimo', 'Texas', 'TX', 'Guadalupe', '187', 29.54510, -98.04080, 4, 1, NULL, NULL),
(1078, '78123', 'Mc Queeney', 'Texas', 'TX', 'Guadalupe', '187', 29.60230, -98.04920, 1, 1, NULL, NULL),
(1079, '78124', 'Marion', 'Texas', 'TX', 'Guadalupe', '187', 29.56830, -98.15170, 4, 1, NULL, NULL),
(1080, '78154', 'Schertz', 'Texas', 'TX', 'Guadalupe', '187', 29.57900, -98.27780, 4, 1, NULL, NULL),
(1081, '78155', 'Seguin', 'Texas', 'TX', 'Guadalupe', '187', 29.56130, -97.96280, 4, 1, NULL, NULL),
(1082, '78156', 'Seguin', 'Texas', 'TX', 'Guadalupe', '187', 29.61180, -97.97120, 4, 1, NULL, NULL),
(1083, '78638', 'Kingsbury', 'Texas', 'TX', 'Guadalupe', '187', 29.67260, -97.83060, 4, 1, NULL, NULL),
(1084, '78670', 'Staples', 'Texas', 'TX', 'Guadalupe', '187', 29.77060, -97.81860, 4, 1, NULL, NULL),
(1085, '79021', 'Cotton Center', 'Texas', 'TX', 'Hale', '189', 33.97530, -102.03140, 4, 1, NULL, NULL),
(1086, '79032', 'Edmonson', 'Texas', 'TX', 'Hale', '189', 34.27890, -101.89410, 4, 1, NULL, NULL),
(1087, '79041', 'Hale Center', 'Texas', 'TX', 'Hale', '189', 34.06690, -101.87360, 4, 1, NULL, NULL),
(1088, '79072', 'Plainview', 'Texas', 'TX', 'Hale', '189', 34.19620, -101.72590, 4, 1, NULL, NULL),
(1089, '79073', 'Plainview', 'Texas', 'TX', 'Hale', '189', 34.06890, -101.82700, 4, 1, NULL, NULL),
(1090, '79250', 'Petersburg', 'Texas', 'TX', 'Hale', '189', 33.87680, -101.60460, 4, 1, NULL, NULL),
(1091, '79311', 'Abernathy', 'Texas', 'TX', 'Hale', '189', 33.85000, -101.86110, 4, 1, NULL, NULL),
(1092, '79233', 'Estelline', 'Texas', 'TX', 'Hall', '191', 34.53030, -100.44550, 4, 1, NULL, NULL),
(1093, '79239', 'Lakeview', 'Texas', 'TX', 'Hall', '191', 34.67240, -100.72590, 4, 1, NULL, NULL),
(1094, '79245', 'Memphis', 'Texas', 'TX', 'Hall', '191', 34.71220, -100.53470, 4, 1, NULL, NULL),
(1095, '79261', 'Turkey', 'Texas', 'TX', 'Hall', '191', 34.40360, -100.84490, 4, 1, NULL, NULL),
(1096, '76436', 'Carlton', 'Texas', 'TX', 'Hamilton', '193', 31.89490, -98.18420, 4, 1, NULL, NULL),
(1097, '76457', 'Hico', 'Texas', 'TX', 'Hamilton', '193', 31.95970, -98.02490, 4, 1, NULL, NULL),
(1098, '76531', 'Hamilton', 'Texas', 'TX', 'Hamilton', '193', 31.67810, -98.11310, 4, 1, NULL, NULL),
(1099, '76565', 'Pottsville', 'Texas', 'TX', 'Hamilton', '193', 31.68370, -98.35610, 4, 1, NULL, NULL),
(1100, '79040', 'Gruver', 'Texas', 'TX', 'Hansford', '195', 36.28680, -101.40890, 4, 1, NULL, NULL),
(1101, '79062', 'Morse', 'Texas', 'TX', 'Hansford', '195', 36.05960, -101.47290, 4, 1, NULL, NULL),
(1102, '79081', 'Spearman', 'Texas', 'TX', 'Hansford', '195', 36.19270, -101.19520, 4, 1, NULL, NULL),
(1103, '79225', 'Chillicothe', 'Texas', 'TX', 'Hardeman', '197', 34.24390, -99.51570, 4, 1, NULL, NULL),
(1104, '79252', 'Quanah', 'Texas', 'TX', 'Hardeman', '197', 34.29550, -99.74940, 4, 1, NULL, NULL),
(1105, '77374', 'Thicket', 'Texas', 'TX', 'Hardin', '199', 30.37650, -94.63610, 4, 1, NULL, NULL),
(1106, '77376', 'Votaw', 'Texas', 'TX', 'Hardin', '199', 30.43340, -94.68040, 4, 1, NULL, NULL),
(1107, '77519', 'Batson', 'Texas', 'TX', 'Hardin', '199', 30.22500, -94.60960, 4, 1, NULL, NULL),
(1108, '77585', 'Saratoga', 'Texas', 'TX', 'Hardin', '199', 30.28410, -94.52940, 4, 1, NULL, NULL),
(1109, '77625', 'Kountze', 'Texas', 'TX', 'Hardin', '199', 30.37030, -94.32590, 4, 1, NULL, NULL),
(1110, '77656', 'Silsbee', 'Texas', 'TX', 'Hardin', '199', 30.32440, -94.19070, 4, 1, NULL, NULL),
(1111, '77657', 'Lumberton', 'Texas', 'TX', 'Hardin', '199', 30.28180, -94.21910, 4, 1, NULL, NULL),
(1112, '77659', 'Sour Lake', 'Texas', 'TX', 'Hardin', '199', 30.14910, -94.37330, 4, 1, NULL, NULL),
(1113, '77663', 'Village Mills', 'Texas', 'TX', 'Hardin', '199', 30.51850, -94.44580, 4, 1, NULL, NULL),
(1114, '77001', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.81310, -95.30980, 4, 1, NULL, NULL),
(1115, '77002', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.75940, -95.35940, 4, 1, NULL, NULL),
(1116, '77003', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.74890, -95.33910, 4, 1, NULL, NULL),
(1117, '77004', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.72470, -95.36250, 4, 1, NULL, NULL),
(1118, '77005', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.71790, -95.42630, 4, 1, NULL, NULL),
(1119, '77006', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.74090, -95.39230, 4, 1, NULL, NULL),
(1120, '77007', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.77360, -95.40340, 4, 1, NULL, NULL),
(1121, '77008', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.79910, -95.41180, 4, 1, NULL, NULL),
(1122, '77009', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.79360, -95.36750, 4, 1, NULL, NULL),
(1123, '77010', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.75430, -95.36090, 4, 1, NULL, NULL),
(1124, '77011', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.74200, -95.30730, 4, 1, NULL, NULL),
(1125, '77012', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.71490, -95.28190, 4, 1, NULL, NULL),
(1126, '77013', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.78420, -95.23010, 4, 1, NULL, NULL),
(1127, '77014', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.97960, -95.46250, 4, 1, NULL, NULL),
(1128, '77015', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.78530, -95.18520, 4, 1, NULL, NULL),
(1129, '77016', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.85790, -95.30320, 4, 1, NULL, NULL),
(1130, '77017', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.68630, -95.25550, 4, 1, NULL, NULL),
(1131, '77018', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.82720, -95.42660, 4, 1, NULL, NULL),
(1132, '77019', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.75170, -95.40540, 4, 1, NULL, NULL),
(1133, '77020', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.77580, -95.31210, 4, 1, NULL, NULL),
(1134, '77021', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.69540, -95.35620, 4, 1, NULL, NULL),
(1135, '77022', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.82990, -95.37690, 4, 1, NULL, NULL),
(1136, '77023', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.72420, -95.31780, 4, 1, NULL, NULL),
(1137, '77024', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.76960, -95.52010, 4, 1, NULL, NULL),
(1138, '77025', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.68890, -95.43410, 4, 1, NULL, NULL),
(1139, '77026', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.79720, -95.32880, 4, 1, NULL, NULL),
(1140, '77027', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.73960, -95.44600, 4, 1, NULL, NULL),
(1141, '77028', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.82970, -95.28790, 4, 1, NULL, NULL),
(1142, '77029', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.76300, -95.25670, 4, 1, NULL, NULL),
(1143, '77030', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.70410, -95.40100, 4, 1, NULL, NULL),
(1144, '77031', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.65810, -95.54130, 4, 1, NULL, NULL),
(1145, '77032', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.93680, -95.32990, 4, 1, NULL, NULL),
(1146, '77033', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.66860, -95.33820, 4, 1, NULL, NULL),
(1147, '77034', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.63640, -95.22160, 4, 1, NULL, NULL),
(1148, '77035', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.65180, -95.48540, 4, 1, NULL, NULL),
(1149, '77036', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.69840, -95.54050, 4, 1, NULL, NULL),
(1150, '77037', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.88920, -95.39350, 4, 1, NULL, NULL),
(1151, '77038', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.91960, -95.43860, 4, 1, NULL, NULL),
(1152, '77039', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.90670, -95.33340, 4, 1, NULL, NULL),
(1153, '77040', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.87440, -95.52780, 4, 1, NULL, NULL),
(1154, '77041', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.86020, -95.58170, 4, 1, NULL, NULL),
(1155, '77042', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.74040, -95.55890, 4, 1, NULL, NULL),
(1156, '77043', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.80520, -95.56070, 4, 1, NULL, NULL),
(1157, '77044', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.86350, -95.19760, 4, 1, NULL, NULL),
(1158, '77045', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.62970, -95.43820, 4, 1, NULL, NULL),
(1159, '77046', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.73300, -95.43060, 4, 1, NULL, NULL),
(1160, '77047', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.62540, -95.37500, 4, 1, NULL, NULL),
(1161, '77048', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.63210, -95.34160, 4, 1, NULL, NULL),
(1162, '77049', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.82350, -95.18480, 4, 1, NULL, NULL),
(1163, '77050', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.90150, -95.28480, 4, 1, NULL, NULL),
(1164, '77051', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.65790, -95.36880, 4, 1, NULL, NULL),
(1165, '77052', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.76330, -95.36330, 4, 1, NULL, NULL),
(1166, '77053', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.59620, -95.45870, 4, 1, NULL, NULL),
(1167, '77054', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.68520, -95.40170, 4, 1, NULL, NULL),
(1168, '77055', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.79710, -95.49580, 4, 1, NULL, NULL),
(1169, '77056', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.74460, -95.46830, 4, 1, NULL, NULL),
(1170, '77057', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.74220, -95.49030, 4, 1, NULL, NULL),
(1171, '77058', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.57160, -95.09980, 4, 1, NULL, NULL),
(1172, '77059', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.59750, -95.11340, 4, 1, NULL, NULL),
(1173, '77060', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.93350, -95.39810, 4, 1, NULL, NULL),
(1174, '77061', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.66520, -95.27900, 4, 1, NULL, NULL),
(1175, '77062', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.57210, -95.13030, 4, 1, NULL, NULL),
(1176, '77063', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.73480, -95.52200, 4, 1, NULL, NULL),
(1177, '77064', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.91900, -95.55690, 4, 1, NULL, NULL),
(1178, '77065', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.93190, -95.61060, 4, 1, NULL, NULL),
(1179, '77066', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.96100, -95.49470, 4, 1, NULL, NULL),
(1180, '77067', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.95470, -95.45220, 4, 1, NULL, NULL),
(1181, '77068', 'Houston', 'Texas', 'TX', 'Harris', '201', 30.00690, -95.48970, 4, 1, NULL, NULL),
(1182, '77069', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.98630, -95.52080, 4, 1, NULL, NULL),
(1183, '77070', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.97810, -95.58030, 4, 1, NULL, NULL),
(1184, '77071', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.65180, -95.51760, 4, 1, NULL, NULL),
(1185, '77072', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.69900, -95.58620, 4, 1, NULL, NULL),
(1186, '77073', 'Houston', 'Texas', 'TX', 'Harris', '201', 30.01980, -95.40870, 4, 1, NULL, NULL),
(1187, '77074', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.68960, -95.51060, 4, 1, NULL, NULL),
(1188, '77075', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.62230, -95.26000, 4, 1, NULL, NULL),
(1189, '77076', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.85800, -95.38340, 4, 1, NULL, NULL),
(1190, '77077', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.74770, -95.60300, 4, 1, NULL, NULL),
(1191, '77078', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.84970, -95.25820, 4, 1, NULL, NULL),
(1192, '77079', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.77380, -95.59800, 4, 1, NULL, NULL),
(1193, '77080', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.81590, -95.52300, 4, 1, NULL, NULL),
(1194, '77081', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.71190, -95.48450, 4, 1, NULL, NULL),
(1195, '77082', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.72230, -95.62850, 4, 1, NULL, NULL),
(1196, '77083', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.69470, -95.65110, 4, 1, NULL, NULL),
(1197, '77084', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.84400, -95.66230, 4, 1, NULL, NULL),
(1198, '77085', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.62180, -95.48190, 4, 1, NULL, NULL),
(1199, '77086', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.92270, -95.49390, 4, 1, NULL, NULL),
(1200, '77087', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.68760, -95.30110, 4, 1, NULL, NULL),
(1201, '77088', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.88170, -95.45390, 4, 1, NULL, NULL),
(1202, '77089', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.59400, -95.22180, 4, 1, NULL, NULL),
(1203, '77090', 'Houston', 'Texas', 'TX', 'Harris', '201', 30.01670, -95.44700, 4, 1, NULL, NULL),
(1204, '77091', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.85340, -95.44350, 4, 1, NULL, NULL),
(1205, '77092', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.83240, -95.47200, 4, 1, NULL, NULL),
(1206, '77093', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.86170, -95.34030, 4, 1, NULL, NULL),
(1207, '77094', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.77050, -95.71070, 4, 1, NULL, NULL),
(1208, '77095', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.89410, -95.64810, 4, 1, NULL, NULL),
(1209, '77096', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.67220, -95.48610, 4, 1, NULL, NULL),
(1210, '77098', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.73500, -95.41180, 4, 1, NULL, NULL),
(1211, '77099', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.67090, -95.58660, 4, 1, NULL, NULL),
(1212, '77201', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.83400, -95.43420, 4, 1, NULL, NULL),
(1213, '77202', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.83400, -95.43420, 4, 1, NULL, NULL),
(1214, '77203', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.76330, -95.36330, 4, 1, NULL, NULL),
(1215, '77204', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.83400, -95.43420, 4, 1, NULL, NULL),
(1216, '77205', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.83400, -95.43420, 4, 1, NULL, NULL),
(1217, '77206', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.83400, -95.43420, 4, 1, NULL, NULL),
(1218, '77207', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.83400, -95.43420, 4, 1, NULL, NULL),
(1219, '77208', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.76330, -95.36330, 4, 1, NULL, NULL),
(1220, '77209', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.76330, -95.36330, 4, 1, NULL, NULL),
(1221, '77210', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.76330, -95.36330, 4, 1, NULL, NULL),
(1222, '77212', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.76330, -95.36330, 4, 1, NULL, NULL),
(1223, '77213', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.83400, -95.43420, 4, 1, NULL, NULL),
(1224, '77215', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.83400, -95.43420, 4, 1, NULL, NULL),
(1225, '77216', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.83400, -95.43420, 4, 1, NULL, NULL),
(1226, '77217', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.83400, -95.43420, 4, 1, NULL, NULL),
(1227, '77218', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.83400, -95.43420, 4, 1, NULL, NULL),
(1228, '77219', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.83400, -95.43420, 4, 1, NULL, NULL),
(1229, '77220', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.83400, -95.43420, 4, 1, NULL, NULL),
(1230, '77221', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.83400, -95.43420, 4, 1, NULL, NULL),
(1231, '77222', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.83400, -95.43420, 4, 1, NULL, NULL),
(1232, '77223', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.83400, -95.43420, 4, 1, NULL, NULL),
(1233, '77224', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.83400, -95.43420, 4, 1, NULL, NULL),
(1234, '77225', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.83400, -95.43420, 4, 1, NULL, NULL),
(1235, '77226', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.83400, -95.43420, 4, 1, NULL, NULL),
(1236, '77227', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.83400, -95.43420, 4, 1, NULL, NULL),
(1237, '77228', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.83400, -95.43420, 4, 1, NULL, NULL),
(1238, '77229', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.83400, -95.43420, 4, 1, NULL, NULL),
(1239, '77230', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.83400, -95.43420, 4, 1, NULL, NULL),
(1240, '77231', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.83400, -95.43420, 4, 1, NULL, NULL),
(1241, '77233', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.83400, -95.43420, 4, 1, NULL, NULL),
(1242, '77234', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.83400, -95.43420, 4, 1, NULL, NULL),
(1243, '77235', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.83400, -95.43420, 4, 1, NULL, NULL),
(1244, '77236', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.83400, -95.43420, 4, 1, NULL, NULL),
(1245, '77237', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.83400, -95.43420, 4, 1, NULL, NULL),
(1246, '77238', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.83400, -95.43420, 4, 1, NULL, NULL),
(1247, '77240', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.83400, -95.43420, 4, 1, NULL, NULL),
(1248, '77241', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.83400, -95.43420, 4, 1, NULL, NULL),
(1249, '77242', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.83400, -95.43420, 4, 1, NULL, NULL),
(1250, '77243', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.83400, -95.43420, 4, 1, NULL, NULL),
(1251, '77244', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.83400, -95.43420, 4, 1, NULL, NULL),
(1252, '77245', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.83400, -95.43420, 4, 1, NULL, NULL),
(1253, '77248', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.83400, -95.43420, 4, 1, NULL, NULL),
(1254, '77249', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.83400, -95.43420, 4, 1, NULL, NULL),
(1255, '77251', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.83400, -95.43420, 4, 1, NULL, NULL),
(1256, '77252', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.76330, -95.36330, 4, 1, NULL, NULL),
(1257, '77253', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.76330, -95.36330, 4, 1, NULL, NULL),
(1258, '77254', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.83400, -95.43420, 4, 1, NULL, NULL),
(1259, '77255', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.83400, -95.43420, 4, 1, NULL, NULL),
(1260, '77256', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.83400, -95.43420, 4, 1, NULL, NULL),
(1261, '77257', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.83400, -95.43420, 4, 1, NULL, NULL),
(1262, '77258', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.83400, -95.43420, 4, 1, NULL, NULL),
(1263, '77259', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.83400, -95.43420, 4, 1, NULL, NULL),
(1264, '77261', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.83400, -95.43420, 4, 1, NULL, NULL),
(1265, '77262', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.83400, -95.43420, 4, 1, NULL, NULL),
(1266, '77263', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.83400, -95.43420, 4, 1, NULL, NULL),
(1267, '77265', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.83400, -95.43420, 4, 1, NULL, NULL),
(1268, '77266', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.83400, -95.43420, 4, 1, NULL, NULL),
(1269, '77267', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.83400, -95.43420, 4, 1, NULL, NULL),
(1270, '77268', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.83400, -95.43420, 4, 1, NULL, NULL),
(1271, '77269', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.83400, -95.43420, 4, 1, NULL, NULL),
(1272, '77270', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.83400, -95.43420, 4, 1, NULL, NULL),
(1273, '77271', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.83400, -95.43420, 4, 1, NULL, NULL),
(1274, '77272', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.83400, -95.43420, 4, 1, NULL, NULL),
(1275, '77273', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.83400, -95.43420, 4, 1, NULL, NULL),
(1276, '77274', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.83400, -95.43420, 4, 1, NULL, NULL),
(1277, '77275', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.83400, -95.43420, 4, 1, NULL, NULL),
(1278, '77277', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.83400, -95.43420, 4, 1, NULL, NULL),
(1279, '77279', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.83400, -95.43420, 4, 1, NULL, NULL),
(1280, '77280', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.83400, -95.43420, 4, 1, NULL, NULL),
(1281, '77282', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.83400, -95.43420, 4, 1, NULL, NULL),
(1282, '77284', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.83400, -95.43420, 4, 1, NULL, NULL),
(1283, '77287', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.83400, -95.43420, 4, 1, NULL, NULL),
(1284, '77288', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.83400, -95.43420, 4, 1, NULL, NULL),
(1285, '77289', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.83400, -95.43420, 4, 1, NULL, NULL),
(1286, '77290', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.83400, -95.43420, 4, 1, NULL, NULL),
(1287, '77291', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.83400, -95.43420, 4, 1, NULL, NULL),
(1288, '77292', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.74430, -95.33260, 4, 1, NULL, NULL),
(1289, '77293', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.83400, -95.43420, 4, 1, NULL, NULL),
(1290, '77297', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.76330, -95.36330, 4, 1, NULL, NULL),
(1291, '77299', 'Houston', 'Texas', 'TX', 'Harris', '201', 29.83400, -95.43420, 4, 1, NULL, NULL),
(1292, '77315', 'North Houston', 'Texas', 'TX', 'Harris', '201', 29.83400, -95.43420, 4, 1, NULL, NULL),
(1293, '77325', 'Humble', 'Texas', 'TX', 'Harris', '201', 29.99880, -95.26220, 4, 1, NULL, NULL),
(1294, '77336', 'Huffman', 'Texas', 'TX', 'Harris', '201', 30.05650, -95.10510, 4, 1, NULL, NULL),
(1295, '77337', 'Hufsmith', 'Texas', 'TX', 'Harris', '201', 30.12220, -95.59660, 4, 1, NULL, NULL),
(1296, '77338', 'Humble', 'Texas', 'TX', 'Harris', '201', 30.00410, -95.28250, 4, 1, NULL, NULL),
(1297, '77339', 'Humble', 'Texas', 'TX', 'Harris', '201', 30.05630, -95.21070, 4, 1, NULL, NULL),
(1298, '77345', 'Humble', 'Texas', 'TX', 'Harris', '201', 30.05660, -95.17070, 4, 1, NULL, NULL),
(1299, '77346', 'Humble', 'Texas', 'TX', 'Harris', '201', 30.00420, -95.17280, 4, 1, NULL, NULL),
(1300, '77347', 'Humble', 'Texas', 'TX', 'Harris', '201', 29.99880, -95.26220, 4, 1, NULL, NULL),
(1301, '77373', 'Spring', 'Texas', 'TX', 'Harris', '201', 30.05320, -95.37730, 4, 1, NULL, NULL),
(1302, '77375', 'Tomball', 'Texas', 'TX', 'Harris', '201', 30.07390, -95.62010, 4, 1, NULL, NULL),
(1303, '77377', 'Tomball', 'Texas', 'TX', 'Harris', '201', 30.06150, -95.68210, 4, 1, NULL, NULL),
(1304, '77379', 'Spring', 'Texas', 'TX', 'Harris', '201', 30.03770, -95.53260, 4, 1, NULL, NULL),
(1305, '77382', 'Spring', 'Texas', 'TX', 'Harris', '201', 30.21060, -95.52570, 4, 1, NULL, NULL),
(1306, '77383', 'Spring', 'Texas', 'TX', 'Harris', '201', 30.07990, -95.41720, 4, 1, NULL, NULL),
(1307, '77388', 'Spring', 'Texas', 'TX', 'Harris', '201', 30.05050, -95.46950, 4, 1, NULL, NULL),
(1308, '77389', 'Spring', 'Texas', 'TX', 'Harris', '201', 30.10440, -95.50660, 4, 1, NULL, NULL),
(1309, '77391', 'Spring', 'Texas', 'TX', 'Harris', '201', 30.07990, -95.41720, 4, 1, NULL, NULL),
(1310, '77396', 'Humble', 'Texas', 'TX', 'Harris', '201', 29.95070, -95.26220, 4, 1, NULL, NULL),
(1311, '77401', 'Bellaire', 'Texas', 'TX', 'Harris', '201', 29.70230, -95.46110, 4, 1, NULL, NULL),
(1312, '77402', 'Bellaire', 'Texas', 'TX', 'Harris', '201', 29.83400, -95.43420, 4, 1, NULL, NULL),
(1313, '77410', 'Cypress', 'Texas', 'TX', 'Harris', '201', 29.96910, -95.69720, 4, 1, NULL, NULL),
(1314, '77411', 'Alief', 'Texas', 'TX', 'Harris', '201', 29.71110, -95.59630, 4, 1, NULL, NULL),
(1315, '77413', 'Barker', 'Texas', 'TX', 'Harris', '201', 29.78440, -95.68490, 4, 1, NULL, NULL),
(1316, '77429', 'Cypress', 'Texas', 'TX', 'Harris', '201', 29.97660, -95.63580, 4, 1, NULL, NULL),
(1317, '77433', 'Cypress', 'Texas', 'TX', 'Harris', '201', 29.88360, -95.70250, 4, 1, NULL, NULL),
(1318, '77447', 'Hockley', 'Texas', 'TX', 'Harris', '201', 30.07290, -95.81040, 4, 1, NULL, NULL),
(1319, '77449', 'Katy', 'Texas', 'TX', 'Harris', '201', 29.83230, -95.73600, 4, 1, NULL, NULL),
(1320, '77450', 'Katy', 'Texas', 'TX', 'Harris', '201', 29.74500, -95.73260, 4, 1, NULL, NULL),
(1321, '77491', 'Katy', 'Texas', 'TX', 'Harris', '201', 29.78580, -95.82440, 4, 1, NULL, NULL),
(1322, '77492', 'Katy', 'Texas', 'TX', 'Harris', '201', 29.78580, -95.82440, 4, 1, NULL, NULL),
(1323, '77493', 'Katy', 'Texas', 'TX', 'Harris', '201', 29.86780, -95.82980, 4, 1, NULL, NULL),
(1324, '77494', 'Katy', 'Texas', 'TX', 'Harris', '201', 29.74040, -95.83040, 4, 1, NULL, NULL),
(1325, '77501', 'Pasadena', 'Texas', 'TX', 'Harris', '201', 29.69110, -95.20910, 4, 1, NULL, NULL),
(1326, '77502', 'Pasadena', 'Texas', 'TX', 'Harris', '201', 29.67890, -95.19820, 4, 1, NULL, NULL),
(1327, '77503', 'Pasadena', 'Texas', 'TX', 'Harris', '201', 29.68770, -95.15720, 4, 1, NULL, NULL),
(1328, '77504', 'Pasadena', 'Texas', 'TX', 'Harris', '201', 29.65010, -95.18850, 4, 1, NULL, NULL),
(1329, '77505', 'Pasadena', 'Texas', 'TX', 'Harris', '201', 29.65180, -95.14640, 4, 1, NULL, NULL),
(1330, '77506', 'Pasadena', 'Texas', 'TX', 'Harris', '201', 29.70090, -95.19890, 4, 1, NULL, NULL),
(1331, '77507', 'Pasadena', 'Texas', 'TX', 'Harris', '201', 29.62240, -95.05450, 4, 1, NULL, NULL),
(1332, '77508', 'Pasadena', 'Texas', 'TX', 'Harris', '201', 29.56990, -95.10660, 4, 1, NULL, NULL),
(1333, '77520', 'Baytown', 'Texas', 'TX', 'Harris', '201', 29.74610, -94.96530, 4, 1, NULL, NULL),
(1334, '77521', 'Baytown', 'Texas', 'TX', 'Harris', '201', 29.77050, -94.96950, 4, 1, NULL, NULL),
(1335, '77522', 'Baytown', 'Texas', 'TX', 'Harris', '201', 29.73550, -94.97740, 4, 1, NULL, NULL),
(1336, '77530', 'Channelview', 'Texas', 'TX', 'Harris', '201', 29.79140, -95.13170, 4, 1, NULL, NULL),
(1337, '77532', 'Crosby', 'Texas', 'TX', 'Harris', '201', 29.92490, -95.05780, 4, 1, NULL, NULL),
(1338, '77536', 'Deer Park', 'Texas', 'TX', 'Harris', '201', 29.68260, -95.12220, 4, 1, NULL, NULL),
(1339, '77547', 'Galena Park', 'Texas', 'TX', 'Harris', '201', 29.73920, -95.24000, 4, 1, NULL, NULL),
(1340, '77562', 'Highlands', 'Texas', 'TX', 'Harris', '201', 29.82960, -95.03930, 4, 1, NULL, NULL),
(1341, '77571', 'La Porte', 'Texas', 'TX', 'Harris', '201', 29.68840, -95.05130, 4, 1, NULL, NULL),
(1342, '77572', 'La Porte', 'Texas', 'TX', 'Harris', '201', 29.66580, -95.01940, 4, 1, NULL, NULL),
(1343, '77586', 'Seabrook', 'Texas', 'TX', 'Harris', '201', 29.58320, -95.03700, 4, 1, NULL, NULL),
(1344, '77587', 'South Houston', 'Texas', 'TX', 'Harris', '201', 29.66010, -95.22580, 4, 1, NULL, NULL),
(1345, '77598', 'Webster', 'Texas', 'TX', 'Harris', '201', 29.55640, -95.14400, 4, 1, NULL, NULL),
(1346, '75642', 'Elysian Fields', 'Texas', 'TX', 'Harrison', '203', 32.36850, -94.18300, 4, 1, NULL, NULL),
(1347, '75650', 'Hallsville', 'Texas', 'TX', 'Harrison', '203', 32.50730, -94.53330, 4, 1, NULL, NULL),
(1348, '75651', 'Harleton', 'Texas', 'TX', 'Harrison', '203', 32.65790, -94.46520, 4, 1, NULL, NULL),
(1349, '75659', 'Jonesville', 'Texas', 'TX', 'Harrison', '203', 32.50750, -94.11060, 4, 1, NULL, NULL),
(1350, '75661', 'Karnack', 'Texas', 'TX', 'Harrison', '203', 32.62050, -94.20010, 4, 1, NULL, NULL),
(1351, '75670', 'Marshall', 'Texas', 'TX', 'Harrison', '203', 32.53380, -94.36190, 4, 1, NULL, NULL),
(1352, '75671', 'Marshall', 'Texas', 'TX', 'Harrison', '203', 32.54490, -94.36740, 4, 1, NULL, NULL),
(1353, '75672', 'Marshall', 'Texas', 'TX', 'Harrison', '203', 32.51650, -94.32510, 4, 1, NULL, NULL),
(1354, '75688', 'Scottsville', 'Texas', 'TX', 'Harrison', '203', 32.55400, -94.23940, 4, 1, NULL, NULL),
(1355, '75692', 'Waskom', 'Texas', 'TX', 'Harrison', '203', 32.51840, -94.13550, 4, 1, NULL, NULL),
(1356, '75694', 'Woodlawn', 'Texas', 'TX', 'Harrison', '203', 32.65370, -94.34270, 4, 1, NULL, NULL),
(1357, '79018', 'Channing', 'Texas', 'TX', 'Hartley', '205', 35.68370, -102.33020, 4, 1, NULL, NULL),
(1358, '79044', 'Hartley', 'Texas', 'TX', 'Hartley', '205', 35.88560, -102.39690, 4, 1, NULL, NULL),
(1359, '76388', 'Weinert', 'Texas', 'TX', 'Haskell', '207', 33.32490, -99.66640, 4, 1, NULL, NULL),
(1360, '79521', 'Haskell', 'Texas', 'TX', 'Haskell', '207', 33.15800, -99.73090, 4, 1, NULL, NULL),
(1361, '79539', 'O Brien', 'Texas', 'TX', 'Haskell', '207', 33.37490, -99.84730, 4, 1, NULL, NULL),
(1362, '79544', 'Rochester', 'Texas', 'TX', 'Haskell', '207', 33.31050, -99.85940, 4, 1, NULL, NULL),
(1363, '79547', 'Rule', 'Texas', 'TX', 'Haskell', '207', 33.18420, -99.88940, 4, 1, NULL, NULL),
(1364, '79548', 'Rule', 'Texas', 'TX', 'Haskell', '207', 33.07710, -99.95900, 4, 1, NULL, NULL),
(1365, '78610', 'Buda', 'Texas', 'TX', 'Hays', '209', 30.09180, -97.85340, 4, 1, NULL, NULL),
(1366, '78619', 'Driftwood', 'Texas', 'TX', 'Hays', '209', 30.10720, -98.03270, 4, 1, NULL, NULL),
(1367, '78620', 'Dripping Springs', 'Texas', 'TX', 'Hays', '209', 30.22680, -98.10290, 4, 1, NULL, NULL),
(1368, '78640', 'Kyle', 'Texas', 'TX', 'Hays', '209', 29.99660, -97.83350, 4, 1, NULL, NULL),
(1369, '78666', 'San Marcos', 'Texas', 'TX', 'Hays', '209', 29.87540, -97.94040, 4, 1, NULL, NULL),
(1370, '78667', 'San Marcos', 'Texas', 'TX', 'Hays', '209', 30.05440, -98.00360, 4, 1, NULL, NULL),
(1371, '78676', 'Wimberley', 'Texas', 'TX', 'Hays', '209', 30.02650, -98.11230, 4, 1, NULL, NULL),
(1372, '79014', 'Canadian', 'Texas', 'TX', 'Hemphill', '211', 35.90450, -100.38410, 4, 1, NULL, NULL),
(1373, '75124', 'Eustace', 'Texas', 'TX', 'Henderson', '213', 32.29650, -96.01370, 4, 1, NULL, NULL),
(1374, '75143', 'Kemp', 'Texas', 'TX', 'Henderson', '213', 32.24860, -96.21610, 4, 1, NULL, NULL),
(1375, '75148', 'Malakoff', 'Texas', 'TX', 'Henderson', '213', 32.17050, -96.00600, 4, 1, NULL, NULL),
(1376, '75163', 'Trinidad', 'Texas', 'TX', 'Henderson', '213', 32.13830, -96.08310, 4, 1, NULL, NULL),
(1377, '75751', 'Athens', 'Texas', 'TX', 'Henderson', '213', 32.19350, -95.84320, 4, 1, NULL, NULL),
(1378, '75752', 'Athens', 'Texas', 'TX', 'Henderson', '213', 32.21510, -95.79000, 4, 1, NULL, NULL),
(1379, '75756', 'Brownsboro', 'Texas', 'TX', 'Henderson', '213', 32.30400, -95.60000, 4, 1, NULL, NULL),
(1380, '75758', 'Chandler', 'Texas', 'TX', 'Henderson', '213', 32.25720, -95.53930, 4, 1, NULL, NULL),
(1381, '75770', 'Larue', 'Texas', 'TX', 'Henderson', '213', 32.16080, -95.59270, 4, 1, NULL, NULL),
(1382, '75778', 'Murchison', 'Texas', 'TX', 'Henderson', '213', 32.32570, -95.77370, 4, 1, NULL, NULL),
(1383, '75782', 'Poynor', 'Texas', 'TX', 'Henderson', '213', 32.07970, -95.59380, 4, 1, NULL, NULL),
(1384, '78501', 'Mcallen', 'Texas', 'TX', 'Hidalgo County', '215', 26.21540, -98.23590, 4, 1, NULL, NULL),
(1385, '78502', 'Mcallen', 'Texas', 'TX', 'Hidalgo County', '215', 26.25670, -98.19890, 4, 1, NULL, NULL),
(1386, '78503', 'Mcallen', 'Texas', 'TX', 'Hidalgo County', '215', 26.17710, -98.25200, 4, 1, NULL, NULL),
(1387, '78504', 'Mcallen', 'Texas', 'TX', 'Hidalgo County', '215', 26.25560, -98.23030, 4, 1, NULL, NULL),
(1388, '78505', 'Mcallen', 'Texas', 'TX', 'Hidalgo County', '215', 26.20340, -98.23000, 4, 1, NULL, NULL),
(1389, '78516', 'Alamo', 'Texas', 'TX', 'Hidalgo County', '215', 26.19060, -98.11640, 4, 1, NULL, NULL),
(1390, '78537', 'Donna', 'Texas', 'TX', 'Hidalgo County', '215', 26.16710, -98.05290, 4, 1, NULL, NULL),
(1391, '78538', 'Edcouch', 'Texas', 'TX', 'Hidalgo County', '215', 26.33280, -97.96220, 4, 1, NULL, NULL),
(1392, '78539', 'Edinburg', 'Texas', 'TX', 'Hidalgo County', '215', 26.27920, -98.18320, 4, 1, NULL, NULL),
(1393, '78540', 'Edinburg', 'Texas', 'TX', 'Hidalgo County', '215', 26.30170, -98.16330, 4, 1, NULL, NULL),
(1394, '78541', 'Edinburg', 'Texas', 'TX', 'Hidalgo County', '215', 26.44810, -98.28420, 4, 1, NULL, NULL),
(1395, '78542', 'Edinburg', 'Texas', 'TX', 'Hidalgo County', '215', 26.46610, -98.06950, 4, 1, NULL, NULL),
(1396, '78543', 'Elsa', 'Texas', 'TX', 'Hidalgo County', '215', 26.29740, -97.98840, 4, 1, NULL, NULL),
(1397, '78549', 'Hargill', 'Texas', 'TX', 'Hidalgo County', '215', 26.46710, -98.03930, 4, 1, NULL, NULL),
(1398, '78557', 'Hidalgo', 'Texas', 'TX', 'Hidalgo County', '215', 26.10280, -98.25360, 4, 1, NULL, NULL),
(1399, '78558', 'La Blanca', 'Texas', 'TX', 'Hidalgo County', '215', 26.31250, -98.03350, 4, 1, NULL, NULL),
(1400, '78560', 'La Joya', 'Texas', 'TX', 'Hidalgo County', '215', 26.24260, -98.47470, 4, 1, NULL, NULL),
(1401, '78562', 'La Villa', 'Texas', 'TX', 'Hidalgo County', '215', 26.29990, -97.92470, 4, 1, NULL, NULL),
(1402, '78563', 'Linn', 'Texas', 'TX', 'Hidalgo County', '215', 26.62790, -98.24980, 4, 1, NULL, NULL),
(1403, '78565', 'Los Ebanos', 'Texas', 'TX', 'Hidalgo County', '215', 26.24340, -98.56170, 4, 1, NULL, NULL),
(1404, '78570', 'Mercedes', 'Texas', 'TX', 'Hidalgo County', '215', 26.15130, -97.91850, 4, 1, NULL, NULL),
(1405, '78572', 'Mission', 'Texas', 'TX', 'Hidalgo County', '215', 26.21590, -98.32530, 4, 1, NULL, NULL),
(1406, '78573', 'Mission', 'Texas', 'TX', 'Hidalgo County', '215', 26.29370, -98.30080, 4, 1, NULL, NULL),
(1407, '78576', 'Penitas', 'Texas', 'TX', 'Hidalgo County', '215', 26.27800, -98.44690, 4, 1, NULL, NULL),
(1408, '78577', 'Pharr', 'Texas', 'TX', 'Hidalgo County', '215', 26.17710, -98.18700, 4, 1, NULL, NULL),
(1409, '78579', 'Progreso', 'Texas', 'TX', 'Hidalgo County', '215', 26.09220, -97.95330, 4, 1, NULL, NULL),
(1410, '78589', 'San Juan', 'Texas', 'TX', 'Hidalgo County', '215', 26.20440, -98.15370, 4, 1, NULL, NULL),
(1411, '78595', 'Sullivan City', 'Texas', 'TX', 'Hidalgo County', '215', 26.27200, -98.56270, 4, 1, NULL, NULL),
(1412, '78596', 'Weslaco', 'Texas', 'TX', 'Hidalgo County', '215', 26.16940, -97.98870, 4, 1, NULL, NULL),
(1413, '78599', 'Weslaco', 'Texas', 'TX', 'Hidalgo County', '215', 26.15950, -97.99080, 4, 1, NULL, NULL),
(1414, '76055', 'Itasca', 'Texas', 'TX', 'Hill', '217', 32.16360, -97.14600, 4, 1, NULL, NULL),
(1415, '76621', 'Abbott', 'Texas', 'TX', 'Hill', '217', 31.89160, -97.06710, 4, 1, NULL, NULL),
(1416, '76622', 'Aquilla', 'Texas', 'TX', 'Hill', '217', 31.85890, -97.22580, 4, 1, NULL, NULL),
(1417, '76627', 'Blum', 'Texas', 'TX', 'Hill', '217', 32.10520, -97.36520, 4, 1, NULL, NULL),
(1418, '76628', 'Brandon', 'Texas', 'TX', 'Hill', '217', 32.05440, -96.97560, 4, 1, NULL, NULL),
(1419, '76631', 'Bynum', 'Texas', 'TX', 'Hill', '217', 31.99070, -96.98370, 4, 1, NULL, NULL),
(1420, '76636', 'Covington', 'Texas', 'TX', 'Hill', '217', 32.15950, -97.25910, 4, 1, NULL, NULL),
(1421, '76645', 'Hillsboro', 'Texas', 'TX', 'Hill', '217', 32.01490, -97.11980, 4, 1, NULL, NULL),
(1422, '76648', 'Hubbard', 'Texas', 'TX', 'Hill', '217', 31.84360, -96.80000, 4, 1, NULL, NULL),
(1423, '76650', 'Irene', 'Texas', 'TX', 'Hill', '217', 31.99210, -96.87140, 4, 1, NULL, NULL),
(1424, '76660', 'Malone', 'Texas', 'TX', 'Hill', '217', 31.92400, -96.89070, 4, 1, NULL, NULL),
(1425, '76666', 'Mertens', 'Texas', 'TX', 'Hill', '217', 32.02750, -96.89810, 4, 1, NULL, NULL),
(1426, '76673', 'Mount Calm', 'Texas', 'TX', 'Hill', '217', 31.75750, -96.89440, 4, 1, NULL, NULL),
(1427, '76676', 'Penelope', 'Texas', 'TX', 'Hill', '217', 31.85510, -96.93720, 4, 1, NULL, NULL),
(1428, '76692', 'Whitney', 'Texas', 'TX', 'Hill', '217', 31.97130, -97.34630, 4, 1, NULL, NULL),
(1429, '79313', 'Anton', 'Texas', 'TX', 'Hockley', '219', 33.80430, -102.16520, 4, 1, NULL, NULL),
(1430, '79336', 'Levelland', 'Texas', 'TX', 'Hockley', '219', 33.57880, -102.36760, 4, 1, NULL, NULL),
(1431, '79338', 'Levelland', 'Texas', 'TX', 'Hockley', '219', 33.59320, -102.36270, 4, 1, NULL, NULL),
(1432, '79353', 'Pep', 'Texas', 'TX', 'Hockley', '219', 33.81040, -102.56560, 4, 1, NULL, NULL),
(1433, '79358', 'Ropesville', 'Texas', 'TX', 'Hockley', '219', 33.45750, -102.15840, 4, 1, NULL, NULL),
(1434, '79367', 'Smyer', 'Texas', 'TX', 'Hockley', '219', 33.59180, -102.16920, 4, 1, NULL, NULL),
(1435, '79372', 'Sundown', 'Texas', 'TX', 'Hockley', '219', 33.44820, -102.48980, 4, 1, NULL, NULL),
(1436, '79380', 'Whitharral', 'Texas', 'TX', 'Hockley', '219', 33.73540, -102.34190, 4, 1, NULL, NULL),
(1437, '76035', 'Cresson', 'Texas', 'TX', 'Hood', '221', 32.53430, -97.61580, 4, 1, NULL, NULL),
(1438, '76048', 'Granbury', 'Texas', 'TX', 'Hood', '221', 32.42500, -97.77420, 4, 1, NULL, NULL),
(1439, '76049', 'Granbury', 'Texas', 'TX', 'Hood', '221', 32.44880, -97.72850, 4, 1, NULL, NULL),
(1440, '76462', 'Lipan', 'Texas', 'TX', 'Hood', '221', 32.50720, -97.95360, 4, 1, NULL, NULL),
(1441, '76467', 'Paluxy', 'Texas', 'TX', 'Hood', '221', 32.27070, -97.90750, 4, 1, NULL, NULL),
(1442, '76476', 'Tolar', 'Texas', 'TX', 'Hood', '221', 32.37720, -97.88020, 4, 1, NULL, NULL),
(1443, '75420', 'Brashear', 'Texas', 'TX', 'Hopkins', '223', 33.11550, -95.73450, 4, 1, NULL, NULL),
(1444, '75431', 'Como', 'Texas', 'TX', 'Hopkins', '223', 33.01070, -95.47340, 4, 1, NULL, NULL),
(1445, '75433', 'Cumby', 'Texas', 'TX', 'Hopkins', '223', 33.11180, -95.79450, 4, 1, NULL, NULL),
(1446, '75437', 'Dike', 'Texas', 'TX', 'Hopkins', '223', 33.26480, -95.44230, 4, 1, NULL, NULL),
(1447, '75471', 'Pickton', 'Texas', 'TX', 'Hopkins', '223', 33.05190, -95.38510, 4, 1, NULL, NULL),
(1448, '75478', 'Saltillo', 'Texas', 'TX', 'Hopkins', '223', 33.17670, -95.34330, 4, 1, NULL, NULL),
(1449, '75481', 'Sulphur Bluff', 'Texas', 'TX', 'Hopkins', '223', 33.33340, -95.37400, 4, 1, NULL, NULL),
(1450, '75482', 'Sulphur Springs', 'Texas', 'TX', 'Hopkins', '223', 33.13450, -95.59220, 4, 1, NULL, NULL),
(1451, '75483', 'Sulphur Springs', 'Texas', 'TX', 'Hopkins', '223', 33.13840, -95.60110, 4, 1, NULL, NULL),
(1452, '75835', 'Crockett', 'Texas', 'TX', 'Houston', '225', 31.32080, -95.39280, 4, 1, NULL, NULL),
(1453, '75844', 'Grapeland', 'Texas', 'TX', 'Houston', '225', 31.49720, -95.44470, 4, 1, NULL, NULL),
(1454, '75847', 'Kennard', 'Texas', 'TX', 'Houston', '225', 31.33840, -95.15410, 4, 1, NULL, NULL),
(1455, '75849', 'Latexo', 'Texas', 'TX', 'Houston', '225', 31.39520, -95.47410, 4, 1, NULL, NULL),
(1456, '75851', 'Lovelady', 'Texas', 'TX', 'Houston', '225', 31.05640, -95.55010, 4, 1, NULL, NULL),
(1457, '75858', 'Ratcliff', 'Texas', 'TX', 'Houston', '225', 31.39160, -95.13970, 4, 1, NULL, NULL),
(1458, '79511', 'Coahoma', 'Texas', 'TX', 'Howard', '227', 32.29420, -101.31970, 4, 1, NULL, NULL),
(1459, '79720', 'Big Spring', 'Texas', 'TX', 'Howard', '227', 32.27870, -101.45780, 1, 1, NULL, NULL),
(1460, '79721', 'Big Spring', 'Texas', 'TX', 'Howard', '227', 32.27330, -101.37400, 1, 1, NULL, NULL),
(1461, '79733', 'Forsan', 'Texas', 'TX', 'Howard', '227', 32.11050, -101.36690, 4, 1, NULL, NULL),
(1462, '79748', 'Knott', 'Texas', 'TX', 'Howard', '227', 32.41250, -101.65170, 4, 1, NULL, NULL),
(1463, '79837', 'Dell City', 'Texas', 'TX', 'Hudspeth', '229', 31.92400, -105.20990, 4, 1, NULL, NULL),
(1464, '79839', 'Fort Hancock', 'Texas', 'TX', 'Hudspeth', '229', 31.29850, -105.84520, 4, 1, NULL, NULL),
(1465, '79847', 'Salt Flat', 'Texas', 'TX', 'Hudspeth', '229', 31.82440, -105.22120, 1, 1, NULL, NULL),
(1466, '79851', 'Sierra Blanca', 'Texas', 'TX', 'Hudspeth', '229', 31.19380, -105.32190, 4, 1, NULL, NULL),
(1467, '75135', 'Caddo Mills', 'Texas', 'TX', 'Hunt', '231', 33.06830, -96.23910, 4, 1, NULL, NULL),
(1468, '75401', 'Greenville', 'Texas', 'TX', 'Hunt', '231', 33.18540, -96.13040, 4, 1, NULL, NULL),
(1469, '75402', 'Greenville', 'Texas', 'TX', 'Hunt', '231', 33.10470, -96.09270, 4, 1, NULL, NULL),
(1470, '75403', 'Greenville', 'Texas', 'TX', 'Hunt', '231', 33.21850, -96.04870, 4, 1, NULL, NULL),
(1471, '75404', 'Greenville', 'Texas', 'TX', 'Hunt', '231', 33.05630, -96.08100, 4, 1, NULL, NULL),
(1472, '75422', 'Campbell', 'Texas', 'TX', 'Hunt', '231', 33.15100, -95.94390, 4, 1, NULL, NULL),
(1473, '75423', 'Celeste', 'Texas', 'TX', 'Hunt', '231', 33.26490, -96.20760, 4, 1, NULL, NULL),
(1474, '75428', 'Commerce', 'Texas', 'TX', 'Hunt', '231', 33.24930, -95.90970, 4, 1, NULL, NULL),
(1475, '75429', 'Commerce', 'Texas', 'TX', 'Hunt', '231', 33.23770, -95.90890, 4, 1, NULL, NULL),
(1476, '75453', 'Lone Oak', 'Texas', 'TX', 'Hunt', '231', 32.99160, -95.94340, 4, 1, NULL, NULL),
(1477, '75458', 'Merit', 'Texas', 'TX', 'Hunt', '231', 33.24270, -96.29160, 4, 1, NULL, NULL),
(1478, '75474', 'Quinlan', 'Texas', 'TX', 'Hunt', '231', 32.89830, -96.12610, 4, 1, NULL, NULL),
(1479, '75496', 'Wolfe City', 'Texas', 'TX', 'Hunt', '231', 33.36050, -96.06910, 4, 1, NULL, NULL),
(1480, '79007', 'Borger', 'Texas', 'TX', 'Hutchinson', '233', 35.66780, -101.39740, 4, 1, NULL, NULL),
(1481, '79008', 'Borger', 'Texas', 'TX', 'Hutchinson', '233', 35.66780, -101.39740, 4, 1, NULL, NULL),
(1482, '79036', 'Fritch', 'Texas', 'TX', 'Hutchinson', '233', 35.64410, -101.58450, 4, 1, NULL, NULL),
(1483, '79078', 'Sanford', 'Texas', 'TX', 'Hutchinson', '233', 35.70200, -101.53460, 4, 1, NULL, NULL),
(1484, '79083', 'Stinnett', 'Texas', 'TX', 'Hutchinson', '233', 35.83770, -101.45000, 4, 1, NULL, NULL),
(1485, '76930', 'Barnhart', 'Texas', 'TX', 'Irion', '235', 31.15960, -101.19180, 4, 1, NULL, NULL),
(1486, '76941', 'Mertzon', 'Texas', 'TX', 'Irion', '235', 31.28290, -100.82210, 4, 1, NULL, NULL),
(1487, '76427', 'Bryson', 'Texas', 'TX', 'Jack', '237', 33.16150, -98.38740, 4, 1, NULL, NULL),
(1488, '76458', 'Jacksboro', 'Texas', 'TX', 'Jack', '237', 33.23470, -98.16810, 4, 1, NULL, NULL),
(1489, '76459', 'Jermyn', 'Texas', 'TX', 'Jack', '237', 33.26360, -98.39310, 4, 1, NULL, NULL),
(1490, '76486', 'Perrin', 'Texas', 'TX', 'Jack', '237', 33.03400, -98.06920, 4, 1, NULL, NULL),
(1491, '77957', 'Edna', 'Texas', 'TX', 'Jackson', '239', 28.95270, -96.64880, 4, 1, NULL, NULL),
(1492, '77961', 'Francitas', 'Texas', 'TX', 'Jackson', '239', 28.85970, -96.33860, 4, 1, NULL, NULL),
(1493, '77962', 'Ganado', 'Texas', 'TX', 'Jackson', '239', 29.03080, -96.50310, 4, 1, NULL, NULL),
(1494, '77969', 'La Salle', 'Texas', 'TX', 'Jackson', '239', 28.76710, -96.64920, 4, 1, NULL, NULL),
(1495, '77970', 'La Ward', 'Texas', 'TX', 'Jackson', '239', 28.84390, -96.46410, 4, 1, NULL, NULL),
(1496, '77971', 'Lolita', 'Texas', 'TX', 'Jackson', '239', 28.77240, -96.47930, 4, 1, NULL, NULL),
(1497, '77991', 'Vanderbilt', 'Texas', 'TX', 'Jackson', '239', 28.80680, -96.60420, 4, 1, NULL, NULL),
(1498, '75951', 'Jasper', 'Texas', 'TX', 'Jasper', '241', 30.86730, -93.99770, 4, 1, NULL, NULL),
(1499, '75956', 'Kirbyville', 'Texas', 'TX', 'Jasper', '241', 30.65830, -93.89840, 4, 1, NULL, NULL),
(1500, '77612', 'Buna', 'Texas', 'TX', 'Jasper', '241', 30.41320, -93.99130, 4, 1, NULL, NULL),
(1501, '77615', 'Evadale', 'Texas', 'TX', 'Jasper', '241', 30.31290, -94.07310, 4, 1, NULL, NULL),
(1502, '79734', 'Fort Davis', 'Texas', 'TX', 'Jeff Davis', '243', 30.61310, -103.93640, 4, 1, NULL, NULL),
(1503, '79854', 'Valentine', 'Texas', 'TX', 'Jeff Davis', '243', 30.62000, -104.48110, 4, 1, NULL, NULL),
(1504, '77613', 'China', 'Texas', 'TX', 'Jefferson', '245', 30.01080, -94.36300, 4, 1, NULL, NULL),
(1505, '77619', 'Groves', 'Texas', 'TX', 'Jefferson', '245', 29.94480, -93.91520, 4, 1, NULL, NULL),
(1506, '77622', 'Hamshire', 'Texas', 'TX', 'Jefferson', '245', 29.86680, -94.31870, 4, 1, NULL, NULL),
(1507, '77627', 'Nederland', 'Texas', 'TX', 'Jefferson', '245', 29.97160, -94.00120, 4, 1, NULL, NULL),
(1508, '77629', 'Nome', 'Texas', 'TX', 'Jefferson', '245', 30.00150, -94.41890, 4, 1, NULL, NULL),
(1509, '77640', 'Port Arthur', 'Texas', 'TX', 'Jefferson', '245', 29.87090, -93.96430, 4, 1, NULL, NULL),
(1510, '77641', 'Port Arthur', 'Texas', 'TX', 'Jefferson', '245', 29.84760, -94.12970, 4, 1, NULL, NULL),
(1511, '77642', 'Port Arthur', 'Texas', 'TX', 'Jefferson', '245', 29.92120, -93.92700, 4, 1, NULL, NULL),
(1512, '77643', 'Port Arthur', 'Texas', 'TX', 'Jefferson', '245', 29.96210, -93.86790, 4, 1, NULL, NULL),
(1513, '77651', 'Port Neches', 'Texas', 'TX', 'Jefferson', '245', 29.97700, -93.96260, 4, 1, NULL, NULL),
(1514, '77655', 'Sabine Pass', 'Texas', 'TX', 'Jefferson', '245', 29.73360, -93.89430, 4, 1, NULL, NULL),
(1515, '77701', 'Beaumont', 'Texas', 'TX', 'Jefferson', '245', 30.06880, -94.10390, 4, 1, NULL, NULL),
(1516, '77702', 'Beaumont', 'Texas', 'TX', 'Jefferson', '245', 30.08710, -94.12540, 4, 1, NULL, NULL),
(1517, '77703', 'Beaumont', 'Texas', 'TX', 'Jefferson', '245', 30.11320, -94.11970, 4, 1, NULL, NULL);
INSERT INTO `zip_codes` (`id`, `zipcode`, `city`, `state`, `state_abbr`, `county_area`, `code`, `latitude`, `longitude`, `some_field`, `status`, `created_at`, `updated_at`) VALUES
(1518, '77704', 'Beaumont', 'Texas', 'TX', 'Jefferson', '245', 30.12360, -94.15390, 4, 1, NULL, NULL),
(1519, '77705', 'Beaumont', 'Texas', 'TX', 'Jefferson', '245', 30.02110, -94.11570, 4, 1, NULL, NULL),
(1520, '77706', 'Beaumont', 'Texas', 'TX', 'Jefferson', '245', 30.09480, -94.16480, 4, 1, NULL, NULL),
(1521, '77707', 'Beaumont', 'Texas', 'TX', 'Jefferson', '245', 30.06860, -94.17550, 4, 1, NULL, NULL),
(1522, '77708', 'Beaumont', 'Texas', 'TX', 'Jefferson', '245', 30.14000, -94.16040, 4, 1, NULL, NULL),
(1523, '77710', 'Beaumont', 'Texas', 'TX', 'Jefferson', '245', 30.08600, -94.10180, 4, 1, NULL, NULL),
(1524, '77713', 'Beaumont', 'Texas', 'TX', 'Jefferson', '245', 30.08500, -94.26070, 4, 1, NULL, NULL),
(1525, '77720', 'Beaumont', 'Texas', 'TX', 'Jefferson', '245', 30.08600, -94.10180, 4, 1, NULL, NULL),
(1526, '77725', 'Beaumont', 'Texas', 'TX', 'Jefferson', '245', 30.08600, -94.10180, 4, 1, NULL, NULL),
(1527, '77726', 'Beaumont', 'Texas', 'TX', 'Jefferson', '245', 30.11180, -94.19010, 4, 1, NULL, NULL),
(1528, '78360', 'Guerra', 'Texas', 'TX', 'Jim Hogg', '247', 26.88310, -98.89360, 4, 1, NULL, NULL),
(1529, '78361', 'Hebbronville', 'Texas', 'TX', 'Jim Hogg', '247', 27.29970, -98.68290, 4, 1, NULL, NULL),
(1530, '78332', 'Alice', 'Texas', 'TX', 'Jim Wells', '249', 27.74320, -98.08360, 4, 1, NULL, NULL),
(1531, '78333', 'Alice', 'Texas', 'TX', 'Jim Wells', '249', 27.75220, -98.06970, 4, 1, NULL, NULL),
(1532, '78342', 'Ben Bolt', 'Texas', 'TX', 'Jim Wells', '249', 27.64750, -98.08140, 4, 1, NULL, NULL),
(1533, '78372', 'Orange Grove', 'Texas', 'TX', 'Jim Wells', '249', 27.94870, -97.98380, 4, 1, NULL, NULL),
(1534, '78375', 'Premont', 'Texas', 'TX', 'Jim Wells', '249', 27.35440, -98.13300, 4, 1, NULL, NULL),
(1535, '78383', 'Sandia', 'Texas', 'TX', 'Jim Wells', '249', 28.02500, -97.87050, 4, 1, NULL, NULL),
(1536, '76009', 'Alvarado', 'Texas', 'TX', 'Johnson', '251', 32.43950, -97.21300, 4, 1, NULL, NULL),
(1537, '76028', 'Burleson', 'Texas', 'TX', 'Johnson', '251', 32.53160, -97.30900, 4, 1, NULL, NULL),
(1538, '76031', 'Cleburne', 'Texas', 'TX', 'Johnson', '251', 32.34850, -97.33110, 4, 1, NULL, NULL),
(1539, '76033', 'Cleburne', 'Texas', 'TX', 'Johnson', '251', 32.35090, -97.41030, 4, 1, NULL, NULL),
(1540, '76044', 'Godley', 'Texas', 'TX', 'Johnson', '251', 32.42820, -97.53490, 4, 1, NULL, NULL),
(1541, '76050', 'Grandview', 'Texas', 'TX', 'Johnson', '251', 32.27040, -97.17920, 4, 1, NULL, NULL),
(1542, '76058', 'Joshua', 'Texas', 'TX', 'Johnson', '251', 32.46630, -97.40110, 4, 1, NULL, NULL),
(1543, '76059', 'Keene', 'Texas', 'TX', 'Johnson', '251', 32.39370, -97.32870, 4, 1, NULL, NULL),
(1544, '76061', 'Lillian', 'Texas', 'TX', 'Johnson', '251', 32.50650, -97.18750, 4, 1, NULL, NULL),
(1545, '76084', 'Venus', 'Texas', 'TX', 'Johnson', '251', 32.43300, -97.10870, 4, 1, NULL, NULL),
(1546, '76093', 'Rio Vista', 'Texas', 'TX', 'Johnson', '251', 32.25320, -97.36780, 4, 1, NULL, NULL),
(1547, '76097', 'Burleson', 'Texas', 'TX', 'Johnson', '251', 32.54210, -97.32080, 4, 1, NULL, NULL),
(1548, '79501', 'Anson', 'Texas', 'TX', 'Jones', '253', 32.74890, -99.89530, 4, 1, NULL, NULL),
(1549, '79503', 'Avoca', 'Texas', 'TX', 'Jones', '253', 32.88320, -99.69640, 4, 1, NULL, NULL),
(1550, '79520', 'Hamlin', 'Texas', 'TX', 'Jones', '253', 32.87990, -100.12800, 4, 1, NULL, NULL),
(1551, '79525', 'Hawley', 'Texas', 'TX', 'Jones', '253', 32.64230, -99.92390, 4, 1, NULL, NULL),
(1552, '79533', 'Lueders', 'Texas', 'TX', 'Jones', '253', 32.87120, -99.57830, 4, 1, NULL, NULL),
(1553, '79553', 'Stamford', 'Texas', 'TX', 'Jones', '253', 32.94530, -99.78590, 4, 1, NULL, NULL),
(1554, '78111', 'Ecleto', 'Texas', 'TX', 'Karnes', '255', 29.04640, -97.75140, 4, 1, NULL, NULL),
(1555, '78113', 'Falls City', 'Texas', 'TX', 'Karnes', '255', 28.98140, -98.01560, 4, 1, NULL, NULL),
(1556, '78116', 'Gillett', 'Texas', 'TX', 'Karnes', '255', 29.05140, -97.83430, 4, 1, NULL, NULL),
(1557, '78117', 'Hobson', 'Texas', 'TX', 'Karnes', '255', 28.94450, -97.97070, 4, 1, NULL, NULL),
(1558, '78118', 'Karnes City', 'Texas', 'TX', 'Karnes', '255', 28.88280, -97.90710, 4, 1, NULL, NULL),
(1559, '78119', 'Kenedy', 'Texas', 'TX', 'Karnes', '255', 28.80460, -97.84560, 4, 1, NULL, NULL),
(1560, '78144', 'Panna Maria', 'Texas', 'TX', 'Karnes', '255', 28.95620, -97.89820, 4, 1, NULL, NULL),
(1561, '78151', 'Runge', 'Texas', 'TX', 'Karnes', '255', 28.88760, -97.71380, 4, 1, NULL, NULL),
(1562, '75114', 'Crandall', 'Texas', 'TX', 'Kaufman County', '257', 32.62790, -96.45580, 4, 1, NULL, NULL),
(1563, '75118', 'Elmo', 'Texas', 'TX', 'Kaufman County', '257', 32.59960, -96.30270, 4, 1, NULL, NULL),
(1564, '75126', 'Forney', 'Texas', 'TX', 'Kaufman County', '257', 32.74910, -96.45980, 4, 1, NULL, NULL),
(1565, '75142', 'Kaufman', 'Texas', 'TX', 'Kaufman County', '257', 32.54600, -96.28520, 4, 1, NULL, NULL),
(1566, '75147', 'Mabank', 'Texas', 'TX', 'Kaufman County', '257', 32.36850, -96.06850, 4, 1, NULL, NULL),
(1567, '75156', 'Mabank', 'Texas', 'TX', 'Kaufman County', '257', 32.29720, -96.11840, 4, 1, NULL, NULL),
(1568, '75157', 'Rosser', 'Texas', 'TX', 'Kaufman County', '257', 32.46260, -96.45330, 4, 1, NULL, NULL),
(1569, '75158', 'Scurry', 'Texas', 'TX', 'Kaufman County', '257', 32.48180, -96.39250, 4, 1, NULL, NULL),
(1570, '75160', 'Terrell', 'Texas', 'TX', 'Kaufman County', '257', 32.75150, -96.28310, 4, 1, NULL, NULL),
(1571, '75161', 'Terrell', 'Texas', 'TX', 'Kaufman County', '257', 32.73320, -96.19500, 4, 1, NULL, NULL),
(1572, '78004', 'Bergheim', 'Texas', 'TX', 'Kendall', '259', 29.89790, -98.56420, 4, 1, NULL, NULL),
(1573, '78006', 'Boerne', 'Texas', 'TX', 'Kendall', '259', 29.89310, -98.68570, 4, 1, NULL, NULL),
(1574, '78013', 'Comfort', 'Texas', 'TX', 'Kendall', '259', 29.96770, -98.90500, 4, 1, NULL, NULL),
(1575, '78027', 'Kendalia', 'Texas', 'TX', 'Kendall', '259', 29.94060, -98.51660, 4, 1, NULL, NULL),
(1576, '78074', 'Waring', 'Texas', 'TX', 'Kendall', '259', 29.95230, -98.79440, 4, 1, NULL, NULL),
(1577, '78338', 'Armstrong', 'Texas', 'TX', 'Kenedy', '261', 26.86980, -97.76980, 4, 1, NULL, NULL),
(1578, '78385', 'Sarita', 'Texas', 'TX', 'Kenedy', '261', 27.14960, -97.85760, 4, 1, NULL, NULL),
(1579, '79518', 'Girard', 'Texas', 'TX', 'Kent', '263', 33.36320, -100.69370, 4, 1, NULL, NULL),
(1580, '79528', 'Jayton', 'Texas', 'TX', 'Kent', '263', 33.25180, -100.58250, 4, 1, NULL, NULL),
(1581, '78010', 'Center Point', 'Texas', 'TX', 'Kerr', '265', 29.94410, -99.03640, 4, 1, NULL, NULL),
(1582, '78024', 'Hunt', 'Texas', 'TX', 'Kerr', '265', 30.00270, -99.48230, 4, 1, NULL, NULL),
(1583, '78025', 'Ingram', 'Texas', 'TX', 'Kerr', '265', 30.07310, -99.26900, 4, 1, NULL, NULL),
(1584, '78028', 'Kerrville', 'Texas', 'TX', 'Kerr', '265', 30.04160, -99.14080, 4, 1, NULL, NULL),
(1585, '78029', 'Kerrville', 'Texas', 'TX', 'Kerr', '265', 30.03320, -99.14100, 4, 1, NULL, NULL),
(1586, '78058', 'Mountain Home', 'Texas', 'TX', 'Kerr', '265', 30.17250, -99.48500, 4, 1, NULL, NULL),
(1587, '76849', 'Junction', 'Texas', 'TX', 'Kimble', '267', 30.47540, -99.74730, 4, 1, NULL, NULL),
(1588, '76854', 'London', 'Texas', 'TX', 'Kimble', '267', 30.67680, -99.57650, 4, 1, NULL, NULL),
(1589, '76874', 'Roosevelt', 'Texas', 'TX', 'Kimble', '267', 30.47000, -100.09010, 4, 1, NULL, NULL),
(1590, '79236', 'Guthrie', 'Texas', 'TX', 'King', '269', 33.62060, -100.32290, 4, 1, NULL, NULL),
(1591, '78832', 'Brackettville', 'Texas', 'TX', 'Kinney', '271', 29.30960, -100.41550, 4, 1, NULL, NULL),
(1592, '78363', 'Kingsville', 'Texas', 'TX', 'Kleberg', '273', 27.42290, -97.84070, 4, 1, NULL, NULL),
(1593, '78364', 'Kingsville', 'Texas', 'TX', 'Kleberg', '273', 27.51590, -97.85610, 4, 1, NULL, NULL),
(1594, '78379', 'Riviera', 'Texas', 'TX', 'Kleberg', '273', 27.32170, -97.77870, 4, 1, NULL, NULL),
(1595, '76363', 'Goree', 'Texas', 'TX', 'Knox', '275', 33.47480, -99.52580, 4, 1, NULL, NULL),
(1596, '76371', 'Munday', 'Texas', 'TX', 'Knox', '275', 33.45610, -99.63260, 4, 1, NULL, NULL),
(1597, '79505', 'Benjamin', 'Texas', 'TX', 'Knox', '275', 33.58400, -99.79230, 4, 1, NULL, NULL),
(1598, '79529', 'Knox City', 'Texas', 'TX', 'Knox', '275', 33.41830, -99.81370, 4, 1, NULL, NULL),
(1599, '75411', 'Arthur City', 'Texas', 'TX', 'Lamar', '277', 33.86430, -95.61110, 4, 1, NULL, NULL),
(1600, '75416', 'Blossom', 'Texas', 'TX', 'Lamar', '277', 33.69450, -95.38230, 4, 1, NULL, NULL),
(1601, '75421', 'Brookston', 'Texas', 'TX', 'Lamar', '277', 33.62460, -95.68880, 4, 1, NULL, NULL),
(1602, '75425', 'Chicota', 'Texas', 'TX', 'Lamar', '277', 33.86900, -95.57110, 4, 1, NULL, NULL),
(1603, '75434', 'Cunningham', 'Texas', 'TX', 'Lamar', '277', 33.40900, -95.37110, 4, 1, NULL, NULL),
(1604, '75435', 'Deport', 'Texas', 'TX', 'Lamar', '277', 33.52210, -95.36540, 4, 1, NULL, NULL),
(1605, '75460', 'Paris', 'Texas', 'TX', 'Lamar', '277', 33.65810, -95.53790, 4, 1, NULL, NULL),
(1606, '75461', 'Paris', 'Texas', 'TX', 'Lamar', '277', 33.66090, -95.55550, 4, 1, NULL, NULL),
(1607, '75462', 'Paris', 'Texas', 'TX', 'Lamar', '277', 33.68050, -95.49050, 4, 1, NULL, NULL),
(1608, '75468', 'Pattonville', 'Texas', 'TX', 'Lamar', '277', 33.57020, -95.39080, 4, 1, NULL, NULL),
(1609, '75470', 'Petty', 'Texas', 'TX', 'Lamar', '277', 33.60980, -95.78910, 4, 1, NULL, NULL),
(1610, '75473', 'Powderly', 'Texas', 'TX', 'Lamar', '277', 33.77790, -95.53070, 4, 1, NULL, NULL),
(1611, '75477', 'Roxton', 'Texas', 'TX', 'Lamar', '277', 33.54290, -95.74160, 4, 1, NULL, NULL),
(1612, '75486', 'Sumner', 'Texas', 'TX', 'Lamar', '277', 33.75890, -95.68070, 4, 1, NULL, NULL),
(1613, '79031', 'Earth', 'Texas', 'TX', 'Lamb', '279', 34.24150, -102.42130, 4, 1, NULL, NULL),
(1614, '79064', 'Olton', 'Texas', 'TX', 'Lamb', '279', 34.18440, -102.14140, 4, 1, NULL, NULL),
(1615, '79082', 'Springlake', 'Texas', 'TX', 'Lamb', '279', 34.23930, -102.30900, 1, 1, NULL, NULL),
(1616, '79312', 'Amherst', 'Texas', 'TX', 'Lamb', '279', 33.99760, -102.44160, 4, 1, NULL, NULL),
(1617, '79326', 'Fieldton', 'Texas', 'TX', 'Lamb', '279', 34.05300, -102.20600, 4, 1, NULL, NULL),
(1618, '79339', 'Littlefield', 'Texas', 'TX', 'Lamb', '279', 33.92120, -102.32070, 4, 1, NULL, NULL),
(1619, '79369', 'Spade', 'Texas', 'TX', 'Lamb', '279', 33.92560, -102.15640, 4, 1, NULL, NULL),
(1620, '79371', 'Sudan', 'Texas', 'TX', 'Lamb', '279', 34.06970, -102.52550, 4, 1, NULL, NULL),
(1621, '76539', 'Kempner', 'Texas', 'TX', 'Lampasas', '281', 31.08870, -98.00990, 4, 1, NULL, NULL),
(1622, '76550', 'Lampasas', 'Texas', 'TX', 'Lampasas', '281', 31.06800, -98.18340, 4, 1, NULL, NULL),
(1623, '76824', 'Bend', 'Texas', 'TX', 'Lampasas', '281', 31.11230, -98.48210, 4, 1, NULL, NULL),
(1624, '76853', 'Lometa', 'Texas', 'TX', 'Lampasas', '281', 31.21670, -98.40060, 4, 1, NULL, NULL),
(1625, '78001', 'Artesia Wells', 'Texas', 'TX', 'La Salle', '283', 28.26390, -99.28020, 4, 1, NULL, NULL),
(1626, '78014', 'Cotulla', 'Texas', 'TX', 'La Salle', '283', 28.43980, -99.23280, 4, 1, NULL, NULL),
(1627, '78019', 'Encinal', 'Texas', 'TX', 'La Salle', '283', 28.04050, -99.35630, 4, 1, NULL, NULL),
(1628, '78021', 'Fowlerton', 'Texas', 'TX', 'La Salle', '283', 28.48870, -98.85180, 4, 1, NULL, NULL),
(1629, '77964', 'Hallettsville', 'Texas', 'TX', 'Lavaca', '285', 29.44260, -96.92340, 4, 1, NULL, NULL),
(1630, '77975', 'Moulton', 'Texas', 'TX', 'Lavaca', '285', 29.57000, -97.10310, 4, 1, NULL, NULL),
(1631, '77984', 'Shiner', 'Texas', 'TX', 'Lavaca', '285', 29.42800, -97.16400, 4, 1, NULL, NULL),
(1632, '77986', 'Sublime', 'Texas', 'TX', 'Lavaca', '285', 29.48760, -96.79460, 4, 1, NULL, NULL),
(1633, '77987', 'Sweet Home', 'Texas', 'TX', 'Lavaca', '285', 29.34260, -97.07560, 4, 1, NULL, NULL),
(1634, '77995', 'Yoakum', 'Texas', 'TX', 'Lavaca', '285', 29.28400, -97.13060, 4, 1, NULL, NULL),
(1635, '77853', 'Dime Box', 'Texas', 'TX', 'Lee', '287', 30.35790, -96.82480, 4, 1, NULL, NULL),
(1636, '78942', 'Giddings', 'Texas', 'TX', 'Lee', '287', 30.17770, -96.93320, 4, 1, NULL, NULL),
(1637, '78947', 'Lexington', 'Texas', 'TX', 'Lee', '287', 30.41910, -97.05240, 4, 1, NULL, NULL),
(1638, '78948', 'Lincoln', 'Texas', 'TX', 'Lee', '287', 30.31770, -96.97020, 4, 1, NULL, NULL),
(1639, '75831', 'Buffalo', 'Texas', 'TX', 'Leon', '289', 31.41210, -95.99040, 4, 1, NULL, NULL),
(1640, '75833', 'Centerville', 'Texas', 'TX', 'Leon', '289', 31.27200, -95.92130, 4, 1, NULL, NULL),
(1641, '75846', 'Jewett', 'Texas', 'TX', 'Leon', '289', 31.37390, -96.19180, 4, 1, NULL, NULL),
(1642, '75850', 'Leona', 'Texas', 'TX', 'Leon', '289', 31.14210, -95.92840, 4, 1, NULL, NULL),
(1643, '75855', 'Oakwood', 'Texas', 'TX', 'Leon', '289', 31.58490, -95.84910, 4, 1, NULL, NULL),
(1644, '77850', 'Concord', 'Texas', 'TX', 'Leon', '289', 31.31380, -95.99350, 4, 1, NULL, NULL),
(1645, '77855', 'Flynn', 'Texas', 'TX', 'Leon', '289', 31.15930, -96.12340, 4, 1, NULL, NULL),
(1646, '77865', 'Marquez', 'Texas', 'TX', 'Leon', '289', 31.23090, -96.23750, 4, 1, NULL, NULL),
(1647, '77871', 'Normangee', 'Texas', 'TX', 'Leon', '289', 31.07050, -96.12500, 4, 1, NULL, NULL),
(1648, '77327', 'Cleveland', 'Texas', 'TX', 'Liberty', '291', 30.33000, -95.02020, 4, 1, NULL, NULL),
(1649, '77328', 'Cleveland', 'Texas', 'TX', 'Liberty', '291', 30.39630, -95.19400, 4, 1, NULL, NULL),
(1650, '77368', 'Romayor', 'Texas', 'TX', 'Liberty', '291', 30.43590, -94.82220, 4, 1, NULL, NULL),
(1651, '77369', 'Rye', 'Texas', 'TX', 'Liberty', '291', 30.45960, -94.74360, 4, 1, NULL, NULL),
(1652, '77533', 'Daisetta', 'Texas', 'TX', 'Liberty', '291', 30.11330, -94.64300, 4, 1, NULL, NULL),
(1653, '77535', 'Dayton', 'Texas', 'TX', 'Liberty', '291', 30.01020, -94.87870, 4, 1, NULL, NULL),
(1654, '77538', 'Devers', 'Texas', 'TX', 'Liberty', '291', 29.99780, -94.57460, 4, 1, NULL, NULL),
(1655, '77561', 'Hardin', 'Texas', 'TX', 'Liberty', '291', 30.15100, -94.73380, 4, 1, NULL, NULL),
(1656, '77564', 'Hull', 'Texas', 'TX', 'Liberty', '291', 30.14630, -94.64240, 4, 1, NULL, NULL),
(1657, '77575', 'Liberty', 'Texas', 'TX', 'Liberty', '291', 30.09460, -94.73780, 4, 1, NULL, NULL),
(1658, '77582', 'Raywood', 'Texas', 'TX', 'Liberty', '291', 30.05420, -94.67640, 4, 1, NULL, NULL),
(1659, '76635', 'Coolidge', 'Texas', 'TX', 'Limestone', '293', 31.74380, -96.65770, 4, 1, NULL, NULL),
(1660, '76642', 'Groesbeck', 'Texas', 'TX', 'Limestone', '293', 31.53570, -96.52340, 4, 1, NULL, NULL),
(1661, '76653', 'Kosse', 'Texas', 'TX', 'Limestone', '293', 31.31470, -96.61950, 4, 1, NULL, NULL),
(1662, '76667', 'Mexia', 'Texas', 'TX', 'Limestone', '293', 31.67840, -96.49520, 4, 1, NULL, NULL),
(1663, '76678', 'Prairie Hill', 'Texas', 'TX', 'Limestone', '293', 31.65910, -96.80940, 4, 1, NULL, NULL),
(1664, '76686', 'Tehuacana', 'Texas', 'TX', 'Limestone', '293', 31.75040, -96.54160, 4, 1, NULL, NULL),
(1665, '76687', 'Thornton', 'Texas', 'TX', 'Limestone', '293', 31.40830, -96.50240, 4, 1, NULL, NULL),
(1666, '79005', 'Booker', 'Texas', 'TX', 'Lipscomb', '295', 36.44290, -100.52380, 4, 1, NULL, NULL),
(1667, '79024', 'Darrouzett', 'Texas', 'TX', 'Lipscomb', '295', 36.44530, -100.32540, 1, 1, NULL, NULL),
(1668, '79034', 'Follett', 'Texas', 'TX', 'Lipscomb', '295', 36.43090, -100.20680, 4, 1, NULL, NULL),
(1669, '79046', 'Higgins', 'Texas', 'TX', 'Lipscomb', '295', 36.13650, -100.09520, 1, 1, NULL, NULL),
(1670, '79056', 'Lipscomb', 'Texas', 'TX', 'Lipscomb', '295', 36.22300, -100.27020, 4, 1, NULL, NULL),
(1671, '78022', 'George West', 'Texas', 'TX', 'Live Oak', '297', 28.32040, -98.11620, 4, 1, NULL, NULL),
(1672, '78060', 'Oakville', 'Texas', 'TX', 'Live Oak', '297', 28.44920, -98.10190, 4, 1, NULL, NULL),
(1673, '78071', 'Three Rivers', 'Texas', 'TX', 'Live Oak', '297', 28.47560, -98.17820, 4, 1, NULL, NULL),
(1674, '78075', 'Whitsett', 'Texas', 'TX', 'Live Oak', '297', 28.63730, -98.25650, 4, 1, NULL, NULL),
(1675, '78350', 'Dinero', 'Texas', 'TX', 'Live Oak', '297', 28.22640, -97.96170, 4, 1, NULL, NULL),
(1676, '76831', 'Castell', 'Texas', 'TX', 'Llano', '299', 30.75650, -98.97120, 4, 1, NULL, NULL),
(1677, '76885', 'Valley Spring', 'Texas', 'TX', 'Llano', '299', 30.86600, -98.83570, 4, 1, NULL, NULL),
(1678, '78607', 'Bluffton', 'Texas', 'TX', 'Llano', '299', 30.82560, -98.51500, 4, 1, NULL, NULL),
(1679, '78609', 'Buchanan Dam', 'Texas', 'TX', 'Llano', '299', 30.75380, -98.42950, 4, 1, NULL, NULL),
(1680, '78639', 'Kingsland', 'Texas', 'TX', 'Llano', '299', 30.66620, -98.44750, 4, 1, NULL, NULL),
(1681, '78643', 'Llano', 'Texas', 'TX', 'Llano', '299', 30.71510, -98.61090, 4, 1, NULL, NULL),
(1682, '78672', 'Tow', 'Texas', 'TX', 'Llano', '299', 30.86090, -98.45960, 4, 1, NULL, NULL),
(1683, '79754', 'Mentone', 'Texas', 'TX', 'Loving', '301', 31.70680, -103.59880, 4, 1, NULL, NULL),
(1684, '79329', 'Idalou', 'Texas', 'TX', 'Lubbock', '303', 33.65040, -101.67860, 4, 1, NULL, NULL),
(1685, '79350', 'New Deal', 'Texas', 'TX', 'Lubbock', '303', 33.72960, -101.83550, 4, 1, NULL, NULL),
(1686, '79363', 'Shallowater', 'Texas', 'TX', 'Lubbock', '303', 33.69190, -101.98360, 4, 1, NULL, NULL),
(1687, '79364', 'Slaton', 'Texas', 'TX', 'Lubbock', '303', 33.43730, -101.64350, 4, 1, NULL, NULL),
(1688, '79366', 'Ransom Canyon', 'Texas', 'TX', 'Lubbock', '303', 33.53340, -101.67960, 4, 1, NULL, NULL),
(1689, '79382', 'Wolfforth', 'Texas', 'TX', 'Lubbock', '303', 33.50590, -102.00910, 4, 1, NULL, NULL),
(1690, '79401', 'Lubbock', 'Texas', 'TX', 'Lubbock', '303', 33.58650, -101.86060, 4, 1, NULL, NULL),
(1691, '79402', 'Lubbock', 'Texas', 'TX', 'Lubbock', '303', 33.59220, -101.85110, 4, 1, NULL, NULL),
(1692, '79403', 'Lubbock', 'Texas', 'TX', 'Lubbock', '303', 33.61960, -101.80980, 4, 1, NULL, NULL),
(1693, '79404', 'Lubbock', 'Texas', 'TX', 'Lubbock', '303', 33.52600, -101.83330, 4, 1, NULL, NULL),
(1694, '79406', 'Lubbock', 'Texas', 'TX', 'Lubbock', '303', 33.58190, -101.87780, 4, 1, NULL, NULL),
(1695, '79407', 'Lubbock', 'Texas', 'TX', 'Lubbock', '303', 33.56840, -101.94230, 4, 1, NULL, NULL),
(1696, '79408', 'Lubbock', 'Texas', 'TX', 'Lubbock', '303', 33.56590, -101.92670, 4, 1, NULL, NULL),
(1697, '79409', 'Lubbock', 'Texas', 'TX', 'Lubbock', '303', 33.57790, -101.85520, 4, 1, NULL, NULL),
(1698, '79410', 'Lubbock', 'Texas', 'TX', 'Lubbock', '303', 33.56930, -101.89040, 4, 1, NULL, NULL),
(1699, '79411', 'Lubbock', 'Texas', 'TX', 'Lubbock', '303', 33.57040, -101.86260, 4, 1, NULL, NULL),
(1700, '79412', 'Lubbock', 'Texas', 'TX', 'Lubbock', '303', 33.54630, -101.85770, 4, 1, NULL, NULL),
(1701, '79413', 'Lubbock', 'Texas', 'TX', 'Lubbock', '303', 33.54660, -101.88710, 4, 1, NULL, NULL),
(1702, '79414', 'Lubbock', 'Texas', 'TX', 'Lubbock', '303', 33.54970, -101.91870, 4, 1, NULL, NULL),
(1703, '79415', 'Lubbock', 'Texas', 'TX', 'Lubbock', '303', 33.60210, -101.87600, 4, 1, NULL, NULL),
(1704, '79416', 'Lubbock', 'Texas', 'TX', 'Lubbock', '303', 33.59240, -101.93670, 4, 1, NULL, NULL),
(1705, '79423', 'Lubbock', 'Texas', 'TX', 'Lubbock', '303', 33.51460, -101.87950, 4, 1, NULL, NULL),
(1706, '79424', 'Lubbock', 'Texas', 'TX', 'Lubbock', '303', 33.51590, -101.93440, 4, 1, NULL, NULL),
(1707, '79430', 'Lubbock', 'Texas', 'TX', 'Lubbock', '303', 33.61000, -101.82130, 4, 1, NULL, NULL),
(1708, '79452', 'Lubbock', 'Texas', 'TX', 'Lubbock', '303', 33.61000, -101.82130, 4, 1, NULL, NULL),
(1709, '79453', 'Lubbock', 'Texas', 'TX', 'Lubbock', '303', 33.57790, -101.85520, 4, 1, NULL, NULL),
(1710, '79457', 'Lubbock', 'Texas', 'TX', 'Lubbock', '303', 33.57790, -101.85520, 4, 1, NULL, NULL),
(1711, '79464', 'Lubbock', 'Texas', 'TX', 'Lubbock', '303', 33.48960, -102.01090, 4, 1, NULL, NULL),
(1712, '79490', 'Lubbock', 'Texas', 'TX', 'Lubbock', '303', 33.61000, -101.82130, 4, 1, NULL, NULL),
(1713, '79491', 'Lubbock', 'Texas', 'TX', 'Lubbock', '303', 33.61000, -101.82130, 4, 1, NULL, NULL),
(1714, '79493', 'Lubbock', 'Texas', 'TX', 'Lubbock', '303', 33.61000, -101.82130, 4, 1, NULL, NULL),
(1715, '79499', 'Lubbock', 'Texas', 'TX', 'Lubbock', '303', 33.61000, -101.82130, 4, 1, NULL, NULL),
(1716, '79351', 'Odonnell', 'Texas', 'TX', 'Lynn', '305', 32.97730, -101.82710, 4, 1, NULL, NULL),
(1717, '79373', 'Tahoka', 'Texas', 'TX', 'Lynn', '305', 33.19880, -101.82200, 4, 1, NULL, NULL),
(1718, '79381', 'Wilson', 'Texas', 'TX', 'Lynn', '305', 33.32990, -101.71220, 4, 1, NULL, NULL),
(1719, '79383', 'New Home', 'Texas', 'TX', 'Lynn', '305', 33.34510, -101.92030, 4, 1, NULL, NULL),
(1720, '76825', 'Brady', 'Texas', 'TX', 'McCulloch', '307', 31.15090, -99.33720, 4, 1, NULL, NULL),
(1721, '76836', 'Doole', 'Texas', 'TX', 'McCulloch', '307', 31.39570, -99.59900, 4, 1, NULL, NULL),
(1722, '76852', 'Lohn', 'Texas', 'TX', 'McCulloch', '307', 31.31730, -99.38330, 4, 1, NULL, NULL),
(1723, '76858', 'Melvin', 'Texas', 'TX', 'McCulloch', '307', 31.19520, -99.57980, 4, 1, NULL, NULL),
(1724, '76872', 'Rochelle', 'Texas', 'TX', 'McCulloch', '307', 31.30000, -99.15720, 4, 1, NULL, NULL),
(1725, '76887', 'Voca', 'Texas', 'TX', 'McCulloch', '307', 30.99590, -99.16820, 4, 1, NULL, NULL),
(1726, '76524', 'Eddy', 'Texas', 'TX', 'McLennan', '309', 31.30880, -97.27740, 4, 1, NULL, NULL),
(1727, '76557', 'Moody', 'Texas', 'TX', 'McLennan', '309', 31.25330, -97.41000, 4, 1, NULL, NULL),
(1728, '76624', 'Axtell', 'Texas', 'TX', 'McLennan', '309', 31.66100, -96.98820, 4, 1, NULL, NULL),
(1729, '76630', 'Bruceville', 'Texas', 'TX', 'McLennan', '309', 31.32670, -97.23420, 4, 1, NULL, NULL),
(1730, '76633', 'China Spring', 'Texas', 'TX', 'McLennan', '309', 31.66730, -97.30020, 1, 1, NULL, NULL),
(1731, '76638', 'Crawford', 'Texas', 'TX', 'McLennan', '309', 31.55980, -97.39000, 4, 1, NULL, NULL),
(1732, '76640', 'Elm Mott', 'Texas', 'TX', 'McLennan', '309', 31.67250, -97.11380, 4, 1, NULL, NULL),
(1733, '76643', 'Hewitt', 'Texas', 'TX', 'McLennan', '309', 31.45820, -97.19660, 4, 1, NULL, NULL),
(1734, '76654', 'Leroy', 'Texas', 'TX', 'McLennan', '309', 31.55360, -97.20320, 4, 1, NULL, NULL),
(1735, '76655', 'Lorena', 'Texas', 'TX', 'McLennan', '309', 31.40930, -97.23020, 4, 1, NULL, NULL),
(1736, '76657', 'Mc Gregor', 'Texas', 'TX', 'McLennan', '309', 31.44310, -97.39430, 1, 1, NULL, NULL),
(1737, '76664', 'Mart', 'Texas', 'TX', 'McLennan', '309', 31.54580, -96.83810, 4, 1, NULL, NULL),
(1738, '76682', 'Riesel', 'Texas', 'TX', 'McLennan', '309', 31.50020, -96.94760, 4, 1, NULL, NULL),
(1739, '76684', 'Ross', 'Texas', 'TX', 'McLennan', '309', 31.71730, -97.11880, 4, 1, NULL, NULL),
(1740, '76691', 'West', 'Texas', 'TX', 'McLennan', '309', 31.77540, -97.12580, 4, 1, NULL, NULL),
(1741, '76701', 'Waco', 'Texas', 'TX', 'McLennan', '309', 31.55250, -97.13960, 4, 1, NULL, NULL),
(1742, '76702', 'Waco', 'Texas', 'TX', 'McLennan', '309', 31.54750, -97.14430, 4, 1, NULL, NULL),
(1743, '76703', 'Waco', 'Texas', 'TX', 'McLennan', '309', 31.55360, -97.20320, 4, 1, NULL, NULL),
(1744, '76704', 'Waco', 'Texas', 'TX', 'McLennan', '309', 31.57730, -97.12410, 4, 1, NULL, NULL),
(1745, '76705', 'Waco', 'Texas', 'TX', 'McLennan', '309', 31.64030, -97.09630, 4, 1, NULL, NULL),
(1746, '76706', 'Waco', 'Texas', 'TX', 'McLennan', '309', 31.51710, -97.11980, 4, 1, NULL, NULL),
(1747, '76707', 'Waco', 'Texas', 'TX', 'McLennan', '309', 31.55270, -97.15880, 4, 1, NULL, NULL),
(1748, '76708', 'Waco', 'Texas', 'TX', 'McLennan', '309', 31.57650, -97.17860, 4, 1, NULL, NULL),
(1749, '76710', 'Waco', 'Texas', 'TX', 'McLennan', '309', 31.53500, -97.18990, 4, 1, NULL, NULL),
(1750, '76711', 'Waco', 'Texas', 'TX', 'McLennan', '309', 31.51750, -97.15470, 4, 1, NULL, NULL),
(1751, '76712', 'Woodway', 'Texas', 'TX', 'McLennan', '309', 31.50510, -97.23110, 4, 1, NULL, NULL),
(1752, '76714', 'Waco', 'Texas', 'TX', 'McLennan', '309', 31.55360, -97.20320, 4, 1, NULL, NULL),
(1753, '76715', 'Waco', 'Texas', 'TX', 'McLennan', '309', 31.55360, -97.20320, 4, 1, NULL, NULL),
(1754, '76716', 'Waco', 'Texas', 'TX', 'McLennan', '309', 31.55360, -97.20320, 4, 1, NULL, NULL),
(1755, '76797', 'Waco', 'Texas', 'TX', 'McLennan', '309', 31.55360, -97.20320, 4, 1, NULL, NULL),
(1756, '76798', 'Waco', 'Texas', 'TX', 'McLennan', '309', 31.54700, -97.12030, 4, 1, NULL, NULL),
(1757, '76799', 'Waco', 'Texas', 'TX', 'McLennan', '309', 31.54110, -97.16150, 4, 1, NULL, NULL),
(1758, '78007', 'Calliham', 'Texas', 'TX', 'McMullen', '311', 28.48050, -98.35030, 4, 1, NULL, NULL),
(1759, '78072', 'Tilden', 'Texas', 'TX', 'McMullen', '311', 28.41980, -98.56930, 4, 1, NULL, NULL),
(1760, '75852', 'Midway', 'Texas', 'TX', 'Madison', '313', 30.98060, -95.70890, 4, 1, NULL, NULL),
(1761, '77864', 'Madisonville', 'Texas', 'TX', 'Madison', '313', 30.95330, -95.90910, 4, 1, NULL, NULL),
(1762, '77872', 'North Zulch', 'Texas', 'TX', 'Madison', '313', 30.92850, -96.09250, 4, 1, NULL, NULL),
(1763, '75564', 'Lodi', 'Texas', 'TX', 'Marion', '315', 32.87180, -94.27290, 4, 1, NULL, NULL),
(1764, '75657', 'Jefferson', 'Texas', 'TX', 'Marion', '315', 32.80590, -94.36550, 4, 1, NULL, NULL),
(1765, '79749', 'Lenorah', 'Texas', 'TX', 'Martin', '317', 32.30460, -101.87620, 4, 1, NULL, NULL),
(1766, '79782', 'Stanton', 'Texas', 'TX', 'Martin', '317', 32.14000, -101.80990, 4, 1, NULL, NULL),
(1767, '79783', 'Tarzan', 'Texas', 'TX', 'Martin', '317', 32.35750, -101.96070, 4, 1, NULL, NULL),
(1768, '76820', 'Art', 'Texas', 'TX', 'Mason', '319', 30.76700, -99.04860, 4, 1, NULL, NULL),
(1769, '76842', 'Fredonia', 'Texas', 'TX', 'Mason', '319', 30.92140, -99.12160, 4, 1, NULL, NULL),
(1770, '76856', 'Mason', 'Texas', 'TX', 'Mason', '319', 30.74340, -99.22610, 4, 1, NULL, NULL),
(1771, '76869', 'Pontotoc', 'Texas', 'TX', 'Mason', '319', 30.89060, -99.02120, 4, 1, NULL, NULL),
(1772, '77404', 'Bay City', 'Texas', 'TX', 'Matagorda', '321', 28.98280, -95.96940, 4, 1, NULL, NULL),
(1773, '77414', 'Bay City', 'Texas', 'TX', 'Matagorda', '321', 28.86360, -95.91730, 4, 1, NULL, NULL),
(1774, '77415', 'Cedar Lane', 'Texas', 'TX', 'Matagorda', '321', 28.92350, -95.72470, 4, 1, NULL, NULL),
(1775, '77419', 'Blessing', 'Texas', 'TX', 'Matagorda', '321', 28.86490, -96.21800, 4, 1, NULL, NULL),
(1776, '77428', 'Collegeport', 'Texas', 'TX', 'Matagorda', '321', 28.72530, -96.17500, 4, 1, NULL, NULL),
(1777, '77440', 'Elmaton', 'Texas', 'TX', 'Matagorda', '321', 28.87600, -96.14340, 4, 1, NULL, NULL),
(1778, '77456', 'Markham', 'Texas', 'TX', 'Matagorda', '321', 28.95470, -96.09280, 4, 1, NULL, NULL),
(1779, '77457', 'Matagorda', 'Texas', 'TX', 'Matagorda', '321', 28.69080, -95.96750, 4, 1, NULL, NULL),
(1780, '77458', 'Midfield', 'Texas', 'TX', 'Matagorda', '321', 28.93620, -96.22650, 4, 1, NULL, NULL),
(1781, '77465', 'Palacios', 'Texas', 'TX', 'Matagorda', '321', 28.71500, -96.21540, 4, 1, NULL, NULL),
(1782, '77468', 'Pledger', 'Texas', 'TX', 'Matagorda', '321', 29.18250, -95.90860, 4, 1, NULL, NULL),
(1783, '77482', 'Van Vleck', 'Texas', 'TX', 'Matagorda', '321', 29.01600, -95.89050, 4, 1, NULL, NULL),
(1784, '77483', 'Wadsworth', 'Texas', 'TX', 'Matagorda', '321', 28.82590, -95.90460, 4, 1, NULL, NULL),
(1785, '78852', 'Eagle Pass', 'Texas', 'TX', 'Maverick', '323', 28.70280, -100.48180, 4, 1, NULL, NULL),
(1786, '78853', 'Eagle Pass', 'Texas', 'TX', 'Maverick', '323', 28.67900, -100.47840, 4, 1, NULL, NULL),
(1787, '78860', 'El Indio', 'Texas', 'TX', 'Maverick', '323', 28.51190, -100.31120, 4, 1, NULL, NULL),
(1788, '78877', 'Quemado', 'Texas', 'TX', 'Maverick', '323', 28.92560, -100.59120, 4, 1, NULL, NULL),
(1789, '78009', 'Castroville', 'Texas', 'TX', 'Medina', '325', 29.35530, -98.88240, 4, 1, NULL, NULL),
(1790, '78016', 'Devine', 'Texas', 'TX', 'Medina', '325', 29.15210, -98.90900, 4, 1, NULL, NULL),
(1791, '78039', 'La Coste', 'Texas', 'TX', 'Medina', '325', 29.30820, -98.81250, 4, 1, NULL, NULL),
(1792, '78056', 'Mico', 'Texas', 'TX', 'Medina', '325', 29.54410, -98.92310, 4, 1, NULL, NULL),
(1793, '78059', 'Natalia', 'Texas', 'TX', 'Medina', '325', 29.21170, -98.85520, 4, 1, NULL, NULL),
(1794, '78066', 'Rio Medina', 'Texas', 'TX', 'Medina', '325', 29.46120, -98.86940, 1, 1, NULL, NULL),
(1795, '78850', 'D Hanis', 'Texas', 'TX', 'Medina', '325', 29.33980, -99.28350, 4, 1, NULL, NULL),
(1796, '78861', 'Hondo', 'Texas', 'TX', 'Medina', '325', 29.34750, -99.14140, 4, 1, NULL, NULL),
(1797, '78886', 'Yancey', 'Texas', 'TX', 'Medina', '325', 29.14040, -99.14280, 4, 1, NULL, NULL),
(1798, '76841', 'Fort Mc Kavett', 'Texas', 'TX', 'Menard', '327', 30.82900, -100.08090, 1, 1, NULL, NULL),
(1799, '76848', 'Hext', 'Texas', 'TX', 'Menard', '327', 30.88160, -99.55400, 4, 1, NULL, NULL),
(1800, '76859', 'Menard', 'Texas', 'TX', 'Menard', '327', 30.91190, -99.78470, 4, 1, NULL, NULL),
(1801, '79701', 'Midland', 'Texas', 'TX', 'Midland', '329', 31.98960, -102.06260, 4, 1, NULL, NULL),
(1802, '79702', 'Midland', 'Texas', 'TX', 'Midland', '329', 31.96370, -102.08010, 4, 1, NULL, NULL),
(1803, '79703', 'Midland', 'Texas', 'TX', 'Midland', '329', 31.97210, -102.13690, 4, 1, NULL, NULL),
(1804, '79704', 'Midland', 'Texas', 'TX', 'Midland', '329', 31.86930, -102.03170, 4, 1, NULL, NULL),
(1805, '79705', 'Midland', 'Texas', 'TX', 'Midland', '329', 32.02950, -102.09150, 4, 1, NULL, NULL),
(1806, '79706', 'Midland', 'Texas', 'TX', 'Midland', '329', 31.88160, -102.01340, 4, 1, NULL, NULL),
(1807, '79707', 'Midland', 'Texas', 'TX', 'Midland', '329', 32.01990, -102.14760, 4, 1, NULL, NULL),
(1808, '79708', 'Midland', 'Texas', 'TX', 'Midland', '329', 31.86930, -102.03170, 4, 1, NULL, NULL),
(1809, '79710', 'Midland', 'Texas', 'TX', 'Midland', '329', 31.86930, -102.03170, 4, 1, NULL, NULL),
(1810, '79711', 'Midland', 'Texas', 'TX', 'Midland', '329', 31.99730, -102.07790, 4, 1, NULL, NULL),
(1811, '79712', 'Midland', 'Texas', 'TX', 'Midland', '329', 31.86930, -102.03170, 4, 1, NULL, NULL),
(1812, '76518', 'Buckholts', 'Texas', 'TX', 'Milam', '331', 30.88580, -97.12410, 4, 1, NULL, NULL),
(1813, '76519', 'Burlington', 'Texas', 'TX', 'Milam', '331', 30.97360, -96.96420, 4, 1, NULL, NULL),
(1814, '76520', 'Cameron', 'Texas', 'TX', 'Milam', '331', 30.85270, -96.97660, 4, 1, NULL, NULL),
(1815, '76523', 'Davilla', 'Texas', 'TX', 'Milam', '331', 30.78240, -97.29800, 4, 1, NULL, NULL),
(1816, '76556', 'Milano', 'Texas', 'TX', 'Milam', '331', 30.73040, -96.88070, 4, 1, NULL, NULL),
(1817, '76567', 'Rockdale', 'Texas', 'TX', 'Milam', '331', 30.65830, -97.00790, 4, 1, NULL, NULL),
(1818, '76577', 'Thorndale', 'Texas', 'TX', 'Milam', '331', 30.60820, -97.17640, 4, 1, NULL, NULL),
(1819, '77857', 'Gause', 'Texas', 'TX', 'Milam', '331', 30.78330, -96.72370, 4, 1, NULL, NULL),
(1820, '76844', 'Goldthwaite', 'Texas', 'TX', 'Mills', '333', 31.44580, -98.57440, 4, 1, NULL, NULL),
(1821, '76864', 'Mullin', 'Texas', 'TX', 'Mills', '333', 31.57480, -98.66350, 4, 1, NULL, NULL),
(1822, '76870', 'Priddy', 'Texas', 'TX', 'Mills', '333', 31.64100, -98.52760, 4, 1, NULL, NULL),
(1823, '76880', 'Star', 'Texas', 'TX', 'Mills', '333', 31.46850, -98.31610, 4, 1, NULL, NULL),
(1824, '79512', 'Colorado City', 'Texas', 'TX', 'Mitchell', '335', 32.39870, -100.86090, 4, 1, NULL, NULL),
(1825, '79532', 'Loraine', 'Texas', 'TX', 'Mitchell', '335', 32.41120, -100.71230, 4, 1, NULL, NULL),
(1826, '79565', 'Westbrook', 'Texas', 'TX', 'Mitchell', '335', 32.36270, -101.04300, 4, 1, NULL, NULL),
(1827, '76230', 'Bowie', 'Texas', 'TX', 'Montague', '337', 33.55680, -97.83730, 4, 1, NULL, NULL),
(1828, '76239', 'Forestburg', 'Texas', 'TX', 'Montague', '337', 33.53980, -97.58480, 4, 1, NULL, NULL),
(1829, '76251', 'Montague', 'Texas', 'TX', 'Montague', '337', 33.66390, -97.72820, 4, 1, NULL, NULL),
(1830, '76255', 'Nocona', 'Texas', 'TX', 'Montague', '337', 33.79820, -97.72700, 4, 1, NULL, NULL),
(1831, '76261', 'Ringgold', 'Texas', 'TX', 'Montague', '337', 33.81640, -97.94400, 4, 1, NULL, NULL),
(1832, '76265', 'Saint Jo', 'Texas', 'TX', 'Montague', '337', 33.74400, -97.55680, 4, 1, NULL, NULL),
(1833, '76270', 'Sunset', 'Texas', 'TX', 'Montague', '337', 33.45390, -97.77090, 4, 1, NULL, NULL),
(1834, '77301', 'Conroe', 'Texas', 'TX', 'Montgomery', '339', 30.31250, -95.45270, 4, 1, NULL, NULL),
(1835, '77302', 'Conroe', 'Texas', 'TX', 'Montgomery', '339', 30.22380, -95.35770, 4, 1, NULL, NULL),
(1836, '77303', 'Conroe', 'Texas', 'TX', 'Montgomery', '339', 30.38140, -95.37490, 4, 1, NULL, NULL),
(1837, '77304', 'Conroe', 'Texas', 'TX', 'Montgomery', '339', 30.32170, -95.52850, 4, 1, NULL, NULL),
(1838, '77305', 'Conroe', 'Texas', 'TX', 'Montgomery', '339', 30.29060, -95.38320, 4, 1, NULL, NULL),
(1839, '77306', 'Conroe', 'Texas', 'TX', 'Montgomery', '339', 30.22770, -95.28510, 4, 1, NULL, NULL),
(1840, '77316', 'Montgomery', 'Texas', 'TX', 'Montgomery', '339', 30.35870, -95.68570, 4, 1, NULL, NULL),
(1841, '77318', 'Willis', 'Texas', 'TX', 'Montgomery', '339', 30.44160, -95.53940, 4, 1, NULL, NULL),
(1842, '77333', 'Dobbin', 'Texas', 'TX', 'Montgomery', '339', 30.33650, -95.77230, 4, 1, NULL, NULL),
(1843, '77353', 'Magnolia', 'Texas', 'TX', 'Montgomery', '339', 30.20940, -95.75080, 4, 1, NULL, NULL),
(1844, '77354', 'Magnolia', 'Texas', 'TX', 'Montgomery', '339', 30.23330, -95.55020, 4, 1, NULL, NULL),
(1845, '77355', 'Magnolia', 'Texas', 'TX', 'Montgomery', '339', 30.15980, -95.74020, 4, 1, NULL, NULL),
(1846, '77356', 'Montgomery', 'Texas', 'TX', 'Montgomery', '339', 30.44110, -95.70970, 4, 1, NULL, NULL),
(1847, '77357', 'New Caney', 'Texas', 'TX', 'Montgomery', '339', 30.15790, -95.19800, 4, 1, NULL, NULL),
(1848, '77362', 'Pinehurst', 'Texas', 'TX', 'Montgomery', '339', 30.15810, -95.68140, 4, 1, NULL, NULL),
(1849, '77365', 'Porter', 'Texas', 'TX', 'Montgomery', '339', 30.12370, -95.26860, 4, 1, NULL, NULL),
(1850, '77372', 'Splendora', 'Texas', 'TX', 'Montgomery', '339', 30.23260, -95.19930, 4, 1, NULL, NULL),
(1851, '77378', 'Willis', 'Texas', 'TX', 'Montgomery', '339', 30.44410, -95.45060, 4, 1, NULL, NULL),
(1852, '77380', 'Spring', 'Texas', 'TX', 'Montgomery', '339', 30.14410, -95.47030, 4, 1, NULL, NULL),
(1853, '77381', 'Spring', 'Texas', 'TX', 'Montgomery', '339', 30.17160, -95.49850, 4, 1, NULL, NULL),
(1854, '77384', 'Conroe', 'Texas', 'TX', 'Montgomery', '339', 30.22570, -95.49240, 4, 1, NULL, NULL),
(1855, '77385', 'Conroe', 'Texas', 'TX', 'Montgomery', '339', 30.18770, -95.42880, 4, 1, NULL, NULL),
(1856, '77386', 'Spring', 'Texas', 'TX', 'Montgomery', '339', 30.12880, -95.42390, 4, 1, NULL, NULL),
(1857, '77387', 'Spring', 'Texas', 'TX', 'Montgomery', '339', 30.11990, -95.42470, 4, 1, NULL, NULL),
(1858, '77393', 'Spring', 'Texas', 'TX', 'Montgomery', '339', 30.32900, -95.46350, 4, 1, NULL, NULL),
(1859, '79013', 'Cactus', 'Texas', 'TX', 'Moore', '341', 36.04120, -102.00070, 4, 1, NULL, NULL),
(1860, '79029', 'Dumas', 'Texas', 'TX', 'Moore', '341', 35.88230, -101.96800, 4, 1, NULL, NULL),
(1861, '79058', 'Masterson', 'Texas', 'TX', 'Moore', '341', 35.63590, -101.96020, 4, 1, NULL, NULL),
(1862, '79086', 'Sunray', 'Texas', 'TX', 'Moore', '341', 36.00970, -101.81230, 4, 1, NULL, NULL),
(1863, '75568', 'Naples', 'Texas', 'TX', 'Morris', '343', 33.19120, -94.68910, 4, 1, NULL, NULL),
(1864, '75571', 'Omaha', 'Texas', 'TX', 'Morris', '343', 33.18080, -94.76390, 4, 1, NULL, NULL),
(1865, '75636', 'Cason', 'Texas', 'TX', 'Morris', '343', 33.02400, -94.82490, 4, 1, NULL, NULL),
(1866, '75638', 'Daingerfield', 'Texas', 'TX', 'Morris', '343', 33.03130, -94.73590, 4, 1, NULL, NULL),
(1867, '75668', 'Lone Star', 'Texas', 'TX', 'Morris', '343', 32.92520, -94.70490, 4, 1, NULL, NULL),
(1868, '79234', 'Flomot', 'Texas', 'TX', 'Motley', '345', 34.23210, -101.00380, 4, 1, NULL, NULL),
(1869, '79244', 'Matador', 'Texas', 'TX', 'Motley', '345', 34.05250, -100.83610, 4, 1, NULL, NULL),
(1870, '79256', 'Roaring Springs', 'Texas', 'TX', 'Motley', '345', 33.89770, -100.85280, 4, 1, NULL, NULL),
(1871, '75760', 'Cushing', 'Texas', 'TX', 'Nacogdoches', '347', 31.79780, -94.85390, 4, 1, NULL, NULL),
(1872, '75788', 'Sacul', 'Texas', 'TX', 'Nacogdoches', '347', 31.82540, -94.91890, 4, 1, NULL, NULL),
(1873, '75937', 'Chireno', 'Texas', 'TX', 'Nacogdoches', '347', 31.51190, -94.43020, 4, 1, NULL, NULL),
(1874, '75943', 'Douglass', 'Texas', 'TX', 'Nacogdoches', '347', 31.65780, -94.86960, 4, 1, NULL, NULL),
(1875, '75944', 'Etoile', 'Texas', 'TX', 'Nacogdoches', '347', 31.36010, -94.40640, 4, 1, NULL, NULL),
(1876, '75946', 'Garrison', 'Texas', 'TX', 'Nacogdoches', '347', 31.81110, -94.52660, 4, 1, NULL, NULL),
(1877, '75958', 'Martinsville', 'Texas', 'TX', 'Nacogdoches', '347', 31.64270, -94.41410, 4, 1, NULL, NULL),
(1878, '75961', 'Nacogdoches', 'Texas', 'TX', 'Nacogdoches', '347', 31.56110, -94.50000, 4, 1, NULL, NULL),
(1879, '75962', 'Nacogdoches', 'Texas', 'TX', 'Nacogdoches', '347', 31.69950, -94.60740, 4, 1, NULL, NULL),
(1880, '75963', 'Nacogdoches', 'Texas', 'TX', 'Nacogdoches', '347', 31.60460, -94.66410, 4, 1, NULL, NULL),
(1881, '75964', 'Nacogdoches', 'Texas', 'TX', 'Nacogdoches', '347', 31.67370, -94.69320, 4, 1, NULL, NULL),
(1882, '75965', 'Nacogdoches', 'Texas', 'TX', 'Nacogdoches', '347', 31.71950, -94.61680, 4, 1, NULL, NULL),
(1883, '75978', 'Woden', 'Texas', 'TX', 'Nacogdoches', '347', 31.50370, -94.52500, 4, 1, NULL, NULL),
(1884, '75102', 'Barry', 'Texas', 'TX', 'Navarro', '349', 32.10140, -96.62510, 4, 1, NULL, NULL),
(1885, '75105', 'Chatfield', 'Texas', 'TX', 'Navarro', '349', 32.29540, -96.38870, 4, 1, NULL, NULL),
(1886, '75109', 'Corsicana', 'Texas', 'TX', 'Navarro', '349', 31.99370, -96.41990, 4, 1, NULL, NULL),
(1887, '75110', 'Corsicana', 'Texas', 'TX', 'Navarro', '349', 32.08680, -96.47620, 4, 1, NULL, NULL),
(1888, '75144', 'Kerens', 'Texas', 'TX', 'Navarro', '349', 32.12750, -96.22980, 4, 1, NULL, NULL),
(1889, '75151', 'Corsicana', 'Texas', 'TX', 'Navarro', '349', 32.06240, -96.47350, 4, 1, NULL, NULL),
(1890, '75153', 'Powell', 'Texas', 'TX', 'Navarro', '349', 32.11960, -96.33270, 4, 1, NULL, NULL),
(1891, '76626', 'Blooming Grove', 'Texas', 'TX', 'Navarro', '349', 32.07580, -96.70100, 4, 1, NULL, NULL),
(1892, '76639', 'Dawson', 'Texas', 'TX', 'Navarro', '349', 31.89740, -96.70850, 4, 1, NULL, NULL),
(1893, '76641', 'Frost', 'Texas', 'TX', 'Navarro', '349', 32.02750, -96.76840, 4, 1, NULL, NULL),
(1894, '76679', 'Purdon', 'Texas', 'TX', 'Navarro', '349', 31.94830, -96.58560, 4, 1, NULL, NULL),
(1895, '76681', 'Richland', 'Texas', 'TX', 'Navarro', '349', 31.90180, -96.43730, 4, 1, NULL, NULL),
(1896, '75928', 'Bon Wier', 'Texas', 'TX', 'Newton', '351', 30.68760, -93.76650, 4, 1, NULL, NULL),
(1897, '75932', 'Burkeville', 'Texas', 'TX', 'Newton', '351', 31.00990, -93.65850, 4, 1, NULL, NULL),
(1898, '75933', 'Call', 'Texas', 'TX', 'Newton', '351', 30.57410, -93.83340, 4, 1, NULL, NULL),
(1899, '75966', 'Newton', 'Texas', 'TX', 'Newton', '351', 30.83510, -93.74970, 4, 1, NULL, NULL),
(1900, '75977', 'Wiergate', 'Texas', 'TX', 'Newton', '351', 31.04140, -93.80390, 4, 1, NULL, NULL),
(1901, '77614', 'Deweyville', 'Texas', 'TX', 'Newton', '351', 30.28940, -93.74880, 4, 1, NULL, NULL),
(1902, '79506', 'Blackwell', 'Texas', 'TX', 'Nolan', '353', 32.08200, -100.31110, 4, 1, NULL, NULL),
(1903, '79535', 'Maryneal', 'Texas', 'TX', 'Nolan', '353', 32.20260, -100.49720, 4, 1, NULL, NULL),
(1904, '79537', 'Nolan', 'Texas', 'TX', 'Nolan', '353', 32.29190, -100.21070, 4, 1, NULL, NULL),
(1905, '79545', 'Roscoe', 'Texas', 'TX', 'Nolan', '353', 32.42760, -100.53910, 4, 1, NULL, NULL),
(1906, '79556', 'Sweetwater', 'Texas', 'TX', 'Nolan', '353', 32.47260, -100.39790, 4, 1, NULL, NULL),
(1907, '78330', 'Agua Dulce', 'Texas', 'TX', 'Nueces', '355', 27.78170, -97.90860, 4, 1, NULL, NULL),
(1908, '78339', 'Banquete', 'Texas', 'TX', 'Nueces', '355', 27.80510, -97.79220, 4, 1, NULL, NULL),
(1909, '78343', 'Bishop', 'Texas', 'TX', 'Nueces', '355', 27.58860, -97.78300, 4, 1, NULL, NULL),
(1910, '78347', 'Chapman Ranch', 'Texas', 'TX', 'Nueces', '355', 27.59880, -97.46200, 4, 1, NULL, NULL),
(1911, '78351', 'Driscoll', 'Texas', 'TX', 'Nueces', '355', 27.67450, -97.74860, 4, 1, NULL, NULL),
(1912, '78373', 'Port Aransas', 'Texas', 'TX', 'Nueces', '355', 27.77070, -97.10510, 4, 1, NULL, NULL),
(1913, '78380', 'Robstown', 'Texas', 'TX', 'Nueces', '355', 27.79840, -97.69950, 4, 1, NULL, NULL),
(1914, '78401', 'Corpus Christi', 'Texas', 'TX', 'Nueces', '355', 27.79410, -97.40300, 4, 1, NULL, NULL),
(1915, '78402', 'Corpus Christi', 'Texas', 'TX', 'Nueces', '355', 27.82620, -97.38570, 4, 1, NULL, NULL),
(1916, '78403', 'Corpus Christi', 'Texas', 'TX', 'Nueces', '355', 27.77700, -97.46320, 4, 1, NULL, NULL),
(1917, '78404', 'Corpus Christi', 'Texas', 'TX', 'Nueces', '355', 27.76830, -97.40130, 4, 1, NULL, NULL),
(1918, '78405', 'Corpus Christi', 'Texas', 'TX', 'Nueces', '355', 27.77620, -97.42710, 4, 1, NULL, NULL),
(1919, '78406', 'Corpus Christi', 'Texas', 'TX', 'Nueces', '355', 27.76840, -97.51440, 4, 1, NULL, NULL),
(1920, '78407', 'Corpus Christi', 'Texas', 'TX', 'Nueces', '355', 27.80420, -97.43560, 4, 1, NULL, NULL),
(1921, '78408', 'Corpus Christi', 'Texas', 'TX', 'Nueces', '355', 27.79450, -97.43810, 4, 1, NULL, NULL),
(1922, '78409', 'Corpus Christi', 'Texas', 'TX', 'Nueces', '355', 27.81460, -97.52700, 4, 1, NULL, NULL),
(1923, '78410', 'Corpus Christi', 'Texas', 'TX', 'Nueces', '355', 27.84580, -97.59600, 4, 1, NULL, NULL),
(1924, '78411', 'Corpus Christi', 'Texas', 'TX', 'Nueces', '355', 27.73110, -97.38770, 4, 1, NULL, NULL),
(1925, '78412', 'Corpus Christi', 'Texas', 'TX', 'Nueces', '355', 27.70610, -97.35370, 4, 1, NULL, NULL),
(1926, '78413', 'Corpus Christi', 'Texas', 'TX', 'Nueces', '355', 27.69100, -97.39830, 4, 1, NULL, NULL),
(1927, '78414', 'Corpus Christi', 'Texas', 'TX', 'Nueces', '355', 27.67700, -97.36500, 4, 1, NULL, NULL),
(1928, '78415', 'Corpus Christi', 'Texas', 'TX', 'Nueces', '355', 27.72620, -97.40780, 4, 1, NULL, NULL),
(1929, '78416', 'Corpus Christi', 'Texas', 'TX', 'Nueces', '355', 27.75360, -97.43470, 4, 1, NULL, NULL),
(1930, '78417', 'Corpus Christi', 'Texas', 'TX', 'Nueces', '355', 27.72900, -97.44940, 4, 1, NULL, NULL),
(1931, '78418', 'Corpus Christi', 'Texas', 'TX', 'Nueces', '355', 27.63490, -97.31030, 4, 1, NULL, NULL),
(1932, '78419', 'Corpus Christi', 'Texas', 'TX', 'Nueces', '355', 27.69520, -97.26940, 4, 1, NULL, NULL),
(1933, '78426', 'Corpus Christi', 'Texas', 'TX', 'Nueces', '355', 27.77700, -97.46320, 4, 1, NULL, NULL),
(1934, '78427', 'Corpus Christi', 'Texas', 'TX', 'Nueces', '355', 27.80060, -97.39640, 4, 1, NULL, NULL),
(1935, '78460', 'Corpus Christi', 'Texas', 'TX', 'Nueces', '355', 27.80060, -97.39640, 4, 1, NULL, NULL),
(1936, '78463', 'Corpus Christi', 'Texas', 'TX', 'Nueces', '355', 27.77700, -97.46320, 4, 1, NULL, NULL),
(1937, '78465', 'Corpus Christi', 'Texas', 'TX', 'Nueces', '355', 27.77700, -97.46320, 4, 1, NULL, NULL),
(1938, '78466', 'Corpus Christi', 'Texas', 'TX', 'Nueces', '355', 27.77700, -97.46320, 4, 1, NULL, NULL),
(1939, '78467', 'Corpus Christi', 'Texas', 'TX', 'Nueces', '355', 27.77700, -97.46320, 4, 1, NULL, NULL),
(1940, '78468', 'Corpus Christi', 'Texas', 'TX', 'Nueces', '355', 27.77700, -97.46320, 4, 1, NULL, NULL),
(1941, '78469', 'Corpus Christi', 'Texas', 'TX', 'Nueces', '355', 27.77700, -97.46320, 4, 1, NULL, NULL),
(1942, '78472', 'Corpus Christi', 'Texas', 'TX', 'Nueces', '355', 27.74020, -97.57920, 4, 1, NULL, NULL),
(1943, '78480', 'Corpus Christi', 'Texas', 'TX', 'Nueces', '355', 27.77700, -97.46320, 4, 1, NULL, NULL),
(1944, '79033', 'Farnsworth', 'Texas', 'TX', 'Ochiltree', '357', 36.29630, -100.98430, 4, 1, NULL, NULL),
(1945, '79070', 'Perryton', 'Texas', 'TX', 'Ochiltree', '357', 36.37480, -100.81560, 4, 1, NULL, NULL),
(1946, '79093', 'Waka', 'Texas', 'TX', 'Ochiltree', '357', 36.27160, -101.04430, 4, 1, NULL, NULL),
(1947, '79001', 'Adrian', 'Texas', 'TX', 'Oldham', '359', 35.27480, -102.66520, 4, 1, NULL, NULL),
(1948, '79010', 'Boys Ranch', 'Texas', 'TX', 'Oldham', '359', 35.53140, -102.25410, 4, 1, NULL, NULL),
(1949, '79092', 'Vega', 'Texas', 'TX', 'Oldham', '359', 35.22410, -102.42140, 4, 1, NULL, NULL),
(1950, '79098', 'Wildorado', 'Texas', 'TX', 'Oldham', '359', 35.19160, -102.21180, 4, 1, NULL, NULL),
(1951, '77611', 'Bridge City', 'Texas', 'TX', 'Orange', '361', 30.01920, -93.83260, 4, 1, NULL, NULL),
(1952, '77626', 'Mauriceville', 'Texas', 'TX', 'Orange', '361', 30.20400, -93.88660, 4, 1, NULL, NULL),
(1953, '77630', 'Orange', 'Texas', 'TX', 'Orange', '361', 30.07090, -93.86590, 4, 1, NULL, NULL),
(1954, '77631', 'Orange', 'Texas', 'TX', 'Orange', '361', 30.09300, -93.73660, 4, 1, NULL, NULL),
(1955, '77632', 'Orange', 'Texas', 'TX', 'Orange', '361', 30.17750, -93.84090, 4, 1, NULL, NULL),
(1956, '77639', 'Orangefield', 'Texas', 'TX', 'Orange', '361', 30.06310, -93.85990, 4, 1, NULL, NULL),
(1957, '77662', 'Vidor', 'Texas', 'TX', 'Orange', '361', 30.15020, -94.00080, 4, 1, NULL, NULL),
(1958, '77670', 'Vidor', 'Texas', 'TX', 'Orange', '361', 30.13160, -94.01550, 4, 1, NULL, NULL),
(1959, '76067', 'Mineral Wells', 'Texas', 'TX', 'Palo Pinto', '363', 32.80850, -98.11280, 4, 1, NULL, NULL),
(1960, '76068', 'Mineral Wells', 'Texas', 'TX', 'Palo Pinto', '363', 32.80850, -98.11280, 4, 1, NULL, NULL),
(1961, '76449', 'Graford', 'Texas', 'TX', 'Palo Pinto', '363', 32.92420, -98.33700, 4, 1, NULL, NULL),
(1962, '76453', 'Gordon', 'Texas', 'TX', 'Palo Pinto', '363', 32.54780, -98.36320, 4, 1, NULL, NULL),
(1963, '76463', 'Mingus', 'Texas', 'TX', 'Palo Pinto', '363', 32.53810, -98.42190, 4, 1, NULL, NULL),
(1964, '76472', 'Santo', 'Texas', 'TX', 'Palo Pinto', '363', 32.59790, -98.17970, 4, 1, NULL, NULL),
(1965, '76475', 'Strawn', 'Texas', 'TX', 'Palo Pinto', '363', 32.59450, -98.49950, 4, 1, NULL, NULL),
(1966, '76484', 'Palo Pinto', 'Texas', 'TX', 'Palo Pinto', '363', 32.76730, -98.29870, 4, 1, NULL, NULL),
(1967, '75631', 'Beckville', 'Texas', 'TX', 'Panola', '365', 32.24520, -94.45550, 4, 1, NULL, NULL),
(1968, '75633', 'Carthage', 'Texas', 'TX', 'Panola', '365', 32.15440, -94.35270, 4, 1, NULL, NULL),
(1969, '75637', 'Clayton', 'Texas', 'TX', 'Panola', '365', 32.10270, -94.49350, 4, 1, NULL, NULL),
(1970, '75639', 'De Berry', 'Texas', 'TX', 'Panola', '365', 32.25430, -94.13560, 4, 1, NULL, NULL),
(1971, '75643', 'Gary', 'Texas', 'TX', 'Panola', '365', 32.03340, -94.38000, 4, 1, NULL, NULL),
(1972, '75669', 'Long Branch', 'Texas', 'TX', 'Panola', '365', 32.04490, -94.53810, 4, 1, NULL, NULL),
(1973, '75685', 'Panola', 'Texas', 'TX', 'Panola', '365', 32.35790, -94.09130, 4, 1, NULL, NULL),
(1974, '76008', 'Aledo', 'Texas', 'TX', 'Parker', '367', 32.70040, -97.60390, 4, 1, NULL, NULL),
(1975, '76066', 'Millsap', 'Texas', 'TX', 'Parker', '367', 32.74570, -97.95610, 4, 1, NULL, NULL),
(1976, '76082', 'Springtown', 'Texas', 'TX', 'Parker', '367', 32.96600, -97.68360, 4, 1, NULL, NULL),
(1977, '76085', 'Weatherford', 'Texas', 'TX', 'Parker', '367', 32.76760, -97.75160, 4, 1, NULL, NULL),
(1978, '76086', 'Weatherford', 'Texas', 'TX', 'Parker', '367', 32.75490, -97.78990, 4, 1, NULL, NULL),
(1979, '76087', 'Weatherford', 'Texas', 'TX', 'Parker', '367', 32.74950, -97.68940, 4, 1, NULL, NULL),
(1980, '76088', 'Weatherford', 'Texas', 'TX', 'Parker', '367', 32.84780, -97.86060, 4, 1, NULL, NULL),
(1981, '76098', 'Azle', 'Texas', 'TX', 'Parker', '367', 32.89570, -97.56360, 4, 1, NULL, NULL),
(1982, '76439', 'Dennis', 'Texas', 'TX', 'Parker', '367', 32.61870, -97.92670, 4, 1, NULL, NULL),
(1983, '76485', 'Peaster', 'Texas', 'TX', 'Parker', '367', 32.87210, -97.86670, 4, 1, NULL, NULL),
(1984, '76487', 'Poolville', 'Texas', 'TX', 'Parker', '367', 32.96800, -97.84720, 4, 1, NULL, NULL),
(1985, '76490', 'Whitt', 'Texas', 'TX', 'Parker', '367', 32.95550, -98.02100, 4, 1, NULL, NULL),
(1986, '79009', 'Bovina', 'Texas', 'TX', 'Parmer', '369', 34.51370, -102.88300, 4, 1, NULL, NULL),
(1987, '79035', 'Friona', 'Texas', 'TX', 'Parmer', '369', 34.64170, -102.72410, 4, 1, NULL, NULL),
(1988, '79053', 'Lazbuddie', 'Texas', 'TX', 'Parmer', '369', 34.38470, -102.58700, 4, 1, NULL, NULL),
(1989, '79325', 'Farwell', 'Texas', 'TX', 'Parmer', '369', 34.38600, -102.99010, 4, 1, NULL, NULL),
(1990, '79730', 'Coyanosa', 'Texas', 'TX', 'Pecos', '371', 31.22950, -103.05380, 4, 1, NULL, NULL),
(1991, '79735', 'Fort Stockton', 'Texas', 'TX', 'Pecos', '371', 30.89080, -102.87990, 4, 1, NULL, NULL),
(1992, '79740', 'Girvin', 'Texas', 'TX', 'Pecos', '371', 31.06290, -102.38850, 4, 1, NULL, NULL),
(1993, '79743', 'Imperial', 'Texas', 'TX', 'Pecos', '371', 31.27290, -102.69260, 4, 1, NULL, NULL),
(1994, '79744', 'Iraan', 'Texas', 'TX', 'Pecos', '371', 30.91560, -101.91520, 4, 1, NULL, NULL),
(1995, '79781', 'Sheffield', 'Texas', 'TX', 'Pecos', '371', 30.69350, -101.86000, 4, 1, NULL, NULL),
(1996, '75934', 'Camden', 'Texas', 'TX', 'Polk', '373', 30.90000, -94.75470, 4, 1, NULL, NULL),
(1997, '75939', 'Corrigan', 'Texas', 'TX', 'Polk', '373', 31.04080, -94.81240, 4, 1, NULL, NULL),
(1998, '75960', 'Moscow', 'Texas', 'TX', 'Polk', '373', 30.91790, -94.85440, 4, 1, NULL, NULL),
(1999, '77326', 'Ace', 'Texas', 'TX', 'Polk', '373', 30.52090, -94.82210, 4, 1, NULL, NULL),
(2000, '77332', 'Dallardsville', 'Texas', 'TX', 'Polk', '373', 30.62850, -94.63190, 4, 1, NULL, NULL),
(2001, '77335', 'Goodrich', 'Texas', 'TX', 'Polk', '373', 30.60790, -94.95920, 4, 1, NULL, NULL),
(2002, '77350', 'Leggett', 'Texas', 'TX', 'Polk', '373', 30.85680, -94.85610, 4, 1, NULL, NULL),
(2003, '77351', 'Livingston', 'Texas', 'TX', 'Polk', '373', 30.68290, -94.89760, 4, 1, NULL, NULL),
(2004, '77360', 'Onalaska', 'Texas', 'TX', 'Polk', '373', 30.82250, -95.10560, 4, 1, NULL, NULL),
(2005, '77399', 'Livingston', 'Texas', 'TX', 'Polk', '373', 30.81790, -94.86910, 4, 1, NULL, NULL),
(2006, '79012', 'Bushland', 'Texas', 'TX', 'Potter', '375', 35.26640, -102.09780, 4, 1, NULL, NULL),
(2007, '79101', 'Amarillo', 'Texas', 'TX', 'Potter', '375', 35.20320, -101.84210, 4, 1, NULL, NULL),
(2008, '79102', 'Amarillo', 'Texas', 'TX', 'Potter', '375', 35.19990, -101.84960, 4, 1, NULL, NULL),
(2009, '79103', 'Amarillo', 'Texas', 'TX', 'Potter', '375', 35.17510, -101.79760, 4, 1, NULL, NULL),
(2010, '79104', 'Amarillo', 'Texas', 'TX', 'Potter', '375', 35.19390, -101.79750, 4, 1, NULL, NULL),
(2011, '79105', 'Amarillo', 'Texas', 'TX', 'Potter', '375', 35.40150, -101.89510, 4, 1, NULL, NULL),
(2012, '79106', 'Amarillo', 'Texas', 'TX', 'Potter', '375', 35.19770, -101.89490, 4, 1, NULL, NULL),
(2013, '79107', 'Amarillo', 'Texas', 'TX', 'Potter', '375', 35.23090, -101.80600, 4, 1, NULL, NULL),
(2014, '79108', 'Amarillo', 'Texas', 'TX', 'Potter', '375', 35.27790, -101.83000, 4, 1, NULL, NULL),
(2015, '79111', 'Amarillo', 'Texas', 'TX', 'Potter', '375', 35.22860, -101.67030, 4, 1, NULL, NULL),
(2016, '79116', 'Amarillo', 'Texas', 'TX', 'Potter', '375', 35.24540, -101.99900, 4, 1, NULL, NULL),
(2017, '79117', 'Amarillo', 'Texas', 'TX', 'Potter', '375', 35.30890, -101.84300, 4, 1, NULL, NULL),
(2018, '79120', 'Amarillo', 'Texas', 'TX', 'Potter', '375', 35.19640, -101.80340, 4, 1, NULL, NULL),
(2019, '79124', 'Amarillo', 'Texas', 'TX', 'Potter', '375', 35.27030, -101.94300, 4, 1, NULL, NULL),
(2020, '79159', 'Amarillo', 'Texas', 'TX', 'Potter', '375', 35.22200, -101.83130, 4, 1, NULL, NULL);
INSERT INTO `zip_codes` (`id`, `zipcode`, `city`, `state`, `state_abbr`, `county_area`, `code`, `latitude`, `longitude`, `some_field`, `status`, `created_at`, `updated_at`) VALUES
(2021, '79166', 'Amarillo', 'Texas', 'TX', 'Potter', '375', 35.40150, -101.89510, 4, 1, NULL, NULL),
(2022, '79168', 'Amarillo', 'Texas', 'TX', 'Potter', '375', 35.40150, -101.89510, 4, 1, NULL, NULL),
(2023, '79172', 'Amarillo', 'Texas', 'TX', 'Potter', '375', 35.40150, -101.89510, 4, 1, NULL, NULL),
(2024, '79174', 'Amarillo', 'Texas', 'TX', 'Potter', '375', 35.40150, -101.89510, 4, 1, NULL, NULL),
(2025, '79178', 'Amarillo', 'Texas', 'TX', 'Potter', '375', 35.40150, -101.89510, 4, 1, NULL, NULL),
(2026, '79185', 'Amarillo', 'Texas', 'TX', 'Potter', '375', 35.40150, -101.89510, 4, 1, NULL, NULL),
(2027, '79189', 'Amarillo', 'Texas', 'TX', 'Potter', '375', 35.40150, -101.89510, 4, 1, NULL, NULL),
(2028, '79843', 'Marfa', 'Texas', 'TX', 'Presidio', '377', 30.29300, -104.08480, 4, 1, NULL, NULL),
(2029, '79845', 'Presidio', 'Texas', 'TX', 'Presidio', '377', 29.55730, -104.35510, 4, 1, NULL, NULL),
(2030, '79846', 'Redford', 'Texas', 'TX', 'Presidio', '377', 29.47020, -104.00550, 4, 1, NULL, NULL),
(2031, '75410', 'Alba', 'Texas', 'TX', 'Rains', '379', 32.76520, -95.59710, 4, 1, NULL, NULL),
(2032, '75440', 'Emory', 'Texas', 'TX', 'Rains', '379', 32.87500, -95.74180, 4, 1, NULL, NULL),
(2033, '75472', 'Point', 'Texas', 'TX', 'Rains', '379', 32.90070, -95.89030, 4, 1, NULL, NULL),
(2034, '79015', 'Canyon', 'Texas', 'TX', 'Randall', '381', 34.97720, -101.92470, 4, 1, NULL, NULL),
(2035, '79016', 'Canyon', 'Texas', 'TX', 'Randall', '381', 34.96540, -101.89590, 4, 1, NULL, NULL),
(2036, '79091', 'Umbarger', 'Texas', 'TX', 'Randall', '381', 34.90100, -102.07880, 4, 1, NULL, NULL),
(2037, '79109', 'Amarillo', 'Texas', 'TX', 'Randall', '381', 35.16630, -101.88680, 4, 1, NULL, NULL),
(2038, '79110', 'Amarillo', 'Texas', 'TX', 'Randall', '381', 35.15450, -101.86410, 4, 1, NULL, NULL),
(2039, '79114', 'Amarillo', 'Texas', 'TX', 'Randall', '381', 35.05000, -101.81750, 4, 1, NULL, NULL),
(2040, '79118', 'Amarillo', 'Texas', 'TX', 'Randall', '381', 35.07630, -101.83490, 4, 1, NULL, NULL),
(2041, '79119', 'Amarillo', 'Texas', 'TX', 'Randall', '381', 35.06420, -101.97430, 4, 1, NULL, NULL),
(2042, '79121', 'Amarillo', 'Texas', 'TX', 'Randall', '381', 35.16970, -101.92660, 4, 1, NULL, NULL),
(2043, '76932', 'Big Lake', 'Texas', 'TX', 'Reagan', '383', 31.41700, -101.54230, 4, 1, NULL, NULL),
(2044, '78833', 'Camp Wood', 'Texas', 'TX', 'Real', '385', 29.67950, -100.00840, 4, 1, NULL, NULL),
(2045, '78873', 'Leakey', 'Texas', 'TX', 'Real', '385', 29.76900, -99.74790, 4, 1, NULL, NULL),
(2046, '78879', 'Rio Frio', 'Texas', 'TX', 'Real', '385', 29.64950, -99.71650, 4, 1, NULL, NULL),
(2047, '75412', 'Bagwell', 'Texas', 'TX', 'Red River', '387', 33.83610, -95.14870, 4, 1, NULL, NULL),
(2048, '75417', 'Bogata', 'Texas', 'TX', 'Red River', '387', 33.46990, -95.19370, 4, 1, NULL, NULL),
(2049, '75426', 'Clarksville', 'Texas', 'TX', 'Red River', '387', 33.62360, -95.04610, 4, 1, NULL, NULL),
(2050, '75436', 'Detroit', 'Texas', 'TX', 'Red River', '387', 33.66270, -95.23850, 4, 1, NULL, NULL),
(2051, '75550', 'Annona', 'Texas', 'TX', 'Red River', '387', 33.55350, -94.89920, 4, 1, NULL, NULL),
(2052, '75554', 'Avery', 'Texas', 'TX', 'Red River', '387', 33.53390, -94.78670, 4, 1, NULL, NULL),
(2053, '79718', 'Balmorhea', 'Texas', 'TX', 'Reeves', '389', 30.98430, -103.74460, 4, 1, NULL, NULL),
(2054, '79770', 'Orla', 'Texas', 'TX', 'Reeves', '389', 31.86400, -103.92970, 4, 1, NULL, NULL),
(2055, '79772', 'Pecos', 'Texas', 'TX', 'Reeves', '389', 31.44670, -103.57910, 4, 1, NULL, NULL),
(2056, '79780', 'Saragosa', 'Texas', 'TX', 'Reeves', '389', 31.04280, -103.63640, 4, 1, NULL, NULL),
(2057, '79785', 'Toyah', 'Texas', 'TX', 'Reeves', '389', 31.28200, -103.80570, 4, 1, NULL, NULL),
(2058, '79786', 'Toyahvale', 'Texas', 'TX', 'Reeves', '389', 30.94430, -103.78930, 4, 1, NULL, NULL),
(2059, '77950', 'Austwell', 'Texas', 'TX', 'Refugio', '391', 28.40230, -96.85300, 4, 1, NULL, NULL),
(2060, '77990', 'Tivoli', 'Texas', 'TX', 'Refugio', '391', 28.45870, -96.89280, 4, 1, NULL, NULL),
(2061, '78340', 'Bayside', 'Texas', 'TX', 'Refugio', '391', 28.09680, -97.21060, 4, 1, NULL, NULL),
(2062, '78377', 'Refugio', 'Texas', 'TX', 'Refugio', '391', 28.31690, -97.27670, 4, 1, NULL, NULL),
(2063, '78393', 'Woodsboro', 'Texas', 'TX', 'Refugio', '391', 28.22320, -97.31920, 4, 1, NULL, NULL),
(2064, '79059', 'Miami', 'Texas', 'TX', 'Roberts', '393', 35.71930, -100.70270, 4, 1, NULL, NULL),
(2065, '76629', 'Bremond', 'Texas', 'TX', 'Robertson', '395', 31.15600, -96.66970, 4, 1, NULL, NULL),
(2066, '77837', 'Calvert', 'Texas', 'TX', 'Robertson', '395', 30.97820, -96.67110, 4, 1, NULL, NULL),
(2067, '77856', 'Franklin', 'Texas', 'TX', 'Robertson', '395', 31.00010, -96.51710, 4, 1, NULL, NULL),
(2068, '77859', 'Hearne', 'Texas', 'TX', 'Robertson', '395', 30.86690, -96.58430, 4, 1, NULL, NULL),
(2069, '77867', 'Mumford', 'Texas', 'TX', 'Robertson', '395', 30.73440, -96.56500, 4, 1, NULL, NULL),
(2070, '77870', 'New Baden', 'Texas', 'TX', 'Robertson', '395', 31.05100, -96.42910, 4, 1, NULL, NULL),
(2071, '77882', 'Wheelock', 'Texas', 'TX', 'Robertson', '395', 30.89770, -96.39000, 4, 1, NULL, NULL),
(2072, '75032', 'Rockwall', 'Texas', 'TX', 'Rockwall', '397', 32.88600, -96.40950, 4, 1, NULL, NULL),
(2073, '75087', 'Rockwall', 'Texas', 'TX', 'Rockwall', '397', 32.94920, -96.44180, 4, 1, NULL, NULL),
(2074, '75132', 'Fate', 'Texas', 'TX', 'Rockwall', '397', 32.94150, -96.38140, 4, 1, NULL, NULL),
(2075, '75189', 'Royse City', 'Texas', 'TX', 'Rockwall', '397', 32.96280, -96.36480, 4, 1, NULL, NULL),
(2076, '76821', 'Ballinger', 'Texas', 'TX', 'Runnels', '399', 31.74680, -99.95890, 4, 1, NULL, NULL),
(2077, '76861', 'Miles', 'Texas', 'TX', 'Runnels', '399', 31.61210, -100.18230, 4, 1, NULL, NULL),
(2078, '76865', 'Norton', 'Texas', 'TX', 'Runnels', '399', 31.87950, -100.13150, 4, 1, NULL, NULL),
(2079, '76875', 'Rowena', 'Texas', 'TX', 'Runnels', '399', 31.64360, -100.01910, 4, 1, NULL, NULL),
(2080, '79566', 'Wingate', 'Texas', 'TX', 'Runnels', '399', 32.03180, -100.11830, 4, 1, NULL, NULL),
(2081, '79567', 'Winters', 'Texas', 'TX', 'Runnels', '399', 31.96200, -99.95510, 4, 1, NULL, NULL),
(2082, '75652', 'Henderson', 'Texas', 'TX', 'Rusk', '401', 32.21310, -94.78340, 4, 1, NULL, NULL),
(2083, '75653', 'Henderson', 'Texas', 'TX', 'Rusk', '401', 32.15320, -94.79940, 4, 1, NULL, NULL),
(2084, '75654', 'Henderson', 'Texas', 'TX', 'Rusk', '401', 32.12600, -94.74880, 4, 1, NULL, NULL),
(2085, '75658', 'Joinerville', 'Texas', 'TX', 'Rusk', '401', 32.19590, -94.90650, 4, 1, NULL, NULL),
(2086, '75666', 'Laird Hill', 'Texas', 'TX', 'Rusk', '401', 32.35320, -94.90550, 4, 1, NULL, NULL),
(2087, '75667', 'Laneville', 'Texas', 'TX', 'Rusk', '401', 31.95080, -94.86600, 4, 1, NULL, NULL),
(2088, '75680', 'Minden', 'Texas', 'TX', 'Rusk', '401', 32.00280, -94.71290, 4, 1, NULL, NULL),
(2089, '75681', 'Mount Enterprise', 'Texas', 'TX', 'Rusk', '401', 31.91250, -94.62350, 4, 1, NULL, NULL),
(2090, '75682', 'New London', 'Texas', 'TX', 'Rusk', '401', 32.25420, -94.93220, 4, 1, NULL, NULL),
(2091, '75684', 'Overton', 'Texas', 'TX', 'Rusk', '401', 32.26900, -94.95290, 4, 1, NULL, NULL),
(2092, '75687', 'Price', 'Texas', 'TX', 'Rusk', '401', 32.15180, -94.95540, 4, 1, NULL, NULL),
(2093, '75689', 'Selman City', 'Texas', 'TX', 'Rusk', '401', 32.18260, -94.93550, 4, 1, NULL, NULL),
(2094, '75691', 'Tatum', 'Texas', 'TX', 'Rusk', '401', 32.32660, -94.59600, 4, 1, NULL, NULL),
(2095, '75930', 'Bronson', 'Texas', 'TX', 'Sabine', '403', 31.33910, -93.99930, 4, 1, NULL, NULL),
(2096, '75931', 'Brookeland', 'Texas', 'TX', 'Sabine', '403', 31.09220, -93.96840, 4, 1, NULL, NULL),
(2097, '75948', 'Hemphill', 'Texas', 'TX', 'Sabine', '403', 31.31610, -93.79050, 4, 1, NULL, NULL),
(2098, '75959', 'Milam', 'Texas', 'TX', 'Sabine', '403', 31.50330, -93.87780, 4, 1, NULL, NULL),
(2099, '75968', 'Pineland', 'Texas', 'TX', 'Sabine', '403', 31.24180, -93.97540, 4, 1, NULL, NULL),
(2100, '75929', 'Broaddus', 'Texas', 'TX', 'San Augustine', '405', 31.29520, -94.21560, 4, 1, NULL, NULL),
(2101, '75972', 'San Augustine', 'Texas', 'TX', 'San Augustine', '405', 31.51520, -94.13260, 4, 1, NULL, NULL),
(2102, '77331', 'Coldspring', 'Texas', 'TX', 'San Jacinto', '407', 30.60270, -95.10860, 4, 1, NULL, NULL),
(2103, '77359', 'Oakhurst', 'Texas', 'TX', 'San Jacinto', '407', 30.71260, -95.30950, 4, 1, NULL, NULL),
(2104, '77364', 'Pointblank', 'Texas', 'TX', 'San Jacinto', '407', 30.75930, -95.22950, 4, 1, NULL, NULL),
(2105, '77371', 'Shepherd', 'Texas', 'TX', 'San Jacinto', '407', 30.49800, -94.99660, 4, 1, NULL, NULL),
(2106, '78335', 'Aransas Pass', 'Texas', 'TX', 'San Patricio', '409', 27.91250, -97.18840, 4, 1, NULL, NULL),
(2107, '78336', 'Aransas Pass', 'Texas', 'TX', 'San Patricio', '409', 27.90950, -97.15910, 4, 1, NULL, NULL),
(2108, '78352', 'Edroy', 'Texas', 'TX', 'San Patricio', '409', 27.97360, -97.67610, 4, 1, NULL, NULL),
(2109, '78359', 'Gregory', 'Texas', 'TX', 'San Patricio', '409', 27.92220, -97.29000, 4, 1, NULL, NULL),
(2110, '78362', 'Ingleside', 'Texas', 'TX', 'San Patricio', '409', 27.86820, -97.20690, 4, 1, NULL, NULL),
(2111, '78368', 'Mathis', 'Texas', 'TX', 'San Patricio', '409', 28.08020, -97.80970, 4, 1, NULL, NULL),
(2112, '78370', 'Odem', 'Texas', 'TX', 'San Patricio', '409', 27.94030, -97.58380, 4, 1, NULL, NULL),
(2113, '78374', 'Portland', 'Texas', 'TX', 'San Patricio', '409', 27.89350, -97.31690, 4, 1, NULL, NULL),
(2114, '78387', 'Sinton', 'Texas', 'TX', 'San Patricio', '409', 28.03390, -97.51960, 4, 1, NULL, NULL),
(2115, '78390', 'Taft', 'Texas', 'TX', 'San Patricio', '409', 27.97650, -97.39660, 4, 1, NULL, NULL),
(2116, '76832', 'Cherokee', 'Texas', 'TX', 'San Saba', '411', 30.98060, -98.66330, 4, 1, NULL, NULL),
(2117, '76871', 'Richland Springs', 'Texas', 'TX', 'San Saba', '411', 31.30030, -98.91280, 4, 1, NULL, NULL),
(2118, '76877', 'San Saba', 'Texas', 'TX', 'San Saba', '411', 31.16270, -98.73090, 4, 1, NULL, NULL),
(2119, '76936', 'Eldorado', 'Texas', 'TX', 'Schleicher', '413', 30.86670, -100.58890, 4, 1, NULL, NULL),
(2120, '79516', 'Dunn', 'Texas', 'TX', 'Scurry', '415', 32.56710, -100.88540, 4, 1, NULL, NULL),
(2121, '79517', 'Fluvanna', 'Texas', 'TX', 'Scurry', '415', 32.85300, -101.10300, 4, 1, NULL, NULL),
(2122, '79526', 'Hermleigh', 'Texas', 'TX', 'Scurry', '415', 32.62940, -100.75470, 4, 1, NULL, NULL),
(2123, '79527', 'Ira', 'Texas', 'TX', 'Scurry', '415', 32.59230, -101.07150, 4, 1, NULL, NULL),
(2124, '79549', 'Snyder', 'Texas', 'TX', 'Scurry', '415', 32.74510, -100.91750, 4, 1, NULL, NULL),
(2125, '79550', 'Snyder', 'Texas', 'TX', 'Scurry', '415', 32.71790, -100.91760, 4, 1, NULL, NULL),
(2126, '76430', 'Albany', 'Texas', 'TX', 'Shackelford', '417', 32.71900, -99.31960, 4, 1, NULL, NULL),
(2127, '76464', 'Moran', 'Texas', 'TX', 'Shackelford', '417', 32.55490, -99.16560, 4, 1, NULL, NULL),
(2128, '75935', 'Center', 'Texas', 'TX', 'Shelby', '419', 31.78650, -94.18690, 4, 1, NULL, NULL),
(2129, '75954', 'Joaquin', 'Texas', 'TX', 'Shelby', '419', 31.94400, -94.06080, 4, 1, NULL, NULL),
(2130, '75973', 'Shelbyville', 'Texas', 'TX', 'Shelby', '419', 31.71310, -93.96980, 4, 1, NULL, NULL),
(2131, '75974', 'Tenaha', 'Texas', 'TX', 'Shelby', '419', 31.94080, -94.24880, 4, 1, NULL, NULL),
(2132, '75975', 'Timpson', 'Texas', 'TX', 'Shelby', '419', 31.88410, -94.39670, 4, 1, NULL, NULL),
(2133, '73960', 'Texhoma', 'Texas', 'TX', 'Sherman', '421', 36.50550, -101.78290, 4, 1, NULL, NULL),
(2134, '79084', 'Stratford', 'Texas', 'TX', 'Sherman', '421', 36.33350, -101.98860, 4, 1, NULL, NULL),
(2135, '75701', 'Tyler', 'Texas', 'TX', 'Smith', '423', 32.32540, -95.29220, 4, 1, NULL, NULL),
(2136, '75702', 'Tyler', 'Texas', 'TX', 'Smith', '423', 32.36200, -95.31170, 4, 1, NULL, NULL),
(2137, '75703', 'Tyler', 'Texas', 'TX', 'Smith', '423', 32.27680, -95.30310, 4, 1, NULL, NULL),
(2138, '75704', 'Tyler', 'Texas', 'TX', 'Smith', '423', 32.37380, -95.40700, 4, 1, NULL, NULL),
(2139, '75705', 'Tyler', 'Texas', 'TX', 'Smith', '423', 32.37660, -95.12520, 4, 1, NULL, NULL),
(2140, '75706', 'Tyler', 'Texas', 'TX', 'Smith', '423', 32.44410, -95.33100, 4, 1, NULL, NULL),
(2141, '75707', 'Tyler', 'Texas', 'TX', 'Smith', '423', 32.30380, -95.19270, 4, 1, NULL, NULL),
(2142, '75708', 'Tyler', 'Texas', 'TX', 'Smith', '423', 32.41900, -95.21060, 4, 1, NULL, NULL),
(2143, '75709', 'Tyler', 'Texas', 'TX', 'Smith', '423', 32.30780, -95.39560, 4, 1, NULL, NULL),
(2144, '75710', 'Tyler', 'Texas', 'TX', 'Smith', '423', 32.34750, -95.30650, 4, 1, NULL, NULL),
(2145, '75711', 'Tyler', 'Texas', 'TX', 'Smith', '423', 32.35130, -95.30110, 4, 1, NULL, NULL),
(2146, '75712', 'Tyler', 'Texas', 'TX', 'Smith', '423', 32.41120, -95.28990, 4, 1, NULL, NULL),
(2147, '75713', 'Tyler', 'Texas', 'TX', 'Smith', '423', 32.41120, -95.28990, 4, 1, NULL, NULL),
(2148, '75750', 'Arp', 'Texas', 'TX', 'Smith', '423', 32.24180, -95.06390, 4, 1, NULL, NULL),
(2149, '75757', 'Bullard', 'Texas', 'TX', 'Smith', '423', 32.10950, -95.33420, 4, 1, NULL, NULL),
(2150, '75762', 'Flint', 'Texas', 'TX', 'Smith', '423', 32.20790, -95.39480, 4, 1, NULL, NULL),
(2151, '75771', 'Lindale', 'Texas', 'TX', 'Smith', '423', 32.50620, -95.40060, 4, 1, NULL, NULL),
(2152, '75789', 'Troup', 'Texas', 'TX', 'Smith', '423', 32.11170, -95.11550, 4, 1, NULL, NULL),
(2153, '75791', 'Whitehouse', 'Texas', 'TX', 'Smith', '423', 32.22200, -95.22660, 4, 1, NULL, NULL),
(2154, '75792', 'Winona', 'Texas', 'TX', 'Smith', '423', 32.46620, -95.12460, 4, 1, NULL, NULL),
(2155, '75798', 'Tyler', 'Texas', 'TX', 'Smith', '423', 32.41120, -95.28990, 4, 1, NULL, NULL),
(2156, '75799', 'Tyler', 'Texas', 'TX', 'Smith', '423', 32.41120, -95.28990, 4, 1, NULL, NULL),
(2157, '76043', 'Glen Rose', 'Texas', 'TX', 'Somervell', '425', 32.22980, -97.76290, 4, 1, NULL, NULL),
(2158, '76070', 'Nemo', 'Texas', 'TX', 'Somervell', '425', 32.27130, -97.65670, 4, 1, NULL, NULL),
(2159, '76077', 'Rainbow', 'Texas', 'TX', 'Somervell', '425', 32.28120, -97.70650, 4, 1, NULL, NULL),
(2160, '78536', 'Delmita', 'Texas', 'TX', 'Starr', '427', 26.68010, -98.41250, 4, 1, NULL, NULL),
(2161, '78545', 'Falcon Heights', 'Texas', 'TX', 'Starr', '427', 26.56200, -99.13350, 4, 1, NULL, NULL),
(2162, '78547', 'Garciasville', 'Texas', 'TX', 'Starr', '427', 26.33960, -98.68760, 4, 1, NULL, NULL),
(2163, '78548', 'Grulla', 'Texas', 'TX', 'Starr', '427', 26.28970, -98.64790, 4, 1, NULL, NULL),
(2164, '78582', 'Rio Grande City', 'Texas', 'TX', 'Starr', '427', 26.39420, -98.81040, 4, 1, NULL, NULL),
(2165, '78584', 'Roma', 'Texas', 'TX', 'Starr', '427', 26.42150, -99.00250, 4, 1, NULL, NULL),
(2166, '78585', 'Salineno', 'Texas', 'TX', 'Starr', '427', 26.51640, -99.11230, 4, 1, NULL, NULL),
(2167, '78588', 'San Isidro', 'Texas', 'TX', 'Starr', '427', 26.71670, -98.45390, 4, 1, NULL, NULL),
(2168, '78591', 'Santa Elena', 'Texas', 'TX', 'Starr', '427', 26.58080, -98.51380, 4, 1, NULL, NULL),
(2169, '76424', 'Breckenridge', 'Texas', 'TX', 'Stephens', '429', 32.75320, -98.90990, 4, 1, NULL, NULL),
(2170, '76429', 'Caddo', 'Texas', 'TX', 'Stephens', '429', 32.68860, -98.65900, 4, 1, NULL, NULL),
(2171, '76951', 'Sterling City', 'Texas', 'TX', 'Sterling', '431', 31.83510, -101.00170, 4, 1, NULL, NULL),
(2172, '79502', 'Aspermont', 'Texas', 'TX', 'Stonewall', '433', 33.13010, -100.23440, 4, 1, NULL, NULL),
(2173, '79540', 'Old Glory', 'Texas', 'TX', 'Stonewall', '433', 33.12980, -100.05590, 4, 1, NULL, NULL),
(2174, '76950', 'Sonora', 'Texas', 'TX', 'Sutton', '435', 30.55580, -100.63070, 4, 1, NULL, NULL),
(2175, '79042', 'Happy', 'Texas', 'TX', 'Swisher', '437', 34.72190, -101.82560, 4, 1, NULL, NULL),
(2176, '79052', 'Kress', 'Texas', 'TX', 'Swisher', '437', 34.37370, -101.73700, 4, 1, NULL, NULL),
(2177, '79088', 'Tulia', 'Texas', 'TX', 'Swisher', '437', 34.55830, -101.80390, 4, 1, NULL, NULL),
(2178, '76001', 'Arlington', 'Texas', 'TX', 'Tarrant County', '439', 32.62890, -97.15170, 4, 1, NULL, NULL),
(2179, '76002', 'Arlington', 'Texas', 'TX', 'Tarrant County', '439', 32.62550, -97.07840, 4, 1, NULL, NULL),
(2180, '76003', 'Arlington', 'Texas', 'TX', 'Tarrant County', '439', 32.74170, -97.22530, 4, 1, NULL, NULL),
(2181, '76004', 'Arlington', 'Texas', 'TX', 'Tarrant County', '439', 32.73570, -97.10810, 4, 1, NULL, NULL),
(2182, '76005', 'Arlington', 'Texas', 'TX', 'Tarrant County', '439', 32.77140, -97.29150, 4, 1, NULL, NULL),
(2183, '76006', 'Arlington', 'Texas', 'TX', 'Tarrant County', '439', 32.77850, -97.08340, 4, 1, NULL, NULL),
(2184, '76007', 'Arlington', 'Texas', 'TX', 'Tarrant County', '439', 32.77140, -97.29150, 4, 1, NULL, NULL),
(2185, '76010', 'Arlington', 'Texas', 'TX', 'Tarrant County', '439', 32.72040, -97.08260, 4, 1, NULL, NULL),
(2186, '76011', 'Arlington', 'Texas', 'TX', 'Tarrant County', '439', 32.75820, -97.10030, 4, 1, NULL, NULL),
(2187, '76012', 'Arlington', 'Texas', 'TX', 'Tarrant County', '439', 32.75400, -97.13480, 4, 1, NULL, NULL),
(2188, '76013', 'Arlington', 'Texas', 'TX', 'Tarrant County', '439', 32.71990, -97.14420, 4, 1, NULL, NULL),
(2189, '76014', 'Arlington', 'Texas', 'TX', 'Tarrant County', '439', 32.69540, -97.08760, 4, 1, NULL, NULL),
(2190, '76015', 'Arlington', 'Texas', 'TX', 'Tarrant County', '439', 32.69310, -97.13470, 4, 1, NULL, NULL),
(2191, '76016', 'Arlington', 'Texas', 'TX', 'Tarrant County', '439', 32.68890, -97.19050, 4, 1, NULL, NULL),
(2192, '76017', 'Arlington', 'Texas', 'TX', 'Tarrant County', '439', 32.65550, -97.15990, 4, 1, NULL, NULL),
(2193, '76018', 'Arlington', 'Texas', 'TX', 'Tarrant County', '439', 32.65480, -97.09200, 4, 1, NULL, NULL),
(2194, '76019', 'Arlington', 'Texas', 'TX', 'Tarrant County', '439', 32.73570, -97.10810, 4, 1, NULL, NULL),
(2195, '76020', 'Azle', 'Texas', 'TX', 'Tarrant County', '439', 32.90350, -97.54120, 4, 1, NULL, NULL),
(2196, '76021', 'Bedford', 'Texas', 'TX', 'Tarrant County', '439', 32.85360, -97.13580, 4, 1, NULL, NULL),
(2197, '76022', 'Bedford', 'Texas', 'TX', 'Tarrant County', '439', 32.82970, -97.14540, 4, 1, NULL, NULL),
(2198, '76034', 'Colleyville', 'Texas', 'TX', 'Tarrant County', '439', 32.88720, -97.14600, 4, 1, NULL, NULL),
(2199, '76036', 'Crowley', 'Texas', 'TX', 'Tarrant County', '439', 32.58140, -97.37030, 4, 1, NULL, NULL),
(2200, '76039', 'Euless', 'Texas', 'TX', 'Tarrant County', '439', 32.85820, -97.08320, 4, 1, NULL, NULL),
(2201, '76040', 'Euless', 'Texas', 'TX', 'Tarrant County', '439', 32.82640, -97.09720, 4, 1, NULL, NULL),
(2202, '76051', 'Grapevine', 'Texas', 'TX', 'Tarrant County', '439', 32.93280, -97.08080, 4, 1, NULL, NULL),
(2203, '76052', 'Haslet', 'Texas', 'TX', 'Tarrant County', '439', 32.95570, -97.33720, 4, 1, NULL, NULL),
(2204, '76053', 'Hurst', 'Texas', 'TX', 'Tarrant County', '439', 32.82110, -97.17560, 4, 1, NULL, NULL),
(2205, '76054', 'Hurst', 'Texas', 'TX', 'Tarrant County', '439', 32.85580, -97.17550, 4, 1, NULL, NULL),
(2206, '76060', 'Kennedale', 'Texas', 'TX', 'Tarrant County', '439', 32.64320, -97.21390, 4, 1, NULL, NULL),
(2207, '76063', 'Mansfield', 'Texas', 'TX', 'Tarrant County', '439', 32.57730, -97.14160, 4, 1, NULL, NULL),
(2208, '76092', 'Southlake', 'Texas', 'TX', 'Tarrant County', '439', 32.94850, -97.15240, 4, 1, NULL, NULL),
(2209, '76094', 'Arlington', 'Texas', 'TX', 'Tarrant County', '439', 32.77140, -97.29150, 4, 1, NULL, NULL),
(2210, '76095', 'Bedford', 'Texas', 'TX', 'Tarrant County', '439', 32.84400, -97.14310, 4, 1, NULL, NULL),
(2211, '76096', 'Arlington', 'Texas', 'TX', 'Tarrant County', '439', 32.77140, -97.29150, 4, 1, NULL, NULL),
(2212, '76099', 'Grapevine', 'Texas', 'TX', 'Tarrant County', '439', 32.93430, -97.07810, 4, 1, NULL, NULL),
(2213, '76101', 'Fort Worth', 'Texas', 'TX', 'Tarrant County', '439', 32.77140, -97.29150, 4, 1, NULL, NULL),
(2214, '76102', 'Fort Worth', 'Texas', 'TX', 'Tarrant County', '439', 32.75890, -97.32800, 4, 1, NULL, NULL),
(2215, '76103', 'Fort Worth', 'Texas', 'TX', 'Tarrant County', '439', 32.74700, -97.26040, 4, 1, NULL, NULL),
(2216, '76104', 'Fort Worth', 'Texas', 'TX', 'Tarrant County', '439', 32.72560, -97.31840, 4, 1, NULL, NULL),
(2217, '76105', 'Fort Worth', 'Texas', 'TX', 'Tarrant County', '439', 32.72330, -97.26900, 4, 1, NULL, NULL),
(2218, '76106', 'Fort Worth', 'Texas', 'TX', 'Tarrant County', '439', 32.79680, -97.35600, 4, 1, NULL, NULL),
(2219, '76107', 'Fort Worth', 'Texas', 'TX', 'Tarrant County', '439', 32.73920, -97.38520, 4, 1, NULL, NULL),
(2220, '76108', 'Fort Worth', 'Texas', 'TX', 'Tarrant County', '439', 32.78220, -97.49690, 4, 1, NULL, NULL),
(2221, '76109', 'Fort Worth', 'Texas', 'TX', 'Tarrant County', '439', 32.70020, -97.37890, 4, 1, NULL, NULL),
(2222, '76110', 'Fort Worth', 'Texas', 'TX', 'Tarrant County', '439', 32.70650, -97.33750, 4, 1, NULL, NULL),
(2223, '76111', 'Fort Worth', 'Texas', 'TX', 'Tarrant County', '439', 32.78240, -97.30030, 4, 1, NULL, NULL),
(2224, '76112', 'Fort Worth', 'Texas', 'TX', 'Tarrant County', '439', 32.74930, -97.21810, 4, 1, NULL, NULL),
(2225, '76113', 'Fort Worth', 'Texas', 'TX', 'Tarrant County', '439', 32.77140, -97.29150, 4, 1, NULL, NULL),
(2226, '76114', 'Fort Worth', 'Texas', 'TX', 'Tarrant County', '439', 32.77960, -97.39280, 4, 1, NULL, NULL),
(2227, '76115', 'Fort Worth', 'Texas', 'TX', 'Tarrant County', '439', 32.67960, -97.33360, 4, 1, NULL, NULL),
(2228, '76116', 'Fort Worth', 'Texas', 'TX', 'Tarrant County', '439', 32.72300, -97.44830, 4, 1, NULL, NULL),
(2229, '76117', 'Haltom City', 'Texas', 'TX', 'Tarrant County', '439', 32.80870, -97.27090, 4, 1, NULL, NULL),
(2230, '76118', 'Fort Worth', 'Texas', 'TX', 'Tarrant County', '439', 32.80130, -97.19520, 4, 1, NULL, NULL),
(2231, '76119', 'Fort Worth', 'Texas', 'TX', 'Tarrant County', '439', 32.69140, -97.26750, 4, 1, NULL, NULL),
(2232, '76120', 'Fort Worth', 'Texas', 'TX', 'Tarrant County', '439', 32.76390, -97.17810, 4, 1, NULL, NULL),
(2233, '76121', 'Fort Worth', 'Texas', 'TX', 'Tarrant County', '439', 32.77140, -97.29150, 4, 1, NULL, NULL),
(2234, '76122', 'Fort Worth', 'Texas', 'TX', 'Tarrant County', '439', 32.77140, -97.29150, 4, 1, NULL, NULL),
(2235, '76123', 'Fort Worth', 'Texas', 'TX', 'Tarrant County', '439', 32.62540, -97.36580, 4, 1, NULL, NULL),
(2236, '76124', 'Fort Worth', 'Texas', 'TX', 'Tarrant County', '439', 32.77140, -97.29150, 4, 1, NULL, NULL),
(2237, '76126', 'Fort Worth', 'Texas', 'TX', 'Tarrant County', '439', 32.64000, -97.54480, 4, 1, NULL, NULL),
(2238, '76127', 'Naval Air Station/ Jrb', 'Texas', 'TX', 'Tarrant County', '439', 32.77710, -97.43010, 1, 1, NULL, NULL),
(2239, '76129', 'Fort Worth', 'Texas', 'TX', 'Tarrant County', '439', 32.77140, -97.29150, 4, 1, NULL, NULL),
(2240, '76130', 'Fort Worth', 'Texas', 'TX', 'Tarrant County', '439', 32.77140, -97.29150, 4, 1, NULL, NULL),
(2241, '76131', 'Fort Worth', 'Texas', 'TX', 'Tarrant County', '439', 32.86320, -97.33770, 4, 1, NULL, NULL),
(2242, '76132', 'Fort Worth', 'Texas', 'TX', 'Tarrant County', '439', 32.67110, -97.40560, 4, 1, NULL, NULL),
(2243, '76133', 'Fort Worth', 'Texas', 'TX', 'Tarrant County', '439', 32.65260, -97.37580, 4, 1, NULL, NULL),
(2244, '76134', 'Fort Worth', 'Texas', 'TX', 'Tarrant County', '439', 32.64690, -97.33250, 4, 1, NULL, NULL),
(2245, '76135', 'Fort Worth', 'Texas', 'TX', 'Tarrant County', '439', 32.82480, -97.45190, 4, 1, NULL, NULL),
(2246, '76136', 'Fort Worth', 'Texas', 'TX', 'Tarrant County', '439', 32.77140, -97.29150, 4, 1, NULL, NULL),
(2247, '76137', 'Fort Worth', 'Texas', 'TX', 'Tarrant County', '439', 32.86640, -97.28910, 4, 1, NULL, NULL),
(2248, '76140', 'Fort Worth', 'Texas', 'TX', 'Tarrant County', '439', 32.61920, -97.27350, 4, 1, NULL, NULL),
(2249, '76147', 'Fort Worth', 'Texas', 'TX', 'Tarrant County', '439', 32.77140, -97.29150, 4, 1, NULL, NULL),
(2250, '76148', 'Fort Worth', 'Texas', 'TX', 'Tarrant County', '439', 32.86210, -97.25080, 4, 1, NULL, NULL),
(2251, '76150', 'Fort Worth', 'Texas', 'TX', 'Tarrant County', '439', 32.77140, -97.29150, 4, 1, NULL, NULL),
(2252, '76155', 'Fort Worth', 'Texas', 'TX', 'Tarrant County', '439', 32.82470, -97.05030, 4, 1, NULL, NULL),
(2253, '76161', 'Fort Worth', 'Texas', 'TX', 'Tarrant County', '439', 32.77140, -97.29150, 4, 1, NULL, NULL),
(2254, '76162', 'Fort Worth', 'Texas', 'TX', 'Tarrant County', '439', 32.77140, -97.29150, 4, 1, NULL, NULL),
(2255, '76163', 'Fort Worth', 'Texas', 'TX', 'Tarrant County', '439', 32.77140, -97.29150, 4, 1, NULL, NULL),
(2256, '76164', 'Fort Worth', 'Texas', 'TX', 'Tarrant County', '439', 32.78110, -97.35460, 4, 1, NULL, NULL),
(2257, '76166', 'Fort Worth', 'Texas', 'TX', 'Tarrant County', '439', 32.75390, -97.33620, 4, 1, NULL, NULL),
(2258, '76177', 'Fort Worth', 'Texas', 'TX', 'Tarrant County', '439', 32.94480, -97.31240, 4, 1, NULL, NULL),
(2259, '76179', 'Fort Worth', 'Texas', 'TX', 'Tarrant County', '439', 32.90740, -97.42570, 4, 1, NULL, NULL),
(2260, '76180', 'North Richland Hills', 'Texas', 'TX', 'Tarrant County', '439', 32.84000, -97.22500, 4, 1, NULL, NULL),
(2261, '76181', 'Fort Worth', 'Texas', 'TX', 'Tarrant County', '439', 32.77140, -97.29150, 4, 1, NULL, NULL),
(2262, '76182', 'North Richland Hills', 'Texas', 'TX', 'Tarrant County', '439', 32.88280, -97.20980, 4, 1, NULL, NULL),
(2263, '76185', 'Fort Worth', 'Texas', 'TX', 'Tarrant County', '439', 32.77140, -97.29150, 4, 1, NULL, NULL),
(2264, '76191', 'Fort Worth', 'Texas', 'TX', 'Tarrant County', '439', 32.77140, -97.29150, 4, 1, NULL, NULL),
(2265, '76192', 'Fort Worth', 'Texas', 'TX', 'Tarrant County', '439', 32.77140, -97.29150, 4, 1, NULL, NULL),
(2266, '76193', 'Fort Worth', 'Texas', 'TX', 'Tarrant County', '439', 32.77140, -97.29150, 4, 1, NULL, NULL),
(2267, '76195', 'Fort Worth', 'Texas', 'TX', 'Tarrant County', '439', 32.72540, -97.32080, 4, 1, NULL, NULL),
(2268, '76196', 'Fort Worth', 'Texas', 'TX', 'Tarrant County', '439', 32.77140, -97.29150, 4, 1, NULL, NULL),
(2269, '76197', 'Fort Worth', 'Texas', 'TX', 'Tarrant County', '439', 32.77140, -97.29150, 4, 1, NULL, NULL),
(2270, '76198', 'Fort Worth', 'Texas', 'TX', 'Tarrant County', '439', 32.77140, -97.29150, 4, 1, NULL, NULL),
(2271, '76199', 'Fort Worth', 'Texas', 'TX', 'Tarrant County', '439', 32.77140, -97.29150, 4, 1, NULL, NULL),
(2272, '76244', 'Keller', 'Texas', 'TX', 'Tarrant County', '439', 32.93100, -97.28430, 4, 1, NULL, NULL),
(2273, '76248', 'Keller', 'Texas', 'TX', 'Tarrant County', '439', 32.92760, -97.24890, 4, 1, NULL, NULL),
(2274, '79508', 'Buffalo Gap', 'Texas', 'TX', 'Taylor', '441', 32.28940, -99.81030, 4, 1, NULL, NULL),
(2275, '79530', 'Lawn', 'Texas', 'TX', 'Taylor', '441', 32.13600, -99.73510, 4, 1, NULL, NULL),
(2276, '79536', 'Merkel', 'Texas', 'TX', 'Taylor', '441', 32.44440, -99.99240, 4, 1, NULL, NULL),
(2277, '79541', 'Ovalo', 'Texas', 'TX', 'Taylor', '441', 32.15530, -99.82290, 4, 1, NULL, NULL),
(2278, '79561', 'Trent', 'Texas', 'TX', 'Taylor', '441', 32.48980, -100.11950, 4, 1, NULL, NULL),
(2279, '79562', 'Tuscola', 'Texas', 'TX', 'Taylor', '441', 32.23500, -99.82440, 4, 1, NULL, NULL),
(2280, '79563', 'Tye', 'Texas', 'TX', 'Taylor', '441', 32.44770, -99.86840, 4, 1, NULL, NULL),
(2281, '79601', 'Abilene', 'Texas', 'TX', 'Taylor', '441', 32.46820, -99.71820, 4, 1, NULL, NULL),
(2282, '79602', 'Abilene', 'Texas', 'TX', 'Taylor', '441', 32.41780, -99.72140, 4, 1, NULL, NULL),
(2283, '79603', 'Abilene', 'Texas', 'TX', 'Taylor', '441', 32.46790, -99.76190, 4, 1, NULL, NULL),
(2284, '79604', 'Abilene', 'Texas', 'TX', 'Taylor', '441', 32.44870, -99.73310, 4, 1, NULL, NULL),
(2285, '79605', 'Abilene', 'Texas', 'TX', 'Taylor', '441', 32.43200, -99.77240, 4, 1, NULL, NULL),
(2286, '79606', 'Abilene', 'Texas', 'TX', 'Taylor', '441', 32.39200, -99.77460, 4, 1, NULL, NULL),
(2287, '79607', 'Dyess Afb', 'Texas', 'TX', 'Taylor', '441', 32.41900, -99.82210, 1, 1, NULL, NULL),
(2288, '79608', 'Abilene', 'Texas', 'TX', 'Taylor', '441', 32.44870, -99.73310, 4, 1, NULL, NULL),
(2289, '79697', 'Abilene', 'Texas', 'TX', 'Taylor', '441', 32.44870, -99.73310, 4, 1, NULL, NULL),
(2290, '79698', 'Abilene', 'Texas', 'TX', 'Taylor', '441', 32.47510, -99.73480, 4, 1, NULL, NULL),
(2291, '79699', 'Abilene', 'Texas', 'TX', 'Taylor', '441', 32.46650, -99.71170, 4, 1, NULL, NULL),
(2292, '78851', 'Dryden', 'Texas', 'TX', 'Terrell', '443', 30.04460, -102.11460, 1, 1, NULL, NULL),
(2293, '79848', 'Sanderson', 'Texas', 'TX', 'Terrell', '443', 30.12210, -102.39740, 1, 1, NULL, NULL),
(2294, '79316', 'Brownfield', 'Texas', 'TX', 'Terry', '445', 33.16980, -102.27620, 4, 1, NULL, NULL),
(2295, '79345', 'Meadow', 'Texas', 'TX', 'Terry', '445', 33.33210, -102.24920, 4, 1, NULL, NULL),
(2296, '79378', 'Wellman', 'Texas', 'TX', 'Terry', '445', 33.04730, -102.42820, 4, 1, NULL, NULL),
(2297, '76483', 'Throckmorton', 'Texas', 'TX', 'Throckmorton', '447', 33.17940, -99.18380, 4, 1, NULL, NULL),
(2298, '76491', 'Woodson', 'Texas', 'TX', 'Throckmorton', '447', 33.07410, -99.05460, 4, 1, NULL, NULL),
(2299, '75455', 'Mount Pleasant', 'Texas', 'TX', 'Titus', '449', 33.17330, -94.96950, 4, 1, NULL, NULL),
(2300, '75456', 'Mount Pleasant', 'Texas', 'TX', 'Titus', '449', 33.15680, -94.96830, 4, 1, NULL, NULL),
(2301, '75493', 'Winfield', 'Texas', 'TX', 'Titus', '449', 33.16730, -95.11190, 4, 1, NULL, NULL),
(2302, '75558', 'Cookville', 'Texas', 'TX', 'Titus', '449', 33.24320, -94.87470, 4, 1, NULL, NULL),
(2303, '76886', 'Veribest', 'Texas', 'TX', 'Tom Green', '451', 31.47660, -100.25950, 4, 1, NULL, NULL),
(2304, '76901', 'San Angelo', 'Texas', 'TX', 'Tom Green', '451', 31.47820, -100.48180, 4, 1, NULL, NULL),
(2305, '76902', 'San Angelo', 'Texas', 'TX', 'Tom Green', '451', 31.46380, -100.43700, 4, 1, NULL, NULL),
(2306, '76903', 'San Angelo', 'Texas', 'TX', 'Tom Green', '451', 31.47070, -100.43860, 4, 1, NULL, NULL),
(2307, '76904', 'San Angelo', 'Texas', 'TX', 'Tom Green', '451', 31.41940, -100.48000, 4, 1, NULL, NULL),
(2308, '76905', 'San Angelo', 'Texas', 'TX', 'Tom Green', '451', 31.46470, -100.39000, 4, 1, NULL, NULL),
(2309, '76906', 'San Angelo', 'Texas', 'TX', 'Tom Green', '451', 31.46380, -100.43700, 4, 1, NULL, NULL),
(2310, '76908', 'Goodfellow Afb', 'Texas', 'TX', 'Tom Green', '451', 31.43210, -100.40220, 1, 1, NULL, NULL),
(2311, '76909', 'San Angelo', 'Texas', 'TX', 'Tom Green', '451', 31.46380, -100.43700, 4, 1, NULL, NULL),
(2312, '76934', 'Carlsbad', 'Texas', 'TX', 'Tom Green', '451', 31.62660, -100.66940, 4, 1, NULL, NULL),
(2313, '76935', 'Christoval', 'Texas', 'TX', 'Tom Green', '451', 31.23460, -100.52030, 4, 1, NULL, NULL),
(2314, '76939', 'Knickerbocker', 'Texas', 'TX', 'Tom Green', '451', 31.25010, -100.59200, 4, 1, NULL, NULL),
(2315, '76940', 'Mereta', 'Texas', 'TX', 'Tom Green', '451', 31.45180, -100.13490, 4, 1, NULL, NULL),
(2316, '76955', 'Vancourt', 'Texas', 'TX', 'Tom Green', '451', 31.31320, -100.13280, 4, 1, NULL, NULL),
(2317, '76957', 'Wall', 'Texas', 'TX', 'Tom Green', '451', 31.37410, -100.30760, 4, 1, NULL, NULL),
(2318, '76958', 'Water Valley', 'Texas', 'TX', 'Tom Green', '451', 31.64530, -100.71600, 4, 1, NULL, NULL),
(2319, '73301', 'Austin', 'Texas', 'TX', 'Travis County', '453', 30.32640, -97.77130, 4, 1, NULL, NULL),
(2320, '73344', 'Austin', 'Texas', 'TX', 'Travis County', '453', 30.32640, -97.77130, 4, 1, NULL, NULL),
(2321, '78617', 'Del Valle', 'Texas', 'TX', 'Travis County', '453', 30.17450, -97.61340, 4, 1, NULL, NULL),
(2322, '78645', 'Leander', 'Texas', 'TX', 'Travis County', '453', 30.44900, -97.96690, 4, 1, NULL, NULL),
(2323, '78651', 'Mc Neil', 'Texas', 'TX', 'Travis County', '453', 30.32640, -97.77130, 1, 1, NULL, NULL),
(2324, '78652', 'Manchaca', 'Texas', 'TX', 'Travis County', '453', 30.12380, -97.83930, 4, 1, NULL, NULL),
(2325, '78653', 'Manor', 'Texas', 'TX', 'Travis County', '453', 30.33880, -97.53230, 4, 1, NULL, NULL),
(2326, '78660', 'Pflugerville', 'Texas', 'TX', 'Travis County', '453', 30.44210, -97.62990, 4, 1, NULL, NULL),
(2327, '78669', 'Spicewood', 'Texas', 'TX', 'Travis County', '453', 30.38990, -98.05390, 4, 1, NULL, NULL),
(2328, '78691', 'Pflugerville', 'Texas', 'TX', 'Travis County', '453', 30.43940, -97.62000, 4, 1, NULL, NULL),
(2329, '78701', 'Austin', 'Texas', 'TX', 'Travis County', '453', 30.27130, -97.74260, 4, 1, NULL, NULL),
(2330, '78702', 'Austin', 'Texas', 'TX', 'Travis County', '453', 30.26380, -97.71660, 4, 1, NULL, NULL),
(2331, '78703', 'Austin', 'Texas', 'TX', 'Travis County', '453', 30.29070, -97.76480, 4, 1, NULL, NULL),
(2332, '78704', 'Austin', 'Texas', 'TX', 'Travis County', '453', 30.24280, -97.76580, 4, 1, NULL, NULL),
(2333, '78705', 'Austin', 'Texas', 'TX', 'Travis County', '453', 30.28960, -97.73960, 4, 1, NULL, NULL),
(2334, '78708', 'Austin', 'Texas', 'TX', 'Travis County', '453', 30.32640, -97.77130, 4, 1, NULL, NULL),
(2335, '78709', 'Austin', 'Texas', 'TX', 'Travis County', '453', 30.32640, -97.77130, 4, 1, NULL, NULL),
(2336, '78710', 'Austin', 'Texas', 'TX', 'Travis County', '453', 30.35200, -97.71510, 4, 1, NULL, NULL),
(2337, '78711', 'Austin', 'Texas', 'TX', 'Travis County', '453', 30.32640, -97.77130, 4, 1, NULL, NULL),
(2338, '78712', 'Austin', 'Texas', 'TX', 'Travis County', '453', 30.28520, -97.73540, 4, 1, NULL, NULL),
(2339, '78713', 'Austin', 'Texas', 'TX', 'Travis County', '453', 30.26720, -97.74310, 4, 1, NULL, NULL),
(2340, '78714', 'Austin', 'Texas', 'TX', 'Travis County', '453', 30.26720, -97.74310, 4, 1, NULL, NULL),
(2341, '78715', 'Austin', 'Texas', 'TX', 'Travis County', '453', 30.26720, -97.74310, 4, 1, NULL, NULL),
(2342, '78716', 'Austin', 'Texas', 'TX', 'Travis County', '453', 30.31620, -97.85880, 4, 1, NULL, NULL),
(2343, '78718', 'Austin', 'Texas', 'TX', 'Travis County', '453', 30.32640, -97.77130, 4, 1, NULL, NULL),
(2344, '78719', 'Austin', 'Texas', 'TX', 'Travis County', '453', 30.18020, -97.66670, 4, 1, NULL, NULL),
(2345, '78720', 'Austin', 'Texas', 'TX', 'Travis County', '453', 30.32640, -97.77130, 4, 1, NULL, NULL),
(2346, '78721', 'Austin', 'Texas', 'TX', 'Travis County', '453', 30.27210, -97.68680, 4, 1, NULL, NULL),
(2347, '78722', 'Austin', 'Texas', 'TX', 'Travis County', '453', 30.28930, -97.71500, 4, 1, NULL, NULL),
(2348, '78723', 'Austin', 'Texas', 'TX', 'Travis County', '453', 30.30850, -97.68490, 4, 1, NULL, NULL),
(2349, '78724', 'Austin', 'Texas', 'TX', 'Travis County', '453', 30.29600, -97.63960, 4, 1, NULL, NULL),
(2350, '78725', 'Austin', 'Texas', 'TX', 'Travis County', '453', 30.25620, -97.62430, 4, 1, NULL, NULL),
(2351, '78726', 'Austin', 'Texas', 'TX', 'Travis County', '453', 30.43000, -97.83260, 4, 1, NULL, NULL),
(2352, '78727', 'Austin', 'Texas', 'TX', 'Travis County', '453', 30.42540, -97.71950, 4, 1, NULL, NULL),
(2353, '78728', 'Austin', 'Texas', 'TX', 'Travis County', '453', 30.44170, -97.68110, 4, 1, NULL, NULL),
(2354, '78730', 'Austin', 'Texas', 'TX', 'Travis County', '453', 30.36070, -97.82410, 4, 1, NULL, NULL),
(2355, '78731', 'Austin', 'Texas', 'TX', 'Travis County', '453', 30.34710, -97.76090, 4, 1, NULL, NULL),
(2356, '78732', 'Austin', 'Texas', 'TX', 'Travis County', '453', 30.37520, -97.90070, 4, 1, NULL, NULL),
(2357, '78733', 'Austin', 'Texas', 'TX', 'Travis County', '453', 30.33140, -97.86660, 4, 1, NULL, NULL),
(2358, '78734', 'Austin', 'Texas', 'TX', 'Travis County', '453', 30.37050, -97.94270, 4, 1, NULL, NULL),
(2359, '78735', 'Austin', 'Texas', 'TX', 'Travis County', '453', 30.24900, -97.84140, 4, 1, NULL, NULL),
(2360, '78736', 'Austin', 'Texas', 'TX', 'Travis County', '453', 30.24440, -97.91600, 4, 1, NULL, NULL),
(2361, '78737', 'Austin', 'Texas', 'TX', 'Travis County', '453', 30.21070, -97.94270, 4, 1, NULL, NULL),
(2362, '78738', 'Austin', 'Texas', 'TX', 'Travis County', '453', 30.33370, -97.98240, 4, 1, NULL, NULL),
(2363, '78739', 'Austin', 'Texas', 'TX', 'Travis County', '453', 30.17200, -97.87840, 4, 1, NULL, NULL),
(2364, '78741', 'Austin', 'Texas', 'TX', 'Travis County', '453', 30.23150, -97.72230, 4, 1, NULL, NULL),
(2365, '78742', 'Austin', 'Texas', 'TX', 'Travis County', '453', 30.23130, -97.67030, 4, 1, NULL, NULL),
(2366, '78744', 'Austin', 'Texas', 'TX', 'Travis County', '453', 30.18760, -97.74720, 4, 1, NULL, NULL),
(2367, '78745', 'Austin', 'Texas', 'TX', 'Travis County', '453', 30.20630, -97.79560, 4, 1, NULL, NULL),
(2368, '78746', 'Austin', 'Texas', 'TX', 'Travis County', '453', 30.29710, -97.81810, 4, 1, NULL, NULL),
(2369, '78747', 'Austin', 'Texas', 'TX', 'Travis County', '453', 30.12040, -97.74330, 4, 1, NULL, NULL),
(2370, '78748', 'Austin', 'Texas', 'TX', 'Travis County', '453', 30.17430, -97.82250, 4, 1, NULL, NULL),
(2371, '78749', 'Austin', 'Texas', 'TX', 'Travis County', '453', 30.21660, -97.85080, 4, 1, NULL, NULL),
(2372, '78750', 'Austin', 'Texas', 'TX', 'Travis County', '453', 30.42240, -97.79670, 4, 1, NULL, NULL),
(2373, '78751', 'Austin', 'Texas', 'TX', 'Travis County', '453', 30.30930, -97.72420, 4, 1, NULL, NULL),
(2374, '78752', 'Austin', 'Texas', 'TX', 'Travis County', '453', 30.33160, -97.70040, 4, 1, NULL, NULL),
(2375, '78753', 'Austin', 'Texas', 'TX', 'Travis County', '453', 30.36490, -97.68270, 4, 1, NULL, NULL),
(2376, '78754', 'Austin', 'Texas', 'TX', 'Travis County', '453', 30.34230, -97.66730, 4, 1, NULL, NULL),
(2377, '78755', 'Austin', 'Texas', 'TX', 'Travis County', '453', 30.32640, -97.77130, 4, 1, NULL, NULL),
(2378, '78756', 'Austin', 'Texas', 'TX', 'Travis County', '453', 30.32230, -97.73900, 4, 1, NULL, NULL),
(2379, '78757', 'Austin', 'Texas', 'TX', 'Travis County', '453', 30.34370, -97.73160, 4, 1, NULL, NULL),
(2380, '78758', 'Austin', 'Texas', 'TX', 'Travis County', '453', 30.37640, -97.70780, 4, 1, NULL, NULL),
(2381, '78759', 'Austin', 'Texas', 'TX', 'Travis County', '453', 30.40360, -97.75260, 4, 1, NULL, NULL),
(2382, '78760', 'Austin', 'Texas', 'TX', 'Travis County', '453', 30.32640, -97.77130, 4, 1, NULL, NULL),
(2383, '78761', 'Austin', 'Texas', 'TX', 'Travis County', '453', 30.32640, -97.77130, 4, 1, NULL, NULL),
(2384, '78762', 'Austin', 'Texas', 'TX', 'Travis County', '453', 30.32640, -97.77130, 4, 1, NULL, NULL),
(2385, '78763', 'Austin', 'Texas', 'TX', 'Travis County', '453', 30.33540, -97.55980, 4, 1, NULL, NULL),
(2386, '78764', 'Austin', 'Texas', 'TX', 'Travis County', '453', 30.44550, -97.65950, 4, 1, NULL, NULL),
(2387, '78765', 'Austin', 'Texas', 'TX', 'Travis County', '453', 30.32640, -97.77130, 4, 1, NULL, NULL),
(2388, '78766', 'Austin', 'Texas', 'TX', 'Travis County', '453', 30.26720, -97.74310, 4, 1, NULL, NULL),
(2389, '78767', 'Austin', 'Texas', 'TX', 'Travis County', '453', 30.26720, -97.74310, 4, 1, NULL, NULL),
(2390, '78768', 'Austin', 'Texas', 'TX', 'Travis County', '453', 30.26720, -97.74310, 4, 1, NULL, NULL),
(2391, '78769', 'Austin', 'Texas', 'TX', 'Travis County', '453', 30.32640, -97.77130, 4, 1, NULL, NULL),
(2392, '78772', 'Austin', 'Texas', 'TX', 'Travis County', '453', 30.32640, -97.77130, 4, 1, NULL, NULL),
(2393, '78773', 'Austin', 'Texas', 'TX', 'Travis County', '453', 30.32640, -97.77130, 4, 1, NULL, NULL),
(2394, '78774', 'Austin', 'Texas', 'TX', 'Travis County', '453', 30.32640, -97.77130, 4, 1, NULL, NULL),
(2395, '78778', 'Austin', 'Texas', 'TX', 'Travis County', '453', 30.32640, -97.77130, 4, 1, NULL, NULL),
(2396, '78779', 'Austin', 'Texas', 'TX', 'Travis County', '453', 30.32640, -97.77130, 4, 1, NULL, NULL),
(2397, '78783', 'Austin', 'Texas', 'TX', 'Travis County', '453', 30.32640, -97.77130, 4, 1, NULL, NULL),
(2398, '78799', 'Austin', 'Texas', 'TX', 'Travis County', '453', 30.26720, -97.74310, 4, 1, NULL, NULL),
(2399, '75834', 'Centralia', 'Texas', 'TX', 'Trinity', '455', 31.25800, -95.03990, 4, 1, NULL, NULL),
(2400, '75845', 'Groveton', 'Texas', 'TX', 'Trinity', '455', 31.06510, -95.09690, 4, 1, NULL, NULL),
(2401, '75856', 'Pennington', 'Texas', 'TX', 'Trinity', '455', 31.19530, -95.24690, 4, 1, NULL, NULL),
(2402, '75862', 'Trinity', 'Texas', 'TX', 'Trinity', '455', 30.94200, -95.34030, 4, 1, NULL, NULL),
(2403, '75865', 'Woodlake', 'Texas', 'TX', 'Trinity', '455', 31.02880, -95.03300, 4, 1, NULL, NULL),
(2404, '75926', 'Apple Springs', 'Texas', 'TX', 'Trinity', '455', 31.22690, -94.98120, 4, 1, NULL, NULL),
(2405, '75936', 'Chester', 'Texas', 'TX', 'Tyler', '457', 30.94470, -94.55540, 4, 1, NULL, NULL),
(2406, '75938', 'Colmesneil', 'Texas', 'TX', 'Tyler', '457', 30.90070, -94.39160, 4, 1, NULL, NULL),
(2407, '75942', 'Doucette', 'Texas', 'TX', 'Tyler', '457', 30.81830, -94.42880, 4, 1, NULL, NULL),
(2408, '75979', 'Woodville', 'Texas', 'TX', 'Tyler', '457', 30.77520, -94.41550, 4, 1, NULL, NULL),
(2409, '75990', 'Woodville', 'Texas', 'TX', 'Tyler', '457', 30.77520, -94.41550, 4, 1, NULL, NULL),
(2410, '77616', 'Fred', 'Texas', 'TX', 'Tyler', '457', 30.56780, -94.18000, 4, 1, NULL, NULL),
(2411, '77624', 'Hillister', 'Texas', 'TX', 'Tyler', '457', 30.67500, -94.35340, 4, 1, NULL, NULL),
(2412, '77660', 'Spurger', 'Texas', 'TX', 'Tyler', '457', 30.63870, -94.16630, 4, 1, NULL, NULL),
(2413, '77664', 'Warren', 'Texas', 'TX', 'Tyler', '457', 30.59780, -94.41200, 4, 1, NULL, NULL),
(2414, '75640', 'Diana', 'Texas', 'TX', 'Upshur County', '459', 32.76980, -94.70320, 4, 1, NULL, NULL),
(2415, '75644', 'Gilmer', 'Texas', 'TX', 'Upshur County', '459', 32.72460, -94.97140, 4, 1, NULL, NULL),
(2416, '75645', 'Gilmer', 'Texas', 'TX', 'Upshur County', '459', 32.60440, -94.84910, 4, 1, NULL, NULL),
(2417, '75683', 'Ore City', 'Texas', 'TX', 'Upshur County', '459', 32.78560, -94.75140, 4, 1, NULL, NULL),
(2418, '75755', 'Big Sandy', 'Texas', 'TX', 'Upshur County', '459', 32.61680, -95.08800, 4, 1, NULL, NULL),
(2419, '75797', 'Big Sandy', 'Texas', 'TX', 'Upshur County', '459', 32.57650, -95.13130, 4, 1, NULL, NULL),
(2420, '79752', 'Mc Camey', 'Texas', 'TX', 'Upton', '461', 31.13190, -102.21530, 1, 1, NULL, NULL),
(2421, '79755', 'Midkiff', 'Texas', 'TX', 'Upton', '461', 31.54240, -101.92440, 4, 1, NULL, NULL),
(2422, '79778', 'Rankin', 'Texas', 'TX', 'Upton', '461', 31.22660, -101.94410, 4, 1, NULL, NULL),
(2423, '78801', 'Uvalde', 'Texas', 'TX', 'Uvalde', '463', 29.21720, -99.79310, 4, 1, NULL, NULL),
(2424, '78802', 'Uvalde', 'Texas', 'TX', 'Uvalde', '463', 29.22370, -99.77940, 4, 1, NULL, NULL),
(2425, '78838', 'Concan', 'Texas', 'TX', 'Uvalde', '463', 29.49520, -99.71260, 4, 1, NULL, NULL),
(2426, '78870', 'Knippa', 'Texas', 'TX', 'Uvalde', '463', 29.29110, -99.63720, 4, 1, NULL, NULL),
(2427, '78881', 'Sabinal', 'Texas', 'TX', 'Uvalde', '463', 29.32670, -99.47810, 4, 1, NULL, NULL),
(2428, '78884', 'Utopia', 'Texas', 'TX', 'Uvalde', '463', 29.59720, -99.55850, 4, 1, NULL, NULL),
(2429, '78837', 'Comstock', 'Texas', 'TX', 'Val Verde', '465', 29.74840, -101.26280, 4, 1, NULL, NULL),
(2430, '78840', 'Del Rio', 'Texas', 'TX', 'Val Verde', '465', 29.41020, -100.89320, 4, 1, NULL, NULL),
(2431, '78841', 'Del Rio', 'Texas', 'TX', 'Val Verde', '465', 29.34650, -100.92890, 4, 1, NULL, NULL),
(2432, '78842', 'Del Rio', 'Texas', 'TX', 'Val Verde', '465', 29.41200, -100.93420, 4, 1, NULL, NULL),
(2433, '78843', 'Laughlin A F B', 'Texas', 'TX', 'Val Verde', '465', 29.35640, -100.79270, 1, 1, NULL, NULL),
(2434, '78847', 'Del Rio', 'Texas', 'TX', 'Val Verde', '465', 29.36270, -100.89680, 4, 1, NULL, NULL),
(2435, '78871', 'Langtry', 'Texas', 'TX', 'Val Verde', '465', 29.80850, -101.55870, 1, 1, NULL, NULL),
(2436, '75103', 'Canton', 'Texas', 'TX', 'Van Zandt', '467', 32.51430, -95.90470, 4, 1, NULL, NULL),
(2437, '75117', 'Edgewood', 'Texas', 'TX', 'Van Zandt', '467', 32.70030, -95.87800, 4, 1, NULL, NULL),
(2438, '75127', 'Fruitvale', 'Texas', 'TX', 'Van Zandt', '467', 32.67700, -95.78990, 4, 1, NULL, NULL),
(2439, '75140', 'Grand Saline', 'Texas', 'TX', 'Van Zandt', '467', 32.66350, -95.70640, 4, 1, NULL, NULL),
(2440, '75169', 'Wills Point', 'Texas', 'TX', 'Van Zandt', '467', 32.72830, -96.00790, 4, 1, NULL, NULL),
(2441, '75754', 'Ben Wheeler', 'Texas', 'TX', 'Van Zandt', '467', 32.41260, -95.63710, 4, 1, NULL, NULL),
(2442, '75790', 'Van', 'Texas', 'TX', 'Van Zandt', '467', 32.52830, -95.65450, 4, 1, NULL, NULL),
(2443, '77901', 'Victoria', 'Texas', 'TX', 'Victoria', '469', 28.80900, -96.99930, 4, 1, NULL, NULL),
(2444, '77902', 'Victoria', 'Texas', 'TX', 'Victoria', '469', 28.92550, -97.10060, 4, 1, NULL, NULL),
(2445, '77903', 'Victoria', 'Texas', 'TX', 'Victoria', '469', 28.80530, -97.00360, 4, 1, NULL, NULL),
(2446, '77904', 'Victoria', 'Texas', 'TX', 'Victoria', '469', 28.86750, -96.99900, 4, 1, NULL, NULL),
(2447, '77905', 'Victoria', 'Texas', 'TX', 'Victoria', '469', 28.75250, -97.03380, 4, 1, NULL, NULL),
(2448, '77951', 'Bloomington', 'Texas', 'TX', 'Victoria', '469', 28.64950, -96.89450, 4, 1, NULL, NULL),
(2449, '77968', 'Inez', 'Texas', 'TX', 'Victoria', '469', 28.89940, -96.80030, 4, 1, NULL, NULL),
(2450, '77973', 'Mcfaddin', 'Texas', 'TX', 'Victoria', '469', 28.55110, -97.00990, 4, 1, NULL, NULL),
(2451, '77976', 'Nursery', 'Texas', 'TX', 'Victoria', '469', 28.95430, -97.09060, 4, 1, NULL, NULL),
(2452, '77977', 'Placedo', 'Texas', 'TX', 'Victoria', '469', 28.69230, -96.82540, 4, 1, NULL, NULL),
(2453, '77988', 'Telferner', 'Texas', 'TX', 'Victoria', '469', 28.84890, -96.89050, 4, 1, NULL, NULL),
(2454, '77320', 'Huntsville', 'Texas', 'TX', 'Walker', '471', 30.84700, -95.59700, 4, 1, NULL, NULL),
(2455, '77334', 'Dodge', 'Texas', 'TX', 'Walker', '471', 30.77200, -95.38380, 4, 1, NULL, NULL),
(2456, '77340', 'Huntsville', 'Texas', 'TX', 'Walker', '471', 30.64480, -95.57980, 4, 1, NULL, NULL),
(2457, '77341', 'Huntsville', 'Texas', 'TX', 'Walker', '471', 30.71420, -95.55080, 4, 1, NULL, NULL),
(2458, '77342', 'Huntsville', 'Texas', 'TX', 'Walker', '471', 30.78130, -95.59530, 4, 1, NULL, NULL),
(2459, '77343', 'Huntsville', 'Texas', 'TX', 'Walker', '471', 30.72350, -95.55080, 4, 1, NULL, NULL),
(2460, '77344', 'Huntsville', 'Texas', 'TX', 'Walker', '471', 30.72350, -95.55080, 4, 1, NULL, NULL),
(2461, '77348', 'Huntsville', 'Texas', 'TX', 'Walker', '471', 30.72350, -95.55080, 4, 1, NULL, NULL),
(2462, '77349', 'Huntsville', 'Texas', 'TX', 'Walker', '471', 30.72350, -95.55080, 4, 1, NULL, NULL),
(2463, '77358', 'New Waverly', 'Texas', 'TX', 'Walker', '471', 30.53540, -95.45320, 4, 1, NULL, NULL),
(2464, '77367', 'Riverside', 'Texas', 'TX', 'Walker', '471', 30.84760, -95.39040, 4, 1, NULL, NULL),
(2465, '77423', 'Brookshire', 'Texas', 'TX', 'Waller', '473', 29.80720, -95.97550, 4, 1, NULL, NULL),
(2466, '77445', 'Hempstead', 'Texas', 'TX', 'Waller', '473', 30.09200, -96.07160, 4, 1, NULL, NULL),
(2467, '77446', 'Prairie View', 'Texas', 'TX', 'Waller', '473', 30.08360, -95.98660, 4, 1, NULL, NULL),
(2468, '77466', 'Pattison', 'Texas', 'TX', 'Waller', '473', 29.81730, -96.00730, 4, 1, NULL, NULL),
(2469, '77484', 'Waller', 'Texas', 'TX', 'Waller', '473', 30.07100, -95.92530, 4, 1, NULL, NULL),
(2470, '79719', 'Barstow', 'Texas', 'TX', 'Ward', '475', 31.46390, -103.39710, 4, 1, NULL, NULL),
(2471, '79742', 'Grandfalls', 'Texas', 'TX', 'Ward', '475', 31.34590, -102.85680, 4, 1, NULL, NULL),
(2472, '79756', 'Monahans', 'Texas', 'TX', 'Ward', '475', 31.58130, -102.90030, 4, 1, NULL, NULL),
(2473, '79777', 'Pyote', 'Texas', 'TX', 'Ward', '475', 31.53870, -103.12670, 4, 1, NULL, NULL),
(2474, '79788', 'Wickett', 'Texas', 'TX', 'Ward', '475', 31.56920, -103.00670, 4, 1, NULL, NULL),
(2475, '77426', 'Chappell Hill', 'Texas', 'TX', 'Washington', '477', 30.18330, -96.23470, 4, 1, NULL, NULL),
(2476, '77833', 'Brenham', 'Texas', 'TX', 'Washington', '477', 30.17740, -96.40280, 4, 1, NULL, NULL),
(2477, '77834', 'Brenham', 'Texas', 'TX', 'Washington', '477', 30.16690, -96.39770, 4, 1, NULL, NULL),
(2478, '77835', 'Burton', 'Texas', 'TX', 'Washington', '477', 30.17670, -96.59250, 4, 1, NULL, NULL),
(2479, '77880', 'Washington', 'Texas', 'TX', 'Washington', '477', 30.31820, -96.22490, 4, 1, NULL, NULL),
(2480, '78040', 'Laredo', 'Texas', 'TX', 'Webb', '479', 27.51550, -99.49860, 1, 1, NULL, NULL),
(2481, '78041', 'Laredo', 'Texas', 'TX', 'Webb', '479', 27.55690, -99.49070, 1, 1, NULL, NULL),
(2482, '78042', 'Laredo', 'Texas', 'TX', 'Webb', '479', 27.56550, -99.47680, 1, 1, NULL, NULL),
(2483, '78043', 'Laredo', 'Texas', 'TX', 'Webb', '479', 27.58790, -99.21760, 1, 1, NULL, NULL),
(2484, '78044', 'Laredo', 'Texas', 'TX', 'Webb', '479', 27.36370, -99.48190, 1, 1, NULL, NULL),
(2485, '78045', 'Laredo', 'Texas', 'TX', 'Webb', '479', 27.63570, -99.59230, 1, 1, NULL, NULL),
(2486, '78046', 'Laredo', 'Texas', 'TX', 'Webb', '479', 27.40470, -99.47430, 1, 1, NULL, NULL),
(2487, '78344', 'Bruni', 'Texas', 'TX', 'Webb', '479', 27.42950, -98.83850, 4, 1, NULL, NULL),
(2488, '78369', 'Mirando City', 'Texas', 'TX', 'Webb', '479', 27.44500, -99.00110, 4, 1, NULL, NULL),
(2489, '78371', 'Oilton', 'Texas', 'TX', 'Webb', '479', 27.46850, -98.95860, 4, 1, NULL, NULL),
(2490, '77420', 'Boling', 'Texas', 'TX', 'Wharton', '481', 29.25290, -95.97400, 4, 1, NULL, NULL),
(2491, '77432', 'Danevang', 'Texas', 'TX', 'Wharton', '481', 29.06700, -96.19760, 4, 1, NULL, NULL),
(2492, '77435', 'East Bernard', 'Texas', 'TX', 'Wharton', '481', 29.47050, -96.12110, 4, 1, NULL, NULL),
(2493, '77436', 'Egypt', 'Texas', 'TX', 'Wharton', '481', 29.40500, -96.23690, 4, 1, NULL, NULL),
(2494, '77437', 'El Campo', 'Texas', 'TX', 'Wharton', '481', 29.20080, -96.27430, 4, 1, NULL, NULL),
(2495, '77443', 'Glen Flora', 'Texas', 'TX', 'Wharton', '481', 29.34750, -96.19330, 4, 1, NULL, NULL),
(2496, '77448', 'Hungerford', 'Texas', 'TX', 'Wharton', '481', 29.41370, -96.09290, 4, 1, NULL, NULL),
(2497, '77453', 'Lane City', 'Texas', 'TX', 'Wharton', '481', 29.21610, -96.02630, 4, 1, NULL, NULL),
(2498, '77454', 'Lissie', 'Texas', 'TX', 'Wharton', '481', 29.53530, -96.23030, 4, 1, NULL, NULL),
(2499, '77455', 'Louise', 'Texas', 'TX', 'Wharton', '481', 29.11140, -96.40330, 4, 1, NULL, NULL),
(2500, '77467', 'Pierce', 'Texas', 'TX', 'Wharton', '481', 29.20520, -96.13690, 4, 1, NULL, NULL),
(2501, '77488', 'Wharton', 'Texas', 'TX', 'Wharton', '481', 29.32050, -96.08580, 4, 1, NULL, NULL),
(2502, '79003', 'Allison', 'Texas', 'TX', 'Wheeler', '483', 35.60590, -100.10070, 4, 1, NULL, NULL),
(2503, '79011', 'Briscoe', 'Texas', 'TX', 'Wheeler', '483', 35.58550, -100.16790, 4, 1, NULL, NULL),
(2504, '79061', 'Mobeetie', 'Texas', 'TX', 'Wheeler', '483', 35.52970, -100.42420, 4, 1, NULL, NULL),
(2505, '79079', 'Shamrock', 'Texas', 'TX', 'Wheeler', '483', 35.21420, -100.24900, 4, 1, NULL, NULL),
(2506, '79096', 'Wheeler', 'Texas', 'TX', 'Wheeler', '483', 35.43190, -100.25680, 4, 1, NULL, NULL),
(2507, '76301', 'Wichita Falls', 'Texas', 'TX', 'Wichita', '485', 33.90530, -98.49760, 4, 1, NULL, NULL),
(2508, '76302', 'Wichita Falls', 'Texas', 'TX', 'Wichita', '485', 33.86430, -98.49400, 4, 1, NULL, NULL),
(2509, '76305', 'Wichita Falls', 'Texas', 'TX', 'Wichita', '485', 33.99950, -98.39380, 4, 1, NULL, NULL),
(2510, '76306', 'Wichita Falls', 'Texas', 'TX', 'Wichita', '485', 33.95190, -98.51450, 4, 1, NULL, NULL),
(2511, '76307', 'Wichita Falls', 'Texas', 'TX', 'Wichita', '485', 33.91370, -98.49340, 4, 1, NULL, NULL);
INSERT INTO `zip_codes` (`id`, `zipcode`, `city`, `state`, `state_abbr`, `county_area`, `code`, `latitude`, `longitude`, `some_field`, `status`, `created_at`, `updated_at`) VALUES
(2512, '76308', 'Wichita Falls', 'Texas', 'TX', 'Wichita', '485', 33.86330, -98.53400, 4, 1, NULL, NULL),
(2513, '76309', 'Wichita Falls', 'Texas', 'TX', 'Wichita', '485', 33.89310, -98.53430, 4, 1, NULL, NULL),
(2514, '76310', 'Wichita Falls', 'Texas', 'TX', 'Wichita', '485', 33.85810, -98.57550, 4, 1, NULL, NULL),
(2515, '76311', 'Sheppard Afb', 'Texas', 'TX', 'Wichita', '485', 33.98240, -98.50880, 1, 1, NULL, NULL),
(2516, '76354', 'Burkburnett', 'Texas', 'TX', 'Wichita', '485', 34.08600, -98.57080, 4, 1, NULL, NULL),
(2517, '76360', 'Electra', 'Texas', 'TX', 'Wichita', '485', 34.03620, -98.91550, 4, 1, NULL, NULL),
(2518, '76367', 'Iowa Park', 'Texas', 'TX', 'Wichita', '485', 33.94230, -98.67450, 4, 1, NULL, NULL),
(2519, '76369', 'Kamay', 'Texas', 'TX', 'Wichita', '485', 33.85790, -98.80810, 4, 1, NULL, NULL),
(2520, '76364', 'Harrold', 'Texas', 'TX', 'Wilbarger', '487', 34.09710, -99.03510, 4, 1, NULL, NULL),
(2521, '76373', 'Oklaunion', 'Texas', 'TX', 'Wilbarger', '487', 34.12950, -99.14290, 4, 1, NULL, NULL),
(2522, '76384', 'Vernon', 'Texas', 'TX', 'Wilbarger', '487', 34.14910, -99.30300, 4, 1, NULL, NULL),
(2523, '76385', 'Vernon', 'Texas', 'TX', 'Wilbarger', '487', 34.15540, -99.26630, 4, 1, NULL, NULL),
(2524, '79247', 'Odell', 'Texas', 'TX', 'Wilbarger', '487', 34.34520, -99.41960, 4, 1, NULL, NULL),
(2525, '78561', 'Lasara', 'Texas', 'TX', 'Willacy County', '489', 26.46480, -97.91110, 4, 1, NULL, NULL),
(2526, '78569', 'Lyford', 'Texas', 'TX', 'Willacy County', '489', 26.40890, -97.78170, 4, 1, NULL, NULL),
(2527, '78574', 'Mission', 'Texas', 'TX', 'Willacy County', '489', 26.31890, -98.37010, 4, 1, NULL, NULL),
(2528, '78580', 'Raymondville', 'Texas', 'TX', 'Willacy County', '489', 26.47920, -97.79670, 4, 1, NULL, NULL),
(2529, '78590', 'San Perlita', 'Texas', 'TX', 'Willacy County', '489', 26.50120, -97.63970, 4, 1, NULL, NULL),
(2530, '78594', 'Sebastian', 'Texas', 'TX', 'Willacy County', '489', 26.34400, -97.81230, 4, 1, NULL, NULL),
(2531, '78598', 'Port Mansfield', 'Texas', 'TX', 'Willacy County', '489', 26.55480, -97.42500, 4, 1, NULL, NULL),
(2532, '76527', 'Florence', 'Texas', 'TX', 'Williamson County', '491', 30.80760, -97.78120, 4, 1, NULL, NULL),
(2533, '76530', 'Granger', 'Texas', 'TX', 'Williamson County', '491', 30.73980, -97.44510, 4, 1, NULL, NULL),
(2534, '76537', 'Jarrell', 'Texas', 'TX', 'Williamson County', '491', 30.81190, -97.59420, 4, 1, NULL, NULL),
(2535, '76573', 'Schwertner', 'Texas', 'TX', 'Williamson County', '491', 30.80550, -97.47050, 4, 1, NULL, NULL),
(2536, '76574', 'Taylor', 'Texas', 'TX', 'Williamson County', '491', 30.57080, -97.40940, 4, 1, NULL, NULL),
(2537, '76578', 'Thrall', 'Texas', 'TX', 'Williamson County', '491', 30.59200, -97.28930, 4, 1, NULL, NULL),
(2538, '78613', 'Cedar Park', 'Texas', 'TX', 'Williamson County', '491', 30.50520, -97.82030, 4, 1, NULL, NULL),
(2539, '78615', 'Coupland', 'Texas', 'TX', 'Williamson County', '491', 30.48720, -97.36760, 4, 1, NULL, NULL),
(2540, '78626', 'Georgetown', 'Texas', 'TX', 'Williamson County', '491', 30.63300, -97.67070, 4, 1, NULL, NULL),
(2541, '78627', 'Georgetown', 'Texas', 'TX', 'Williamson County', '491', 30.63270, -97.67720, 4, 1, NULL, NULL),
(2542, '78628', 'Georgetown', 'Texas', 'TX', 'Williamson County', '491', 30.64110, -97.75110, 4, 1, NULL, NULL),
(2543, '78630', 'Cedar Park', 'Texas', 'TX', 'Williamson County', '491', 30.50520, -97.82030, 4, 1, NULL, NULL),
(2544, '78633', 'Georgetown', 'Texas', 'TX', 'Williamson County', '491', 30.71770, -97.75430, 4, 1, NULL, NULL),
(2545, '78634', 'Hutto', 'Texas', 'TX', 'Williamson County', '491', 30.52570, -97.56720, 4, 1, NULL, NULL),
(2546, '78641', 'Leander', 'Texas', 'TX', 'Williamson County', '491', 30.58350, -97.85750, 4, 1, NULL, NULL),
(2547, '78642', 'Liberty Hill', 'Texas', 'TX', 'Williamson County', '491', 30.66300, -97.93160, 4, 1, NULL, NULL),
(2548, '78646', 'Leander', 'Texas', 'TX', 'Williamson County', '491', 30.57880, -97.85310, 4, 1, NULL, NULL),
(2549, '78664', 'Round Rock', 'Texas', 'TX', 'Williamson County', '491', 30.51450, -97.66800, 4, 1, NULL, NULL),
(2550, '78665', 'Round Rock', 'Texas', 'TX', 'Williamson County', '491', 30.51450, -97.66800, 4, 1, NULL, NULL),
(2551, '78673', 'Walburg', 'Texas', 'TX', 'Williamson County', '491', 30.74150, -97.58910, 4, 1, NULL, NULL),
(2552, '78674', 'Weir', 'Texas', 'TX', 'Williamson County', '491', 30.67470, -97.59290, 4, 1, NULL, NULL),
(2553, '78680', 'Round Rock', 'Texas', 'TX', 'Williamson County', '491', 30.65680, -97.60260, 4, 1, NULL, NULL),
(2554, '78681', 'Round Rock', 'Texas', 'TX', 'Williamson County', '491', 30.50840, -97.70620, 4, 1, NULL, NULL),
(2555, '78682', 'Round Rock', 'Texas', 'TX', 'Williamson County', '491', 30.65680, -97.60260, 4, 1, NULL, NULL),
(2556, '78683', 'Round Rock', 'Texas', 'TX', 'Williamson County', '491', 30.65680, -97.60260, 4, 1, NULL, NULL),
(2557, '78717', 'Austin', 'Texas', 'TX', 'Williamson County', '491', 30.50600, -97.74720, 4, 1, NULL, NULL),
(2558, '78729', 'Austin', 'Texas', 'TX', 'Williamson County', '491', 30.45210, -97.76880, 4, 1, NULL, NULL),
(2559, '78114', 'Floresville', 'Texas', 'TX', 'Wilson', '493', 29.16930, -98.19360, 4, 1, NULL, NULL),
(2560, '78121', 'La Vernia', 'Texas', 'TX', 'Wilson', '493', 29.35090, -98.11300, 4, 1, NULL, NULL),
(2561, '78143', 'Pandora', 'Texas', 'TX', 'Wilson', '493', 29.24840, -97.82870, 4, 1, NULL, NULL),
(2562, '78147', 'Poth', 'Texas', 'TX', 'Wilson', '493', 29.06190, -98.08250, 4, 1, NULL, NULL),
(2563, '78160', 'Stockdale', 'Texas', 'TX', 'Wilson', '493', 29.23190, -97.93510, 4, 1, NULL, NULL),
(2564, '78161', 'Sutherland Springs', 'Texas', 'TX', 'Wilson', '493', 29.28880, -98.05090, 4, 1, NULL, NULL),
(2565, '79745', 'Kermit', 'Texas', 'TX', 'Winkler', '495', 31.85500, -103.09130, 4, 1, NULL, NULL),
(2566, '79789', 'Wink', 'Texas', 'TX', 'Winkler', '495', 31.75270, -103.15610, 4, 1, NULL, NULL),
(2567, '76023', 'Boyd', 'Texas', 'TX', 'Wise', '497', 33.05940, -97.58680, 4, 1, NULL, NULL),
(2568, '76071', 'Newark', 'Texas', 'TX', 'Wise', '497', 33.00700, -97.49230, 4, 1, NULL, NULL),
(2569, '76073', 'Paradise', 'Texas', 'TX', 'Wise', '497', 33.08260, -97.69740, 4, 1, NULL, NULL),
(2570, '76078', 'Rhome', 'Texas', 'TX', 'Wise', '497', 33.05400, -97.48170, 4, 1, NULL, NULL),
(2571, '76225', 'Alvord', 'Texas', 'TX', 'Wise', '497', 33.36980, -97.68850, 4, 1, NULL, NULL),
(2572, '76234', 'Decatur', 'Texas', 'TX', 'Wise', '497', 33.23510, -97.57400, 4, 1, NULL, NULL),
(2573, '76246', 'Greenwood', 'Texas', 'TX', 'Wise', '497', 33.40700, -97.47160, 4, 1, NULL, NULL),
(2574, '76267', 'Slidell', 'Texas', 'TX', 'Wise', '497', 33.37800, -97.39220, 4, 1, NULL, NULL),
(2575, '76426', 'Bridgeport', 'Texas', 'TX', 'Wise', '497', 33.18700, -97.78100, 4, 1, NULL, NULL),
(2576, '76431', 'Chico', 'Texas', 'TX', 'Wise', '497', 33.31930, -97.80310, 4, 1, NULL, NULL),
(2577, '75444', 'Golden', 'Texas', 'TX', 'Wood', '499', 32.72990, -95.56360, 4, 1, NULL, NULL),
(2578, '75494', 'Winnsboro', 'Texas', 'TX', 'Wood', '499', 32.91460, -95.27260, 4, 1, NULL, NULL),
(2579, '75497', 'Yantis', 'Texas', 'TX', 'Wood', '499', 32.92570, -95.53110, 4, 1, NULL, NULL),
(2580, '75765', 'Hawkins', 'Texas', 'TX', 'Wood', '499', 32.64390, -95.22200, 4, 1, NULL, NULL),
(2581, '75773', 'Mineola', 'Texas', 'TX', 'Wood', '499', 32.66610, -95.48700, 4, 1, NULL, NULL),
(2582, '75783', 'Quitman', 'Texas', 'TX', 'Wood', '499', 32.80490, -95.43020, 4, 1, NULL, NULL),
(2583, '79323', 'Denver City', 'Texas', 'TX', 'Yoakum', '501', 32.97110, -102.83130, 4, 1, NULL, NULL),
(2584, '79355', 'Plains', 'Texas', 'TX', 'Yoakum', '501', 33.18940, -102.82940, 4, 1, NULL, NULL),
(2585, '79376', 'Tokio', 'Texas', 'TX', 'Yoakum', '501', 33.18260, -102.57680, 4, 1, NULL, NULL),
(2586, '76372', 'Newcastle', 'Texas', 'TX', 'Young', '503', 33.19010, -98.74460, 4, 1, NULL, NULL),
(2587, '76374', 'Olney', 'Texas', 'TX', 'Young', '503', 33.36010, -98.74270, 4, 1, NULL, NULL),
(2588, '76450', 'Graham', 'Texas', 'TX', 'Young', '503', 33.09930, -98.58320, 4, 1, NULL, NULL),
(2589, '76460', 'Loving', 'Texas', 'TX', 'Young', '503', 33.26890, -98.50240, 4, 1, NULL, NULL),
(2590, '76481', 'South Bend', 'Texas', 'TX', 'Young', '503', 33.00520, -98.68920, 4, 1, NULL, NULL),
(2591, '78067', 'San Ygnacio', 'Texas', 'TX', 'Zapata', '505', 27.15830, -99.32020, 1, 1, NULL, NULL),
(2592, '78076', 'Zapata', 'Texas', 'TX', 'Zapata', '505', 26.88970, -99.25060, 1, 1, NULL, NULL),
(2593, '78564', 'Lopeno', 'Texas', 'TX', 'Zapata', '505', 26.94540, -99.20400, 4, 1, NULL, NULL),
(2594, '78829', 'Batesville', 'Texas', 'TX', 'Zavala', '507', 28.92860, -99.61150, 4, 1, NULL, NULL),
(2595, '78839', 'Crystal City', 'Texas', 'TX', 'Zavala', '507', 28.68700, -99.82640, 4, 1, NULL, NULL),
(2596, '78872', 'La Pryor', 'Texas', 'TX', 'Zavala', '507', 28.94880, -99.85070, 4, 1, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `all_plan_features`
--
ALTER TABLE `all_plan_features`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blogs`
--
ALTER TABLE `blogs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blog_categories`
--
ALTER TABLE `blog_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blog_comments`
--
ALTER TABLE `blog_comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact_us`
--
ALTER TABLE `contact_us`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `diagnosis_categories`
--
ALTER TABLE `diagnosis_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faqs`
--
ALTER TABLE `faqs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faq_categories`
--
ALTER TABLE `faq_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `main_categories`
--
ALTER TABLE `main_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `medicine_faqs`
--
ALTER TABLE `medicine_faqs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `medicine_variants`
--
ALTER TABLE `medicine_variants`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `online_visits`
--
ALTER TABLE `online_visits`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `online_visit_provider_assign`
--
ALTER TABLE `online_visit_provider_assign`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `online_visit_status_timeline`
--
ALTER TABLE `online_visit_status_timeline`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`(191));

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_categories`
--
ALTER TABLE `product_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `provider_categories`
--
ALTER TABLE `provider_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `question_options`
--
ALTER TABLE `question_options`
  ADD PRIMARY KEY (`id`),
  ADD KEY `visit_form_id` (`visit_form_id`,`question_id`,`next_question_id`);

--
-- Indexes for table `sections`
--
ALTER TABLE `sections`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `selected_plan_features`
--
ALTER TABLE `selected_plan_features`
  ADD PRIMARY KEY (`id`),
  ADD KEY `plan_id` (`plan_id`),
  ADD KEY `plan_feature_Id` (`plan_feature_Id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `service_pricing`
--
ALTER TABLE `service_pricing`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sponsors`
--
ALTER TABLE `sponsors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `testimonials`
--
ALTER TABLE `testimonials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `treatment_medicines`
--
ALTER TABLE `treatment_medicines`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `visit_answers`
--
ALTER TABLE `visit_answers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `visit_form_id` (`online_visit_form_id`),
  ADD KEY `question_id` (`question_id`),
  ADD KEY `answer_option_id` (`answer_option_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `service_id` (`service_id`);

--
-- Indexes for table `visit_forms`
--
ALTER TABLE `visit_forms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `visit_form_field_types`
--
ALTER TABLE `visit_form_field_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `visit_form_patient_details`
--
ALTER TABLE `visit_form_patient_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `visit_form_id` (`visit_form_id`);

--
-- Indexes for table `visit_questions`
--
ALTER TABLE `visit_questions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `visit_form_id` (`visit_form_id`,`parent_id`);

--
-- Indexes for table `zip_codes`
--
ALTER TABLE `zip_codes`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `all_plan_features`
--
ALTER TABLE `all_plan_features`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1000;

--
-- AUTO_INCREMENT for table `blogs`
--
ALTER TABLE `blogs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `blog_categories`
--
ALTER TABLE `blog_categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `blog_comments`
--
ALTER TABLE `blog_comments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `contact_us`
--
ALTER TABLE `contact_us`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `diagnosis_categories`
--
ALTER TABLE `diagnosis_categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `faqs`
--
ALTER TABLE `faqs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `faq_categories`
--
ALTER TABLE `faq_categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `main_categories`
--
ALTER TABLE `main_categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `medicine_faqs`
--
ALTER TABLE `medicine_faqs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `medicine_variants`
--
ALTER TABLE `medicine_variants`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `online_visits`
--
ALTER TABLE `online_visits`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `online_visit_provider_assign`
--
ALTER TABLE `online_visit_provider_assign`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `online_visit_status_timeline`
--
ALTER TABLE `online_visit_status_timeline`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `product_categories`
--
ALTER TABLE `product_categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `provider_categories`
--
ALTER TABLE `provider_categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `question_options`
--
ALTER TABLE `question_options`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `sections`
--
ALTER TABLE `sections`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `selected_plan_features`
--
ALTER TABLE `selected_plan_features`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `service_pricing`
--
ALTER TABLE `service_pricing`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `sponsors`
--
ALTER TABLE `sponsors`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `testimonials`
--
ALTER TABLE `testimonials`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `treatment_medicines`
--
ALTER TABLE `treatment_medicines`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'all', AUTO_INCREMENT=63;

--
-- AUTO_INCREMENT for table `visit_answers`
--
ALTER TABLE `visit_answers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `visit_forms`
--
ALTER TABLE `visit_forms`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `visit_form_field_types`
--
ALTER TABLE `visit_form_field_types`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `visit_form_patient_details`
--
ALTER TABLE `visit_form_patient_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `visit_questions`
--
ALTER TABLE `visit_questions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `zip_codes`
--
ALTER TABLE `zip_codes`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2597;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
