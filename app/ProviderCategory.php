<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProviderCategory extends Model
{
	protected $table = 'provider_categories';

	protected $guarded = [];	
}

