<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TreatmentMedicine extends Model
{
	protected $table = 'treatment_medicines';

	protected $guarded = [];	
}

