<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OnlineVisitStatusTimeline extends Model {
	protected $table = 'online_visit_status_timeline';

	protected $guarded = [];
}

