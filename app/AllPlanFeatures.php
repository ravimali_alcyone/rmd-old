<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AllPlanFeatures extends Model
{
	protected $table = 'all_plan_features';

	protected $guarded = [];	
}

