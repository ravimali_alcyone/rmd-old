<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SelectedPlanFeatures extends Model
{
	protected $table = 'selected_plan_features';

	protected $guarded = [];	
}

