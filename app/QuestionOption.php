<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuestionOption extends Model
{
	protected $table = 'question_options';

	protected $guarded = [];	
}

