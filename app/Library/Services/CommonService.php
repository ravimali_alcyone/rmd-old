<?php
namespace App\Library\Services;
use Session;
use Illuminate\Support\Facades\DB;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use App\Library\Services\CommonService;
use App\User;
use App\VisitForm;
use App\VisitFormFieldType;
use App\VisitQuestion;
use App\QuestionOption;
use App\VisitAnswer;


class CommonService
{

	public function removeComma($string){
		$b = str_replace( ',', '', $string );
		if(is_numeric($b)){ $string = $b; }		
		return $string;
	}
	
	public function getActualValueArray($stringArray){
		
		foreach($stringArray as $key => $value){
			
			$newValue = $value;
			
			if($value == 'manage_online_visits') { $newValue = 'Online Visits';	}
			if($value == 'visit_pool') { $newValue = 'Visit Pool';	}
			if($value == 'manage_orders') { $newValue = 'Orders';	}
			if($value == 'manage_products') { $newValue = 'Products';	}
			if($value == 'manage_doctors') { $newValue = 'Doctors';	}
			if($value == 'manage_patients') { $newValue = 'Patients';	}
			if($value == 'manage_contact_us') { $newValue = 'Contact Us';	}
			if($value == 'finances') { $newValue = 'Finances';	}
			if($value == 'manage_website_content') { $newValue = 'Website Content';	}
			if($value == 'manage_sub_admin') { $newValue = 'Sub Admins';	}
			if($value == 'manage_questions') { $newValue = 'Questions';	}
			if($value == 'manage_faq') { $newValue = 'FAQ';	}

			$stringArray[$key] = $newValue;
		}
		return $stringArray;
	}

	public function getTableListing($request, $table, $where, $whereRaw, $isLatest){

			$data = DB::table($table)->where($where);

 			if($whereRaw){
				$column = $whereRaw['column'];
				$value = $whereRaw['value'];
				$data = $data->whereRaw("find_in_set('".$value."', $column)");
			}
			
			if($isLatest == true){
				$data = $data->latest();
			}
			
			$data->get();

			return $data;
	}
	
	public function add_form($request){
		$req = $request->all();
		
		//Visit Form
		$name = $req['name'];
		$service_id = $req['service_id'];
		$short_info = $req['short_info'];
		$instructions = $req['instructions'];
		$id = $req['id'];	

		if ($request->status == 'on') {
			$status = 1;
		} else {
			$status = 0;
		}
		
		$data = array(
			'service_id' => $service_id,
			'name'=> $name,
			'short_info'=> $short_info,
			'instructions'=> $instructions,
			'status' => $status,
		);
				
		if($id != ''){
			VisitForm::where('id', $id)->update($data);
		}else{
			$id = VisitForm::create($data)->id;
		}
		
		return $id;
	}
	
	public function add_question($visit_form_id, $input){
		
 		//Question
		$q_id = $input['q_id'];
		
		$data = array(
			'visit_form_id' => $visit_form_id,
			'question_type'=> $input['type'],
			'question_text'=> $input['label'],
			'is_parent'=> $input['is_parent'],
			'parent_id'=> $input['parent_id']
		);

		if($q_id != ''){
			VisitQuestion::where('id', $q_id)->update($data);
		}else{
			$q_id = VisitQuestion::create($data)->id;
		}
		
		return $q_id; 		
	}

	public function add_option($visit_form_id, $q_id, $input){
					
 		//Option
		$op_id = $input['op_id'];
		
		$data = array(
			'visit_form_id' => $visit_form_id,
			'question_id' => $q_id,
			'option_text' => $input['option_text'],
			'is_conditional' => $input['is_conditional'],
			'next_question_id' => $input['next_question_id'],
		);						

		//for yes/no
		if(isset($input['positive'])){
			$data['positive'] = $input['positive'];
		}
		//echo '<pre>';print_R($data);echo '</pre>';
		
  		if($op_id != ''){
			QuestionOption::where('id', $op_id)->update($data);
		}else{
			$op_id = QuestionOption::create($data)->id;
		}  
		
		return $op_id; 		
	}

	public function getMedicalQuestions($service_id){
		
		$result = array();
		$questionData = array();
		$user_id = session('online_visit_patient_id');
				
		$visit_form = VisitForm::where('service_id', $service_id)->where('status',1)->first();
		
		if($visit_form && $visit_form->count() > 0){
			$visit_questions = VisitQuestion::select('visit_questions.id AS question_id','visit_questions.question_type','visit_questions.question_text','visit_questions.is_parent','visit_questions.parent_id','question_options.id AS option_id','question_options.option_text','question_options.is_conditional','question_options.next_question_id','question_options.positive')
				->leftJoin('question_options', function ($join){
					$join->on('question_options.question_id', '=', 'visit_questions.id')->on('question_options.visit_form_id', '=', 'visit_questions.visit_form_id');
				})			
				->where('visit_questions.visit_form_id',$visit_form->id)
				->where('visit_questions.status',1)
				->orderBy('visit_questions.id','ASC')
				->orderBy('question_options.id','ASC')
				->get();
			
			if($visit_questions && $visit_questions->count() > 0){
				
				$i=0;
				
				foreach($visit_questions->toArray() as $row){
					
					$questionData[$row['question_id']]['question']['question_id'] = $row['question_id'];
					$questionData[$row['question_id']]['question']['question_type'] = $row['question_type'];
					$questionData[$row['question_id']]['question']['question_text'] = $row['question_text'];
					$questionData[$row['question_id']]['question']['is_parent'] = $row['is_parent'];
					$questionData[$row['question_id']]['question']['parent_id'] = $row['parent_id'];
					
					if($row['question_type'] == 'yes_no' || $row['question_type'] == 'single' || $row['question_type'] == 'multiple'){
						$questionData[$row['question_id']]['options'][$i]['option_id'] = $row['option_id'];
						$questionData[$row['question_id']]['options'][$i]['option_text'] = $row['option_text'];
						$questionData[$row['question_id']]['options'][$i]['is_conditional'] = $row['is_conditional'];
						$questionData[$row['question_id']]['options'][$i]['next_question_id'] = $row['next_question_id'];
						$questionData[$row['question_id']]['options'][$i]['positive'] = $row['positive'];						
					}else{
						$questionData[$row['question_id']]['options'] = array();
					}
			
					$i++;
				}
				
				//echo '<pre>';print_R($questionData);die;
			}
		}
		
		$result['questionData'] = $questionData;
		$result['visit_form'] = $visit_form;
		return $result;
	}
	
	
}


?>