<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Section extends Model {
    # This Method Created for Insert & Update Sections.
    public function insertAndUpdateSection($data) {
		$sectionCheck = DB::table('sections')->where(array(
			'section_name' 	=> $data['section_name'],
			'language' 		=> $data['language'],
			'page_name' 	=> $data['page_name']
		))->get();

		if(count($sectionCheck) <= 0) {
			return DB::table('sections')->insert($data);
		} else {
			unset($data['created_at']);
			return DB::table('sections')->where(array('section_name' => $data['section_name'], 'page_name' => $data['page_name'], 'language' => $data['language']))->update($data);
		}
	}

	# User Subscription
	public function UserSubscription($data) {
		if (!empty($data)) {
			$query = DB::table('cp_subscribers')->insert($data);
			return true;
		} else {
			return false;
		}
	}

	# Verified Subscriber
	public function ActiveUserSubscripton($verif_code) {
		$query1 = DB::table('cp_subscribers')->where(['verification_code' => $verif_code, 'is_verified' => 0])->first();

		if ($query1) {
			$query2 = DB::table('cp_subscribers')->where('verification_code', $verif_code)->update(['is_verified' => 1, 'status' => 1]);

			if ($query2) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
}
