<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServicePricing extends Model
{
	protected $table = 'service_pricing';

	protected $guarded = [];	
}

