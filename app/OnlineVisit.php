<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OnlineVisit extends Model {
	protected $table = 'online_visits';

	protected $guarded = [];
}

