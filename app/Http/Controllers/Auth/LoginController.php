<?php
  
namespace App\Http\Controllers\Auth;
   
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
   
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */
  
    use AuthenticatesUsers;
  
    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/provider/home';
   
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
   
    public function login(Request $request)
    {   
        $input = $request->all();
   
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required',
        ]);
   
        if(auth()->attempt(array('email' => $input['email'], 'password' => $input['password'])))
        {
			
			if(auth()->user()->status == 1){
				
				if (auth()->user()->is_admin == 1 && (auth()->user()->user_role == 1 || auth()->user()->user_role == 2) ) {
					return redirect()->route('admin.home');
				}elseif (auth()->user()->is_admin == 0 && auth()->user()->user_role == 3) {
					return redirect()->route('provider.home');
				}else{
					Auth::logout();
					$msg = 'You are not authorized to login.';
					return redirect('/login')->withErrors([$msg]);	
				}				
			}else{
				Auth::logout();
				$msg = 'You must be active to login.';
				return redirect('/login')->withErrors([$msg]);
			}

        }else{
		    $msg = 'Invalid login credentials';
			return redirect('/login')->withErrors([$msg]);				
        }
          
    }
}