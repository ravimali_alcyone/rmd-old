<?php

namespace App\Http\Controllers\front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Session;
use Auth;
use App\Library\Services\CommonService;
use App\User;
use App\Testimonial;
use App\Sponsor;
use App\ServicePricing;
use App\Faq;
use App\FaqCategory;
use App\ContactUs;
use App\ProviderCategory;
use App\Service;
use App\TreatmentMedicine;
use App\VisitForm;
use App\VisitAnswer;

class FrontController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index() {
        $data['page'] = 'home';
		$data['meta_title'] = 'Home';
		$data['meta_description'] = 'ReplenishMD is a new age integrative, Functional, Anti-Aging, Regenerative Medicine, MedSpa, Mobile Lab & Concierge Medical Clinic at your service.';		
		$data['testimonials'] = Testimonial::select('*')->where('status',1)->latest()->get();
		$data['sponsors'] = Sponsor::select('*')->where('status',1)->latest()->get();
		
		$data['providers'] = array();
		$providers = User::select('users.*')->where('user_role',3)->where('status',1)->limit(5)->get();
		$gender = 'male';
		$data['services'] = Service::where('status',1)
								->where(function($query) use ($gender){
									$query->orWhere('available_for',$gender);
									$query->orWhere('available_for','both');
								})
								->get();
		
		if($providers && $providers->count() > 0){
			$i=0;
			foreach($providers as $row){
				
				$categoryString = '';
				$categories = explode(',', $row->category);
				$category_names = ProviderCategory::whereIn('id', $categories)->select('name')->get();
				
				
				if($category_names && $category_names->count() > 0){
					$categories_arr = array();
					
					foreach ($category_names as $key => $value) {
						array_push($categories_arr, $value->name);
					}
					$categoryString = implode(', ', $categories_arr);
				}			
				
				if($row->image == ''){
					$provider_image = '/dist/images/user_icon.png';
				}else{
					$provider_image = '/'.$row->image;
				}
						
				$data['providers'][$i]['id'] = $row->id;
				$data['providers'][$i]['name'] = $row->name;
				$data['providers'][$i]['image'] = $provider_image;
				$data['providers'][$i]['provider_categories'] = $categoryString;
				
				$i++;
			}
		}
		
        return view('front.index', $data);
    }
	
    public function plans() {
        $data['page'] = 'plans';
		$data['meta_title'] = 'Plans';
		$data['meta_description'] = 'ReplenishMD is a new age integrative, Functional, Anti-Aging, Regenerative Medicine, MedSpa, Mobile Lab & Concierge Medical Clinic at your service.';	
		
		$plans = array();
		
		$plan_data = ServicePricing::select('service_pricing.*','all_plan_features.name AS feature_name','all_plan_features.detail AS feature_detail')
						->leftJoin('selected_plan_features','selected_plan_features.plan_id','=','service_pricing.id')
						->leftJoin('all_plan_features','all_plan_features.id','=','selected_plan_features.plan_feature_id')
						->where('service_pricing.status',1)->get()->toArray();
				
		if($plan_data){
			$x = 0;
						
			foreach($plan_data as $key => $value){
				$plans[$value['id']]['id'] = $value['id'];
				$plans[$value['id']]['name'] = $value['name'];
				$plans[$value['id']]['is_monthly'] = $value['is_monthly'];
				$plans[$value['id']]['total_months'] = $value['total_months'];
				$plans[$value['id']]['monthly_price'] = $value['monthly_price'];
				$plans[$value['id']]['is_total'] = $value['is_total'];
				$plans[$value['id']]['total_price'] = $value['total_price'];
				$plans[$value['id']]['monthly_plan_text'] = $value['monthly_plan_text'];
				$plans[$value['id']]['total_plan_text'] = $value['total_plan_text'];
				$plans[$value['id']]['special'] = $value['special'];
				$plans[$value['id']]['detail'] = $value['detail'];
				$plans[$value['id']]['is_most_popular'] = $value['is_most_popular'];
				
				if($value['feature_name'] && $value['feature_detail']){
					$plans[$value['id']]['features'][$x]['feature_name'] = $value['feature_name'];
					$plans[$value['id']]['features'][$x]['feature_detail'] = $value['feature_detail'];					
				}
				
				$x++;
			}
		}
		
		$data['plans'] = $plans;
						
        return view('front.plans', $data);
    }	

    public function how_it_works() {
        $data['page'] = 'how_it_works';
		$data['meta_title'] = 'How It Works';
		$data['meta_description'] = 'ReplenishMD is a new age integrative, Functional, Anti-Aging, Regenerative Medicine, MedSpa, Mobile Lab & Concierge Medical Clinic at your service.';			
        return view('front.how_it_works', $data);
    }
	
    public function about() {
        $data['page'] = 'about';
		$data['meta_title'] = 'About Us';
		$data['meta_description'] = 'ReplenishMD is a new age integrative, Functional, Anti-Aging, Regenerative Medicine, MedSpa, Mobile Lab & Concierge Medical Clinic at your service.';			
        return view('front.about', $data);
    }

    public function contact(Request $request) {
		
		if ($request->ajax()) {
			$req = $request->all();
		
			$this->validate($request, [
				'name' => 'required|string|max:100',
				'email' => 'required|email|max:100',
				'phone' => 'string|max:20',
				'subject' => 'required|string|max:100',
				'message' => 'required|string',
			]);
			
			$input_data = [
				'name' => trim($req['name']),
				'email' => trim($req['email']),
				'phone' => trim($req['phone']),
				'user_category' => 1,
				'status'=> 1,
				'subject'=> trim($req['subject']),
				'comments' => trim($req['message']),
			];
			
			ContactUs::create($input_data);
			
			return response()->json(['status'=>'success', 'message'=>'Message Sent successfully.']);
			
		}else{		
			$data['page'] = 'contact';
			$data['meta_title'] = 'Contact Us';
			$data['meta_description'] = 'ReplenishMD is a new age integrative, Functional, Anti-Aging, Regenerative Medicine, MedSpa, Mobile Lab & Concierge Medical Clinic at your service.';				
			return view('front.contact', $data);
		}		
    }	
	
    public function faq() {

		$data['page'] = 'faq';
		$data['meta_title'] = 'FAQs';
		$data['meta_description'] = 'ReplenishMD is a new age integrative, Functional, Anti-Aging, Regenerative Medicine, MedSpa, Mobile Lab & Concierge Medical Clinic at your service.';			
		$data['categories'] = FaqCategory::where('parent_id', 0)->where('status', 1)->get();
		$data['faqs'] = Faq::select('faqs.id', 'faqs.category_id', 'c.name AS category','faqs.faq_data','faqs.status AS status')->join('faq_categories AS c','c.id','=','faqs.category_id')->orderBy('faqs.created_at','ASC')->get();		
		return view('front.faq', $data);
    }

    public function providers() {
        $data['page'] = 'providers';
		$data['meta_title'] = 'Our Providers';
		$data['meta_description'] = 'ReplenishMD is a new age integrative, Functional, Anti-Aging, Regenerative Medicine, MedSpa, Mobile Lab & Concierge Medical Clinic at your service.';			
		$data['providers'] = array();
		$providers = User::select('users.*')->where('user_role',3)->where('status',1)->get();
		
		if($providers && $providers->count() > 0){
			$i=0;
			foreach($providers as $row){
				
				$categoryString = '';
				$categories = explode(',', $row->category);
				$category_names = ProviderCategory::whereIn('id', $categories)->select('name')->get();
				
				
				if($category_names && $category_names->count() > 0){
					$categories_arr = array();
					
					foreach ($category_names as $key => $value) {
						array_push($categories_arr, $value->name);
					}
					$categoryString = implode(', ', $categories_arr);
				}			
				
				if($row->image == ''){
					$provider_image = '/dist/images/user_icon.png';
				}else{
					$provider_image = '/'.$row->image;
				}
						
				$data['providers'][$i]['id'] = $row->id;
				$data['providers'][$i]['name'] = $row->name;
				$data['providers'][$i]['image'] = $provider_image;
				$data['providers'][$i]['provider_categories'] = $categoryString;
				
				$i++;
			}
		}		
        return view('front.providers', $data);
    }

    public function provider_detail($id) {
        $data['page'] = 'provider_detail';
		$data['meta_title'] = 'Provider Detail';
		$data['meta_description'] = 'ReplenishMD is a new age integrative, Functional, Anti-Aging, Regenerative Medicine, MedSpa, Mobile Lab & Concierge Medical Clinic at your service.';			
		$data['provider'] = array();
		$provider = User::select('users.*')->where('users.id',$id)->where('user_role',3)->where('status',1)->first();
		
		if($provider && $provider->count() > 0){
				
			$categoryString = '';
			$categories = explode(',', $provider->category);
			$category_names = ProviderCategory::whereIn('id', $categories)->select('name')->get();
			
			
			if($category_names && $category_names->count() > 0){
				$categories_arr = array();
				
				foreach ($category_names as $key => $value) {
					array_push($categories_arr, $value->name);
				}
				$categoryString = implode(', ', $categories_arr);
			}			
			
			if($provider->image == ''){
				$provider_image = '/dist/images/user_icon.png';
			}else{
				$provider_image = '/'.$provider->image;
			}
					
			$data['provider']['id'] = $provider->id;
			$data['provider']['name'] = $provider->name;
			$data['provider']['short_info'] = $provider->short_info;
			$data['provider']['clinical_expertise'] = $provider->clinical_expertise;
			$data['provider']['credentials'] = $provider->credentials;
			$data['provider']['awards'] = $provider->awards;
			$data['provider']['image'] = $provider_image;
			$data['provider']['provider_categories'] = $categoryString;
			
			//Other Providers
			
			$data['other_providers'] = array();
			$providers = User::select('users.*')->where('user_role',3)->where('status',1)->where('id','!=',$id)->limit(4)->get();
			
			if($providers && $providers->count() > 0){
				$i=0;
				foreach($providers as $row){
					
					$categoryString2 = '';
					$categories2 = explode(',', $row->category);
					$category_names2 = ProviderCategory::whereIn('id', $categories2)->select('name')->get();
					
					
					if($category_names2 && $category_names2->count() > 0){
						$categories_arr2 = array();
						
						foreach ($category_names2 as $key => $value) {
							array_push($categories_arr2, $value->name);
						}
						$categoryString2 = implode(', ', $categories_arr2);
					}			
					
					if($row->image == ''){
						$provider_image = '/dist/images/user_icon.png';
					}else{
						$provider_image = '/'.$row->image;
					}
							
					$data['other_providers'][$i]['id'] = $row->id;
					$data['other_providers'][$i]['name'] = $row->name;
					$data['other_providers'][$i]['image'] = $provider_image;
					$data['other_providers'][$i]['provider_categories'] = $categoryString2;
					
					$i++;
				}		
			}
			return view('front.provider_detail', $data);
		}else{
			return redirect()->route('home')->withErrors('Invalid Provider Data');			
		}
	
        
    }
	
    public function privacy_policy() {
		$data['meta_title'] = 'Privacy Policy';
		$data['meta_description'] = 'ReplenishMD is a new age integrative, Functional, Anti-Aging, Regenerative Medicine, MedSpa, Mobile Lab & Concierge Medical Clinic at your service.';			
        $data['page'] = 'privacy_policy';
        return view('front.privacy_policy', $data);
    }

    public function terms_conditions() {
		$data['meta_title'] = 'Terms & Conditions';
		$data['meta_description'] = 'ReplenishMD is a new age integrative, Functional, Anti-Aging, Regenerative Medicine, MedSpa, Mobile Lab & Concierge Medical Clinic at your service.';			
        $data['page'] = 'terms_conditions';
        return view('front.terms_conditions', $data);
    }

    public function terms_of_use() {		
        $data['page'] = 'terms_of_use';
		$data['meta_title'] = 'Terms of Use';
		$data['meta_description'] = 'ReplenishMD is a new age integrative, Functional, Anti-Aging, Regenerative Medicine, MedSpa, Mobile Lab & Concierge Medical Clinic at your service.';			
        return view('front.terms_of_use', $data);
    }


	public function get_services(Request $request) {	
	
		$output = '';
		
		if ($request->isMethod('post')) {
			$req = $request->all();
			
			if(isset($req['gender']) && $req['gender'] != '') { 
				$gender = $req['gender'];
				$output = '<option value="">Select consult type...</option>';
				
				$services = Service::where('status',1)
							->where(function($query) use ($gender){
								$query->orWhere('available_for',$gender);
								$query->orWhere('available_for','both');
							})
							->get();
				if($services && $services->count() > 0){
					foreach($services as $row){
						$output .= '<option value="'.$row->id.'">'.$row->name.'</option>';
					}
				}
			}
		}
		
		echo $output;
	}
	
    public function treatment(Request $request) {
		
		if ($request->isMethod('get')) {
			
			$req = $request->all();
			
			if (isset($req['g']) && $req['g'] != '' && isset($req['t']) && $req['t'] != '') {
				
				$data['page'] = 'treatment';
				$gender = trim($req['g']);
				$id = trim($req['t']);
				
				session()->put('online_visit_gender', $gender);
				session()->put('online_visit_service_id', $id);
				
				$data['service_detail'] = Service::where('id',$id)->where('status',1)->first();
				
				if($data['service_detail'] && $data['service_detail']->count() > 0){
					
					if($data['service_detail']->available_for == $gender || $data['service_detail']->available_for == 'both'){
						
					}else{
						return redirect()->route('home')->withErrors('Unfortunately this treatment is currently not available to '.$gender.'.');
					}
					
					$data['meta_title'] = $data['service_detail']->name;
					$data['meta_description'] = $data['service_detail']->short_info;
					
					$data['medicines'] = TreatmentMedicine::where('service_id',$id)->where('status',1)->get();
					
					return view('front.treatment', $data);
					
				}else{
					return redirect()->route('home')->withErrors('Unfortunately this treatment is currently not available.');
				}			
			}else{
				return redirect()->route('home');
			}
			
		}else{
			return redirect()->route('home');
		}		

    }

	public function signup(Request $request) {	
		$data['page'] = 'signup';
		$data['meta_title'] = 'Signup';
		$data['meta_description'] = 'ReplenishMD is a new age integrative, Functional, Anti-Aging, Regenerative Medicine, MedSpa, Mobile Lab & Concierge Medical Clinic at your service.';
		
		if(!Session::has('online_visit_service_id')){
			return view('front.signup_default', $data);	
			
		}else{
			$service_id = session('online_visit_service_id');
			$data['service_detail'] = Service::where('id',$service_id)->where('status',1)->first();
			return view('front.signup', $data);			
		}	
	}
	
	public function signupSubmit(Request $request) {	
	
		$messages = [
			'first_name.regex' => 'First Name field accepts alphabetical characters only.',
			'last_name.regex' => 'Last Name field accepts alphabetical characters only.',
			'password.regex' => 'Password must contain at least 8 characters, at least one number and both lower and uppercase letters and special characters',
		];
		$validator = Validator::make($request->all(), [
			'first_name' => 'required|string|regex:/^[A-Za-z. -]+$/|max:120',
			'last_name' => 'required|string|regex:/^[A-Za-z. -]+$/|max:120',
			'email' => 'required|email|max:120|unique:users,email',
			'password' => 'required|max:100|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[^\w\s]).{8,}$/',
			'password_confirmation' => 'required_with:password|same:password',
			'agree' =>'accepted'
		], $messages)->validate();	

		$data = array(
			'first_name' => trim($request->first_name),
			'last_name' => trim($request->last_name),
			'name' => trim($request->first_name).' '.trim($request->last_name),
			'email' => trim($request->email),
			'password' => Hash::make($request->password),
			'is_admin' => 0,
			'user_role' => 4,
		);
		
		$user = User::create($data);
		
		if($user){
			Auth::login($user); //login	
			session()->put('online_visit_patient_id', $user->id);
			
			if(Session::has('online_visit_service_id')){
				$redirect = route('online_visit_welcome');
			}else{
				$redirect = route('user.dashboard');
			}
			
			return response()->json(['status'=> 'success', 'message' => 'Success', 'redirect' => $redirect]);
		}else{
			return response()->json(['status'=> 'error', 'message' => 'Signup Error']);
		}		
			
	}


	public function sign_in(Request $request) {	
		$data['page'] = 'sign_in';
		$data['meta_title'] = 'Sign In';
		$data['meta_description'] = 'ReplenishMD is a new age integrative, Functional, Anti-Aging, Regenerative Medicine, MedSpa, Mobile Lab & Concierge Medical Clinic at your service.';
		
		return view('front.sign_in', $data);
	}
	
    public function signinSubmit(Request $request){   
        $input = $request->all();
   
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required',
        ]);
   
        if(auth()->attempt(array('email' => $input['email'], 'password' => $input['password']))){
			
			if(auth()->user()->status == 1){
				
				if (auth()->user()->is_admin == 0 && (auth()->user()->user_role == 4) ) {
					$msg = 'Login Successfully.';	
					
					if(Session::has('online_visit_service_id')){
						$redirect = route('online_visit_welcome');
						$user_id = auth()->user()->id;
						session()->put('online_visit_patient_id', $user_id);
						return response()->json(['status'=> 'success', 'redirect'=> $redirect, 'message' => $msg]);
					}else{
						$redirect = route('user.dashboard');
						return response()->json(['status'=> 'success', 'redirect'=> $redirect, 'message' => $msg]);
					}
					
				}elseif (auth()->user()->is_admin == 0 && auth()->user()->user_role == 3) {
					$msg = 'Login Successfully.';
					$redirect = route('user.dashboard');					
					return response()->json(['status'=> 'success', 'redirect'=> $redirect, 'message' => $msg]);
					
				}else{
					$msg = 'You are not authorized to login.';	
					return response()->json(['status'=> 'error', 'redirect'=> '', 'message' => $msg]);
				}				
			}else{
				$msg = 'You must be active to login.';
				return response()->json(['status'=> 'error', 'redirect'=> '', 'message' => $msg]);
			}

        }else{
		    $msg = 'Invalid login credentials';
			return response()->json(['status'=> 'error', 'redirect'=> '', 'message' => $msg]);			
        }     
    }
	
	public function forgot_password(Request $request) {	
		$data['page'] = 'forgot_password';
		$data['meta_title'] = 'Forgot Password';
		$data['meta_description'] = 'ReplenishMD is a new age integrative, Functional, Anti-Aging, Regenerative Medicine, MedSpa, Mobile Lab & Concierge Medical Clinic at your service.';
		
		return view('front.forgot_password', $data);
	}

	public function online_visit_welcome(Request $request) {	
		$data['page'] = 'welcome';
		$data['meta_title'] = 'Online Visit Welcome';
		$data['meta_description'] = 'ReplenishMD is a new age integrative, Functional, Anti-Aging, Regenerative Medicine, MedSpa, Mobile Lab & Concierge Medical Clinic at your service.';
		

		if(!Session::has('online_visit_patient_id')){
			return redirect('home')->withErrors('Please choose treatment and signup first.');
		}
		
		$service_id = session('online_visit_service_id');
		$user_id = session('online_visit_patient_id');
		
		$data['service_detail'] = Service::where('id',$service_id)->where('status',1)->first();
		$data['user'] = User::where('id',$user_id)->first();
		
		if($data['service_detail']->count() > 0 && $data['user']->count() > 0){
			return view('front.online_visit_welcome', $data);
		}else{
			return redirect('home')->withErrors('Please choose treatment and signup first.');
		}				
	}

	public function online_visit_basics(Request $request) {	
		$data['page'] = 'basics';
		$data['meta_title'] = 'Online Visit Basics';
		$data['meta_description'] = 'ReplenishMD is a new age integrative, Functional, Anti-Aging, Regenerative Medicine, MedSpa, Mobile Lab & Concierge Medical Clinic at your service.';
		

		if(!Session::has('online_visit_patient_id')){
			return redirect('home')->withErrors('Please choose treatment and signup first.');
		}
		
		$service_id = session('online_visit_service_id');
		$user_id = session('online_visit_patient_id');
		
		$data['service_detail'] = Service::where('id',$service_id)->where('status',1)->first();
		$data['user'] = User::where('id',$user_id)->first();
		//echo date('d-m-Y',strtotime($data['user']->dob));die;
		
		if($data['service_detail']->count() > 0 && $data['user']->count() > 0){
			$data['max_date'] = date('Y-m-d', strtotime('-18 years'));
			return view('front.online_visit_basics', $data);
		}else{
			return redirect('home')->withErrors('Please choose treatment and signup first.');
		}				
	}
	
	public function basicsSubmit(Request $request) {
		
		$user_id = session('online_visit_patient_id');	
		$service_id = session('online_visit_service_id');
		$gender = $request->gender;
		
		$user = User::where('id',$user_id)->first();
		$service_detail = Service::where('id',$service_id)->where('status',1)->first();

		if($service_detail->count() > 0 && $user->count() > 0){
			

			if($service_detail->available_for == $gender || $service_detail->available_for == 'both'){
				$messages = [
					'zip_code.exists' => 'Only Texas zipcodes are allowable.',
				];
				$gender_rule = '';
			}else{
				if($gender == 'male'){
					$gender1 = 'female';
				}else{
					$gender1 = 'male';
				}
				$messages = [
					'gender.in' => 'Unfortunately this treatment is currently not available to '.$gender1.'.',
					'zip_code.exists' => 'Only Texas zipcodes are allowable.',
				];	
				$gender_rule = '|in:'.$gender1;
			}

			$validator = Validator::make($request->all(), [
				'gender' => 'required|string'.$gender_rule,
				'birth_date' => 'required|date',			
				'zip_code' => 'required|digits:5|exists:zip_codes,zipcode',
				'phone_number' => 'required|digits:10',
			], $messages)->validate();	

			$data = array(
				'gender' => trim($request->gender),
				'dob' => date('Y-m-d',strtotime(trim($request->birth_date))),
				'zip_code' => trim($request->zip_code),
				'phone' => trim($request->phone_number),
			);
			
			User::where('id',$user_id)->update($data);	
			
			return response()->json(['status'=> 'success', 'message' => 'Success']);			
		
		}else{
			return response()->json(['status'=> 'error', 'message' => 'Error Occured.']);
		}		
	}
		
	public function medical_questions(Request $request, CommonService $common) {	
		$data['page'] = 'medical_questions';
		$data['meta_title'] = 'Online Visit Medical Questions';
		$data['meta_description'] = 'ReplenishMD is a new age integrative, Functional, Anti-Aging, Regenerative Medicine, MedSpa, Mobile Lab & Concierge Medical Clinic at your service.';

		if(!Session::has('online_visit_patient_id')){
			return redirect('home')->withErrors('Please choose treatment and signup first.');
		}
		
		$service_id = session('online_visit_service_id');
		$user_id = session('online_visit_patient_id');
		
		$data['service_detail'] = Service::where('id',$service_id)->where('status',1)->first();
		$data['user'] = User::where('id',$user_id)->first();
		
		if($data['service_detail']->count() > 0 && $data['user']->count() > 0){
			
			$result = $common->getMedicalQuestions($service_id);

			$data['medical_questions'] = $result['questionData'];

			//echo '<pre>'; print_r($data['medical_questions']); die;
			$data['visit_form'] = $result['visit_form'];
			
			if($data['visit_form'] && $data['visit_form']->count() > 0 && $data['medical_questions']){
				session()->put('online_visit_form_id', $data['visit_form']->id);					
				return view('front.medical_questions', $data);
			}else{
				return redirect('home')->withErrors('This online visit is currently not available.');
			}
			
			
		}else{
			return redirect('home')->withErrors('Please choose treatment and signup first.');
		}				
	}
	
	public function onlineVisitSubmit(Request $request) {
		
		$user_id = session('online_visit_patient_id');	
		$service_id = session('online_visit_service_id');
		$online_visit_form_id = session('online_visit_form_id');
		
		$user = User::where('id',$user_id)->first();
		$service_detail = Service::where('id',$service_id)->where('status',1)->first();
		$visit_form = VisitForm::where('id', $online_visit_form_id)->where('status',1)->first();

		if($service_detail->count() > 0 && $user->count() > 0 && $visit_form->count() > 0){
			
			$req = $request->all();

			//echo '<pre>'; print_R($req); die;
			
			//Option Answers
			if(isset($req['answers']) && count($req['answers']) > 0){

				if(isset($req['answers']['option']) && count($req['answers']['option']) > 0){
					$option_answers = $req['answers']['option'];
					
					//Delete Old Answer if updating
					VisitAnswer::where('user_id',$user_id)
								->where('service_id',$service_id)
								->where('online_visit_form_id',$online_visit_form_id)
								->delete();

					foreach($option_answers as $q => $options){

						if($options){
							foreach($options as $key => $val){

								if($val != ''){
									$question_id = $q;
									$answer_type = 'option';
									$answer_text = NULL;
									$answer_option_id = $val;

									$data = array(
										'user_id' => $user_id,
										'service_id' => $service_id,
										'online_visit_form_id' => $online_visit_form_id,
										'question_id' => $question_id,
										'answer_type' => $answer_type,
										'answer_text' => $answer_text,
										'answer_option_id' => $answer_option_id,
									); 
									VisitAnswer::create($data);
								}
							}
						}
					}						
				}
				
				//Text Answers
				if(isset($req['answers']['text']) && count($req['answers']['text']) > 0){
					$text_answers = $req['answers']['text'];

					foreach($text_answers as $q => $val){

						if($val != ''){
							$question_id = $q;
							$answer_type = 'text';
							$answer_text = $val;
							$answer_option_id = NULL;

							$data = array(
								'user_id' => $user_id,
								'service_id' => $service_id,
								'online_visit_form_id' => $online_visit_form_id,
								'question_id' => $question_id,
								'answer_type' => $answer_type,
								'answer_text' => $answer_text,
								'answer_option_id' => $answer_option_id,
							); 
							
							VisitAnswer::create($data);
						}						
					}					
				}				

			}	
			
			session()->put('online_visit_questions_done', true);

			return response()->json(['status'=> 'success', 'message' => 'Success']);			
		
		}else{
			return response()->json(['status'=> 'error', 'message' => 'Error Occured.']);
		}		
	}
	
	
	public function treatment_preference(Request $request) {	
		$data['page'] = 'treatment_preference';
		$data['meta_title'] = 'Online Visit - Treatment Preference';
		$data['meta_description'] = 'ReplenishMD is a new age integrative, Functional, Anti-Aging, Regenerative Medicine, MedSpa, Mobile Lab & Concierge Medical Clinic at your service.';
		
		session()->put('online_visit_questions_done', true);

		if(!Session::has('online_visit_patient_id')){
			return redirect('home')->withErrors('Please login first.');
		}

		if(!Session::has('online_visit_service_id')){
			return redirect('home')->withErrors('Please choose treatment first.');
		}
		
		if(!Session::has('online_visit_questions_done')){
			return redirect('medical_questions')->withErrors('Please complete online visit questions first.');
		}			
		
		$service_id = session('online_visit_service_id');
		$user_id = session('online_visit_patient_id');
		
		$data['service_detail'] = Service::where('id',$service_id)->where('status',1)->first();
		$data['user'] = User::where('id',$user_id)->first();
		
		if($data['service_detail']->count() > 0 && $data['user']->count() > 0){

			$data['treatment_medicines'] = TreatmentMedicine::where('service_id',$service_id)->where('status',1)->get();
			return view('front.treatment_preference', $data);
		}else{
			return redirect('home')->withErrors('Some Error occured.');
		}				
	}
	
	
}
