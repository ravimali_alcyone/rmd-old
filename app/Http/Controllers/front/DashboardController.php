<?php

namespace App\Http\Controllers\front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Session;
use App\Library\Services\CommonService;
use App\User;
use App\Testimonial;
use App\Sponsor;
use App\ServicePricing;
use App\Faq;
use App\FaqCategory;
use App\ContactUs;
use App\ProviderCategory;
use App\Service;
use App\TreatmentMedicine;
use App\VisitForm;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
		 $this->middleware('user_auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
	 
    public function index() {
			
        $data['page'] = 'user_dashboard';
		$data['meta_title'] = 'Dashboard';
		$data['meta_description'] = 'ReplenishMD is a new age integrative, Functional, Anti-Aging, Regenerative Medicine, MedSpa, Mobile Lab & Concierge Medical Clinic at your service.';		
        return view('front.user.index', $data);
    }
	
    public function profile() {
        $data['page'] = 'user_profile';
		$data['meta_title'] = 'My Profile';
		$data['meta_description'] = 'ReplenishMD is a new age integrative, Functional, Anti-Aging, Regenerative Medicine, MedSpa, Mobile Lab & Concierge Medical Clinic at your service.';		
        return view('front.user.profile', $data);
    }

    public function visit_records() {
        $data['page'] = 'user_visit_records';
		$data['meta_title'] = 'My Visit Records';
		$data['meta_description'] = 'ReplenishMD is a new age integrative, Functional, Anti-Aging, Regenerative Medicine, MedSpa, Mobile Lab & Concierge Medical Clinic at your service.';		
        return view('front.user.visit_records', $data);
    }

    public function subscriptions() {
        $data['page'] = 'user_subscriptions';
		$data['meta_title'] = 'My Subscriptions';
		$data['meta_description'] = 'ReplenishMD is a new age integrative, Functional, Anti-Aging, Regenerative Medicine, MedSpa, Mobile Lab & Concierge Medical Clinic at your service.';		
        return view('front.user.subscriptions', $data);
    }

    public function orders() {
        $data['page'] = 'user_orders';
		$data['meta_title'] = 'My Orders';
		$data['meta_description'] = 'ReplenishMD is a new age integrative, Functional, Anti-Aging, Regenerative Medicine, MedSpa, Mobile Lab & Concierge Medical Clinic at your service.';		
        return view('front.user.orders', $data);
    }
	
}
