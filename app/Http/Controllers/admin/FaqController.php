<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\FaqCategory;
use App\Faq;
use DataTables;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

class FaqController extends Controller {

    public function __construct(){
        $this->middleware('auth');
	}

    public function index(Request $request){

		if ($request->ajax()) {
			$data = Faq::select('faqs.id', 'faqs.category_id', 'c.name AS category','faqs.faq_data','faqs.status AS status')
			->join('faq_categories AS c','c.id','=','faqs.category_id')
			->orderBy('faqs.created_at','ASC');

			$data = $data->get();

            return Datatables::of($data)->addIndexColumn()
				->addColumn('action', function($row){
					$edit_url = env('APP_URL').'/admin/faqs/add/'.$row->id;

					$btn = '<div class="flex items-center"><a href="'.$edit_url.'" class="button button--sm w-16 shadow-md mr-1 mb-2 btn_blue text-white editUser" data-id="23" title="Edit"> Edit </a>';
					$btn = $btn.'<div class="flex items-center"><a href="javascript:void(0)" onclick="deleteRow('.$row->id.')" class="button button--sm w-16 shadow-md mr-1 mb-2 btn_red text-white deleteUser" data-id="23" title="Edit"> Delete </a>';
					return $btn;
				})
				->addColumn('category', function($row){
					$category = $row->category;
					return $category;
				})
				->addColumn('status', function($row){
					if($row->status == 1){
						// $status = '<span class="chip green lighten-5"><span class="green-text">Active</span></span>';
						$status = '<input type="checkbox" name="status" class="input input--switch border" title="Status" checked onchange="changeStatus('.$row->id.')">';
					}else{
						// $status = '<span class="chip red lighten-5"><span class="red-text">Blocked</span></span>';
						$status = '<input type="checkbox" name="status" class="input input--switch border" title="Status" onchange="changeStatus('.$row->id.')">';
					}
					return $status;
				})				
				->addColumn('faq_rows', function($row) {
					$faq_rows = '';
					$modal_number = rand(1, 1000);
					if($row->faq_data != ''){
						$faq_data = json_decode($row->faq_data, true);

						if($faq_data){
							$faqsData =	'<div>
										<a href="javascript:;" data-toggle="modal" data-target="#button-modal-preview-'.$modal_number.'" class="button button--sm w-20 shadow-md mr-1 mb-2 btn_gray text-white">View All</a> </div>
										<div class="modal" id="button-modal-preview-'.$modal_number.'">
										<div class="modal__content modal__content--xl relative"> <a data-dismiss="modal" href="javascript:;" class="absolute right-0 top-0 mt-3 mr-3"> <i data-feather="x" class="w-8 h-8 text-gray-500"></i> </a>
										<div class="p-5 text-left faq_modal"> <i data-feather="check-circle" class="w-16 h-16 text-theme-9 mx-auto mt-3"></i>';
							foreach($faq_data as $faq) {
								$faqsData .= '<p><b>Question:</b> '.$faq["question"].'</p><p class="mb-5"><b>Answer:</b> '.$faq['answer'].'</p>';
							}
							$faqsData .= '</div>
										</div>
										</div>';
							$faq_rows .= $faqsData;
						}
					}
					return $faq_rows;
				})
				->rawColumns(['category','faq_rows','status','action'])
				->make(true);
        }
		$data['page'] = 'faq';
		$data['categories'] = FaqCategory::where('parent_id', 0)->where('status', 1)->get();
        return view('admin.faq.index', $data);
    }

	public function add(){
		$data['categories'] = FaqCategory::where('parent_id', 0)->where('status', 1)->get();
		$data['sub_categories'] = array();
		$data['faq'] = array();
		$data['page'] = 'faq';
		return view('admin.faq.add', $data);
	}

    public function store(Request $request){
		$req = $request->all();
		
		$faq_data = '';
		$faq_rows = array();

		if(isset($req['status']) && $req['status'] == 'on'){
			$status = 1;
		}else{
			$status = 0;
		}
				
		if(isset($req['faq_data']) && count($req['faq_data']) > 0){
			foreach($req['faq_data'] as $type => $value){
				if($type == 'question'){
					$i=0;
					foreach($value as $row){
						$faq_rows[$i]['question'] = $row;
						$i++;
					}
				}elseif($type == 'answer'){
					$j=0;
					foreach($value as $row){
						$faq_rows[$j]['answer'] = $row;
						$j++;
					}
				}
			}
		}
		if($faq_rows){
			$faq_data = json_encode($faq_rows);
		}

		$faq = Faq::where([
			['category_id', $req['category_id']],
			])->first();

		$where = ['id' => $req['id']];
		$input_data = [
			'category_id' => $req['category_id'],
			'status' => $status,
			'faq_data'=> $faq_data
		];

		if ($request->get('id') == '') {
			if ($faq && $faq->count() > 0) {
				$new_faq_data = json_decode($faq->faq_data);
				$new_faq_data = array_merge($new_faq_data, json_decode($faq_data));

				$input_data_2 = [
					'category_id' => $req['category_id'],
					'status' => $status,
					'faq_data'=> $new_faq_data
				];
				Faq::where([
					['id' => $req['id']],
					['category_id', $req['category_id']],
					])->update($input_data_2);
				// Faq::updateOrCreate($where, $input_data);
			} else {
				Faq::create($input_data);
			}
		} else{
			Faq::updateOrCreate($where, $input_data);
		}
        return response()->json(['status'=>'success', 'message'=>'Saved successfully.']);
    }

    public function edit($id){
        $faq = Faq::find($id);
		if($faq && $faq->count() > 0){
			$data['categories'] = FaqCategory::where('parent_id', 0)->where('status', 1)->get();
			$data['faq'] = $faq;
			$data['page'] = 'faq';
			return view('admin.faq.add', $data);
		} else {
			return redirect()->route('admin.faqs');
		}
    }

    public function destroy($id){
        Faq::find($id)->delete();
        return response()->json(['status'=>'success', 'message'=>'Deleted successfully.']);
    }
	
	public function change_status(Request $request) {
		$user = Faq::where('id', $request->id)->first();
		if ($user->status) {
			$user = Faq::where('id', $request->id)->update(array('status' => 0));
		} else {
			$user = Faq::where('id', $request->id)->update(array('status' => 1));
		}
	}	
}
