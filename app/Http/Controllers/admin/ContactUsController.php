<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ContactUs;
use DataTables;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\Library\Services\CommonService;

class ContactUsController extends Controller {

    public function __construct(){
        $this->middleware('auth');
	}

    public function index(Request $request, CommonService $common){

		if ($request->ajax()) {
			
			$where = array();
			
			if($request->filter_status != ''){
				$where['status'] = $request->filter_status;
			}			
			
			$data = $common->getTableListing($request, $table='contact_us', $where, array(), $isLatest=true);
						
            return Datatables::of($data)->addIndexColumn()
				->addColumn('action', function($row){
					$edit_url = env('APP_URL').'/admin/contact_us/add/'.$row->id;

					$btn = '<div class="flex items-center"><a href="javascript:void(0)" onclick="deleteRow('.$row->id.')" class="button button--sm w-16 shadow-md mr-1 mb-2 btn_red text-white deleteUser" data-id="23" title="Edit"> Delete </a>';
					return $btn;
				})
				->addColumn('user_category', function($row){
					if($row->user_category == 1){ $user_category = 'Visitor'; }
					elseif($row->user_category == 2){ $user_category = 'Provider'; }
					elseif($row->user_category == 3){ $user_category = 'Patient'; }
					else{ $user_category = ''; }
					
					return $user_category;
				})					
				->addColumn('status', function($row){
					if($row->status == 1){
						$status = '<input type="checkbox" name="status" class="input input--switch border"  title="Read" checked onchange="changeStatus('.$row->id.')">';
					}else{
						$status = '<input type="checkbox" name="status" class="input input--switch border"  title="Unread" onchange="changeStatus('.$row->id.')">';
					}
					return $status;
				})	
				->addColumn('subject', function($row){
					$subject = $row->subject;
					return $subject;
				})					
				->rawColumns(['user_category','name','status','action'])
				->make(true);
        }
		$data['page'] = 'contact_us';
        return view('admin.contact_us.index', $data);
    }

    public function destroy($id){
        ContactUs::find($id)->delete();
        return response()->json(['status'=>'success', 'message'=>'Deleted successfully.']);
    }
	
	public function change_status(Request $request) {
		$user = ContactUs::where('id', $request->id)->first();
		if ($user->status) {
			$user = ContactUs::where('id', $request->id)->update(array('status' => 0));
		} else {
			$user = ContactUs::where('id', $request->id)->update(array('status' => 1));
		}
	}
}
