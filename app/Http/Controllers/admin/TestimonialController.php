<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Testimonial;
use DataTables;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

class TestimonialController extends Controller {

    public function __construct(){
        $this->middleware('auth');
	}

    public function index(Request $request){

		if ($request->ajax()) {
			$data = Testimonial::select('*');

			$data = $data->get();

            return Datatables::of($data)->addIndexColumn()
				->addColumn('action', function($row){
					$edit_url = env('APP_URL').'/admin/testimonials/add/'.$row->id;

					$btn = '<div class="flex items-center"><a href="'.$edit_url.'" class="button button--sm w-16 shadow-md mr-1 mb-2 btn_blue text-white editUser" data-id="23" title="Edit"> Edit </a>';
					$btn = $btn.'<div class="flex items-center"><a href="javascript:void(0)" onclick="deleteRow('.$row->id.')" class="button button--sm w-16 shadow-md mr-1 mb-2 btn_red text-white deleteUser" data-id="23" title="Edit"> Delete </a>';
					return $btn;
				})
				->addColumn('image', function($row){
					$image = '<div class="flex">';
					
					if ($row->image != null) {
						$image .= '<div class="w-20 h-20 image-fit zoom-in">
							<img class="tooltip rounded-full tooltipstered" src="/'.$row->image.'">
						</div>';
						
					}
					$image	.= '</div>';
					return $image;
				})
				->addColumn('comments', function($row){
					$comments = $this->limit_text($row->comments, 5);
					return $comments;
				})				
				->addColumn('status', function($row){
					if($row->status == 1){
						$status = '<input type="checkbox" name="status" class="input input--switch border"  title="Status" checked onchange="changeStatus('.$row->id.')">';
					}else{
						$status = '<input type="checkbox" name="status" class="input input--switch border"  title="Status" onchange="changeStatus('.$row->id.')">';
					}
					return $status;
				})				
				->rawColumns(['image','comments','star_rating','status','action'])
				->make(true);
        }
		$data['parent_page'] = 'website_content';
		$data['page'] = 'testimonials';
        return view('admin.testimonials.index', $data);
    }

	public function add(){
		$data['testimonial'] = array();
		$data['parent_page'] = 'website_content';
		$data['page'] = 'testimonials';
		return view('admin.testimonials.add', $data);
	}

    public function store(Request $request){
		$req = $request->all();

		if ($request->get('page') == 'edit') {
			if (!empty($request->input('old_image'))) {
				$image = $request->input('old_image');
			}
		}

		if (!empty($request->file('image'))) {
				$file = $request->file('image');
                $name = time().'.'.$file->getClientOriginalName();
                $destinationPath = 'uploads/testimonials/';
                $file->move($destinationPath, $name);
                $image = 'uploads/testimonials'.'/'.$name;
        }
		
		if ($request->status == 'on') {
			$status = 1;
		} else {
			$status = 0;
		}
		
		$where = ['id' => $req['id']];
		$input_data = [
			'name' => $req['name'],
			'comments' => $req['comments'],
			'star_rating' => $req['star_rating'],
			'image' => isset($image) ? $image : 'images/default.jpg',
			'status' => $status,
		];

		Testimonial::updateOrCreate($where, $input_data);
        return response()->json(['status'=>'success', 'message'=>'Saved successfully.']);
    }

    public function edit($id){
        $testimonial = Testimonial::find($id);
		
		if($testimonial && $testimonial->count() > 0){
			$data['testimonial'] = $testimonial;
			$data['parent_page'] = 'website_content';
			$data['page'] = 'testimonials';
			return view('admin.testimonials.add', $data);
		} else {
			return redirect()->route('admin.testimonials');
		}
    }

    public function destroy($id){
        Testimonial::find($id)->delete();
        return response()->json(['status'=>'success', 'message'=>'Deleted successfully.']);
    }
	
	public function change_status(Request $request) {
		$user = Testimonial::where('id', $request->id)->first();
		if ($user->status) {
			$user = Testimonial::where('id', $request->id)->update(array('status' => 0));
		} else {
			$user = Testimonial::where('id', $request->id)->update(array('status' => 1));
		}
	}

	public function limit_text($text, $limit) {
		if (str_word_count($text, 0) > $limit) {
			$words = str_word_count($text, 2);
			$pos   = array_keys($words);
			$text  = substr($text, 0, $pos[$limit]) . '...';
		}
		return $text;
	}	
}
