<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index() {
        $data['page'] = 'dashboard';
        return view('admin.home', $data);
    }
	
    public function profile() {
        $data['page'] = 'profile';
		$data['user'] = auth()->user();
        return view('admin.profile', $data);
    }

    public function change_password(Request $request){
		$req = $request->all();
        $this->validate($request, [
			'old_password' => ['required'],
			'password' => ['required', 'string', 'min:6', 'confirmed','different:old_password'],
			
        ]);
		
		if (!Hash::check($req['old_password'], auth()->user()->password)) {
			return response()->json(['status'=>'error', 'message'=>'Incorrect Old Password']);
		}
		
		
		$password = Hash::make($req['password']);

		$input_data = [
			'password' => $password,
			'updated_at' => date('Y-m-d H:i:s')
		];

        User::where('id',auth()->user()->id)->update($input_data);
		
        return response()->json(['status'=>'success', 'message'=>'Password Changed Successfully.']);
    }	
	
    public function contact_details(Request $request){
		$req = $request->all();
        $this->validate($request, [
			'email' => ['required','string','email'],			
			'phone' => ['max:20'],
			'address' => ['max:150'],
        ]);
		
		$input_data = [
			'email' => $req['email'],
			'phone' => $req['phone'],
			'address' => $req['address'],
			'updated_at' => date('Y-m-d H:i:s')
		];

        User::where('id',auth()->user()->id)->update($input_data);
		
        return response()->json(['status'=>'success', 'message'=>'Details Updated Successfully.']);
    }

	public function profile_image(Request $request){
		
		if (!empty($request->file('image'))) {
            $file = $request->file('image');
			$name=time().'.'.$file->getClientOriginalName();
			$destinationPath = 'uploads/users/';
			$file->move($destinationPath, $name);
			$image = 'uploads/users'.'/'.$name;
            
        }

		$input_data = [
			'image' => isset($image) ? $image : 'images/default.jpg',
			'updated_at' => date('Y-m-d H:i:s')
		];

        User::where('id',auth()->user()->id)->update($input_data);
		
        return response()->json(['status'=>'success', 'message'=>'Profile Image Changed Successfully.']);		
	}
}
