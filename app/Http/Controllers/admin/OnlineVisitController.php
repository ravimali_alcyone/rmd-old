<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\OnlineVisit;
use App\Service;
use App\OnlineVisitStatusTimeline;
use DataTables;
use DB;

class OnlineVisitController extends Controller {

    public function __construct(){
        $this->middleware('auth');
    }

    public function index(Request $request){
		if ($request->ajax()) {
			$data = OnlineVisit::select('online_visits.*','users.name AS patient_name','users.gender','services.name AS service_name','providers.name AS provider_name')
					->join('users', function($join) {
						$join->on('users.id', '=', 'online_visits.patient_id');
					})
					->leftJoin('online_visit_provider_assign', function($join) {
						$join->on('online_visit_provider_assign.online_visit_id', '=', 'online_visits.id');
					})
					->leftJoin('users AS providers', function($join) {
						$join->on('online_visit_provider_assign.provider_id', '=', 'providers.id');
					})
					->join('services', function($join) {
						$join->on('services.id', '=', 'online_visits.service_id');
					});

			if($request->service_id != ''){
				$data = $data->where('online_visits.service_id',$request->service_id);
			}

			$data = $data->where('online_visits.visit_type','online')->latest()->get();

            return Datatables::of($data)
				->addIndexColumn()
				->addColumn('action', function($row){
					$view_url = env('APP_URL').'/admin/online_visits/view/'.$row->id;
					$btn = '<div class="flex items-center"><a href="'.$view_url.'" data-id="'.$row->id.'" title="View" class="button button--sm w-16 shadow-md mr-1 mb-2 btn_gray viewUser">View</a>';

					$btn = $btn.' <a href="javascript:void(0)" onclick="deleteRow('.$row->id.')" data-id="'.$row->id.'" title="Delete" class="button button--sm w-16 shadow-md mr-1 mb-2 btn_red deleteUser">Delete</a>';

					$btn = $btn.'</div>';
					return $btn;
				})
				->addColumn('visit_type', function($row){
					if($row->visit_type == 'online'){
						$visit_type = 'Online';
					}elseif($row->visit_type == 'concierge'){
						$visit_type = 'Concierge';
					}
					return $visit_type;
				})

				->addColumn('treatment', function($row){
					$treatment = $row->service_name;
					return $treatment;
				})
				->addColumn('provider_name', function($row){
					$provider_name = $row->provider_name;
					return $provider_name;
				})
				->addColumn('datetime', function($row){
					$datetime = date('m-d-Y H:i',strtotime($row->created_at));
					//$datetime = $row->created_at;
					return $datetime;
				})
				->addColumn('visit_status', function($row){
					// $visit_status = $this->getFullText($row->visit_status);
					// return $visit_status;
					$html = '<select data-hide-search="true" class="select2 w-full visit_status_btn visit_status" onchange="changeStatus('.$row->id.', this.options[this.selectedIndex].value)">';
					$html .= '<option value="in_pool" '.($row->visit_status == 'in_pool' ? 'selected' : ' ').'>In-Pool</option>';
					$html .= '<option value="not_assigned" '.($row->visit_status == 'not_assigned' ? 'selected' : ' ').'>Not Assigned</option>';
					$html .= '<option value="auto_assigned" '.($row->visit_status == 'auto_assigned' ? 'selected' : ' ').' disabled>Auto Assigned</option>';
					$html .= '<option value="picked" '.($row->visit_status == 'picked' ? 'selected' : ' ').' disabled>Picked</option>';
					$html .= '<option value="reviewed" '.($row->visit_status == 'reviewed' ? 'selected' : ' ').'>Reviewed</option>';
					$html .= '<option value="contacted" '.($row->visit_status == 'contacted' ? 'selected' : ' ').'>Contacted</option>';
					$html .= '<option value="report_submitted" '.($row->visit_status == 'report_submitted' ? 'selected' : ' ').'>Report Submitted</option>';
					$html .= '</select>';
					return $html;
				})
				->rawColumns(['name','visit_type','treatment','provider_name','datetime','visit_status','action'])
				->make(true);
        }

		$data['page'] = 'online_visits';
		$data['services'] = Service::where('visit_type', 'online')->where('status', 1)->get();
		
		//Update New Status
		OnlineVisit::where('is_new',1)->where('visit_type','online')->update(array('is_new' => 0));
		
        return view('admin.online_visit.index', $data);
    }

    public function index2(Request $request){
		if ($request->ajax()) {
			$data = OnlineVisit::select('online_visits.*','users.name AS patient_name','users.gender','services.name AS service_name','providers.name AS provider_name')
					->join('users', function($join) {
						$join->on('users.id', '=', 'online_visits.patient_id');
					})
					->leftJoin('online_visit_provider_assign', function($join) {
						$join->on('online_visit_provider_assign.online_visit_id', '=', 'online_visits.id');
					})
					->leftJoin('users AS providers', function($join) {
						$join->on('online_visit_provider_assign.provider_id', '=', 'providers.id');
					})
					->join('services', function($join) {
						$join->on('services.id', '=', 'online_visits.service_id');
					});

			if($request->service_id != ''){
				$data = $data->where('online_visits.service_id',$request->service_id);
			}

			$data = $data->where('online_visits.visit_type','concierge')->latest()->get();

            return Datatables::of($data)
				->addIndexColumn()
				->addColumn('action', function($row){
					$view_url = env('APP_URL').'/admin/concierge_visits/view/'.$row->id;
					$btn = '<div class="flex items-center"><a href="'.$view_url.'" data-id="'.$row->id.'" title="View" class="button button--sm w-16 shadow-md mr-1 mb-2 btn_gray viewUser">View</a>';

					$btn = $btn.' <a href="javascript:void(0)" onclick="deleteRow('.$row->id.')" data-id="'.$row->id.'" title="Delete" class="button button--sm w-16 shadow-md mr-1 mb-2 btn_red deleteUser">Delete</a>';

					$btn = $btn.'</div>';
					return $btn;
				})
				->addColumn('visit_type', function($row){
					if($row->visit_type == 'online'){
						$visit_type = 'Online';
					}elseif($row->visit_type == 'concierge'){
						$visit_type = 'Concierge';
					}
					return $visit_type;
				})

				->addColumn('treatment', function($row){
					$treatment = $row->service_name;
					return $treatment;
				})
				->addColumn('provider_name', function($row){
					$provider_name = $row->provider_name;
					return $provider_name;
				})
				->addColumn('datetime', function($row){
					$datetime = date('m-d-Y H:i',strtotime($row->created_at));
					//$datetime = $row->created_at;
					return $datetime;
				})
				->addColumn('visit_status', function($row){
					// $visit_status = $this->getFullText($row->visit_status);
					// return $visit_status;
					$html = '<select data-hide-search="true" class="select2 w-full visit_status_btn visit_status" onchange="changeStatus('.$row->id.', this.options[this.selectedIndex].value)">';
					$html .= '<option value="in_pool" '.($row->visit_status == 'in_pool' ? 'selected' : ' ').'>In-Pool</option>';
					$html .= '<option value="not_assigned" '.($row->visit_status == 'not_assigned' ? 'selected' : ' ').'>Not Assigned</option>';
					$html .= '<option value="auto_assigned" '.($row->visit_status == 'auto_assigned' ? 'selected' : ' ').' disabled>Auto Assigned</option>';
					$html .= '<option value="picked" '.($row->visit_status == 'picked' ? 'selected' : ' ').' disabled>Picked</option>';
					$html .= '<option value="reviewed" '.($row->visit_status == 'reviewed' ? 'selected' : ' ').'>Reviewed</option>';
					$html .= '<option value="contacted" '.($row->visit_status == 'contacted' ? 'selected' : ' ').'>Contacted</option>';
					$html .= '<option value="report_submitted" '.($row->visit_status == 'report_submitted' ? 'selected' : ' ').'>Report Submitted</option>';
					$html .= '</select>';
					return $html;
				})
				->rawColumns(['name','visit_type','treatment','provider_name','datetime','visit_status','action'])
				->make(true);
        }

		$data['page'] = 'concierge_visits';
		$data['services'] = Service::where('visit_type', 'concierge')->where('status', 1)->get();
		
		//Update New Status
		OnlineVisit::where('is_new',1)->where('visit_type','concierge')->update(array('is_new' => 0));
			
        return view('admin.online_visit.index', $data);
    }

    public function destroy($id){
        OnlineVisit::find($id)->delete();
        return response()->json(['status'=>'success', 'message'=>'Deleted successfully.']);
	}

	public function change_status(Request $request) {
		$visit = OnlineVisit::where('id', $request->id)->first();
		// if ($user->status) {
		$result = OnlineVisit::where('id', $request->id)->update(array('visit_status' => $request->visit_status));

		$input_data = array('online_visit_id' => $request->id, 'old_visit_status' => $visit->visit_status, 'new_visit_status' => $request->visit_status);
		$status_timeline = OnlineVisitStatusTimeline::create($input_data);

		if ($result && $status_timeline) {
			return response()->json(['status'=>'success', 'message'=>'Visit status changed successfully.']);
		}
	}

	public function view($id) {

		$online_visit = OnlineVisit::select('online_visits.*','users.id AS patient_id','users.name AS patient_name','users.image','users.gender','users.email','users.dob','users.age','users.phone','users.address','services.name AS service_name','providers.name AS provider_name','providers.id AS provider_id')
		
		->join('users', function($join) {
			$join->on('users.id', '=', 'online_visits.patient_id');
		})
		->leftJoin('online_visit_provider_assign', function($join) {
			$join->on('online_visit_provider_assign.online_visit_id', '=', 'online_visits.id');
		})
		->leftJoin('users AS providers', function($join) {
			$join->on('online_visit_provider_assign.provider_id', '=', 'providers.id');
		})
		->join('services', function($join) {
			$join->on('services.id', '=', 'online_visits.service_id');
		});

		$online_visit = $online_visit->where('online_visits.id',$id)->where('online_visits.visit_type','online')->first();
		
		if(!$online_visit){
			return redirect()->back();
		}

		$data['online_visit'] = $online_visit;
				
		$data['status_timeline'] = OnlineVisitStatusTimeline::where('online_visit_id',$id)->get();
		$data['page'] = 'online_visits';
		return view('admin.online_visit.view', $data);
	}

	public function view2($id) {

		$online_visit = OnlineVisit::select('online_visits.*','users.id AS patient_id','users.name AS patient_name','users.image','users.gender','users.email','users.dob','users.age','users.phone','users.address','services.name AS service_name','providers.name AS provider_name','providers.id AS provider_id')
		
		->join('users', function($join) {
			$join->on('users.id', '=', 'online_visits.patient_id');
		})
		->leftJoin('online_visit_provider_assign', function($join) {
			$join->on('online_visit_provider_assign.online_visit_id', '=', 'online_visits.id');
		})
		->leftJoin('users AS providers', function($join) {
			$join->on('online_visit_provider_assign.provider_id', '=', 'providers.id');
		})
		->join('services', function($join) {
			$join->on('services.id', '=', 'online_visits.service_id');
		});

		$online_visit = $online_visit->where('online_visits.id',$id)->where('online_visits.visit_type','concierge')->first();
		
		if(!$online_visit){
			return redirect()->back();
		}		

		$data['online_visit'] = $online_visit;
		$data['status_timeline'] = OnlineVisitStatusTimeline::where('online_visit_id',$id)->get();
		$data['page'] = 'concierge_visits';
		return view('admin.online_visit.view', $data);
	}

	public static function getFullText($text){
		$result = '';

		if($text == 'auto_assigned') { $result = 'Auto Assigned'; }
		elseif($text == 'in_pool') { $result = 'In-Pool'; }
		elseif($text == 'not_assigned') { $result = 'Not Assigned'; }
		elseif($text == 'picked') { $result = 'Picked'; }
		elseif($text == 'reviewed') { $result = 'Reviewed'; }
		elseif($text == 'contacted') { $result = 'Contacted'; }
		elseif($text == 'report_submitted') { $result = 'Report Submitted'; }

		return $result;
	}
	
	public static function getVisitCount($type){
		return $count = OnlineVisit::where('is_new',1)->where('visit_type',$type)->count();		
	}
	
}
