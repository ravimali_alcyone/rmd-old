<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ServicePricing;
use App\AllPlanFeatures;
use App\SelectedPlanFeatures;
use DataTables;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

class ServicePricingController extends Controller {

    public function __construct(){
        $this->middleware('auth');
	}

    public function index(Request $request){

		if ($request->ajax()) {
			$data = ServicePricing::select('*');

			$data = $data->latest()->get();

            return Datatables::of($data)->addIndexColumn()
				->addColumn('action', function($row){
					$edit_url = env('APP_URL').'/admin/service_pricing/add/'.$row->id;

					$btn = '<div class="flex items-center"><a href="'.$edit_url.'" class="button button--sm w-16 shadow-md mr-1 mb-2 btn_blue text-white editUser" data-id="23" title="Edit"> Edit </a>';
					$btn = $btn.'<div class="flex items-center"><a href="javascript:void(0)" onclick="deleteRow('.$row->id.')" class="button button--sm w-16 shadow-md mr-1 mb-2 btn_red text-white deleteUser" data-id="23" title="Edit"> Delete </a>';
					return $btn;
				})
				->addColumn('features', function($row) {
					$features = '';
					$modal_number = rand(1, 1000);
					
					$feature_data = SelectedPlanFeatures::select('all_plan_features.name','all_plan_features.detail')
									->join('all_plan_features','all_plan_features.id','=','selected_plan_features.plan_feature_id')
									->where('selected_plan_features.plan_id',$row->id)
									->where('selected_plan_features.status',1)
									->get()
									->toArray();
					
					if($feature_data){

						$service_pricingData =	'<div>
									<a href="javascript:;" data-toggle="modal" data-target="#button-modal-preview-'.$modal_number.'" class="button button--sm w-20 shadow-md mr-1 mb-2 btn_gray text-white">View All</a> </div>
									<div class="modal" id="button-modal-preview-'.$modal_number.'">
									<div class="modal__content modal__content--xl relative"> <a data-dismiss="modal" href="javascript:;" class="absolute right-0 top-0 mt-3 mr-3"> <i data-feather="x" class="w-8 h-8 text-gray-500"></i> </a>
									<div class="p-5 text-left faq_modal"> <i data-feather="check-circle" class="w-16 h-16 text-theme-9 mx-auto mt-3"></i>';
									
						foreach($feature_data as $key => $val) {
							$service_pricingData .= '<p><b>Feature:</b> '.$val["name"].'</p><p class="mb-5"><b>Description:</b> '.$val["detail"].'</p>';
						}
						$service_pricingData .= '</div>
									</div>
									</div>';
						$features .= $service_pricingData;
						
					}
					
					return $features;
				})
				->addColumn('status', function($row){
					if($row->status == 1){
						// $status = '<span class="chip green lighten-5"><span class="green-text">Active</span></span>';
						$status = '<input type="checkbox" name="status" class="input input--switch border"  title="Status" checked onchange="changeStatus('.$row->id.')">';
					}else{
						// $status = '<span class="chip red lighten-5"><span class="red-text">Blocked</span></span>';
						$status = '<input type="checkbox" name="status" class="input input--switch border"  title="Status" onchange="changeStatus('.$row->id.')">';
					}
					return $status;
				})				
				->rawColumns(['features','status','action'])
				->make(true);
        }
		$data['page'] = 'service_pricing';
        return view('admin.service_pricing.index', $data);
    }

	public function add(){
		$data['service_pricing'] = array();
		$data['all_features'] = AllPlanFeatures::where('status',1)->get();
		$data['selected_features'] = array();
		$data['page'] = 'service_pricing';
		return view('admin.service_pricing.add', $data);
	}

    public function store(Request $request){
		$req = $request->all();
		
		if ($request->status == 'on') {
			$status = 1;
		} else {
			$status = 0;
		}
		
		$where = ['id' => $req['id']];
		$input_data = [
			'name' => $req['name'],
			'total_months' => $req['total_months'],
			'is_monthly' => $req['is_monthly'],
			'monthly_price' => $req['monthly_price'],
			'monthly_plan_text' => $req['monthly_plan_text'],
			'is_total' => $req['is_total'],
			'total_price' => $req['total_price'],
			'total_plan_text' => $req['total_plan_text'],
			'special' => $req['special'],
			'detail' => $req['detail'],
			'is_most_popular' => $req['is_most_popular'],
			'status' => $status
		];

		$plan_id = ServicePricing::updateOrCreate($where, $input_data)->id;
		
		$selected_features = SelectedPlanFeatures::select('plan_feature_id')->where('status',1)->where('plan_id',$plan_id)->pluck('plan_feature_id')->toArray();
		
		if($selected_features){
			SelectedPlanFeatures::where('plan_id',$plan_id)->delete();
		}

		if(isset($req['features']) && count(['features']) > 0){
			
			foreach($req['features'] as $key => $value){
									
				$i_data = array(
					'plan_id' => $plan_id,
					'plan_feature_id' => $value,
					'status' => 1
				);
				
				SelectedPlanFeatures::create($i_data);
				
			}
		}		
		
        return response()->json(['status'=>'success', 'message'=>'Saved successfully.']);
    }

    public function edit($id){
        $service_pricing = ServicePricing::find($id);
		
		if($service_pricing && $service_pricing->count() > 0){
			$data['service_pricing'] = $service_pricing;
			$data['all_features'] = AllPlanFeatures::where('status',1)->get();
			$data['selected_features'] = SelectedPlanFeatures::select('plan_feature_id')->where('status',1)->where('plan_id',$id)->pluck('plan_feature_id')->toArray();
			$data['page'] = 'service_pricing';
			return view('admin.service_pricing.add', $data);
		} else {
			return redirect()->route('admin.service_pricing');
		}
    }

    public function destroy($id){
        ServicePricing::find($id)->delete();
		SelectedPlanFeatures::where('plan_id',$id)->delete();
        return response()->json(['status'=>'success', 'message'=>'Deleted successfully.']);
    }
	
	public function change_status(Request $request) {
		$user = ServicePricing::where('id', $request->id)->first();
		if ($user->status) {
			$user = ServicePricing::where('id', $request->id)->update(array('status' => 0));
		} else {
			$user = ServicePricing::where('id', $request->id)->update(array('status' => 1));
		}
	}	
}
