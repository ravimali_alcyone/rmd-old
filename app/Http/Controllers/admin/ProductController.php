<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Product;
use App\ProductCategory;
use DataTables;
use DB;

class ProductController extends Controller {

    public function __construct(){
        $this->middleware('auth');
    }

    public function index(Request $request){
		if ($request->ajax()) {

			$data = Product::select('*');

			if($request->filter_category != ''){
				$data = $data->where('category',$request->filter_category);
			}

			if($request->filter_status != ''){
				$data = $data->where('status',$request->filter_status);
			}

			 $data = $data->latest()->get();

            return Datatables::of($data)
				->addIndexColumn()
				->addColumn('action', function($row){
					$view_url = env('APP_URL').'/admin/products/view/'.$row->id;
					$edit_url = env('APP_URL').'/admin/products/edit/'.$row->id;
					$btn = '<div class="flex items-center"><a href="'.$view_url.'" data-id="'.$row->id.'" title="View" class="button button--sm w-16 shadow-md mr-1 mb-2 btn_gray viewUser">View</a>';
					$btn = $btn.'<a href="'.$edit_url.'"   data-id="'.$row->id.'" title="Edit" class="button button--sm w-16 shadow-md mr-1 mb-2 btn_blue viewUser">Edit</a>';
					$btn = $btn.' <a href="javascript:void(0)" onclick="deleteRow('.$row->id.')" data-id="'.$row->id.'" title="Delete" class="button button--sm w-16 shadow-md mr-1 mb-2 btn_red deleteUser">Delete</a></div>';
					return $btn;
				})
				->addColumn('category', function($row){

					$category = \DB::table('product_categories')->where('id', $row->category)->pluck('name')->first();
					$sub_category = \DB::table('product_categories')->where('id', $row->sub_category)->pluck('name')->first();;
					$sub_sub_category = \DB::table('product_categories')->where('id', $row->sub_sub_category)->pluck('name')->first();;

					$data = '<h4 style="font-size: 14px;" class="mt-1">'.$category.'</h4> <h5 style="font-size: 12px;" class="mt-1">'.$sub_category.'</h5> <h6 style="font-size: 11px;" class="mt-1">'.$sub_sub_category.'</h6>';
					return $data;
				})

				->addColumn('image', function($row){
					$images_arr = json_decode($row->images);
					$images = '<div class="flex">';
					if ($images_arr != null) {
						foreach ($images_arr as $key => $value) {
							$images .= '<div class="w-10 h-10 image-fit zoom-in -ml-5">
								<img class="tooltip rounded-full tooltipstered" src="/'.$value.'">
							</div>';
						}
					}
					$images	.= '</div>';
					return $images;
				})
				->addColumn('price_value', function($row){
					$price = '$'.$row->price_value;
					return $price;
				})
				->addColumn('status', function($row){
					if($row->status == 1){
						// $status = '<span class="chip green lighten-5"><span class="green-text">Active</span></span>';
						$status = '<input type="checkbox" name="status" class="input input--switch border"  title="Status" checked onchange="changeStatus('.$row->id.')">';
					}else{
						// $status = '<span class="chip red lighten-5"><span class="red-text">Blocked</span></span>';
						$status = '<input type="checkbox" name="status" class="input input--switch border"  title="Status" onchange="changeStatus('.$row->id.')">';
					}
					return $status;
				})
				->rawColumns(['name','image','category','price_value','stock_qty','status','action'])
				->make(true);
        }

		$data['page'] = 'products';
		$data['categories'] = ProductCategory::where('parent_id', 0)->where('status', 1)->get();
        return view('admin.product.index', $data);
    }

	public function add(){
		$data['product'] = array();
		$data['page'] = 'products';
		$data['categories'] = \DB::table('product_categories')->where('parent_id', 0)->where('status', 1)->get();
		$data['sub_categories'] = array();
		return view('admin.product.add', $data);
	}

    public function store(Request $request){

		if ($request->get('page') == 'edit') {
			if (!empty($request->input('old_image'))) {
				foreach($request->input('old_image') as $key => $value) {
					$images[] = $value;
				}
			}
		}

		if (!empty($request->file('images'))) {
            foreach($request->file('images') as $key => $file) {
                $name=time().'.'.$file->getClientOriginalName();
                $destinationPath = 'uploads/products/';
                $file->move($destinationPath, $name);
                $images[$key] = 'uploads/products'.'/'.$name;
            }
        }

		if ($request->status == 'on') {
			$status = 1;
		} else {
			$status = 0;
		}

		if($request->sub_sub_category == ''){
			$sub_sub_category = 0;
		} else {
			$sub_sub_category = $request->sub_sub_category;
		}

		if($request->sub_category == ''){
			$sub_category = 0;
		} else {
			$sub_category = $request->sub_category;
		}

		if($request->category == ''){
			$category = 0;
		} else {
			$category = $request->category;
		}

		$input_data = [
			'name' => $request->name,
			'category' => $category,
			'sub_category' => $sub_category,
			'sub_sub_category' => $sub_sub_category,
			// 'images' => $request->images,
			'price_type' => $request->price_type,
			'price_value' => $request->price_value,
			'discount_price' => $request->discount_price,
			'stock_status' => $request->stock_status,
			'description' => $request->description,
			'details' => $request->details,
			'product_info' => $request->product_info,
			'stock_qty' => $request->stock_qty,
			'manufacturer_name' => $request->manufacturer_name,
			'is_direct_purchase' => $request->is_direct_purchase,
			'safety_info' => $request->safety_info,
			'associate_problems' => $request->associate_problems,
			'status' => $status,
			'images' => isset($images) ? json_encode($images) : ''
		];
		if ($request->get('page') == 'add') {
			$product = Product::create($input_data);
			if ($product->save()) {
				return response()->json(['status'=>'success', 'message'=>'Saved successfully.']);
			}
		} elseif ($request->get('page') == 'edit') {
			$product = Product::where('id', $request->id)->update($input_data);
			return response()->json(['status'=>'success', 'message'=>'Saved successfully.']);
		}
    }

    public function edit($id){
        $product = Product::find($id);
		if($product && $product->count() > 0){
			$data['page'] = 'products';
			$data['categories'] = \DB::table('product_categories')->where('parent_id', 0)->where('status', 1)->get();
			$data['sub_categories'] = \DB::table('product_categories')->where('parent_id', $product->category)->where('status', 1)->get();
			$data['product'] = $product;
			return view('admin.product.add', $data);
		} else {
			return redirect()->route('admin.products');
		}
    }

    public function destroy($id){
        Product::find($id)->delete();
        return response()->json(['status'=>'success', 'message'=>'Deleted successfully.']);
	}

	public function get_sub_category(Request $request){
		$req = $request->all();
		$output = '<option value="">--Select--</option>';
		$category_id = $req['category_id'];

		if($category_id != ''){
			$sub_categories = \DB::table('product_categories')->where('parent_id',$category_id)->get();

			if($sub_categories && $sub_categories->count() > 0){
				foreach($sub_categories as $sub_category){
					$output .= '<option value="'.$sub_category->id.'">'.$sub_category->name.'</option>';
				}
			}
		}
		echo $output;
	}

	public function get_sub_sub_category(Request $request){
		$req = $request->all();
		$output = '<option value="">--Select--</option>';
		$category_id = $req['category_id'];

		if($category_id != ''){
			$sub_categories = \DB::table('product_categories')->where('parent_id', $category_id)->get();

			if($sub_categories && $sub_categories->count() > 0){
				foreach($sub_categories as $sub_category){
					$output .= '<option value="'.$sub_category->id.'">'.$sub_category->name.'</option>';
				}
			}
		}
		echo $output;
    }

	public function change_status(Request $request) {
		$user = Product::where('id', $request->id)->first();
		if ($user->status) {
			$user = Product::where('id', $request->id)->update(array('status' => 0));
		} else {
			$user = Product::where('id', $request->id)->update(array('status' => 1));
		}
	}

	public function view($id) {
		$product = Product::where('id', $id)->first();
		// $categories = $provider->category;
		$category_name = DB::table('provider_categories')->where('id', $product->category)->select('name')->first();
		// $categories_arr = array();
		// foreach ($category_names as $key => $value) {
		// 	array_push($categories_arr, $value->name);
		// }
		// $categoryString = implode(', ', $categories_arr);

		$data['product'] = $product;
		$data['images'] = json_decode($product->images);
		$data['category'] = $category_name->name;
		$data['page'] = 'products';
		return view('admin.product.view', $data);
	}
}
