<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\ProviderCategory;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\Library\Services\CommonService;
use DataTables;
use DB;

class ProviderController extends Controller {

    public function __construct(){
        $this->middleware('auth');
    }

    public function index(Request $request, CommonService $common, $cat_id=null){


		if ($request->ajax()) {

			$where = array('is_admin' => 0,	'user_role' => 3);

			if($request->filter_status != ''){
				$where['status'] = $request->filter_status;
			}

			if($request->filter_category != ''){
				$whereRaw['column'] = 'category';
				$whereRaw['value'] = $request->filter_category;
			}else{
				$whereRaw = array();
			}

			// if ($id != null) {
			// 	$whereRaw['column'] = 'category';
			// 	$whereRaw['value'] = $id;
			// }

			$data = $common->getTableListing($request, $table='users', $where, $whereRaw, $isLatest=false);

            return Datatables::of($data)
				->addIndexColumn()
				->addColumn('action', function($row){
					//$edit_url = env('APP_URL').'/admin/doctors/add/'.$row->id;
					$view_url = env('APP_URL').'/admin/providers/view/'.$row->id;
					$edit_url = "#";
					$btn = '<div class="flex items-center"><a href="'.$view_url.'" data-id="'.$row->id.'" title="View" class="button button--sm w-16 shadow-md mr-1 mb-2 btn_gray viewUser">View</a>';
					// $btn = $btn.'<a href="'.$edit_url.'"   data-id="'.$row->id.'" title="Edit" class="button button--sm w-16 shadow-md mr-1 mb-2 bg-theme-10 text-white editUser">Edit</a>';
					$btn = $btn.' <a href="javascript:void(0)" onclick="deleteRow('.$row->id.')" data-id="'.$row->id.'" title="Delete" class="button button--sm w-16 shadow-md mr-1 mb-2 btn_red deleteUser">Delete</a></div>';
					// $btn = '<div class="flex items-center"><a href="#" class="button button--sm w-16 shadow-md mr-1 mb-2 bg-theme-10 text-white editUser" data-id="23" title="Edit"> Edit </a>';
					// $btn = $btn.'<div class="flex items-center"><a href="#" class="button button--sm w-16 shadow-md mr-1 mb-2 bg-theme-6 text-white deleteUser" data-id="23" title="Edit"> Delete </a>';
					return $btn;
				})
				->addColumn('category', function($row){

					$categories = explode(',', $row->category);
					$category_names = DB::table('provider_categories')->whereIn('id', $categories)->select('name')->get();
					$categories_arr = array();
					foreach ($category_names as $key => $value) {
						array_push($categories_arr, $value->name);
					}
					$categoryString = implode(', ', $categories_arr);
					return $categoryString;
				})
				->addColumn('status', function($row){
					if($row->status == 1){
						// $status = '<span class="chip green lighten-5"><span class="green-text">Active</span></span>';
						$status = '<input type="checkbox" name="status" class="input input--switch border"  title="Status" checked onchange="changeStatus('.$row->id.')">';
					}else{
						// $status = '<span class="chip red lighten-5"><span class="red-text">Blocked</span></span>';
						$status = '<input type="checkbox" name="status" class="input input--switch border"  title="Status" onchange="changeStatus('.$row->id.')">';
					}
					return $status;
				})
				->addColumn('image', function($row){

					if($row->image == ''){
						$url = env('APP_URL').'/dist/images/user_icon.png';
					}else{
						$url = env('APP_URL').'/'.$row->image;
					}

					$image = '<div class="w-10 h-10 image-fit zoom-in">
								<img class="tooltip rounded-full tooltipstered" src="'.$url.'">
							</div>';

					return $image;
				})
				->rawColumns(['name','image','category','status','action'])
				->make(true);
        }
		$data['page'] = 'providers';
		$data['cat_id'] = $cat_id;
		$data['categories'] = ProviderCategory::where('parent_id', 0)->where('status', 1)->get();
        return view('admin.providers.index', $data);
    }

	public function add(){
		$data['user'] = array();
		return view('admin.providers.add', $data);
	}

    public function store(Request $request){
		$req = $request->all();
        $this->validate($request, [
			'name' => 'required|string|max:255',
            'email' => 'required|email|max:255|unique:users,email,'.$input['id'],
        ]);

		$password = Hash::make('123456');
		if(isset($input['status']) && $req['status'] == 'on'){
			$status = 1;
		}else{
			$status = 0;
		}

		$where = ['id' => $req['id']];

		$input_data =
		[
			'name' => $req['name'],
			'email' => $req['email'],
			'user_role' => 3,
			'status'=> $status,
			'password' => $password,
			'is_admin' => 0
		];

		if($input['id'] != ''){
			unset($input_data['password']);
		}

        User::updateOrCreate($where,$input_data);

        return response()->json(['status'=>'success', 'message'=>'Saved successfully.']);
    }

    public function edit($id){
        $user = User::find($id);
		if($user && $user->count() > 0){
			$data['user'] = $user;
			return view('admin.doctor.add',$data);
		}else{
			return redirect()->route('admin.doctors');
		}
    }

    public function destroy($id){
        User::find($id)->delete();
        return response()->json(['status'=>'success', 'message'=>'Deleted successfully.']);
    }

	public function change_status(Request $request) {
		$user = User::where('id', $request->id)->first();
		if ($user->status) {
			$user = User::where('id', $request->id)->update(array('status' => 0));
		} else {
			$user = User::where('id', $request->id)->update(array('status' => 1));
		}
	}

	public function view($id) {
		$provider = User::where('id', $id)->where('user_role', 3)->first();
		$categories = explode(',', $provider->category);
		$data['categories'] = DB::table('provider_categories')->whereIn('id', $categories)->select('name', 'id')->get();
		// $categories_arr = array();
		// foreach ($category_names as $key => $value) {
		// 	array_push($categories_arr, $value->name);
		// }
		// $categoryString = implode(', ', $categories_arr);

		$data['provider'] = $provider;
		// $data['categories'] = $categoryString;
		$data['page'] = 'providers';
		return view('admin.providers.view', $data);
	}
}
