<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\Library\Services\CommonService;
use App\User;
use App\Service;
use App\VisitForm;
use App\VisitFormFieldType;
use App\VisitQuestion;
use App\QuestionOption;
use DataTables;

class VisitFormController extends Controller {

    public function __construct(){
        $this->middleware('auth');
	}

    public function index(Request $request){

		if ($request->ajax()) {
			$data = VisitForm::select('visit_forms.id','visit_forms.name', 'services.name AS service','visit_forms.status')
			->join('services','services.id','=','visit_forms.service_id')
			->orderBy('visit_forms.created_at','ASC');

			$data = $data->get();

            return Datatables::of($data)->addIndexColumn()
				->addColumn('action', function($row){
					$edit_url = env('APP_URL').'/admin/visit_forms/add/'.$row->id;

					$btn = '<div class="flex items-center"><a href="'.$edit_url.'" class="button button--sm w-16 shadow-md mr-1 mb-2 btn_blue text-white editUser" data-id="23" title="Edit"> Edit </a>';
					$btn = $btn.'<div class="flex items-center"><a href="javascript:void(0)" onclick="deleteRow('.$row->id.')" class="button button--sm w-16 shadow-md mr-1 mb-2 btn_red text-white deleteUser" data-id="23" title="Edit"> Delete </a>';
					return $btn;
				})
				->addColumn('service', function($row){
					$service = $row->service;
					return $service;
				})
				->addColumn('status', function($row){
					if($row->status == 1){
						$status = '<input type="checkbox" name="status" class="input input--switch border"  title="Status" checked onchange="changeStatus('.$row->id.')">';
					}else{
						$status = '<input type="checkbox" name="status" class="input input--switch border"  title="Status" onchange="changeStatus('.$row->id.')">';
					}
					return $status;
				})				
				->rawColumns(['service','name','status','action'])
				->make(true);
        }
		$data['page'] = 'visit_forms';
        return view('admin.visit_form.index', $data);
    }

	public function add(){
		$data['services'] = Service::select('services.*')->leftJoin('visit_forms','services.id','!=','visit_forms.service_id')->where('services.status', 1)->groupBy('services.id')->get();
		$data['visit_form'] = array();
		$data['page'] = 'visit_forms';
		return view('admin.visit_form.add', $data);
	}
	
	public function addQuestion($formid){
        $visit_form = VisitForm::find($formid);
		
		if($visit_form && $visit_form->count() > 0){
			$data['form_field_types'] = VisitFormFieldType::select('*')->where('status',1)->get();
			$data['visit_form'] = $visit_form;
			$data['visit_questions'] = VisitQuestion::where('visit_form_id', $visit_form->id)->where('status',1)->get();
			$data['page'] = 'visit_forms';
			return view('admin.visit_form.addQuestion', $data);			
		} else {
			return redirect()->route('admin.visit_forms');
		}	
	}

    public function store(Request $request, CommonService $common){
		$req = $request->all();
			
		$visit_form_id = $common->add_form($request); //get visit form id from add/update				
			
		return response()->json(['status'=>'success', 'message'=>'Saved successfully.', 'id' => $visit_form_id]);
		
    }

    public function storeQuestion(Request $request, CommonService $common){
		$req = $request->all();
		
		if(isset($req['visit_form_data']) && count($req['visit_form_data']) > 0){
			
			$visit_form_data = $req['visit_form_data'];	
			$q_ids = array();
			$visitQArray = array();
			
			$visit_form_id = $request->formid; //get visit form id from add/update				
					
			//echo '<pre>'; print_r($visit_form_data); die;

			foreach($visit_form_data as $key => $value){
				$input = array();
				$input['type'] = $type = key($value);
				$input['label'] = $value[$type]['label'];
				$input['q_id'] = isset($value[$type]['q_id']) ? $value[$type]['q_id'] : '';
				$input['is_parent'] = $value[$type]['is_parent'];
								
				if($req['formid'] == ''){
					if($value[$type]['parent_id'] == ''){
						$input['parent_id'] = 0;
					}else{
						$input['parent_id'] = $q_ids ? $q_ids[$value[$type]['parent_id']] : 0;
					}						
				}else{
					$input['parent_id'] = $value[$type]['parent_id'];
				}		
							
				$q_id = $common->add_question($visit_form_id, $input);
				array_push($q_ids,$q_id);
				$visitQArray[$key] = $q_id;

			}

		//Now get added questions from DB
		$visit_questions = VisitQuestion::where('visit_form_id',$visit_form_id)->where('status',1)->get();

		if($visit_questions && $visit_questions->count() > 0){
			foreach($visit_questions as $key => $value ){
				$input = array();
				$qID = $value->id;
				$nQKey = 0;		
				
				if($value->question_type == 'yes_no'){	
					
					$input['positive'] = $visit_form_data[$key][$value->question_type]['positive'];
					$input['negative'] = $visit_form_data[$key][$value->question_type]['negative'];			
					$option_data1 = array();
					
					if($input['positive']){
						$option_data1[0]['op_id'] = $input['positive']['op_id'];
						$option_data1[0]['option_text'] = $input['positive']['value'];
						$option_data1[0]['is_conditional'] = $input['positive']['cond'];
						$nQKey = $input['positive']['next_question_id'];
						$option_data1[0]['next_question_id'] = $visitQArray[$nQKey];
						$option_data1[0]['positive'] = 1;
					}
										
					if($input['negative']){
						$option_data1[1]['op_id'] = $input['negative']['op_id'];
						$option_data1[1]['option_text'] = $input['negative']['value'];
						$option_data1[1]['is_conditional'] = $input['negative']['cond'];
						$nQKey = $input['negative']['next_question_id'];
						$option_data1[1]['next_question_id'] = $visitQArray[$nQKey];
						$option_data1[1]['positive'] = 0;
					}

					if($option_data1){
						foreach($option_data1 as $od => $inop){
							$common->add_option($visit_form_id, $qID, $inop);
						}
					}					
				}
				
				elseif($value->question_type == 'single'){										

					$option = $visit_form_data[$key][$value->question_type]['option'];
					$option_data2 = array();
					
					if($option){
						foreach($option as $k => $op){
							
							if($k == 'op_id'){
								foreach($op as $o => $opval){
									$option_data2[$o]['op_id'] = $opval;
								}
							}
							
							if($k == 'value'){
								foreach($op as $o => $opval){
									$option_data2[$o]['option_text'] = $opval;
								}
							}
							
							elseif($k == 'cond'){
								foreach($op as $o => $opval){
									$option_data2[$o]['is_conditional'] = $opval;
								}								
							}
							
							if($k == 'next_question_id'){
								foreach($op as $o => $opval){
									$nQKey = $opval;
									$option_data2[$o]['next_question_id'] = $visitQArray[$nQKey];									
								}									
							}
						}
					}
					
					if($option_data2){
						foreach($option_data2 as $od => $inop){
							$common->add_option($visit_form_id, $qID, $inop);
						}
					}
				}
				
				elseif($value->question_type == 'multiple'){					
					$option = $visit_form_data[$key][$value->question_type]['option'];
					$option_data3 = array();
					
					if($option){
						
						foreach($option as $k => $op){
							
							if($k == 'op_id'){
								foreach($op as $o => $opval){
									$option_data3[$o]['op_id'] = $opval;
								}
							}
							
							if($k == 'value'){
								foreach($op as $o => $opval){
									$option_data3[$o]['option_text'] = $opval;
								}
							}
							
							elseif($k == 'cond'){
								foreach($op as $o => $opval){
									$option_data3[$o]['is_conditional'] = $opval;
								}								
							}
							
							if($k == 'next_question_id'){
								foreach($op as $o => $opval){
									$nQKey = $opval;
									$option_data3[$o]['next_question_id'] = $visitQArray[$nQKey];										
								}									
							}
						}
					}
					
					if($option_data3){
						foreach($option_data3 as $od => $inop){
							$op_id = $common->add_option($visit_form_id, $q_id, $inop);
						}
					}					
				}
			}
		}
			
			return response()->json(['status'=>'success', 'message'=>'Saved successfully.']);
		}else{
			return response()->json(['status'=>'error', 'message'=>'Please add questions.']);
		}
    }

    public function edit($id){
        $visit_form = VisitForm::find($id);
		if($visit_form && $visit_form->count() > 0){
			$data['services'] = Service::where('id', $visit_form->service_id)->get();
			$data['visit_form'] = $visit_form;
			$data['form_field_types'] = VisitFormFieldType::select('*')->where('status',1)->get();
			$data['page'] = 'visit_forms';
			return view('admin.visit_form.add', $data);
		} else {
			return redirect()->route('admin.visit_forms');
		}
    }
	
    public static function getOptions($vid,$qid){
        $options = QuestionOption::where('visit_form_id', $vid)->where('question_id', $qid)->where('status',1)->get();
        return $options;
    }
	
    public function destroy($id){
        VisitForm::find($id)->delete();
        return response()->json(['status'=>'success', 'message'=>'Deleted successfully.']);
    }
	
    public function destroy_field($id){
        VisitFormField::find($id)->delete();
        return response()->json(['status'=>'success', 'message'=>'Deleted successfully.']);
    }	
}
