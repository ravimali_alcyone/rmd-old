<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\TreatmentMedicine;
use App\MedicineFaq;
use App\Service;
use DataTables;

class TreatmentMedicineController extends Controller {

    public function __construct(){
        $this->middleware('auth');
    }

    public function index(Request $request){
		if ($request->ajax()) {

			$data = TreatmentMedicine::select('*');

			if($request->filter_service != ''){
				$data = $data->where('service_id',$request->filter_service);
			}

			if($request->filter_status != ''){
				$data = $data->where('status',$request->filter_status);
			}

			 $data = $data->latest()->get();

            return Datatables::of($data)
				->addIndexColumn()
				->addColumn('action', function($row){
					$view_url = env('APP_URL').'/admin/treatment_medicines/view/'.$row->id;
					$edit_url = env('APP_URL').'/admin/treatment_medicines/edit/'.$row->id;
					$btn = '<div class="flex items-center"><a href="'.$view_url.'" data-id="'.$row->id.'" title="View" class="button button--sm w-16 shadow-md mr-1 mb-2 btn_gray viewUser">View</a>';
					$btn = $btn.'<a href="'.$edit_url.'"   data-id="'.$row->id.'" title="Edit" class="button button--sm w-16 shadow-md mr-1 mb-2 btn_blue viewUser">Edit</a>';
					$btn = $btn.' <a href="javascript:void(0)" onclick="deleteRow('.$row->id.')" data-id="'.$row->id.'" title="Delete" class="button button--sm w-16 shadow-md mr-1 mb-2 btn_red deleteUser">Delete</a></div>';
					return $btn;
				})
				->addColumn('service', function($row){

					$service = Service::where('id', $row->service_id)->pluck('name')->first();

					$data = $service;
					return $data;
				})

				->addColumn('image', function($row){
					$images_arr = json_decode($row->images);
					$images = '<div class="flex">';
					if ($images_arr != null) {
						foreach ($images_arr as $key => $value) {
							$images .= '<div class="w-10 h-10 image-fit zoom-in -ml-5">
								<img class="tooltip rounded-full tooltipstered" src="/'.$value.'">
							</div>';
						}
					}
					$images	.= '</div>';
					return $images;
				})
				->addColumn('start_price', function($row){
					$price = '$'.$row->start_price;
					return $price;
				})
				->addColumn('status', function($row){
					if($row->status == 1){
						$status = '<input type="checkbox" name="status" class="input input--switch border"  title="Status" checked onchange="changeStatus('.$row->id.')">';
					}else{
						$status = '<input type="checkbox" name="status" class="input input--switch border"  title="Status" onchange="changeStatus('.$row->id.')">';
					}
					return $status;
				})
				->rawColumns(['name','image','service_id','start_price','status','action'])
				->make(true);
        }

		$data['page'] = 'treatment_medicines';
		$data['services'] = Service::where('status', 1)->get();
        return view('admin.treatment_medicine.index', $data);
    }

	public function add(){
		$data['treatment_medicine'] = array();
		$data['page'] = 'treatment_medicines';
		$data['services'] = Service::where('status', 1)->get();
		$data['faq_rows'] = array();
		return view('admin.treatment_medicine.add', $data);
	}

    public function store(Request $request){

		$req = $request->all();
		
		if ($request->get('page') == 'edit') {
			if (!empty($request->input('old_image'))) {
				foreach($request->input('old_image') as $key => $value) {
					$images[] = $value;
				}
			}
		}

		if (!empty($request->file('images'))) {
            foreach($request->file('images') as $key => $file) {
                $name=time().'.'.$file->getClientOriginalName();
                $destinationPath = 'uploads/treatment_medicines/';
                $file->move($destinationPath, $name);
                $images[$key] = 'uploads/treatment_medicines'.'/'.$name;
            }
        }

		if ($request->status == 'on') {
			$status = 1;
		} else {
			$status = 0;
		}

		if($request->service_id == ''){
			$service_id = 0;
		} else {
			$service_id = $request->service_id;
		}

		$input_data = [
			'name' => $request->name,
			'other_name' => $request->other_name,
			'chemical_name' => $request->chemical_name,
			'service_id' => $service_id,
			'start_price' => $request->start_price,
			'description' => $request->description,
			'status' => $status,
			'images' => isset($images) ? json_encode($images) : ''
		];
		
		$faq_rows = array();
		
		if(isset($req['faq_data']) && count($req['faq_data']) > 0){
			foreach($req['faq_data'] as $type => $value){
				if($type == 'question'){
					$i=0;
					foreach($value as $row){
						$faq_rows[$i]['question_data'] = $row;
						$i++;
					}
				}elseif($type == 'answer'){
					$j=0;
					foreach($value as $row){
						$faq_rows[$j]['answer_data'] = $row;
						$j++;
					}
				}
			}
		}
		
			
		//echo '<pre>';print_R($faq_rows);die;
		if ($request->get('page') == 'add') {
			$treatment_medicine = TreatmentMedicine::create($input_data);
			
			if ($treatment_medicine->save()) {
				$medicine_id = $treatment_medicine->id;
				
				if($faq_rows){
					foreach($faq_rows as $key => $value){
						$faq_data = array(
							'medicine_id' => $medicine_id,
							'question_data' => $value['question_data'],
							'answer_data' => $value['answer_data'],
						);
						MedicineFaq::create($faq_data);
					}
				}else{
					MedicineFaq::where('medicine_id',$medicine_id)->delete();			
				}
				
				return response()->json(['status'=>'success', 'message'=>'Added successfully.']);
			}
			
		} elseif ($request->get('page') == 'edit') {
			$treatment_medicine = TreatmentMedicine::where('id', $request->id)->update($input_data);
			$medicine_id = $request->id;
			
			MedicineFaq::where('medicine_id',$medicine_id)->delete();
			
			if($faq_rows){
								
				foreach($faq_rows as $key => $value){
					$faq_data = array(
						'medicine_id' => $medicine_id,
						'question_data' => $value['question_data'],
						'answer_data' => $value['answer_data'],
					);
					MedicineFaq::create($faq_data);
				}
			}			
			return response()->json(['status'=>'success', 'message'=>'Updated successfully.']);
		}
			
    }

    public function edit($id){
        $treatment_medicine = TreatmentMedicine::find($id);
		$data['faq_rows'] = array();
		if($treatment_medicine && $treatment_medicine->count() > 0){
			$data['page'] = 'treatment_medicines';
			$data['services'] = Service::where('status', 1)->get();
			$data['treatment_medicine'] = $treatment_medicine;
			$data['faq_rows'] = MedicineFaq::where('medicine_id',$treatment_medicine->id)->get();
			
			return view('admin.treatment_medicine.add', $data);
		} else {
			return redirect()->route('admin.treatment_medicines');
		}
    }

    public function destroy($id){
        TreatmentMedicine::find($id)->delete();
        MedicineFaq::where('medicine_id',$id)->delete();
        return response()->json(['status'=>'success', 'message'=>'Deleted successfully.']);
	}


	public function change_status(Request $request) {
		$user = TreatmentMedicine::where('id', $request->id)->first();
		if ($user->status) {
			$user = TreatmentMedicine::where('id', $request->id)->update(array('status' => 0));
		} else {
			$user = TreatmentMedicine::where('id', $request->id)->update(array('status' => 1));
		}
	}

	public function view($id) {
		$treatment_medicine = TreatmentMedicine::where('id', $id)->first();
		$service_name = Service::where('id', $treatment_medicine->service_id)->select('name')->first();
		$data['treatment_medicine'] = $treatment_medicine;
		$data['images'] = json_decode($treatment_medicine->images);
		$data['service'] = $service_name->name;
		$data['page'] = 'treatment_medicines';
		return view('admin.treatment_medicine.view', $data);
	}
	
	public function imageUpload(Request $request){
		if($request->hasFile('upload')) {
            $originName = $request->file('upload')->getClientOriginalName();
            $fileName = pathinfo($originName, PATHINFO_FILENAME);
            $extension = $request->file('upload')->getClientOriginalExtension();
            $fileName = $fileName.'_'.time().'.'.$extension;
        
            $request->file('upload')->move(public_path('images'), $fileName);
   
            $CKEditorFuncNum = $request->input('CKEditorFuncNum');
            $url = asset('images/'.$fileName); 
            $msg = 'Image uploaded successfully'; 
            $response = "<script>window.parent.CKEDITOR.tools.callFunction($CKEditorFuncNum, '$url', '$msg')</script>";
               
            @header('Content-type: text/html; charset=utf-8'); 
            echo $response;
        }
	}
}
