<?php $__env->startSection('content'); ?>
<section class="rmd_service_intro">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="rs_intro_details">
                        <div class="rsid_img"><img class="img-fluid" src="/<?php echo e($service_detail->image); ?>" alt="<?php echo e($service_detail->name); ?>"></div>
                        <div class="rsid_details">
                            <h3><?php echo e($service_detail->name); ?> treatment</h3>
                            <p><?php echo e($service_detail->short_info); ?></p>
                            <a href="<?php echo e(route('signup')); ?>" class="btn-primary">Start Online Visit</a>
                            <div class="treat_info">
                                <h5>Treatment overview</h5>
                                <ul>
                                    <li><i class="fa fa-check-circle"></i>Free 2-day shipping</li>
                                    <li><i class="fa fa-check-circle"></i>Starting at $2/dose</li>
                                    <li><i class="fa fa-check-circle"></i>Free, unlimited follow ups</li>
                                    <li><i class="fa fa-check-circle"></i>No commitment / cancel anytime</li>
                                </ul>
                            </div>
							<?php echo $service_detail->description; ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="rmd_ser_treat_types">
        <div class="container">
		<?php if($medicines && $medicines->count() > 0): ?>			
            <div class="row">
                <div class="col mt-5 mb-5">
                    <div class="title"><h2>Treatments for <?php echo e($service_detail->name); ?></h2></div>
                    <div class="medi_list">
					<?php $__currentLoopData = $medicines; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="medi_box">
                            <div class="medi_img_box">
                                <div class="medi_name"><?php echo e($row->name); ?></div>
								<?php $images = json_decode($row->images,true);
									if($images){
										$image = $images[0];
									}else{
										$image = 'assets/images/default_medicine.jpg';
									}
								?>
                                <img src="<?php echo e($image); ?>" alt="<?php echo e($row->name); ?>">
                            </div>
                            <div class="medi_info">
                                <div class="medi_pack_info">
                                    <div class="medi_pack_price"><small>Starting at</small>$<?php echo e($row->start_price); ?>/per dose</div>
                                    <a class="btn bor_btn" href="<?php echo e(route('signup')); ?>">Get Started</a>
                                </div>
                                <strong>Important safety information</strong>
                                <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et. <a href="#" class="rm_link">read more</a></p>
                            </div>
                        </div>
					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                </div>
            </div>
		<?php endif; ?>
            <div class="row">
                <div class="col">
                    <div class="hiw_ser_btm">
                        <div class="title"><h2>Let’s take care of it</h2><p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores.</p></div>
                        <div class="hiw_ser_btm_boxes">
                            <div class="hiwb_box">
                                <div class="hiwb_img"><img src="assets/images/fiw_1.jpg" alt=""></div>
                                <div class="hieb_category">Online visit</div>
                                <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et.</p>
                            </div>
                            <div class="hiwb_box">
                                <div class="hiwb_img"><img src="assets/images/fiw_2.jpg" alt=""></div>
                                <div class="hieb_category">Deliveries</div>
                                <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et.</p>
                            </div>
                            <div class="hiwb_box">
                                <div class="hiwb_img"><img src="assets/images/fiw_3.jpg" alt=""></div>
                                <div class="hieb_category">Ongoing care</div>
                                <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et.</p>
                            </div>
                        </div>
                        <div class="w-100 text-center mt-4 mb-4"><a href="<?php echo e(route('signup')); ?>" class="btn-primary">Start Online Visit</a></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <section class="work_join_sec faqs_wrapper">
        <div class="data_wrap">
            <div class="container">
                <div class="content_wrapper">
                    <div class="title text-center">FAQ</div>
                    <div class="faqs">
                    <div id="accordion_2">
                        <div class="card">
                            <div class="card-header">
                                <a class="card-link" data-toggle="collapse" href="#collapse_16"><span class="blue_dot"></span> How much does a visit cost?</a>
                            </div>
                            <div id="collapse_16" class="collapse show" data-parent="#accordion_2">
                                <div class="card-body">
                                    Unfortunately we cannot accept returns of prescription products for reuse or resale, and all sales are final. However, if you feel we have made an error in the filling of your prescription.
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header">
                                <a class="collapsed card-link" data-toggle="collapse" href="#collapse_17"><span class="blue_dot"></span> How much does the medication cost?</a>
                            </div>
                            <div id="collapse_17" class="collapse" data-parent="#accordion_2">
                                <div class="card-body">
                                    Unfortunately we cannot accept returns of prescription products for reuse or resale, and all sales are final. However, if you feel we have made an error in the filling of your prescription.
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header">
                                <a class="collapsed card-link" data-toggle="collapse" href="#collapse_18"><span class="blue_dot"></span> Do I need to be home to sign for my order?</a>
                            </div>
                            <div id="collapse_18" class="collapse" data-parent="#accordion_2">
                                <div class="card-body">
                                    Unfortunately we cannot accept returns of prescription products for reuse or resale, and all sales are final. However, if you feel we have made an error in the filling of your prescription.
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header">
                                <a class="collapsed card-link" data-toggle="collapse" href="#collapse_19"><span class="blue_dot"></span> When will I receive my order?</a>
                            </div>
                            <div id="collapse_19" class="collapse" data-parent="#accordion_2">
                                <div class="card-body">
                                    Unfortunately we cannot accept returns of prescription products for reuse or resale, and all sales are final. However, if you feel we have made an error in the filling of your prescription.
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header">
                                <a class="collapsed card-link" data-toggle="collapse" href="#collapse_20"><span class="blue_dot"></span> Is ReplenishMD covered by insurance?</a>
                            </div>
                            <div id="collapse_20" class="collapse" data-parent="#accordion_2">
                                <div class="card-body">
                                    Unfortunately we cannot accept returns of prescription products for reuse or resale, and all sales are final. However, if you feel we have made an error in the filling of your prescription.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('front.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\laravel\replenishmd\resources\views/front/treatment.blade.php ENDPATH**/ ?>