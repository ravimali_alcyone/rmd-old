<?php $__env->startSection('content'); ?>
    <section class="sign_in_sec_wrapper">
        <div class="sign_in_sec shadow">
            <img src="<?php echo e(asset('assets/images/rmd_logo_red.svg')); ?>" alt="rmd_logo_red">
            <h1 class="title">Log in to your RMD platform account to continue.</h1>
            <form id="loginForm">

                <div class="form-group">
                    <input type="email" class="form-control" placeholder="Email" id="email" autofocus>
                </div>
                <div class="form-group">
                    <input type="password" class="form-control" placeholder="Password" id="pwd">
                </div>
                <button type="submit" class="btn btn-primary">Sign In</button>
            </form>
            <a href="<?php echo e(route('forgot_password')); ?>" class="link">LOST YOUR PASSWORD?</a>
        </div>
    </section>	
	
	<script>
		$("#loginForm").submit(function(e) {
			e.preventDefault();
			$(".loader").css('display', 'flex');
			
			$.ajax({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				data: $('#loginForm').serialize(),
				url: "<?php echo e(route('signin_submission')); ?>",
				type: "POST",
				// dataType: 'json',
				success: function (response) {
	
					if(response['status'] == 'success'){
						swal({
							title: response['message'],
							icon: 'success'
						});

						setTimeout(function(){
							$(".loader").css('display', 'none');
							swal.close();
							$('#loginForm').trigger("reset");
							window.location = "<?php echo e(route('online_visit_welcome')); ?>";
						}, 2000);
					}
				},
				error: function (data) {
					$(".loader").css('display', 'none');
					let errors = data.responseJSON.errors;
					
					$.each(errors, function(key, value) {
						errorAlert(value[0],5000,'bottom-left');
					});					
/* 					swal({
						title: 'Error Occured.',
						icon: 'error'
					}) */
				}
			});
		});		
	</script>	
	
<?php $__env->stopSection(); ?>
<?php echo $__env->make('front.online_visit_layout.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\laravel\replenishmd\resources\views/front/sign_in.blade.php ENDPATH**/ ?>