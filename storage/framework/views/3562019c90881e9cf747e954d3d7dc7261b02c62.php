    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-2">
                    <a href="<?php echo e(env('APP_URL')); ?>"><img src="<?php echo e(asset('assets/images/foot_logo.svg')); ?>" class="img-fluid" alt=""></a>
                </div>
                <div class="col-md-6">
                    <div class="foot_links_list">
                        <ul class="foot_list">
                            <li><b>Heathcare</b></li>
                            <li><a href="#">For Individuals</a></li>
                            <li><a href="#">Locations</a></li>
                            <li><a href="<?php echo e(env('APP_URL')); ?>/providers">Doctors</a></li>
                            <li><a href="#">Health Coaches</a></li>
                            <li><a href="#">Store</a></li>
                            <li><a href="#">Log in</a></li>
                            <li><a href="#">Join now</a></li>
                        </ul>
                        <ul class="foot_list">
                            <li><b>Articles</b></li>
                            <li><a href="#">Health Concerns</a></li>
                            <li><a href="#">Recipes & Nutrition</a></li>
                            <li><a href="#">Optimizations</a></li>
                            <li><a href="#">News</a></li>
                            <li><a href="#">Guides</a></li>
                        </ul>
                        <ul class="foot_list">
                            <li><b>Company</b></li>
                            <li><a href="#">Mission</a></li>
                            <li><a href="#">Origin</a></li>
                            <li><a href="#">Careers</a></li>
                            <li><a href="#">Team</a></li>
                            <li><a href="#">Press</a></li>
                        </ul>
                        <ul class="foot_list">
                            <li><b>Help & Support</b></li>
                            <li><a href="<?php echo e(env('APP_URL')); ?>/contact">Contact</a></li>
                            <li><a href="#">Common Questions</a></li>
                            <li><a href="#">Membership</a></li>
                            <li><a href="#">Message Us</a></li>
                            <li><a href="#">Talk to us</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="h_contact">
                        <b>Have a question?</b>
                        <p>Call or text us (Mon – Fri: 9:30am - 9pm EST).<br>Message and data rates may apply.</p>
                        <div class="cont_numbr">+1 (832) 443-2909</div>
                        <p>Email us anytime</p>
                        <div class="cont_email">care@replenishmd.com</div>
                        <ul class="social_links">
                            <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                            <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                            <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fab fa-youtube"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col text-center py-5 copy_text">© 2020 ReplenishMD ‧ 
					<a href="<?php echo e(env('APP_URL')); ?>/terms-of-use">Terms of Use</a> ‧ 
					<a href="<?php echo e(env('APP_URL')); ?>/terms-conditions">Membership Terms & Conditions</a> ‧ 
					<a href="<?php echo e(env('APP_URL')); ?>/privacy-policy">Privacy Policy</a> ‧ 
					<a href="#">Notice of Privacy Practices</a>
				</div>
            </div>
        </div>
    </footer>

    <script src="<?php echo e(asset('assets/js/bootstrap.bundle.min.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/js/jquery.easing.min.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/js/scrolling-nav.js')); ?>"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/2.1.3/TweenMax.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.7/ScrollMagic.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.7/plugins/animation.gsap.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.7/plugins/debug.addIndicators.min.js"></script>
    <script src="<?php echo e(asset('assets/js/animation.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/js/owl.carousel.js')); ?>" type="text/javascript"></script>
    <script>
        $(window).scroll(function() {
            var scroll = $(window).scrollTop();
			var hClass = "<?php if($page == 'home'){echo 'head_bg';}else{echo 'fixed-top';}?>";
            if (scroll >= 100) {
                $("#mainNav").addClass(hClass+" shadow");
            } else {
                $("#mainNav").removeClass(hClass+" shadow");
            }
        });
        
        $("#get_started_btn").click(function() {
            $('html, body').animate({
                scrollTop: $("#consult").offset().top - 120
            }, 500);
        });

		$(".select2").select2({
			minimumResultsForSearch: -1
		});
	</script>	
</body>
</html><?php /**PATH C:\laravel\replenishmd\resources\views/front/layouts/footer.blade.php ENDPATH**/ ?>