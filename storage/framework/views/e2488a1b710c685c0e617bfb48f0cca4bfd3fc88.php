<?php $__env->startSection('content'); ?>
     <div class="tabs_wrapper welcome basics">
        <div>
            <a href="javascript:void(0);" class="how_it_works">The Basics</a>
            <div class="steps">
                <span class="active ml-0"></span>
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>
    </div>

    <div class="main_welcome_wrapper">
        <section class="welcome_content_wrapper the_basics_sec">
            <div>
                <h3 class="heading">The Basics</h3>
                <p class="description">This information helps your doctor determine if you're eligible for treatment.</p>

                <form id="basicsForm">
                    <div class="form-group">
                        <label for="biological_sex">Biological Sex</label>
                        <div class="biological_sex">
                            <span class="male <?php if($user->gender != 'female'){ echo 'active';}?>" data="male">Male</span>
                            <span class="female <?php if($user->gender == 'female'){ echo 'active';}?>" data="female">Female</span>
                        </div>
						<input type="hidden" name="gender" id="gender" value="<?php if($user->gender){ echo $user->gender; }else{ echo 'male'; } ?>" />
                    </div>
                    <div class="form-group">
                        <label for="birthdate">Birthdate</label>
                        <input type="date" name="birth_date" class="form-control" placeholder="MM/DD/YYYY" id="birth_date" required="required" autofocus="" max="2002-01-01" value="<?php if($user->dob){ echo $user->dob;}?>">
                    </div>
                    <div class="form-group">
                        <label for="zip_code">Zipcode</label>
                        <input type="number" name="zip_code" class="form-control" placeholder="######" id="zip_code" required="required" maxlength="5" value="<?php if($user->zip_code): ?><?php echo e($user->zip_code); ?><?php endif; ?>">
                    </div>
                    <div class="form-group">
                        <label for="phone_number">Phone number</label>
                        <input type="number" name="phone_number" class="form-control" placeholder="" id="phone_number" required="required" maxlength="10" value="<?php if($user->phone): ?><?php echo e($user->phone); ?><?php endif; ?>">
                    </div>
                    <button type="submit" id="saveBtn" class="btn btn-primary">Next</button>
                </form>
            </div>
        </section>
    </div>
    <script>
        $(document).ready(function() {
            $(".the_basics_sec .biological_sex span").click(function() {
                $(".the_basics_sec .biological_sex span").removeClass("active");
                $(this).addClass("active");
				let gender = $(this).attr('data');
				$("#gender").val(gender);
            });
        });
		
		$("#basicsForm").submit(function(e) {
			e.preventDefault();
			$(".loader").css('display', 'flex');
			
			$.ajax({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				data: $('#basicsForm').serialize(),
				url: "<?php echo e(route('basics_submission')); ?>",
				type: "POST",
				// dataType: 'json',
				success: function (response) {
	
					if(response['status'] == 'success'){
						swal({
							title: response['message'],
							icon: 'success'
						});

						setTimeout(function(){
							$(".loader").css('display', 'none');
							swal.close();
							$('#basicsForm').trigger("reset");
							window.location = "<?php echo e(route('medical_questions')); ?>";
						}, 2000);
					}
				},
				error: function (data) {
					$(".loader").css('display', 'none');
					let errors = data.responseJSON.errors;
					
					$.each(errors, function(key, value) {
						errorAlert(value[0],5000,'bottom-left');
					});					
/* 					swal({
						title: 'Error Occured.',
						icon: 'error'
					}) */
					$('#saveBtn').html('Save');
				}
			});
		});			
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('front.online_visit_layout.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\laravel\replenishmd\resources\views/front/online_visit_basics.blade.php ENDPATH**/ ?>