<?php $__env->startSection('content'); ?>

    <section class="inner_banner experts_banner">
        <div class="container">
            <div class="content_wrapper">
                <div class="b_text">
                    <h1>Our Experts</h1>
                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et.</p>
                </div>
            </div>
        </div>
        <img src="/assets/images/bottom_curve.svg" alt="bottom_curve">
    </section>

    <section class="contact_head_sec">
        <div class="container">
            <div class="title">Your ReplenishMD Care Team
                <p>Lorem ipsum dolor sit amet, consetetur sadipscing</p>
            </div>
		<?php if($providers): ?>
            <div class="member_wrapper">
			<?php $__currentLoopData = $providers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $provider): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="item_mem">
                    <div class="exprt_box">
                        <div class="exprt_img"><img src="<?php echo e($provider['image']); ?>" class="img-fluid" alt="<?php echo e($provider['name']); ?>"></div>
                        <div class="eprt_info">
                            <strong><?php echo e($provider['name']); ?></strong>
                            <p><?php echo e($provider['provider_categories']); ?></p>
                            <a class="vb_link" href="#">View full bio <i class="fa fa-long-arrow-alt-right"></i></a>
                        </div>
                    </div>
                </div>
			<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>
		<?php endif; ?>
        </div>
    </section>

    <section class="work_join_sec about experts_p">
        <div class="data_wrap">
            <div class="container">
                <div class="content_wrapper">
                    <div class="title">Best in class medical experts
                        <p>Lorem ipsum dolor sit amet, consetetur</p>
                    </div>
                    <div class="boxes_wrapper d_flex_j_center">
                        <div class="box">
                            <span class="fas fa-file-medical icon"></span>
                            <h6 class="title">Licensed & <br>accredited doctors</h6>
                        </div>
                        <div class="box">
                            <span class="medal_icon icon"></span>
                            <h6 class="title">Trained in Functional <br>Medicine</h6>
                        </div>
                        <div class="box">
                            <span class="fas fa-flask icon"></span>
                            <h6 class="title">Experienced in personalized <br>labs & analysis</h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('front.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\laravel\replenishmd\resources\views/front/providers.blade.php ENDPATH**/ ?>