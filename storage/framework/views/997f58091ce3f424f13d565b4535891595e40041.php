<?php $__env->startSection('content'); ?>
    <section class="sign_up_sec">
        <div class="sign_up_wrapper">
            <div class="left">
                <div class="owl-carousel clients">
                    <div class="item_box">
                        <div class="tk_b">
                            <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.</p>
                            <div class="rate_by">
                                <div class="reviewr_img"><img src="./assets/images/c1.png" class="img-fluid" alt=""></div>
                                <div class="reviewer_info">
                                    <ul class="rating">
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                    </ul>
                                    <div class="reviewer_name">Mariya Brown</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item_box">
                        <div class="tk_b">
                            <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.</p>
                            <div class="rate_by">
                                <div class="reviewr_img"><img src="./assets/images/c2.png" class="img-fluid" alt=""></div>
                                <div class="reviewer_info">
                                    <ul class="rating">
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                    </ul>
                                    <div class="reviewer_name">Ethen Haddox</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item_box">
                        <div class="tk_b">
                            <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.</p>
                            <div class="rate_by">
                                <div class="reviewr_img"><img src="./assets/images/c3.png" class="img-fluid" alt=""></div>
                                <div class="reviewer_info">
                                    <ul class="rating">
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                    </ul>
                                    <div class="reviewer_name">Calicadoo</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="right">
                <div class="form_wrapper">
                    <h3 class="title">Get started with your online visit for <?php echo e($service_detail->name); ?></h3>
                    <p class="des">This is an opportunity for you to tell your doctor about your health, medical history, and lifestyle. Your doctor will use this information to evaluate your symptoms and, if appropriate, prescribe medication for treatment.
                    </p>

                    <form id="signupForm">
                        <div class="form-group">
                            <input type="email" name="email" class="form-control" placeholder="Email" id="email" required="required" autofocus>
                        </div>
                        <div class="input_wrapper">
                            <div class="form-group">
                                <input type="text" name="first_name" class="form-control" placeholder="First Name" id="first_name" required="required">
                            </div>
                            <div class="form-group">
                                <input type="text" name="last_name" class="form-control" placeholder="Last Name" id="last_name" required="required">
                            </div>
                        </div>
                        <div class="input_wrapper">
                            <div class="form-group">
                                <input type="password" name="password" class="form-control" placeholder="Password" id="password" required="required">
                            </div>
                            <div class="form-group">
                                <input type="password" name="password_confirmation" class="form-control" placeholder="Confirm Password" id="confirm_password" required="required">
                            </div>
                        </div>
                        <div class="agree_with_terms_box">
                            <div class="checkbox d-inline-block">
                                <input type="checkbox" id="checkbox2" name="agree" value="1">
                                <label for="checkbox2"><span></span></label>
                            </div>
                            <span>I agree to <a href="#" class="terms">terms</a> and <a href="#">privacy policy</a> and consent to <a href="#">telehealth</a></span>
                        </div>
                        <button type="submit" id="saveBtn" class="btn btn-primary">Submit</button>
                        <p class="mb-0">Already a member? <a href="#" class="login_link">Log in and continue</a></p>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <section class="terms_sec_2">
        <div class="content_wrapper shadow">
            <h4 class="heading">Terms and conditions of use</h4>
            <p class="description">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren,
                no sea takimata sanctus est</p>
            <p class="description">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren,
                no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam
                et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>
            <p class="description">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren,
                no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut </p>
            <p class="description">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren,
                no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut </p>
            <p class="description">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam</p>
            <div class="divider"></div>
            <a href="javascript:void(0)" class="close_btn"><span class="fa fas fa-times"></span></a>
        </div>
    </section>
	
	<script>

		$(document).ready(function() {
			if ($(window).width() <= 568) {
				let itemWidth = $(".owl-carousel.clients .owl-item:first-child");
				itemWidth = itemWidth[0].style.width;
				$(".owl-carousel.clients .owl-item").attr("style", "width: " + itemWidth + " !important;");
			}

			// Close terms modal
			$(".terms_sec_2 .close_btn").click(function() {
				$(".terms_sec_2").hide();
			});
			$(".sign_up_sec .terms").click(function() {
				$(".terms_sec_2 ").css('display', 'flex');
			});
		});
			
			
		$("#signupForm").submit(function(e) {
			e.preventDefault();
			$(".loader").css('display', 'flex');
			
			$.ajax({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				data: $('#signupForm').serialize(),
				url: "<?php echo e(route('signup_submission')); ?>",
				type: "POST",
				// dataType: 'json',
				success: function (response) {
	
					if(response['status'] == 'success'){
						swal({
							title: response['message'],
							icon: 'success'
						});

						setTimeout(function(){
							$(".loader").css('display', 'none');
							swal.close();
							$('#signupForm').trigger("reset");
							window.location = "<?php echo e(route('online_visit_welcome')); ?>";
						}, 2000);
					}
				},
				error: function (data) {
					$(".loader").css('display', 'none');
					let errors = data.responseJSON.errors;
					
					$.each(errors, function(key, value) {
						errorAlert(value[0],5000,'bottom-left');
					});					
/* 					swal({
						title: 'Error Occured.',
						icon: 'error'
					}) */
					$('#saveBtn').html('Save');
				}
			});
		});		
	</script>	
<?php $__env->stopSection(); ?>
<?php echo $__env->make('front.online_visit_layout.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\laravel\replenishmd\resources\views/front/signup.blade.php ENDPATH**/ ?>