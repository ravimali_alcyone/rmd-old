<?php $__env->startSection('content'); ?>
    <div class="tabs_wrapper welcome basics m_qs">
        <div>
            <a href="javascript:void(0);" class="how_it_works">Medical Questions</a>
            <div class="steps">
                <span class="active ml-0"></span>
                <span class="active"></span>
                <span></span>
                <span></span>
            </div>
        </div>
        <span id="pageBackBtn" class="back_arrow fa fas fa-arrow-left" onclick="location.href = '<?php echo e(route('online_visit_basics')); ?>';"></span>
        <span id="blockBackBtn" class="back_arrow fa fas fa-arrow-left" onclick="goPrev()" style="display:none;"></span>
    </div>

    <div class="main_welcome_wrapper">
		<form id="onlineVisitForm">
        <section class="welcome_content_wrapper medical_qs">
            <div class="medical_qs_welcome">
                <h3 class="heading">Medical Questions</h3>
                <p class="description text-center">Tell your doctor about your symptoms and overall health. Your doctor needs this information to determine the most appropriate treatment for you. It's important that you are honest and respond as accurately as possible.</p>
                <div class="img_wrapper text-center">
                    <img src="<?php echo e(asset('assets/images/medical_qs.svg')); ?>" alt="medical_qs" class="img-fluid">
                </div>
                <button type="button" class="btn btn-primary">Continue</button>
            </div>
		
		<?php 
			if($medical_questions){
				$x=1;
				$button_text = 'Next';
				$is_submit = 0;

				foreach($medical_questions as $key => $val){
					
					if(count($medical_questions) == $x){ 
						$button_text = 'Save and Continue';
						$is_submit = 1;
					}
					
					if($val['question']['question_type'] == 'single'){
		?>
            <div class="questions medical_qs_<?php echo $x;?> animate__animated animate__fadeInRight" <?php if($x != 1){ echo 'style="display:none;"'; } ?> data-id="<?php echo $val['question']['question_id'];?>">
                <h5 class="question"><?php echo $val['question']['question_text'];?></h5>
				<input type="hidden" value="<?php echo $val['question']['question_id'];?>" name="question[]"/>
                <div class="answers_box">
			<?php 
				if($val['options']){
					$y=1;
					foreach($val['options'] as $key2 =>

					$op){
			?>
                    <div>
                        <input type="radio" name="answers[option][<?php echo $val['question']['question_id'];?>][0]" value="<?php echo $op['option_id'];?>" id="q<?php echo $x;?>_a<?php echo $y;?>">
                        <label for="q<?php echo $x;?>_a<?php echo $y;?>"><?php echo $op['option_text'];?></label>
                    </div>
			<?php 
				$y++;
					}
				}
			?>
                    <div class="btn_wrapper">
                        <a href="javascript:void(0);" class="btn btn-primary medical_qs_<?php echo $x;?>_btn disabled" <?php if($is_submit == 1){ echo 'onclick="goSubmit(this);"';}?> ><?php echo $button_text;?></a>
                    </div>
                </div>
            </div>
			<?php }elseif($val['question']['question_type'] == 'multiple'){?>
			
            <div class="questions medical_qs_<?php echo $x;?> animate__animated animate__fadeInRight" <?php if($x != 1){ echo 'style="display:none;"'; } ?> data-id="<?php echo $val['question']['question_id'];?>">
                <h5 class="question"><?php echo $val['question']['question_text'];?></h5>
				<input type="hidden" value="<?php echo $val['question']['question_id'];?>" name="question[]"/>
                <div class="answers_box">
			<?php 
				if($val['options']){
					$y=1;
					foreach($val['options'] as $key2 => $op){
			?>				
                    <div class="form-group">
                        <input type="checkbox" name="answers[option][<?php echo $val['question']['question_id'];?>][]" value="<?php echo $op['option_id'];?>" id="q<?php echo $x;?>_a<?php echo $y;?>" class="checkbox">
                        <label for="q<?php echo $x;?>_a<?php echo $y;?>" class="checkbox_label"><?php echo $op['option_text'];?></label>
                    </div>
			<?php 
				$y++;
					}
				}
			?>					
                    <div class="btn_wrapper">
                        <a href="javascript:void(0);" class="btn btn-primary medical_qs_<?php echo $x;?>_btn disabled" <?php if($is_submit == 1){ echo 'onclick="goSubmit(this);"';}?> ><?php echo $button_text;?></a>
                    </div>
                </div>
            </div>
		<?php }elseif($val['question']['question_type'] == 'long_text'){?>	
		
            <div class="questions medical_qs_<?php echo $x;?> animate__animated animate__fadeInRight" <?php if($x != 1){ echo 'style="display:none;"'; } ?> data-id="<?php echo $val['question']['question_id'];?>">
                <h5 class="question mb-0"><?php echo $val['question']['question_text'];?></h5>
				<input type="hidden" value="<?php echo $val['question']['question_id'];?>" name="question[]"/>
                <div class="answers_box">
                    <div class="comment_box">
                        <textarea name="answers[text][<?php echo $val['question']['question_id'];?>]" cols="30" rows="4" placeholder="Please type your answer here..."></textarea>
                    </div>
                    <div class="btn_wrapper">
                        <a href="javascript:void(0);" class="btn btn-primary medical_qs_<?php echo $x;?>_btn disabled" <?php if($is_submit == 1){ echo 'onclick="goSubmit(this);"';}?> ><?php echo $button_text;?></a>
                    </div>
                </div>
            </div>
		<?php }elseif($val['question']['question_type'] == 'short_text'){?>	
		
            <div class="questions medical_qs_<?php echo $x;?> animate__animated animate__fadeInRight" <?php if($x != 1){ echo 'style="display:none;"'; } ?> data-id="<?php echo $val['question']['question_id'];?>">
                <h5 class="question mb-0"><?php echo $val['question']['question_text'];?></h5>
				<input type="hidden" value="<?php echo $val['question']['question_id'];?>" name="question[]"/>
                <div class="answers_box">
                    <div class="comment_box">
                        <input name="answers[text][<?php echo $val['question']['question_id'];?>]" placeholder="Please type your answer here...">
                    </div>
                    <div class="btn_wrapper">
                        <a href="javascript:void(0);" class="btn btn-primary medical_qs_<?php echo $x;?>_btn disabled" <?php if($is_submit == 1){ echo 'onclick="goSubmit(this);"';}?> ><?php echo $button_text;?></a>
                    </div>
                </div>
            </div>			
		<?php }elseif($val['question']['question_type'] == 'yes_no'){?>			
		
            <div class="questions medical_qs_<?php echo $x;?> animate__animated animate__fadeInRight" <?php if($x != 1){ echo 'style="display:none;"'; } ?> data-id="<?php echo $val['question']['question_id'];?>">
                <h5 class="question"><?php echo $val['question']['question_text'];?></h5>
				<input type="hidden" value="<?php echo $val['question']['question_id'];?>" name="question[]"/>
                <div class="answers_box">
			<?php 
				if($val['options']){
					$y=1;
					foreach($val['options'] as $key2 =>

					$op){
			?>
                    <div>
                        <input type="radio" name="answers[option][<?php echo $val['question']['question_id'];?>]" value="<?php echo $op['option_id'];?>" id="q<?php echo $x;?>_a<?php echo $y;?>">
                        <label for="q<?php echo $x;?>_a<?php echo $y;?>"><?php echo $op['option_text'];?></label>
                    </div>
			<?php 
				$y++;
					}
				}
			?>
                    <div class="btn_wrapper">
                        <a href="javascript:void(0);" class="btn btn-primary medical_qs_<?php echo $x;?>_btn disabled" <?php if($is_submit == 1){ echo 'onclick="goSubmit(this);"';}?> ><?php echo $button_text;?></a>
                    </div>
                </div>
            </div>		
		<?php 
				}
			$x++;
				}
			}
		?>
        </section>
		<button type="submit" id="SaveBtn" style="display:none;" />
		</form>		
    </div>
	
    <script>
        $(document).ready(function() {
            $(".the_basics_sec .biological_sex span").click(function() {
                $(".the_basics_sec .biological_sex span").removeClass("active");
                $(this).addClass("active");
            });

            $(".medical_qs_welcome button").click(function() {
                $(".medical_qs_welcome").hide();
                $("#pageBackBtn").hide();
                $("#blockBackBtn").show();
                $(".medical_qs_1").show();
            });

		<?php 
			if($medical_questions){
				$x=1;
				foreach($medical_questions as $key => $val){
		?>
		
            $(".medical_qs_<?php echo $x;?>_btn").click(function() {
				console.log(<?php echo $x;?>);
                $(".medical_qs_<?php echo $x;?>").hide();
                $(".medical_qs_<?php echo $x+1;?>").show();
            });
		<?php 
			$x++;
				}
			}
		?>			
			
			//Radio
			$(".answers_box input[type=radio]").click(function(){
				$(this).parent().parent().find('.btn_wrapper a').removeClass('disabled');
			});
			
			//Checkbox
			$(".answers_box input[type=checkbox]").click(function(){
				
				var atLeastOneIsChecked = false;
				$(this).parent().parent().find('input:checkbox').each(function() {
					if ($(this).is(':checked')) {
					  atLeastOneIsChecked = true;
					  return false;
					}
				});

				if(atLeastOneIsChecked == true){
					$(this).parent().parent().find('.btn_wrapper a').removeClass('disabled');
				}else{
					$(this).parent().parent().find('.btn_wrapper a').addClass('disabled');
				}
				
			});	
			
			//Text
			$('.answers_box .comment_box input,textarea').on('input',function(e){
				
				if($(this).val() == ''){
					$(this).parent().parent().find('.btn_wrapper a').addClass('disabled');
				}else{
					$(this).parent().parent().find('.btn_wrapper a').removeClass('disabled');
				}
			});			
		
        });
		
		
		function goPrev(){
			$('.welcome_content_wrapper.medical_qs .questions').each(function() {
				if( $(this).css('display')== 'block' ) {
					let q_id = $(this).attr('data-id');
					if(q_id == 1){
						$(this).hide();					
						$('.medical_qs_welcome').show();
						$("#pageBackBtn").show();
						$("#blockBackBtn").hide();					
					}else{
						let pid = q_id - 1;
						$(this).hide();
						$(".welcome_content_wrapper.medical_qs [data-id="+pid+"]").show();
					}						
				}
			});			
		}
		
		function goSubmit(){
			$("#SaveBtn").trigger('click');
		}
		
		$("#onlineVisitForm").submit(function(e) {
			e.preventDefault();
						swal({
							title: 'working in backend.',
							icon: 'success'
						});			
			return false;
			$(".loader").css('display', 'flex');
			
			$.ajax({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				data: $('#onlineVisitForm').serialize(),
				url: "<?php echo e(route('onlineVisitSubmit')); ?>",
				type: "POST",
				// dataType: 'json',
				success: function (response) {
	
					if(response['status'] == 'success'){
						swal({
							title: response['message'],
							icon: 'success'
						});

						setTimeout(function(){
							$(".loader").css('display', 'none');
							swal.close();
							//window.location = "<?php echo e(route('treatment_preference')); ?>";
						}, 2000);
					}
				},
				error: function (data) {
					$(".loader").css('display', 'none');
					let errors = data.responseJSON.errors;
					
					$.each(errors, function(key, value) {
						errorAlert(value[0],5000,'bottom-left');
					});					
 					swal({
						title: 'Error Occured.',
						icon: 'error'
					})
				}
			});
		});		
    </script>	
<?php $__env->stopSection(); ?>
<?php echo $__env->make('front.online_visit_layout.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\laravel\replenishmd\resources\views/front/medical_questions.blade.php ENDPATH**/ ?>