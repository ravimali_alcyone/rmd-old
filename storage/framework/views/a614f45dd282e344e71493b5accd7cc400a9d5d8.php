<!DOCTYPE html>
<html lang="en">
    <!-- BEGIN: Head -->
    <head>
        <meta charset="utf-8">
        <link href="<?php echo e(asset('dist/images/logo.svg')); ?>" rel="shortcut icon">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="ReplenishMD admin login.">
        <meta name="keywords" content="ReplenishMD admin login">
        <meta name="author" content="Alcyone">
        <title>Login | <?php echo e(config('app.name', 'Laravel')); ?> - Admin </title>
        <!-- BEGIN: CSS Assets-->
        <link rel="stylesheet" href="<?php echo e(asset('dist/css/app.css')); ?>" />
        <!-- END: CSS Assets-->
    </head>
    <!-- END: Head -->
    <body class="login">
        <div class="container sm:px-10">
            <div class="block xl:grid grid-cols-2 gap-4">
				
				<?php echo $__env->yieldContent('content'); ?>
				
            </div>
        </div>
        <!-- BEGIN: Dark Mode Switcher-->
        <div style="display:none"class="dark-mode-switcher shadow-md fixed bottom-0 right-0 box dark:bg-dark-2 border rounded-full w-40 h-12 flex items-center justify-center z-50 mb-10 mr-10">
            <div class="mr-4 text-gray-700 dark:text-gray-300">Dark Mode</div>
            <input class="input input--switch border" type="checkbox" value="1">
        </div>
        <!-- END: Dark Mode Switcher-->
        <!-- BEGIN: JS Assets-->
        <script src="<?php echo e(asset('dist/js/app.js')); ?>"></script>
        <!-- END: JS Assets-->	
    </body>
</html><?php /**PATH C:\laravel\replenishmd\resources\views/layouts/login.blade.php ENDPATH**/ ?>