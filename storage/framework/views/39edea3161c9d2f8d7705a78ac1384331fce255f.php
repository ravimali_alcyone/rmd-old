<?php $__env->startSection('content'); ?>

    <section class="inner_banner banner_with_spike plans_banner">
        <div class="blue_bg_overlay">
            <div class="container">
                <div class="content_wrapper">
                    <div class="b_text text-center">
                        <h1>Terms of Use</h1>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                    </div>
                </div>
            </div>
            <img src="/assets/images/bottom_curve.svg" alt="bottom_curve">
        </div>
    </section>





    <section class="oac">
        <div class="terms_sec">
            <!-- <h1 class="title">ReplenishMD Medical Direct Health Care Program Membership Agreement</h1> -->

            <div class="sub_sec">
                <!-- <p class="sub_heading">INITIAL NOTICES:</p> -->
                <p>These terms and conditions of use (“Terms of Use”) govern your use of our online interfaces and properties (e.g., websites) owned and controlled by ReplenishMD, Inc., including the www.parsleyhealth.com website and the ReplenishMD
                    mobile application (the “Site”), as well as the services (“Services”) and products (“Products”) available to users through the Site. ReplenishMD, Inc. (“ReplenishMD,” “we,” “us,” and “our”) contracts with ReplenishMD Group
                    FL, P.A., ReplenishMD PC, ReplenishMD, P.L.L.C., ReplenishMD Group DE, P.C., and other affiliated practices (collectively “ReplenishMD”) regarding online telehealth medical consultations and secure messaging between
                    ReplenishMD physicians, health coaches, and other healthcare professionals (individually the “Provider” and collectively the “Providers”) and their patients. (See Section 20 for supplemental terms applicable to Providers.) The
                    professional medical services (which are provided by ReplenishMD) and the non-clinical Site services (which are provided by ReplenishMD) are collectively referred to in this Terms of Use as the “Services”. The terms “you” and
                    “your” means you, your dependent(s) if any, and any other person accessing your ReplenishMD Account.</p>
                <p>Your acceptance of, and compliance with, these Terms of Use is a condition to your use of the Site and Services and purchase of Products. By clicking “accept”, you acknowledge that you have read, understand, and accept all terms and conditions
                    contained within these Terms of Use, the Notice of Privacy Practices provided to you by the ReplenishMD, and our Privacy Policy. If you do not agree to be bound by these terms, you are not authorized to access or use this Site
                    or Services; promptly exit this Site.</p>

                <p>Binding Arbitration. These Terms of Use provide that all disputes between you and ReplenishMD that in any way relate to these Terms of Use or your use of the Site will be resolved by BINDING ARBITRATION. ACCORDINGLY, YOU AGREE TO GIVE
                    UP YOUR RIGHT TO GO TO COURT (INCLUDING IN A CLASS ACTION PROCEEDING) to assert or defend your rights under these Terms of Use. Your rights will be determined by a NEUTRAL ARBITRATOR and NOT a judge or jury and your claims cannot be
                    brought as a class action. Please review the Section below entitled Dispute Resolution; Arbitration Agreement for the details regarding your agreement to arbitrate any disputes with ReplenishMD.</p>
            </div>
        </div>
    </section>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('front.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\laravel\replenishmd\resources\views/front/terms_of_use.blade.php ENDPATH**/ ?>