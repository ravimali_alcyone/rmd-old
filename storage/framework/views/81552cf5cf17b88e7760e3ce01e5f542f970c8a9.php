<!DOCTYPE HTML>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<?php if(isset($meta_description)): ?><meta name="description" content="<?php echo e($meta_description); ?>"><?php endif; ?>
    <title><?php echo e(config('app.name')); ?> <?php if(isset($meta_title)): ?> - <?php echo e($meta_title); ?> <?php endif; ?></title>
	<meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
    <link href="<?php echo e(asset('assets/images/favicon.png')); ?>" rel="shortcut icon" type="image/x-icon">
    <link href="<?php echo e(asset('assets/css/bootstrap.min.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css" type="text/css" rel="stylesheet">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;600;700&display=swap">
    <link href="<?php echo e(asset('assets/css/main.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('assets/css/owl.carousel.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('assets/css/responsive.css')); ?>" rel="stylesheet" />
	<link rel="stylesheet" href="<?php echo e(asset('plugins/sweetalert/sweetalert.css')); ?>" />
	<link href="<?php echo e(asset('assets/css/select2.min.css')); ?>" rel="stylesheet" />
	<link href="<?php echo e(asset('assets/css/jquery-toaster.css')); ?>" rel="stylesheet" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css">
	
	<script src="<?php echo e(asset('assets/js/jquery.min.js')); ?>"></script>
	<script src="<?php echo e(asset('plugins/sweetalert/sweetalert.min.js')); ?>"></script>
	<script src="<?php echo e(asset('assets/js/select2.min.js')); ?>"></script>	
	<script src="<?php echo e(asset('assets/js/jquery-toaster.min.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/js/bootstrap.bundle.min.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/js/jquery.easing.min.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/js/owl.carousel.js')); ?>" type="text/javascript"></script>	
	<script src="<?php echo e(asset('assets/js/custom.js')); ?>"></script>	
</head>

<body id="page-top">
    <!-- Loader -->
    <div class="loader" style="display:none;">
        <img src="<?php echo e(asset('assets/images/loading.gif')); ?>" alt="loading">
    </div>
	
    <header class="sign_up_header shadow">
        <div class="wrapper">
            <div>
                <a href="<?php echo e(env('APP_URL')); ?>"><img src="<?php echo e(asset('assets/images/rmd_logo_red.svg')); ?>" alt="Red Logo RMD"></a>
            </div>
            <div>
                <a href="#" class="blue_btn">Help?</a>
            </div>
        </div>
    </header>		

<?php /**PATH C:\laravel\replenishmd\resources\views/front/online_visit_layout/header.blade.php ENDPATH**/ ?>