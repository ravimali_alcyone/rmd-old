<?php $__env->startSection('content'); ?>

    <section class="inner_banner banner_with_spike plans_banner">
        <div class="blue_bg_overlay">
            <div class="container">
                <div class="content_wrapper">
                    <div class="b_text text-center">
                        <h1>Pricing & Plans</h1>
                        <p>As a ReplenishMD member, you receive a highly trained care team who use holistic medicine to transform your health.</p>
                    </div>
                </div>
            </div>
            <img src="/assets/images/bottom_curve.svg" alt="bottom_curve">
        </div>
    </section>

    <section class="plans_sec">
        <div class="container">
            <h1 class="title">Select Right Plan For You!</h1>
            <div class="plans_wrapper">
                <div class="plan shadow">
                    <div class="head">
                        <h4 class="title">Free Trial</h4>
                        <p class="amount"><span class="fa fa-"></span> 0.00</p>
                        <p class="short_des">Try for free for first 3 weeks</p>
                    </div>
                    <div class="body">
                        <p>Work with our Care Team online to understand your health concerns and start a personalized health plan.</p>
                        <div class="points_wrapper">
                            <div class="point mt-0"><span class="fa fas fa-check"></span>3 weeks of membership</div>
                            <div class="point"><span class="fa fas fa-check"></span>1 care manager visit (online)</div>
                            <div class="point"><span class="fa fas fa-check"></span>1 health coach visit (online)</div>
                            <div class="point"><span class="fa fas fa-check"></span>Introductory health plan</div>
                        </div>
                        <button class="btn btn-primary">Select</button>
                    </div>
                </div>
                <div class="plan shadow">
                    <div class="head">
                        <h4 class="title">12 Month Plan</h4>
                        <p class="amount"><span class="fa fa-"></span> 150.00</p>
                        <p class="short_des">Monthly for 12 months</p>
                    </div>
                    <div class="body">
                        <p>One full year of care and access to membership benefits.</p>
                        <div class="points_wrapper">
                            <div class="point mt-0"><span class="fa fas fa-check"></span>5 doctor visits (in person or online)</div>
                            <div class="point"><span class="fa fas fa-check"></span>5 health coach visits (online)</div>
                            <div class="point"><span class="fa fas fa-check"></span>Long-term health plan</div>
                            <div class="point"><span class="fa fas fa-check"></span>Unlimited messaging</div>
                            <div class="point"><span class="fa fas fa-check"></span>Advanced testing available</div>
                        </div>
                        <button class="btn btn-primary">Select</button>
                    </div>
                    <p class="popular_tag">MOST POPULAR</p>
                </div>
                <div class="plan shadow">
                    <div class="head">
                        <h4 class="title">4 Month Plan</h4>
                        <p class="amount"><span class="fa fa-"></span> 175.00</p>
                        <p class="short_des">Monthly for 4 months</p>
                    </div>
                    <div class="body">
                        <p>Work with your care team and access your membership benefits.</p>
                        <div class="points_wrapper">
                            <div class="point mt-0"><span class="fa fas fa-check"></span> 2 doctor visits (online)</div>
                            <div class="point"><span class="fa fas fa-check"></span> 3 health coach visits (online)</div>
                            <div class="point"><span class="fa fas fa-check"></span> Targeted health plan</div>
                            <div class="point"><span class="fa fas fa-check"></span> Unlimited messaging</div>
                            <div class="point"><span class="fa fas fa-check"></span> Advanced testing available</div>
                        </div>
                        <button class="btn btn-primary">Select</button>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="about_sec">
        <div class="container">
            <div class="row expertise_row">
                <div class="col-sm-12 col-md-6">
                    <div class="img_wrapper">
                        <img src="/assets/images/care_1.png" alt="Care_1" class="w-auto">
                    </div>
                </div>
                <div class="col-sm-12 col-md-6">
                    <div class="content_wrapper">
                        <h1 class="title">Covering your care.</h1>
                        <p class="p_md">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna</p>
                        <ul>
                            <li>
                                <span class="fa fas fa-check"></span>
                                <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna...</p>
                            </li>
                            <li>
                                <span class="fa fas fa-check"></span>
                                <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna...</p>
                            </li>
                            <li>
                                <span class="fa fas fa-check"></span>
                                <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna...</p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="oac plans_oac">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="title">All membership plans include</div>
                    <div class="oac_list">
                        <div class="oac_box">
                            <div class="oacb_icon"><img src="/assets/images/icon1.svg" class="img-fluid" alt=""></div>
                            <div class="oac_detail">
                                <h4>Concierge services</h4>
                                <p>Whether it is a Botox party, a weight loss consultation or testosterone replacement therapy consult, our providers can come to you where ever you are.</p>
                            </div>
                        </div>
                        <div class="oac_box">
                            <div class="oacb_icon"><img src="/assets/images/icon2.svg" class="img-fluid" alt=""></div>
                            <div class="oac_detail">
                                <h4>Get quick and Easy Treatment Options</h4>
                                <p>We've created a host of treatments that members and soon to be members desire. We've made the process to get treated streamlined and more convenient - just for you.</p>
                            </div>
                        </div>
                        <div class="oac_box">
                            <div class="oacb_icon"><img src="/assets/images/icon3.svg" class="img-fluid" alt=""></div>
                            <div class="oac_detail">
                                <h4>Get Support from Our Community</h4>
                                <p>We have a q&a section where you can ask licensed providers questions about treatments and conditions. We also have a forum for added support.</p>
                            </div>
                        </div>
                        <div class="oac_box">
                            <div class="oacb_icon"><img src="/assets/images/icon1.svg" class="img-fluid" alt=""></div>
                            <div class="oac_detail">
                                <h4>Contests/ Challenges</h4>
                                <p>We have a q&a section where you can ask licensed providers questions about treatments and conditions. We also have a forum for added support.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('front.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\laravel\replenishmd\resources\views/front/plans.blade.php ENDPATH**/ ?>