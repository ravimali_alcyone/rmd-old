
var controller = new ScrollMagic.Controller();

$(".mob_img").each(function() {
  var tl = new TimelineMax();
  var child = $(this).find(".phone_mock");
  var child2 = $(this).find(".frt_img");
  tl.to(child, 1, { y: -180, ease: Linear.easeNone });
  tl.to(child2, 1, { y: -120, ease: Linear.ease}, "-=1");
  var scene = new ScrollMagic.Scene({
    triggerElement: this,
    triggerHook: 0.4,
    duration: "100%"
  })
    .setTween(tl)
    .addTo(controller);
});


$(".why_rmd_2 .img_box").each(function() {
    var tl = new TimelineMax();
    var child = $(this).find(".w_rmd_img");
    var child2 = $(this).find(".frt_img");
    tl.to(child, 1, { y: -180, ease: Linear.easeNone });
    tl.to(child2, 1, { y: -120, ease: Linear.ease }, "-=1");
    var scene = new ScrollMagic.Scene({
            triggerElement: this,
            triggerHook: 0.4,
            duration: "100%"
        })
        .setTween(tl)
        .addTo(controller);
});