$(document).ready(function() {

    $('.exp_carousel').owlCarousel({
        loop: true,
        margin: 0,
        responsiveClass: true,
        nav: true,
        autoplay: false,
        responsive: {
            0: {
                items: 1,

            },
            400: {
                items: 1,
            },
            600: {
                items: 1,
            },
            800: {
                items: 3,
            },
            1500: {
                items: 3,
            }
        }
    });
    $('.talks').owlCarousel({
        loop: true,
        margin: 0,
        responsiveClass: true,
        nav: true,
        autoplay: false,
        responsive: {
            0: {
                items: 1,

            },
            400: {
                items: 1,
            },
            600: {
                items: 1,
            },
            800: {
                items: 3,
            },
            1500: {
                items: 4,
            }
        }
    });
    $('.expert_caro').owlCarousel({
        loop: true,
        margin: 15,
        responsiveClass: true,
        nav: false,
        autoplay: false,
        center: true,
        responsive: {
            0: {
                items: 1,

            },
            400: {
                items: 1,
            },
            600: {
                items: 1,
            },
            800: {
                items: 3,
            },
            1500: {
                items: 4,
            }
        }
    });
    $('.sponsers_caro').owlCarousel({
        loop: true,
        margin: 15,
        responsiveClass: true,
        nav: false,
        autoplay: false,
        center: true,
        responsive: {
            0: {
                items: 1,

            },
            400: {
                items: 1,
            },
            600: {
                items: 1,
            },
            812: {
                items: 3,
            },
            1500: {
                items: 5,
            }
        }
    });

    $('.clients').owlCarousel({
        center: false,
        items: 1,
        loop: true,
        margin: 0,
        responsiveClass: true,
        nav: false,
        autoplay: false,
        dots: true
    });

});



	
// Success Snackbar Toast
function successAlert(message, time, position) {
    $.toast({
        heading: '<img src="/../assets/images/icon-check-circle.png" alt="icon-check-circle" width="20" class="check-circle"> ' + message,
        showHideTransition: 'slide',
		bgColor: "#28a745",
        position: position,
        hideAfter: false
    })
    setTimeout(function() {
        $(".jq-toast-single").css('display', 'none');
    }, time);
}


// Error Snackbar Toast
function errorAlert(message, time, position) {
    $.toast({
        heading: '<img src="/../assets/images/icon-warning.png" alt="icon-warning" width="20" class="check-circle"> ' + message,
        showHideTransition: 'slide',
        bgColor: "#9e1220",
        position: position,
        hideAfter: false,
    });
    setTimeout(function() {
        $(".jq-toast-single").css('display', 'none');
    }, time);
}

// Warning Snackbar Toast
function warningAlert(message, time, position) {
    $.toast({
        heading: '<img src="/../assets/images/icon-warning.png" alt="icon-warning" width="20" class="check-circle"> ' + message,
        showHideTransition: 'slide',
        bgColor: "#ff9c07",
        position: position,
        hideAfter: false,
    });
    setTimeout(function() {
        $(".jq-toast-single").css('display', 'none');
    }, time);
}

