-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 18, 2020 at 01:32 PM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `replenishmd`
--

-- --------------------------------------------------------

--
-- Table structure for table `contact_us`
--

CREATE TABLE `contact_us` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_category` tinyint(1) NOT NULL DEFAULT 1 COMMENT '1=visitor, 2=provider, 3=patient',
  `subject` enum('product_questions','order_inquiry','complaint','other') COLLATE utf8mb4_unicode_ci NOT NULL,
  `other` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `comments` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0=unread, 1= read',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contact_us`
--

INSERT INTO `contact_us` (`id`, `name`, `user_category`, `subject`, `other`, `email`, `phone`, `comments`, `status`, `created_at`, `updated_at`) VALUES
(1, 'John Doe', 1, 'order_inquiry', NULL, 'john.d877@yopmail.com', '9809809801', 'My order is still not completed. please help', 0, '2020-08-07 18:30:00', '2020-08-08 05:41:35'),
(2, 'Robert Clark', 2, 'other', 'login issue', 'john.d877@yopmail.com', '9879879871', 'I can\'t login using my credentials and even I can\'t reset my password.', 0, '2020-08-07 18:30:00', '2020-08-07 18:30:00');

-- --------------------------------------------------------

--
-- Table structure for table `diagnosis_categories`
--

CREATE TABLE `diagnosis_categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `parent_id` int(10) NOT NULL DEFAULT 0,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `diagnosis_categories`
--

INSERT INTO `diagnosis_categories` (`id`, `parent_id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 0, 'Hypertension', 1, '2020-07-22 18:30:00', '2020-07-22 18:30:00'),
(2, 0, 'Hyperlipidemia', 1, '2020-07-22 18:30:00', '2020-07-22 18:30:00'),
(3, 0, 'Anxiety', 1, '2020-07-22 18:30:00', '2020-07-22 18:30:00'),
(4, 0, 'Back pain', 1, '2020-07-22 18:30:00', '2020-07-22 18:30:00'),
(5, 0, 'Urinary tract infection', 1, '2020-07-22 18:30:00', '2020-07-22 18:30:00'),
(6, 0, 'Obesity', 1, '2020-07-22 18:30:00', '2020-07-22 18:30:00'),
(7, 0, 'Allergic rhinitis', 1, '2020-07-22 18:30:00', '2020-07-22 18:30:00'),
(8, 0, 'Osteoarthritis', 1, '2020-07-22 18:30:00', '2020-07-22 18:30:00');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `faqs`
--

CREATE TABLE `faqs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category_id` int(10) NOT NULL DEFAULT 0,
  `sub_category_id` int(10) NOT NULL DEFAULT 0,
  `faq_data` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `faqs`
--

INSERT INTO `faqs` (`id`, `category_id`, `sub_category_id`, `faq_data`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 0, '[{\"question\":\"Who is ReplenishMD for?\",\"answer\":\"ReplenishMD is for adults who are experiencing erectile dysfunction, hair loss, premature ejaculation, cold sores, or genital herpes and prefer to receive treatment from the comfort of their home.\"},{\"question\":\"Who is ReplenishMD?\",\"answer\":\"ReplenishMD is the healthcare company that makes Roman. We are a direct-to-consumer telehealth company that handles everything from diagnosis to the convenient delivery of medication.\"},{\"question\":\"Who are the healthcare professionals on the ReplenishMD platform?\",\"answer\":\"All of the physicians and nurse practitioners on the ReplenishMD platform are U.S. licensed healthcare professionals. Each doctor or nurse practitioner undergoes an extensive background check and license verification process.   When you message or talk with a physician or nurse practitioner on the ReplenishMD platform you can see their resume, medical license, and credentials anytime you want. Just click on their name in the messaging thread in your account and take a look.\"}]', 1, '2020-07-22 08:03:28', '2020-07-23 02:16:23'),
(2, 2, 4, '[{\"question\":\"How does hair loss treatment with ReplenishMD work?\",\"answer\":\"We use telemedicine technology and U.S. licensed physicians  to provide hair loss treatment that\\u2019s reliable, convenient, and discreet.\"},{\"question\":\"What type of hair loss treatment do you prescribe?\",\"answer\":\"Our physicians may prescribe finasteride (generic Propecia) to treat male pattern hair loss (androgenic alopecia). This medication helps patients maintain the hair they have or possibly even regrow some hair they have lost.\"},{\"question\":\"Is hair loss treatment effective?\",\"answer\":\"Hair loss treatment can work to preserve hair and possibly even regrow hair. In one study, Finasteride was effective at stopping hair loss in 83% of men compared to 28% for the placebo group.\"},{\"question\":\"Do I need to take a photo of my hair?\",\"answer\":\"Yes. During your online visit, we\\u2019ll ask you to take and upload a few photos. This helps your physician determine the appropriate treatment option.\"}]', 1, '2020-07-23 02:11:54', '2020-07-23 02:11:54'),
(3, 2, 5, '[{\"question\":\"How does cold sore (oral herpes) treatment with ReplenishMD work?\",\"answer\":\"We use telemedicine technology and U.S. licensed healthcare professionals to provide cold sore treatments discreetly, conveniently, and inexpensively.  It starts with your online visit. Your doctor or nurse practitioner needs to know about your health (e.g., your medications, lifestyle issues, prior surgeries) and how cold sores affect you.\"},{\"question\":\"What type of cold sore medication do you prescribe?\",\"answer\":\"Our Physicians prescribe Valacyclovir (generic Valtrex) to treat cold sores (oral herpes). When used properly, this medication can abort an outbreak at the first sign that one is coming on or lessen its severity.\"},{\"question\":\"Is cold sore medication effective?\",\"answer\":\"Cold sore (oral herpes) medication is available, and it works extremely well. But it requires you to dedicate the time to learn about your condition and work with your physician to craft the ideal, personalized treatment plan.\"},{\"question\":\"Can a doctor treat cold sores remotely?\",\"answer\":\"Yes. Our doctors and nurse practitioners can prescribe medication and help you dial in the most effective treatment plan for your unique needs.\"}]', 1, '2020-07-22 08:04:11', '2020-07-23 02:10:03'),
(4, 7, 0, '[{\"question\":\"How much does a visit cost?\",\"answer\":\"Your online visit costs $15. This doesn\\u2019t include the cost of your medication (if prescribed). If a physician or nurse practitioner determines you\\u2019re not a good candidate for telemedicine, you\\u2019ll get a full refund for your online visit.\"},{\"question\":\"Can I return my shipment?\",\"answer\":\"Unfortunately we cannot accept returns of prescription products for reuse or resale, and all sales are final. However, if you feel we have made an error in the filling of your prescription.\"},{\"question\":\"How much does the medication cost?\",\"answer\":\"If prescribed, the cost of treatment depends on the type of medication and the amount of medication you receive. Prices at your local pharmacy may vary and are often twice the cost of the Ro Pharmacy Network.\"},{\"question\":\"Do I need to be home to sign for my order?\",\"answer\":\"Signatures are not required upon delivery for the convenience of our members.\"},{\"question\":\"When will I receive my order?\",\"answer\":\"Once your order is shipped you will receive an email link to track your shipment. All orders are shipped 2 Day Air to ensure medication is received as soon as possible.  Orders are only able to be shipped on weekdays; orders placed after 5 PM will ship next day.\"},{\"question\":\"Is ReplenishMD covered by insurance?\",\"answer\":\"Services on the ReplenishMD platform are not covered by insurance, but the $15 online visit is less than most co-pays.\"}]', 1, '2020-07-23 02:13:55', '2020-07-23 02:15:54');

-- --------------------------------------------------------

--
-- Table structure for table `faq_categories`
--

CREATE TABLE `faq_categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `parent_id` int(10) NOT NULL DEFAULT 0,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `faq_categories`
--

INSERT INTO `faq_categories` (`id`, `parent_id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 0, 'About', 1, '2020-07-05 18:30:00', '2020-07-05 18:30:00'),
(2, 0, 'Diagnoses', 1, '2020-07-05 18:30:00', '2020-07-05 18:30:00'),
(4, 2, 'Hair Loss', 1, '2020-07-05 18:30:00', '2020-07-05 18:30:00'),
(5, 2, 'Cold Sores', 1, '2020-07-05 18:30:00', '2020-07-05 18:30:00'),
(6, 2, 'Premature ejaculation', 1, '2020-07-05 18:30:00', '2020-07-05 18:30:00'),
(7, 0, 'Cost, Pricing & Services', 1, '2020-07-05 18:30:00', '2020-07-05 18:30:00');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2019_08_19_000000_create_failed_jobs_table', 1),
(3, '2014_10_12_100000_create_password_resets_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `online_visits`
--

CREATE TABLE `online_visits` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `visit_type` enum('online','concierge','','') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'online',
  `patient_id` int(10) NOT NULL DEFAULT 0 COMMENT 'F.K. users table patients',
  `service_id` int(10) NOT NULL DEFAULT 0 COMMENT 'F.K.(services) table Treatment ooking for',
  `feedback_detail` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `visit_amount` float DEFAULT NULL,
  `visit_status` enum('auto_assigned','in_pool','not_assigned','picked','reviewed','contacted','report_submitted','being_evaluated','complete') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'in_pool',
  `payment_status` enum('null','pending','done','failed','refunded') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `online_visits`
--

INSERT INTO `online_visits` (`id`, `visit_type`, `patient_id`, `service_id`, `feedback_detail`, `visit_amount`, `visit_status`, `payment_status`, `created_at`, `updated_at`) VALUES
(1, 'online', 4, 2, 'It is very good.', 100, 'picked', 'done', '2020-08-09 05:59:00', '2020-08-18 04:07:14'),
(2, 'concierge', 17, 5, 'I am feeling good now.', 50, 'picked', 'done', '2020-08-10 09:43:08', '2020-08-18 04:08:34'),
(3, 'online', 40, 1, 'It is very good.', 100, 'in_pool', 'done', '2020-08-09 05:59:00', '2020-08-12 04:39:25'),
(4, 'concierge', 41, 9, 'I am feeling good now.', 50, 'in_pool', 'done', '2020-08-10 09:43:08', '2020-08-17 08:15:24');

-- --------------------------------------------------------

--
-- Table structure for table `online_visit_provider_assign`
--

CREATE TABLE `online_visit_provider_assign` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `online_visit_id` int(10) NOT NULL DEFAULT 0 COMMENT 'F.K. online visits table',
  `provider_id` int(10) NOT NULL DEFAULT 0 COMMENT 'F.K. of providers(users table)',
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT 'Active/Inactive, differnt from visits status',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `online_visit_provider_assign`
--

INSERT INTO `online_visit_provider_assign` (`id`, `online_visit_id`, `provider_id`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 3, 1, '2020-08-09 05:59:00', '2020-08-09 05:59:00'),
(2, 2, 31, 1, '2020-08-10 09:43:08', '2020-08-10 05:59:00'),
(3, 3, 0, 1, '2020-08-09 05:59:00', '2020-08-09 05:59:00'),
(4, 4, 0, 1, '2020-08-10 09:43:08', '2020-08-10 05:59:00');

-- --------------------------------------------------------

--
-- Table structure for table `online_visit_status_timeline`
--

CREATE TABLE `online_visit_status_timeline` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `online_visit_id` int(10) NOT NULL DEFAULT 0 COMMENT 'F.K. online visits table',
  `old_visit_status` enum('auto_assigned','in_pool','not_assigned','picked','reviewed','contacted','report_submitted') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `new_visit_status` enum('auto_assigned','in_pool','not_assigned','picked','reviewed','contacted','report_submitted') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `online_visit_status_timeline`
--

INSERT INTO `online_visit_status_timeline` (`id`, `online_visit_id`, `old_visit_status`, `new_visit_status`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, 'in_pool', '2020-08-08 22:43:08', '2020-08-08 22:43:08'),
(2, 2, NULL, 'in_pool', '2020-08-08 22:43:08', '2020-08-08 22:43:08'),
(3, 3, NULL, 'in_pool', '2020-08-08 22:43:08', '2020-08-08 22:43:08'),
(4, 4, NULL, 'in_pool', '2020-08-08 22:43:08', '2020-08-08 22:43:08'),
(5, 1, 'in_pool', 'picked', '2020-08-18 04:07:14', '2020-08-18 04:07:14'),
(6, 2, 'in_pool', 'picked', '2020-08-18 04:08:34', '2020-08-18 04:08:34');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('admin@gmail.com', '$2y$10$ewPMDnWpbgwH3Nst3WbiQuap.3Tp.Sf95fNWTERg1/.oPZnybL/3S', '2020-07-04 03:37:45');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category` int(10) NOT NULL DEFAULT 0,
  `sub_category` int(10) NOT NULL DEFAULT 0,
  `sub_sub_category` int(10) DEFAULT 0,
  `images` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price_type` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price_value` float NOT NULL DEFAULT 0,
  `discount_price` float NOT NULL DEFAULT 0,
  `stock_status` enum('in','out','','') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'in',
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_info` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `stock_qty` int(10) NOT NULL DEFAULT 0,
  `manufacturer_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `is_direct_purchase` tinyint(1) NOT NULL DEFAULT 1,
  `safety_info` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `associate_problems` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `category`, `sub_category`, `sub_sub_category`, `images`, `price_type`, `price_value`, `discount_price`, `stock_status`, `description`, `details`, `product_info`, `stock_qty`, `manufacturer_name`, `is_direct_purchase`, `safety_info`, `associate_problems`, `status`, `created_at`, `updated_at`) VALUES
(20, 'PhytoMulti®', 3, 0, 0, '[\"uploads\\/products\\/1595855401.test2.jfif\",\"uploads\\/products\\/1595855401.test1.jfif\"]', 'dose', 60, 50, 'in', '<p><strong>PhytoMulti&reg;</strong>&nbsp;takes you beyond basic wellness support. It has a proprietary blend of 13 concentrated extracts and phytonutrients with scientifically tested biological activity to support cellular health and overall wellness.*</p>', '<p><strong>Other Ingredients:&nbsp;</strong>Microcrystalline cellulose, croscarmellose sodium, cellulose, stearic acid (vegetable), silica, and coating [hypromellose, medium-chain triglycerides, hydroxypropylcellulose, and sodium copper chlorophyllin (color)].</p>\n\n<p><strong>Directions:&nbsp;</strong>Take one to two tablets once daily with food or as directed by your healthcare practitioner.</p>\n\n<p><strong>This product is non-GMO and gluten-free.</strong></p>', '<table border=\"0\" style=\"width:100%\">\n	<tbody>\n		<tr>\n			<th>Ingredient</th>\n			<th>Amount Per Serving</th>\n			<th>% Daily Value</th>\n		</tr>\n		<tr>\n			<td>Serving Size</td>\n			<td>2 Tablets</td>\n			<td>&nbsp;</td>\n		</tr>\n		<tr>\n			<td>Servings Per Container</td>\n			<td>60</td>\n			<td>&nbsp;</td>\n		</tr>\n	</tbody>\n	<tbody>\n		<tr>\n			<td>Total Carbohydrate</td>\n			<td>&lt;1 g</td>\n			<td>&lt;1%*</td>\n		</tr>\n		<tr>\n			<td>&nbsp;&nbsp;&nbsp; Dietary Fiber</td>\n			<td>&lt;1 g</td>\n			<td>2%*</td>\n		</tr>\n		<tr>\n			<td>Vitamin A (50% from mixed carotenoids and 50% as retinyl acetate)</td>\n			<td>3,000 mcg</td>\n			<td>333%</td>\n		</tr>\n		<tr>\n			<td>Vitamin C (as ascorbic acid and ascorbyl palmitate)</td>\n			<td>120 mg</td>\n			<td>133%</td>\n		</tr>\n		<tr>\n			<td>Vitamin D (as cholecalciferol)</td>\n			<td>1000 IU</td>\n			<td>125%</td>\n		</tr>\n		<tr>\n			<td>Vitamin E (as d-alpha tocopheryl succinate)</td>\n			<td>67 mg</td>\n			<td>447%</td>\n		</tr>\n		<tr>\n			<td>Vitamin K (as phytonadione)</td>\n			<td>120 mcg</td>\n			<td>100%</td>\n		</tr>\n		<tr>\n			<td>Thiamin (as thiamin mononitrate)</td>\n			<td>25 mg</td>\n			<td>2,083%</td>\n		</tr>\n		<tr>\n			<td>Riboflavin</td>\n			<td>15 mg</td>\n			<td>1,154%</td>\n		</tr>\n		<tr>\n			<td>Niacin (as niacinamide and niacin)</td>\n			<td>50 mg</td>\n			<td>313%</td>\n		</tr>\n		<tr>\n			<td>Vitamin B6&nbsp;(as pyridoxine HCl)</td>\n			<td>25 mg</td>\n			<td>1,471%</td>\n		</tr>\n		<tr>\n			<td>Folate (as calcium L-5-methyltetrahydrofolate)&dagger;</td>\n			<td>1,360 mcg DFE</td>\n			<td>340%</td>\n		</tr>\n		<tr>\n			<td>Vitamin B12&nbsp;(as methylcobalamin)</td>\n			<td>200 mcg</td>\n			<td>8,333%</td>\n		</tr>\n		<tr>\n			<td>Biotin</td>\n			<td>500 mcg</td>\n			<td>1,667%</td>\n		</tr>\n		<tr>\n			<td>Pantothenic Acid (as calcium D-pantothenate)</td>\n			<td>75 mg</td>\n			<td>1,500%</td>\n		</tr>\n	</tbody>\n</table>', 500, 'PhytoMulti.com', 1, '<p><strong>Warning:&nbsp;</strong>Do not use if pregnant or nursing. Excess vitamin A may increase the risk of birth defects. Pregnant women and women who may become pregnant should not exceed 3,000 mcg of preformed vitamin A per day.</p>\n\n<p><strong>Caution:&nbsp;</strong>If taking medications consult your healthcare practitioner before use. Keep out of the reach of children.</p>\n\n<p><strong>Storage:</strong>&nbsp;Keep tightly closed in a cool, dry place.</p>\n\n<p>&dagger;As Metafolin&reg;. Metafolin&reg;&nbsp;is a registered trademark of Merck KGaA, Darmstadt Germany</p>', NULL, 0, '2020-07-25 03:03:23', '2020-07-28 04:54:09'),
(21, 'OmegaGenics® EPA-DHA 1000', 2, 0, 0, '[\"uploads\\/products\\/1595855988.42845.jfif\",\"uploads\\/products\\/1595855988.19981.jfif\"]', 'dose', 150, 140, 'in', '<p><strong>OmegaGenics&reg;&nbsp;EPA-DHA 1000</strong>&nbsp;features a concentrated, purified source of omega-3 fatty acids in triglyceride form from sustainably sourced, cold-water fish. Each softgel provides a total of 710 mg EPA and 290 mg DHA.</p>\n\n<p>All OmegaGenics formulas are tested for purity and quality and stabilized with antioxidants to maintain freshness. Learn more about TruQuality&reg;</p>', '<p><strong>ngredients:</strong>&nbsp;Marine lipid concentrate [fish (anchovy, sardine, and mackerel) oil], softgel shell (gelatin, glycerin, water), contains 2 percent or less of natural lemon flavor, mixed tocopherols (antioxidant), rosemary extract, and ascorbyl palmitate (antioxidant).&nbsp;<strong>Contains: Fish (anchovy, sardine, and mackerel).</strong></p>\n\n<p><strong>Directions:</strong>&nbsp;Take one softgel up to three times daily with food or as directed by your healthcare practitioner.</p>\n\n<p><strong>This product is non-GMO and gluten-free.</strong></p>', '<table border=\"0\" style=\"width:100%\">\n	<tbody>\n		<tr>\n			<th>Ingredients</th>\n			<th>Amount Per Serving</th>\n			<th>% Daily Value</th>\n		</tr>\n		<tr>\n			<td>Serving Size</td>\n			<td>1 Softgel</td>\n			<td>&nbsp;</td>\n		</tr>\n		<tr>\n			<td>Servings Per Container</td>\n			<td>60</td>\n			<td>&nbsp;</td>\n		</tr>\n		<tr>\n			<td>Calories</td>\n			<td>15</td>\n			<td>&nbsp;</td>\n		</tr>\n		<tr>\n			<td>Total Fat</td>\n			<td>1.5 g</td>\n			<td>2%*</td>\n		</tr>\n		<tr>\n			<td>Cholesterol</td>\n			<td>5 mg</td>\n			<td>2%</td>\n		</tr>\n		<tr>\n			<td>Marine Lipid Concentrate</td>\n			<td>1.4 g</td>\n			<td>**</td>\n		</tr>\n		<tr>\n			<td>&nbsp;&nbsp;&nbsp;EPA (Eicosapentaenoic acid triglyceride)</td>\n			<td>710 mg</td>\n			<td>**</td>\n		</tr>\n		<tr>\n			<td>&nbsp;&nbsp;&nbsp;DHA (Docosahexaenoic acid triglyceride)</td>\n			<td>290 mg</td>\n			<td>**</td>\n		</tr>\n		<tr>\n			<td>&nbsp;&nbsp;&nbsp;Other Omega-3 Fatty Acid Triglycerides</td>\n			<td>100 mg</td>\n			<td>**</td>\n		</tr>\n	</tbody>\n</table>', 300, 'Metagenics', 1, '<p><strong>Caution:</strong>&nbsp;Consult your healthcare practitioner if pregnant, nursing, or taking other nutritional supplements or medications. Keep out of the reach of children.</p>\n\n<p><strong>Storage:</strong>&nbsp;Keep tightly closed in a cool, dry place.</p>\n\n<p>This product is manufactured in a facility that processes soy and fish.</p>\n\n<p>*Percent Daily Values are based on a 2,000 calorie diet.<br />\n**Daily Value not established.</p>', NULL, 1, '2020-07-27 07:49:48', '2020-08-05 05:36:27');

-- --------------------------------------------------------

--
-- Table structure for table `product_categories`
--

CREATE TABLE `product_categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `parent_id` int(10) NOT NULL DEFAULT 0,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_categories`
--

INSERT INTO `product_categories` (`id`, `parent_id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 0, 'Gastrointestinal Health', 1, '2020-07-05 18:30:00', '2020-07-05 18:30:00'),
(2, 0, 'Cardiometabolic Health', 1, '2020-07-05 18:30:00', '2020-07-05 18:30:00'),
(3, 0, 'Immune Health', 1, '2020-07-05 18:30:00', '2020-07-05 18:30:00'),
(4, 0, 'Muscle, Bone & Joint Health', 1, '2020-07-05 18:30:00', '2020-07-05 18:30:00'),
(5, 0, 'Neurological Health', 1, '2020-07-05 18:30:00', '2020-07-05 18:30:00'),
(6, 0, 'Stress Management', 1, '2020-07-05 18:30:00', '2020-07-05 18:30:00'),
(7, 0, 'Metabolic Detoxification', 1, '2020-07-05 18:30:00', '2020-07-05 18:30:00'),
(8, 0, 'General Wellness', 1, '2020-07-05 18:30:00', '2020-07-05 18:30:00'),
(9, 0, 'Children\'s Health', 1, '2020-07-05 18:30:00', '2020-07-05 18:30:00'),
(10, 0, 'Medical Foods', 1, '2020-07-05 18:30:00', '2020-07-05 18:30:00');

-- --------------------------------------------------------

--
-- Table structure for table `provider_categories`
--

CREATE TABLE `provider_categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `parent_id` int(10) NOT NULL DEFAULT 0,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `provider_categories`
--

INSERT INTO `provider_categories` (`id`, `parent_id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 0, 'Physician', 1, '2020-07-20 18:30:00', '2020-07-20 18:30:00'),
(2, 0, 'Nurse Practitioner', 1, '2020-07-20 18:30:00', '2020-07-20 18:30:00'),
(3, 0, 'Naturopathic Doctor', 1, '2020-07-20 18:30:00', '2020-07-20 18:30:00'),
(4, 0, 'Physical Therapist', 1, '2020-07-20 18:30:00', '2020-07-20 18:30:00'),
(5, 0, 'Psychologist', 1, '2020-07-20 18:30:00', '2020-07-20 18:30:00'),
(6, 0, 'Chiropractor', 1, '2020-07-20 18:30:00', '2020-07-20 18:30:00'),
(7, 0, 'Physician Assistant', 1, '2020-07-20 18:30:00', '2020-07-20 18:30:00'),
(8, 0, 'Nutritionist', 1, '2020-07-20 18:30:00', '2020-07-20 18:30:00'),
(9, 0, 'Fitness Instructor', 1, '2020-07-20 18:30:00', '2020-07-20 18:30:00'),
(10, 0, 'Dentist', 1, '2020-07-20 18:30:00', '2020-07-20 18:30:00');

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `visit_type` enum('online','concierge','','') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'online',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `visit_type`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'online', 'General', 1, '2020-07-22 18:30:00', '2020-07-22 18:30:00'),
(2, 'online', 'Erectile dysfunction', 1, '2020-07-22 18:30:00', '2020-07-22 18:30:00'),
(3, 'online', 'Nutrition', 1, '2020-07-22 18:30:00', '2020-07-22 18:30:00'),
(4, 'online', 'Counseling', 1, '2020-07-22 18:30:00', '2020-07-22 18:30:00'),
(5, 'concierge', 'Botox', 1, '2020-07-22 18:30:00', '2020-07-22 18:30:00'),
(6, 'concierge', 'Dermal Fillers', 1, '2020-07-22 18:30:00', '2020-07-22 18:30:00'),
(7, 'concierge', 'Weight Loss Consult', 1, '2020-07-22 18:30:00', '2020-07-22 18:30:00'),
(8, 'concierge', 'Bio Identical Harmones', 1, '2020-07-22 18:30:00', '2020-07-22 18:30:00'),
(9, 'concierge', 'Testosterone', 1, '2020-07-22 18:30:00', '2020-07-22 18:30:00');

-- --------------------------------------------------------

--
-- Table structure for table `service_pricing`
--

CREATE TABLE `service_pricing` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `is_monthly` tinyint(1) NOT NULL DEFAULT 1,
  `total_months` int(5) NOT NULL,
  `monthly_price` float DEFAULT NULL,
  `is_total` tinyint(1) NOT NULL DEFAULT 0,
  `total_price` float NOT NULL,
  `monthly_plan_text` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `total_plan_text` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `special` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `detail` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `features` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `service_pricing`
--

INSERT INTO `service_pricing` (`id`, `name`, `is_monthly`, `total_months`, `monthly_price`, `is_total`, `total_price`, `monthly_plan_text`, `total_plan_text`, `special`, `detail`, `features`, `status`, `created_at`, `updated_at`) VALUES
(1, '4 Month Plan', 1, 4, 175, 1, 595, 'Monthly for 4 months', 'One upfront payment', 'Pay upfront and save $100', 'Focused online care designed to improve your biggest health concerns and transform how you feel in a shorter period of time.', '[{\"feature\":\"4 months of care\",\"desc\":\"4 months to work with your care team and access your membership benefits.\"},{\"feature\":\"2 doctor visits (online)\",\"desc\":\"Our doctors will focus on understanding medical history, your most pressing health concern and goals. You\'ll review advanced testing and a personalized health plan in two 30 minute visits during your 3 month plan.\"},{\"feature\":\"3 health coach visits (online)\",\"desc\":\"Our coaches are certified in functional nutrition. They create a game plan for success for you to follow. You\'ll have a 30 min initial, another 30 min follow up and 15-min final check in at the end of The Program.\"},{\"feature\":\"Targeted health plan\",\"desc\":\"Get access to our proprietary clinical expertise through a personalized health plan that’s designed to focus on improving your biggest health concern and acheive results.\"},{\"feature\":\"Unlimited messaging\",\"desc\":\"\"},{\"feature\":\"Advanced testing available\",\"desc\":\"Our in-depth diagnostic panel looks at inflammation, hormones, nutrient status, heart health and more. Additional costs may apply depending on insurance.\"}]', 1, '2020-08-04 18:30:00', '2020-08-05 07:59:29'),
(2, '12 Month Plan', 1, 12, 150, 1, 1600, 'Monthly for 12 months', 'One upfront payment', 'Pay upfront and save $200', 'Ongoing medical care designed to manage and resolve persistent concerns, helping you achieve your best possible health.', '[{\"feature\":\"12 months of care\",\"desc\":\"One full year of care and access to membership benefits.\"},{\"feature\":\"5 doctor visits (in person or online)\",\"desc\":\"The average person spends 15 minutes a year with a doctor. Our first visit is 75 minutes during which our doctors map your health biography, discuss your symptoms, and design a personalized health plan for you. After your initial visit, the first follow up is 60 minutes, all other follow ups are 30 minutes.\"},{\"feature\":\"5 health coach visits (online)\",\"desc\":\"Our coaches are certified in functional nutrition. They create a game plan for success for you to follow. The initial visit is 45 minutes, all other follow ups are 30 minutes.\"},{\"feature\":\"Long-term health plan\",\"desc\":\"Each plan includes an individualized plan for diagnostic testing, nutrition, fitness, mental health, medications, supplementation and coaching support.\"},{\"feature\":\"Unlimited messaging\",\"desc\":null},{\"feature\":\"Advanced testing available\",\"desc\":\"Our in-depth diagnostic panel looks at inflammation, hormones, nutrient status, heart health and more. Additional costs may apply depending on insurance.\"}]', 1, '2020-08-04 18:30:00', '2020-08-05 07:41:41');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL COMMENT 'all',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'admin and provider',
  `is_admin` tinyint(1) DEFAULT 0 COMMENT 'all',
  `user_role` tinyint(1) NOT NULL DEFAULT 4 COMMENT 'all',
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'all',
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'all',
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT 'all',
  `menu_permissions` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'admin only',
  `first_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '''''' COMMENT 'patient',
  `last_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '''''' COMMENT 'patient',
  `image` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT 'provider and patient',
  `address` text COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'patient',
  `gender` enum('male','female','other','') COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT 'patient',
  `dob` date DEFAULT NULL COMMENT 'patient',
  `age` int(3) DEFAULT NULL COMMENT 'patient',
  `household_income` float DEFAULT 0 COMMENT 'patient',
  `dmm_fund` float DEFAULT 0 COMMENT 'patient, Discretionary monthly maintenance fund',
  `monthly_budget` float DEFAULT 0 COMMENT 'provider/patient',
  `email_verified_at` timestamp NULL DEFAULT NULL COMMENT 'all',
  `category` text COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'only providers, multiple entries',
  `diagnosis` text COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'for patients f.k. ids in comma separated values',
  `insurance` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT 'patient',
  `allergies` text COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'patient',
  `smokers` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT 'patient',
  `google_adsense_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT 'all',
  `bing_ads_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT 'all',
  `amazon_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT 'all',
  `rmd_ad_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT 'all',
  `provider_title` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT 'provider',
  `license_number` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT 'provider',
  `provider_licenses` text COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'provider',
  `npi` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT 'provider',
  `dea` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT 'provider, for physician, NP, PA',
  `practice_address` text COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'provider',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'all',
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'all',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'all'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `is_admin`, `user_role`, `email`, `password`, `status`, `menu_permissions`, `first_name`, `last_name`, `image`, `phone`, `address`, `gender`, `dob`, `age`, `household_income`, `dmm_fund`, `monthly_budget`, `email_verified_at`, `category`, `diagnosis`, `insurance`, `allergies`, `smokers`, `google_adsense_id`, `bing_ads_id`, `amazon_id`, `rmd_ad_id`, `provider_title`, `license_number`, `provider_licenses`, `npi`, `dea`, `practice_address`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Mathew Van', 1, 1, 'admin@gmail.com', '$2y$10$91.5BbINQoSfjNoN7iClHO0vZbMRpFbkkYPK/ILwNSThMfn86C/rm', 1, '[\"manage_online_visits\",\"visit_pool\",\"manage_orders\",\"manage_products\",\"manage_doctors\",\"manage_patients\",\"manage_contact_us\",\"finances\",\"manage_website_content\",\"manage_sub_admin\",\"manage_questions\",\"manage_faq\"]', '', '', NULL, '', NULL, '', NULL, NULL, 0, 0, 0, NULL, '0', NULL, '', NULL, '', '', '', '', '', '', '', NULL, '', '', '', NULL, '2020-07-04 02:23:12', '2020-07-04 02:23:12'),
(2, 'Nick Barton', 1, 2, 'subadmin@gmail.com', '$2y$10$HlTAd/GjdBK3Qbh7x2JzVu0GhA2jvjD/pinadAnK.thpQvAtr0uEW', 1, '[\"manage_doctors\",\"manage_patients\",\"manage_contact_us\",\"manage_website_content\",\"manage_questions\",\"manage_faq\"]', '', '', NULL, '', NULL, '', NULL, NULL, 0, 0, 0, NULL, '3', NULL, '', NULL, '', '', '', '', '', '', '', NULL, '', '', '', NULL, '2020-07-04 02:23:12', '2020-07-07 04:11:34'),
(3, 'Dr. Gaby', 0, 3, 'doctor@gmail.com', '$2y$10$wHTlxAEtvUol5RrmBhsWF.GApk.Calxblr9IGp77V6RpnJ/R04Kp6', 1, '', '', '', 'profile-1.jpg', '7898546525', '180 Wast 56th Street, 12th Floor, Meldegam, Belgium 10022', 'male', '0000-00-00', 24, 0, 0, 0, NULL, '5,6', NULL, '', NULL, '', '58978456', '4895df428', '4526-DB-45672', '579864', 'Doctor', '985469874565E', '659745684', '', '', 'Indore', NULL, '2020-07-04 02:23:12', '2020-07-20 04:40:31'),
(4, 'John Doe', 0, 4, 'patient@gmail.com', '$2y$10$wHTlxAEtvUol5RrmBhsWF.GApk.Calxblr9IGp77V6RpnJ/R04Kp6', 1, '', 'John', 'Doe', NULL, '598-443-2805', '180 Wast 56th Street, 12th Floor, Meldegam, Belgium 10022', 'male', '1990-07-09', 30, 50000, 2000, 15000, NULL, '0', '1,2,3', 'Yes', 'Red eyes, itchy rash, runny nose, shortness of breath, swelling, sneezing', 'Yes', '1234', '1234', '1234', '1234', '', '', 'test000.jpg', '', '', '', NULL, '2020-07-04 03:15:28', '2020-07-04 03:15:28'),
(5, 'Sam Barton', 1, 2, 'subadmin2@gmail.com', '$2y$10$HlTAd/GjdBK3Qbh7x2JzVu0GhA2jvjD/pinadAnK.thpQvAtr0uEW', 1, '[\"manage_online_visits\",\"visit_pool\",\"manage_orders\",\"manage_products\",\"manage_doctors\",\"manage_patients\",\"manage_contact_us\",\"finances\",\"manage_website_content\",\"manage_questions\",\"manage_faq\"]', '', '', NULL, '', NULL, '', NULL, NULL, 0, 0, 0, NULL, '2', NULL, '', NULL, '', '', '', '', '', '', '', NULL, '', '', '', NULL, '2020-07-04 02:23:12', '2020-07-04 02:23:12'),
(6, 'Nisha Roy', 1, 2, 'subadmin3@gmail.com', '$2y$10$HlTAd/GjdBK3Qbh7x2JzVu0GhA2jvjD/pinadAnK.thpQvAtr0uEW', 1, '[\"manage_online_visits\",\"visit_pool\",\"manage_orders\",\"manage_products\",\"manage_doctors\",\"manage_patients\",\"manage_contact_us\",\"finances\",\"manage_website_content\",\"manage_questions\",\"manage_faq\"]', '', '', NULL, '', NULL, '', NULL, NULL, 0, 0, 0, NULL, '4', NULL, '', NULL, '', '', '', '', '', '', '', NULL, '', '', '', NULL, '2020-07-04 02:23:12', '2020-07-04 02:23:12'),
(7, 'Thomas Smith', 1, 2, 'subadmin4@gmail.com', '$2y$10$HlTAd/GjdBK3Qbh7x2JzVu0GhA2jvjD/pinadAnK.thpQvAtr0uEW', 1, '[\"manage_online_visits\",\"visit_pool\",\"manage_orders\",\"manage_products\",\"manage_doctors\",\"manage_patients\",\"manage_contact_us\",\"finances\",\"manage_website_content\",\"manage_questions\",\"manage_faq\"]', '', '', NULL, '', NULL, '', NULL, NULL, 0, 0, 0, NULL, '4', NULL, '', NULL, '', '', '', '', '', '', '', NULL, '', '', '', NULL, '2020-07-04 02:23:12', '2020-07-04 02:23:12'),
(8, 'Josh Anderson', 1, 2, 'subadmin5@gmail.com', '$2y$10$HlTAd/GjdBK3Qbh7x2JzVu0GhA2jvjD/pinadAnK.thpQvAtr0uEW', 0, '[\"manage_online_visits\",\"visit_pool\",\"manage_orders\",\"manage_products\",\"manage_doctors\",\"manage_patients\",\"manage_contact_us\",\"finances\",\"manage_website_content\",\"manage_questions\",\"manage_faq\"]', '', '', NULL, '', NULL, '', NULL, NULL, 0, 0, 0, NULL, '2', NULL, '', NULL, '', '', '', '', '', '', '', NULL, '', '', '', NULL, '2020-07-04 02:23:12', '2020-07-04 02:23:12'),
(9, 'Robert Broad', 1, 2, 'subadmin6@gmail.com', '$2y$10$HlTAd/GjdBK3Qbh7x2JzVu0GhA2jvjD/pinadAnK.thpQvAtr0uEW', 1, '[\"manage_online_visits\",\"visit_pool\",\"manage_orders\",\"manage_products\",\"manage_doctors\",\"manage_patients\",\"manage_contact_us\",\"finances\",\"manage_website_content\",\"manage_questions\",\"manage_faq\"]', '', '', NULL, '', NULL, '', NULL, NULL, 0, 0, 0, NULL, '2', NULL, '', NULL, '', '', '', '', '', '', '', NULL, '', '', '', NULL, '2020-07-04 02:23:12', '2020-07-04 02:23:12'),
(10, 'Priyanka Rai', 1, 2, 'subadmin7@gmail.com', '$2y$10$HlTAd/GjdBK3Qbh7x2JzVu0GhA2jvjD/pinadAnK.thpQvAtr0uEW', 1, '[\"manage_online_visits\",\"visit_pool\",\"manage_orders\",\"manage_products\",\"manage_doctors\",\"manage_patients\",\"manage_contact_us\",\"finances\",\"manage_website_content\",\"manage_questions\",\"manage_faq\"]', '', '', NULL, '', NULL, '', NULL, NULL, 0, 0, 0, NULL, '4', NULL, '', NULL, '', '', '', '', '', '', '', NULL, '', '', '', NULL, '2020-07-04 02:23:12', '2020-07-04 02:23:12'),
(11, 'Ayaz Khan', 1, 2, 'subadmin8@gmail.com', '$2y$10$HlTAd/GjdBK3Qbh7x2JzVu0GhA2jvjD/pinadAnK.thpQvAtr0uEW', 0, '[\"manage_online_visits\",\"visit_pool\",\"manage_orders\",\"manage_products\",\"manage_doctors\",\"manage_patients\",\"manage_contact_us\",\"finances\",\"manage_website_content\",\"manage_questions\",\"manage_faq\"]', '', '', NULL, '', NULL, '', NULL, NULL, 0, 0, 0, NULL, '1', NULL, '', NULL, '', '', '', '', '', '', '', NULL, '', '', '', NULL, '2020-07-04 02:23:12', '2020-07-04 02:23:12'),
(12, 'Steve Anderson', 1, 2, 'subadmin9@gmail.com', '$2y$10$HlTAd/GjdBK3Qbh7x2JzVu0GhA2jvjD/pinadAnK.thpQvAtr0uEW', 1, '[\"manage_online_visits\",\"visit_pool\",\"manage_orders\",\"manage_products\",\"manage_doctors\",\"manage_patients\",\"manage_contact_us\",\"finances\",\"manage_website_content\",\"manage_questions\",\"manage_faq\"]', '', '', NULL, '', NULL, '', NULL, NULL, 0, 0, 0, NULL, '2', NULL, '', NULL, '', '', '', '', '', '', '', NULL, '', '', '', NULL, '2020-07-04 02:23:12', '2020-07-04 02:23:12'),
(13, 'John Parker', 1, 2, 'subadmin10@gmail.com', '$2y$10$HlTAd/GjdBK3Qbh7x2JzVu0GhA2jvjD/pinadAnK.thpQvAtr0uEW', 1, '[\"manage_online_visits\",\"visit_pool\",\"manage_orders\",\"manage_products\",\"manage_doctors\",\"manage_patients\",\"manage_contact_us\",\"finances\",\"manage_website_content\",\"manage_questions\",\"manage_faq\"]', '', '', NULL, '', NULL, '', NULL, NULL, 0, 0, 0, NULL, '3', NULL, '', NULL, '', '', '', '', '', '', '', NULL, '', '', '', NULL, '2020-07-04 02:23:12', '2020-07-04 02:23:12'),
(14, 'Rex Doe', 1, 2, 'subadmin11@gmail.com', '$2y$10$HlTAd/GjdBK3Qbh7x2JzVu0GhA2jvjD/pinadAnK.thpQvAtr0uEW', 0, '[\"manage_online_visits\",\"visit_pool\",\"manage_orders\",\"manage_products\",\"manage_doctors\",\"manage_patients\",\"manage_contact_us\",\"finances\",\"manage_website_content\",\"manage_questions\",\"manage_faq\"]', '', '', NULL, '', NULL, '', NULL, NULL, 0, 0, 0, NULL, '3', NULL, '', NULL, '', '', '', '', '', '', '', NULL, '', '', '', NULL, '2020-07-04 02:23:12', '2020-07-04 02:23:12'),
(15, 'Clint Rogers', 1, 2, 'subadmin12@gmail.com', '$2y$10$HlTAd/GjdBK3Qbh7x2JzVu0GhA2jvjD/pinadAnK.thpQvAtr0uEW', 1, '[\"manage_online_visits\",\"visit_pool\",\"manage_orders\",\"manage_products\",\"manage_doctors\",\"manage_patients\",\"manage_contact_us\",\"finances\",\"manage_website_content\",\"manage_questions\",\"manage_faq\"]', '', '', NULL, '', NULL, '', NULL, NULL, 0, 0, 0, NULL, '2', NULL, '', NULL, '', '', '', '', '', '', '', NULL, '', '', '', NULL, '2020-07-04 02:23:12', '2020-07-04 02:23:12'),
(16, 'Steve Banner', 1, 2, 'subadmin13@gmail.com', '$2y$10$HlTAd/GjdBK3Qbh7x2JzVu0GhA2jvjD/pinadAnK.thpQvAtr0uEW', 1, '[\"manage_online_visits\",\"visit_pool\",\"manage_orders\",\"manage_products\",\"manage_doctors\",\"manage_patients\",\"manage_contact_us\",\"finances\",\"manage_website_content\",\"manage_questions\",\"manage_faq\"]', '', '', NULL, '', NULL, '', NULL, NULL, 0, 0, 0, NULL, '3', NULL, '', NULL, '', '', '', '', '', '', '', NULL, '', '', '', NULL, '2020-07-04 02:23:12', '2020-07-04 02:23:12'),
(17, 'Steve Johnson', 0, 4, 'test@gmail.com', '$2y$10$Z5aYTOxrwcmJPX6tWBU8oeovC4UHDif5YaKrIhlACoAjwXa.OWXXq', 1, '', 'Steve', 'Johnson', 'profile-1.jpg', '832-443-2909', '160 East 56th Street, 12th Floor, New York, New York 10022', 'male', '1994-06-10', 26, 40000, 3000, 13000, NULL, '0', '5,6,7', 'No', 'Hay fever, food allergies, atopic dermatitis, allergic asthma, anaphylaxis', 'No', '2345', '2345', '2345', '2345', '', '', 'test0002.jpg', '', '', '', NULL, '2020-07-06 11:29:19', '2020-07-28 06:11:11'),
(23, 'Mark Smith', 1, 2, 'abc@gmail.com', '$2y$10$.65NcdpEGPzQMUaxPG.4d.CZTfvc0fm3piOyGEDeRdkw3nl34TeOW', 0, '[\"manage_online_visits\",\"visit_pool\",\"manage_orders\",\"manage_products\",\"manage_doctors\",\"manage_patients\",\"manage_contact_us\",\"finances\",\"manage_questions\"]', '', '', NULL, '', NULL, '', NULL, NULL, 0, 0, 0, NULL, '1', NULL, '', NULL, '', '', '', '', '', '', '', NULL, '', '', '', NULL, '2020-07-07 01:39:57', '2020-07-28 06:09:38'),
(31, 'Dr. R.K. Singh', 0, 3, 'doctor2@gmail.com', '$2y$10$0xAUZmlfOIktsuSZVqlQpe8vg3A8H8PZtsuOifFOo9W1JeAjV68bO', 0, '[\"manage_visit_requests\",\"visit_pool\",\"availability\",\"my_account\"]', '', '', 'profile-2.jpg', '', '180 Wast 56th Street, 12th Floor, Meldegam, Belgium 10022', '', NULL, NULL, 0, 0, 0, NULL, '3,4', NULL, '', NULL, '', '', '', '', '', '', '', NULL, '', '', '', NULL, '2020-07-08 06:29:14', '2020-07-08 06:29:14'),
(32, 'Dr. R.S. Khanna', 0, 3, 'doctor3@gmail.com', '$2y$10$l8MGpAP9e.cC7rOmMl/4pugiGYLa9J305jSx.QyiZkbdYmcxtjuhC', 1, '[\"manage_visit_requests\",\"visit_pool\",\"availability\",\"my_account\"]', '', '', 'profile-3.jpg', '', '180 Wast 56th Street, 12th Floor, Meldegam, Belgium 10022', '', NULL, NULL, 0, 0, 0, NULL, '1,2', NULL, '', NULL, '', '', '', '', '', '', '', NULL, '', '', '', NULL, '2020-07-08 06:42:39', '2020-07-08 06:42:39'),
(33, 'Dr. Frank', 0, 3, 'doctor4@gmail.com', '$2y$10$pnu.8EBAfw95bEuHMlPfsu78SOADwsVyoaarl.nNliEfA7Ocya6gG', 0, '[\"manage_visit_requests\",\"visit_pool\",\"availability\",\"my_account\"]', '', '', 'profile-4.jpg', '', '180 Wast 56th Street, 12th Floor, Meldegam, Belgium 10022', '', NULL, NULL, 0, 0, 0, NULL, '1', NULL, '', NULL, '', '', '', '', '', '', '', NULL, '', '', '', NULL, '2020-07-08 06:45:40', '2020-07-20 02:20:56'),
(35, 'Dr. Richards', 0, 3, 'doctor111@gmail.com', '$2y$10$wHTlxAEtvUol5RrmBhsWF.GApk.Calxblr9IGp77V6RpnJ/R04Kp6', 1, '', '', '', 'profile-5.jpg', '', '180 Wast 56th Street, 12th Floor, Meldegam, Belgium 10022', '', NULL, NULL, 0, 0, 0, NULL, '6', NULL, '', NULL, '', '', '', '', '', '', '', NULL, '', '', '', NULL, '2020-07-04 02:23:12', '2020-07-04 02:23:12'),
(36, 'Dr.K.R.K. Singh', 0, 3, 'doctor222@gmail.com', '$2y$10$0xAUZmlfOIktsuSZVqlQpe8vg3A8H8PZtsuOifFOo9W1JeAjV68bO', 0, '[\"manage_visit_requests\",\"visit_pool\",\"availability\",\"my_account\"]', '', '', 'profile-6.jpg', '', '180 Wast 56th Street, 12th Floor, Meldegam, Belgium 10022', '', NULL, NULL, 0, 0, 0, NULL, '6', NULL, '', NULL, '', '', '', '', '', '', '', NULL, '', '', '', NULL, '2020-07-08 06:29:14', '2020-07-08 06:29:14'),
(37, 'Dr. R.S. Kapoor', 0, 3, 'doctor333@gmail.com', '$2y$10$l8MGpAP9e.cC7rOmMl/4pugiGYLa9J305jSx.QyiZkbdYmcxtjuhC', 0, '[\"manage_visit_requests\",\"visit_pool\",\"availability\",\"my_account\"]', '', '', 'profile-1.jpg', '', '180 Wast 56th Street, 12th Floor, Meldegam, Belgium 10022', '', NULL, NULL, 0, 0, 0, NULL, '4', NULL, '', NULL, '', '', '', '', '', '', '', NULL, '', '', '', NULL, '2020-07-08 06:42:39', '2020-07-08 06:42:39'),
(38, 'Dr. Sara ', 0, 3, 'doctor444@gmail.com', '$2y$10$pnu.8EBAfw95bEuHMlPfsu78SOADwsVyoaarl.nNliEfA7Ocya6gG', 1, '[\"manage_visit_requests\",\"visit_pool\",\"availability\",\"my_account\"]', '', '', 'profile-2.jpg', '', '180 Wast 56th Street, 12th Floor, Meldegam, Belgium 10022', '', NULL, NULL, 0, 0, 0, NULL, '2', NULL, '', NULL, '', '', '', '', '', '', '', NULL, '', '', '', NULL, '2020-07-08 06:45:40', '2020-07-20 02:21:04'),
(39, 'Dr. Robert', 0, 3, 'doctor555@gmail.com', '$2y$10$xuEtJvPiD90rJlp.oaVqVujiZoHDWc7jMnAjMy0Ba8qcgkv2ySuD.', 0, '[\"manage_visit_requests\",\"visit_pool\",\"availability\",\"my_account\"]', '', '', NULL, '', '180 Wast 56th Street, 12th Floor, Meldegam, Belgium 10022', '', NULL, NULL, 0, 0, 0, NULL, '5', NULL, '', NULL, '', '', '', '', '', '', '', NULL, '', '', '', NULL, '2020-07-08 06:49:17', '2020-07-08 06:49:17'),
(40, 'John Wick', 0, 4, 'patient123@gmail.com', '$2y$10$wHTlxAEtvUol5RrmBhsWF.GApk.Calxblr9IGp77V6RpnJ/R04Kp6', 1, '', 'John', 'Wick', NULL, '598-443-2805', '180 Wast 56th Street, 12th Floor, Meldegam, Belgium 10022', 'male', '1990-07-09', 30, 50000, 2000, 15000, NULL, '0', '1,2,3', 'Yes', 'Red eyes, itchy rash, runny nose, shortness of breath, swelling, sneezing', 'Yes', '1234', '1234', '1234', '1234', '', '', 'test000.jpg', '', '', '', NULL, '2020-07-04 03:15:28', '2020-07-04 03:15:28'),
(41, 'Michel Johnson', 0, 4, 'test2345@gmail.com', '$2y$10$Z5aYTOxrwcmJPX6tWBU8oeovC4UHDif5YaKrIhlACoAjwXa.OWXXq', 1, '', 'Michel', 'Johnson', 'profile-1.jpg', '832-443-2909', '160 East 56th Street, 12th Floor, New York, New York 10022', 'male', '1994-06-10', 26, 40000, 3000, 13000, NULL, '0', '5,6,7', 'No', 'Hay fever, food allergies, atopic dermatitis, allergic asthma, anaphylaxis', 'No', '2345', '2345', '2345', '2345', '', '', 'test0002.jpg', '', '', '', NULL, '2020-07-06 11:29:19', '2020-07-28 06:11:11');

-- --------------------------------------------------------

--
-- Table structure for table `visit_forms`
--

CREATE TABLE `visit_forms` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `service_id` int(10) NOT NULL DEFAULT 0,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `short_info` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instructions` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `steps` int(3) NOT NULL DEFAULT 1,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `visit_forms`
--

INSERT INTO `visit_forms` (`id`, `service_id`, `name`, `short_info`, `instructions`, `steps`, `status`, `created_at`, `updated_at`) VALUES
(1, 2, 'ED (Erectile Dysfunction) Consultation.', NULL, 'One more thing, before we begin, there are terms and conditions regarding Telemedicine and Telehealth that we have to discuss. We have concisely summed them up on this page,  It\'s nothing fancy, but we want you to understand that we treat these evaluations just as importantly as if you were seeing the doctor in person.*', 12, 1, '2020-08-18 09:30:00', '2020-08-18 09:30:00');

-- --------------------------------------------------------

--
-- Table structure for table `visit_form_fields`
--

CREATE TABLE `visit_form_fields` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `visit_form_id` int(10) NOT NULL DEFAULT 0,
  `step_no` int(3) NOT NULL DEFAULT 1,
  `field_order` int(3) NOT NULL DEFAULT 1,
  `field_label` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `field_type` enum('input','select','checkbox','radio','textarea','submit','button','file','date') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `extra_info` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_final_step` tinyint(1) NOT NULL DEFAULT 0,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `visit_form_fields`
--

INSERT INTO `visit_form_fields` (`id`, `visit_form_id`, `step_no`, `field_order`, `field_label`, `field_type`, `extra_info`, `is_final_step`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 'First Name', 'input', NULL, 0, 1, '2020-08-18 09:30:00', '2020-08-18 09:30:00'),
(2, 1, 2, 1, 'Last Name', 'input', NULL, 0, 1, '2020-08-18 09:30:00', '2020-08-18 09:30:00'),
(3, 1, 3, 1, 'Email Address', 'input', NULL, 0, 1, '2020-08-18 09:30:00', '2020-08-18 09:30:00'),
(4, 1, 4, 1, 'Phone No.', 'input', NULL, 0, 1, '2020-08-18 09:30:00', '2020-08-18 09:30:00'),
(5, 1, 5, 1, '09:00 AM', 'radio', 'Timings', 0, 1, '2020-08-18 09:30:00', '2020-08-18 09:30:00'),
(6, 1, 5, 2, '09:30 AM', 'radio', 'Timings', 0, 1, '2020-08-18 09:30:00', '2020-08-18 09:30:00'),
(7, 1, 5, 3, '10:00 AM', 'radio', 'Timings', 0, 1, '2020-08-18 09:30:00', '2020-08-18 09:30:00'),
(8, 1, 5, 4, '10:30 AM', 'radio', 'Timings', 0, 1, '2020-08-18 09:30:00', '2020-08-18 09:30:00'),
(9, 1, 6, 1, 'Address', 'textarea', '', 0, 1, '2020-08-18 09:30:00', '2020-08-18 09:30:00'),
(10, 1, 7, 1, 'City', 'input', '', 0, 1, '2020-08-18 09:30:00', '2020-08-18 09:30:00'),
(11, 1, 8, 1, 'State', 'input', '', 0, 1, '2020-08-18 09:30:00', '2020-08-18 09:30:00'),
(12, 1, 9, 1, 'Zip code', 'input', '', 0, 1, '2020-08-18 09:30:00', '2020-08-18 09:30:00'),
(13, 1, 10, 1, 'Date of birth', 'date', '', 0, 1, '2020-08-18 09:30:00', '2020-08-18 09:30:00'),
(14, 1, 11, 1, 'race', 'input', '', 0, 1, '2020-08-18 09:30:00', '2020-08-18 09:30:00'),
(15, 1, 12, 1, 'ID Proof', 'file', '', 1, 1, '2020-08-18 09:30:00', '2020-08-18 09:30:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `contact_us`
--
ALTER TABLE `contact_us`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `diagnosis_categories`
--
ALTER TABLE `diagnosis_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faqs`
--
ALTER TABLE `faqs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faq_categories`
--
ALTER TABLE `faq_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `online_visits`
--
ALTER TABLE `online_visits`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `online_visit_provider_assign`
--
ALTER TABLE `online_visit_provider_assign`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `online_visit_status_timeline`
--
ALTER TABLE `online_visit_status_timeline`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`(191));

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_categories`
--
ALTER TABLE `product_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `provider_categories`
--
ALTER TABLE `provider_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `service_pricing`
--
ALTER TABLE `service_pricing`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `visit_forms`
--
ALTER TABLE `visit_forms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `visit_form_fields`
--
ALTER TABLE `visit_form_fields`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `contact_us`
--
ALTER TABLE `contact_us`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `diagnosis_categories`
--
ALTER TABLE `diagnosis_categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `faqs`
--
ALTER TABLE `faqs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `faq_categories`
--
ALTER TABLE `faq_categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `online_visits`
--
ALTER TABLE `online_visits`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `online_visit_provider_assign`
--
ALTER TABLE `online_visit_provider_assign`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `online_visit_status_timeline`
--
ALTER TABLE `online_visit_status_timeline`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `product_categories`
--
ALTER TABLE `product_categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `provider_categories`
--
ALTER TABLE `provider_categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `service_pricing`
--
ALTER TABLE `service_pricing`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'all', AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `visit_forms`
--
ALTER TABLE `visit_forms`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `visit_form_fields`
--
ALTER TABLE `visit_form_fields`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
