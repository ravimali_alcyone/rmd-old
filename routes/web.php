<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Route::group(['middleware' => ['guest']], function () {
	Route::get('/', function () {
        return redirect('/login');
    });
}); */

Route::get('admin', 'admin\HomeController@index')->name('admin')->middleware('is_admin');
Route::get('admin/home', 'admin\HomeController@index')->name('admin.home')->middleware('is_admin');
Route::get('admin/profile', 'admin\HomeController@profile')->name('admin.profile')->middleware('is_admin');
Route::post('admin/change_password', 'admin\HomeController@change_password')->name('admin.change_password')->middleware('is_admin');
Route::post('admin/contact_details', 'admin\HomeController@contact_details')->name('admin.contact_details')->middleware('is_admin');
Route::post('admin/profile_image', 'admin\HomeController@profile_image')->name('admin.profile_image')->middleware('is_admin');

//Sub-Admins
Route::any('admin/subadmins','admin\AdminController@index')->name('admin.subadmins');
Route::get('admin/subadmins/add','admin\AdminController@add')->name('admin.subadmins.add');
Route::get('admin/subadmins/add/{id}','admin\AdminController@edit')->name('admin.subadmins.edit');
Route::post('admin/subadmins/store', 'admin\AdminController@store')->name('admin.subadmins.store');
Route::delete('admin/subadmins/destroy/{id}','admin\AdminController@destroy')->name('admin.subadmins.destroy');
Route::post('admin/subadmins/change_status','admin\AdminController@change_status');

//Faqs
Route::any('admin/faqs','admin\FaqController@index')->name('admin.faqs');
Route::get('admin/faqs/add','admin\FaqController@add')->name('admin.faqs.add');
Route::get('admin/faqs/add/{id}','admin\FaqController@edit')->name('admin.faqs.edit');
Route::post('admin/faqs/store', 'admin\FaqController@store')->name('admin.faqs.store');
Route::delete('admin/faqs/destroy/{id}','admin\FaqController@destroy')->name('admin.faqs.destroy');
Route::post('admin/faqs/change_status', 'admin\FaqController@change_status')->name('admin.faqs.change_status');

// Providers
Route::any('admin/providers/{cat_id?}','admin\ProviderController@index')->name('admin.providers');
Route::get('admin/providers/add','admin\ProviderController@add')->name('admin.providers.add');
Route::get('admin/providers/add/{id}','admin\ProviderController@edit')->name('admin.providers.edit');
Route::post('admin/providers/store', 'admin\ProviderController@store')->name('admin.providers.store');
Route::delete('admin/providers/destroy/{id}','admin\ProviderController@destroy')->name('admin.providers.destroy');
Route::post('admin/providers/change_status','admin\ProviderController@change_status');
Route::get('admin/providers/view/{id}','admin\ProviderController@view');

// Patients
Route::any('admin/patients','admin\PatientController@index')->name('admin.patients');
Route::get('admin/patients/add','admin\PatientController@add')->name('admin.patients.add');
Route::get('admin/patients/add/{id}','admin\PatientController@edit')->name('admin.patients.edit');
Route::post('admin/patients/store', 'admin\PatientController@store')->name('admin.patients.store');
Route::delete('admin/patients/destroy/{id}','admin\PatientController@destroy')->name('admin.patients.destroy');
Route::post('admin/patients/change_status','admin\PatientController@change_status');
Route::get('admin/patients/view/{id}','admin\PatientController@view');

// Products
Route::any('admin/products','admin\ProductController@index')->name('admin.products');
Route::get('admin/products/add','admin\ProductController@add')->name('admin.products.add');
Route::get('admin/products/edit/{id}','admin\ProductController@edit')->name('admin.products.edit');
Route::post('admin/products/store', 'admin\ProductController@store')->name('admin.products.store');
Route::delete('admin/products/destroy/{id}','admin\ProductController@destroy')->name('admin.products.destroy');
Route::post('admin/products/change_status','admin\ProductController@change_status');
Route::post('admin/products/get_sub_category', 'admin\ProductController@get_sub_category')->name('admin.products.get_sub_category');
Route::post('admin/products/get_sub_sub_category', 'admin\ProductController@get_sub_sub_category')->name('admin.products.get_sub_sub_category');
Route::get('admin/products/view/{id}','admin\ProductController@view');


//Service Pricing
Route::any('admin/service_pricing','admin\ServicePricingController@index')->name('admin.service_pricing');
Route::get('admin/service_pricing/add','admin\ServicePricingController@add')->name('admin.service_pricing.add');
Route::get('admin/service_pricing/add/{id}','admin\ServicePricingController@edit')->name('admin.service_pricing.edit');
Route::post('admin/service_pricing/store', 'admin\ServicePricingController@store')->name('admin.service_pricing.store');
Route::delete('admin/service_pricing/destroy/{id}','admin\ServicePricingController@destroy')->name('admin.service_pricing.destroy');
Route::post('admin/service_pricing/change_status','admin\ServicePricingController@change_status');

//Plan Features
Route::any('admin/plan_features','admin\PlanFeaturesController@index')->name('admin.plan_features');
Route::get('admin/plan_features/add','admin\PlanFeaturesController@add')->name('admin.plan_features.add');
Route::get('admin/plan_features/add/{id}','admin\PlanFeaturesController@edit')->name('admin.plan_features.edit');
Route::post('admin/plan_features/store', 'admin\PlanFeaturesController@store')->name('admin.plan_features.store');
Route::delete('admin/plan_features/destroy/{id}','admin\PlanFeaturesController@destroy')->name('admin.plan_features.destroy');
Route::post('admin/plan_features/change_status','admin\PlanFeaturesController@change_status');

//Contact Us
Route::any('admin/contact_us','admin\ContactUsController@index')->name('admin.contact_us');
Route::delete('admin/contact_us/destroy/{id}','admin\ContactUsController@destroy')->name('admin.contact_us.destroy');
Route::post('admin/contact_us/change_status','admin\ContactUsController@change_status');


// Online Visits
Route::any('admin/online_visits','admin\OnlineVisitController@index')->name('admin.online_visits');
Route::delete('admin/online_visits/destroy/{id}','admin\OnlineVisitController@destroy')->name('admin.online_visits.destroy');
Route::post('admin/online_visits/change_status','admin\OnlineVisitController@change_status');
Route::get('admin/online_visits/view/{id}','admin\OnlineVisitController@view');

Route::any('admin/concierge_visits','admin\OnlineVisitController@index2')->name('admin.concierge_visits');
Route::get('admin/concierge_visits/view/{id}','admin\OnlineVisitController@view2');

//Services/Treatments
Route::any('admin/services','admin\ServiceController@index')->name('admin.services');
Route::get('admin/services/add','admin\ServiceController@add')->name('admin.services.add');
Route::get('admin/services/add/{id}','admin\ServiceController@edit')->name('admin.services.edit');
Route::post('admin/services/store', 'admin\ServiceController@store')->name('admin.services.store');
Route::delete('admin/services/destroy/{id}','admin\ServiceController@destroy')->name('admin.services.destroy');
Route::post('admin/services/change_status','admin\ServiceController@change_status');


//Visit Forms
Route::any('admin/visit_forms','admin\VisitFormController@index')->name('admin.visit_forms');
Route::get('admin/visit_forms/add','admin\VisitFormController@add')->name('admin.visit_forms.add');
Route::get('admin/visit_forms/add/{id}','admin\VisitFormController@edit')->name('admin.visit_forms.edit');
Route::post('admin/visit_forms/store', 'admin\VisitFormController@store')->name('admin.visit_forms.store');
Route::delete('admin/visit_forms/destroy/{id}','admin\VisitFormController@destroy')->name('admin.visit_forms.destroy');

Route::get('admin/visit_form_questions/add/{formid}','admin\VisitFormController@addQuestion')->name('admin.visit_form_questions.add');
Route::post('admin/visit_form_questions/store', 'admin\VisitFormController@storeQuestion')->name('admin.visit_form_questions.store');

Route::delete('admin/visit_forms/destroy_field/{id}','admin\VisitFormController@destroy_field')->name('admin.visit_forms.destroy_field');
Route::post('admin/visit_forms/get_services', 'admin\VisitFormController@get_services')->name('admin.visit_forms.get_services');


//Testimonials
Route::any('admin/testimonials','admin\TestimonialController@index')->name('admin.testimonials');
Route::get('admin/testimonials/add','admin\TestimonialController@add')->name('admin.testimonials.add');
Route::get('admin/testimonials/add/{id}','admin\TestimonialController@edit')->name('admin.testimonials.edit');
Route::post('admin/testimonials/store', 'admin\TestimonialController@store')->name('admin.testimonials.store');
Route::delete('admin/testimonials/destroy/{id}','admin\TestimonialController@destroy')->name('admin.testimonials.destroy');
Route::post('admin/testimonials/change_status','admin\TestimonialController@change_status');

//Sponsors
Route::any('admin/sponsors','admin\SponsorController@index')->name('admin.sponsors');
Route::get('admin/sponsors/add','admin\SponsorController@add')->name('admin.sponsors.add');
Route::get('admin/sponsors/add/{id}','admin\SponsorController@edit')->name('admin.sponsors.edit');
Route::post('admin/sponsors/store', 'admin\SponsorController@store')->name('admin.sponsors.store');
Route::delete('admin/sponsors/destroy/{id}','admin\SponsorController@destroy')->name('admin.sponsors.destroy');
Route::post('admin/sponsors/change_status','admin\SponsorController@change_status');


// Blogs
Route::any('admin/blogs','admin\BlogController@index')->name('admin.blogs');
Route::get('admin/blogs/add','admin\BlogController@add')->name('admin.blogs.add');
Route::get('admin/blogs/edit/{id}','admin\BlogController@edit')->name('admin.blogs.edit');
Route::post('admin/blogs/store', 'admin\BlogController@store')->name('admin.blogs.store');
Route::delete('admin/blogs/destroy/{id}','admin\BlogController@destroy')->name('admin.blogs.destroy');
Route::post('admin/blogs/change_status','admin\BlogController@change_status');
Route::post('admin/blogs/get_sub_category', 'admin\BlogController@get_sub_category')->name('admin.blogs.get_sub_category');
Route::get('admin/blogs/view/{id}','admin\BlogController@view');


// Treatment Medicines
Route::any('admin/treatment_medicines','admin\TreatmentMedicineController@index')->name('admin.treatment_medicines');
Route::get('admin/treatment_medicines/add','admin\TreatmentMedicineController@add')->name('admin.treatment_medicines.add');
Route::get('admin/treatment_medicines/edit/{id}','admin\TreatmentMedicineController@edit')->name('admin.treatment_medicines.edit');
Route::post('admin/treatment_medicines/store', 'admin\TreatmentMedicineController@store')->name('admin.treatment_medicines.store');
Route::delete('admin/treatment_medicines/destroy/{id}','admin\TreatmentMedicineController@destroy')->name('admin.treatment_medicines.destroy');
Route::post('admin/treatment_medicines/change_status','admin\TreatmentMedicineController@change_status');
Route::post('admin/treatment_medicines/get_services', 'admin\TreatmentMedicineController@get_services')->name('admin.treatment_medicines.get_services');
Route::post('image-upload', 'admin\TreatmentMedicineController@imageUpload')->name('image-upload');
Route::get('admin/treatment_medicines/view/{id}','admin\TreatmentMedicineController@view');


Auth::routes();

//Provider Routes
Route::any('provider/signup', 'Auth\RegisterController@showRegistrationForm')->name('provider.signup');

Route::get('provider/home', 'provider\HomeController@index')->name('provider.home');
Route::get('provider/profile', 'provider\HomeController@profile')->name('provider.profile');
Route::post('provider/change_password', 'provider\HomeController@change_password')->name('provider.change_password');
Route::post('provider/contact_details', 'provider\HomeController@contact_details')->name('provider.contact_details');
Route::post('provider/profile_image', 'provider\HomeController@profile_image')->name('provider.profile_image');


//Website Content
Route::get('admin/website_content', 'admin\WebsiteContentController@index')->name('admin.website_content');
Route::get('admin/website_content/contact', 'admin\WebsiteContentController@contact')->name('admin.website_content.contact');


# Front Routes
Route::get('/', 'front\FrontController@index')->name('home');
Route::get('/home', 'front\FrontController@index')->name('home');
Route::get('/plans', 'front\FrontController@plans');
Route::get('/how_it_works', 'front\FrontController@how_it_works');
Route::get('/about', 'front\FrontController@about');
Route::get('/contact', 'front\FrontController@contact');
Route::post('/contactSubmit', 'front\FrontController@contact')->name('front.contactSubmit');
Route::get('/faq', 'front\FrontController@faq');
Route::get('/providers', 'front\FrontController@providers');
Route::get('/providers/{id}', 'front\FrontController@provider_detail');
Route::get('/privacy-policy', 'front\FrontController@privacy_policy');
Route::get('/terms-conditions', 'front\FrontController@terms_conditions');
Route::get('/terms-of-use', 'front\FrontController@terms_of_use');

Route::post('/get_services', 'front\FrontController@get_services')->name('get_services');
Route::get('/treatment', 'front\FrontController@treatment');
Route::get('/signup', 'front\FrontController@signup')->name('signup');
Route::post('/signupSubmit', 'front\FrontController@signupSubmit')->name('signup_submission');
Route::get('/online-visit/welcome', 'front\FrontController@online_visit_welcome')->name('online_visit_welcome');
Route::get('/online-visit/basics', 'front\FrontController@online_visit_basics')->name('online_visit_basics');
Route::post('/basicsSubmit', 'front\FrontController@basicsSubmit')->name('basics_submission');
Route::post('/onlineVisitSubmit', 'front\FrontController@onlineVisitSubmit')->name('onlineVisitSubmit');
Route::get('/online-visit/medical_questions', 'front\FrontController@medical_questions')->name('medical_questions');
Route::get('/online-visit/treatment_preference', 'front\FrontController@treatment_preference')->name('treatment_preference');

Route::get('/sign_in', 'front\FrontController@sign_in')->name('sign_in');
Route::post('/signinSubmit', 'front\FrontController@signinSubmit')->name('signin_submission');
Route::get('/forgot_password', 'front\FrontController@forgot_password')->name('forgot_password');


Route::get('/user/dashboard', 'front\DashboardController@index')->name('user.dashboard');
Route::get('/user/profile', 'front\DashboardController@profile')->name('user.profile');
Route::get('/user/visit_records', 'front\DashboardController@visit_records')->name('user.visit_records');
Route::get('/user/subscriptions', 'front\DashboardController@subscriptions')->name('user.subscriptions');
Route::get('/user/orders', 'front\DashboardController@orders')->name('user.orders');

